# SpiderDisk - 百度云网盘资源分享系统

![](docs/images/57fx-0.png)

曾经使用 `ThinkPHP` v3.2 开发的一套牛逼的百度云网盘资源分享系统：在一年左右的时间做到百度权重7，`巅峰时期`最高访问流量： IP 22w、PV 120w，纯靠 4C4G 的云服务器支撑...

这套程序主要是在我刚走出学校工作的第一年内单枪匹马开发的，通过开发这套程序，当年我熟悉了PHP、MySQL及MySQL优化、Bootstrap、Coreseek（sphinx中文版全文搜索）、SEO优化等等很多层面的知识...

非常感谢当年在这套程序开发过程中指点过我的朋友，也非常感谢当年那个刚走出校园对世界一脸憧憬奋发向上的自己.

而放出这套程序源码，主要是为了对过往的“辉煌”做个纪念，对这份源码做个存档.

而那个网站，则被当年懵懂的自己贱卖了...后来想想真的是贱卖了，错过了一次财富自由的机会...

## 战绩

### 百度权重

> 最高的时候是百度权重7

![](docs/images/57fx-aizhan.jpg)


### 站点流量

> CNZZ 和 百度统计

![](docs/images/57fx-cnzz.JPG)

--------------

![](docs/images/57fx-baidutongji.JPG)

--------------

![](docs/images/57fx-baidutongji2.PNG)

### 网站页面

![](docs/images/57fx-8-qzone.png)

--------------

![](docs/images/57fx-mobile.PNG)

--------------

![](docs/images/57fx-1.png)

--------------

![](docs/images/57fx-2.png)

--------------

![](docs/images/57fx-3.png)

--------------

![](docs/images/57fx-5.png)

--------------

![](docs/images/57fx-6.jpg)

--------------

![](docs/images/57fx-7-wenku.png)





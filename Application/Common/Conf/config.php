<?php
return array(
	//'配置项'=>'配置值'
	'DB_TYPE'   => 'mysql', 
    'DB_HOST'   => 'localhost', 
    'DB_NAME'   => 'wangpan', 
    'DB_USER'   => 'root', 
    'DB_PWD'    => '', 
    'DB_PORT'   => 3306, 
    'DB_PREFIX' => 'hc_', 
    'DB_CHARSET'=> 'utf8', 
	
// 	'DATA_CACHE_TIME'=>3,

// 	'TMPL_CACHE_ON'=>true,


//     'TOKEN_ON'      =>    true,  
//     'TOKEN_NAME'    =>    '__hash__',    
//     'TOKEN_TYPE'    =>    'md5', 
//     'TOKEN_RESET'   =>    true, 
	
	//discuz发帖机器人数据库链接
	'DB_CONFIG2' => 'mysql://root:@localhost:3306/ultrax',
	//ucenter配置
	'AUTH_KEY'=>'e37ec5TAfR2j0kuL',	//discuz目录的config的config_global.php文件中的$_config['security']['authkey']
	'COOKIE_PRE'=>'YsK2_',			//print_r打印cookie，可查看到前缀
		
	'LAYOUT_ON'=>true,
	'LAYOUT_NAME'=>'layout',
		

	'URL_CASE_INSENSITIVE'  =>  true,
		
		// 允许访问的模块列表
	'MODULE_ALLOW_LIST'    =>    array('Home','Admin'),
	'DEFAULT_MODULE'       =>    'Home',  // 默认模块
	
	//分表的share表的最大ID
	'MAX_SHARE_ID'=>7444675,
		
		
	'URL_ROUTER_ON' => true, //URL路由
	'URL_MODEL' => 2, // URL模式
	'URL_PATHINFO_DEPR'=>'-',
// 	'URL_ROUTE_RULES' => array(
// 			'detail/:id' => 'Detail/index',
// 			'u/:id' => 'User/home'
// 	),
	
	//定义资源类型
	'MUSIC' =>array('mp3','wma','ape','flac','aac','mmf','amr','m4a','m4r','ogg','wav'), 
	'VIDEO' =>array('mp4','3gp','mpg','avi','wmv','flv','swf','f4v','rmvb','rm','asf','vob','mov','mkv'),
	'BOOK'=>array('txt','pdf','doc','docx','xls','ppt','pptx','pps','umd','epub','chm'),
	'SOFTWARE'=>array('apk','ipa','exe','dmg'),
	'ZIP'=>array('zip','rar','7z','gz','tar','iso'),
	'PIC'=>array('jpg','jpeg','png','gif','bmp','psd')
		
);
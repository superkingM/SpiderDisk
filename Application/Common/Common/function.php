<?php
use Think\Crypt;
//判断是否属手机
// function is_mobile(){
// 	$user_agent = $_SERVER['HTTP_USER_AGENT'];
// 	$mobile_agents = Array("240x320","acer","acoon","acs-","abacho","ahong","airness","alcatel","amoi","android","anywhereyougo.com","applewebkit/525","applewebkit/532","asus","audio","au-mic","avantogo","becker","benq","bilbo","bird","blackberry","blazer","bleu","cdm-","compal","coolpad","danger","dbtel","dopod","elaine","eric","etouch","fly ","fly_","fly-","go.web","goodaccess","gradiente","grundig","haier","hedy","hitachi","htc","huawei","hutchison","inno","ipad","ipaq","ipod","jbrowser","kddi","kgt","kwc","lenovo","lg ","lg2","lg3","lg4","lg5","lg7","lg8","lg9","lg-","lge-","lge9","longcos","maemo","mercator","meridian","micromax","midp","mini","mitsu","mmm","mmp","mobi","mot-","moto","nec-","netfront","newgen","nexian","nf-browser","nintendo","nitro","nokia","nook","novarra","obigo","palm","panasonic","pantech","philips","phone","pg-","playstation","pocket","pt-","qc-","qtek","rover","sagem","sama","samu","sanyo","samsung","sch-","scooter","sec-","sendo","sgh-","sharp","siemens","sie-","softbank","sony","spice","sprint","spv","symbian","tablet","talkabout","tcl-","teleca","telit","tianyu","tim-","toshiba","tsm","up.browser","utec","utstar","verykool","virgin","vk-","voda","voxtel","vx","wap","wellco","wig browser","wii","windows ce","wireless","xda","xde","zte");
// 	$is_mobile = false;
// 	foreach ($mobile_agents as $device) {
// 		if (stristr($user_agent, $device)) {
// 			$is_mobile = true;
// 			break;
// 		}
// 	}
// 	return $is_mobile;
// }

function is_mobile(){
	$_SERVER['ALL_HTTP'] = isset($_SERVER['ALL_HTTP']) ? $_SERVER['ALL_HTTP'] : '';
	$mobile_browser = '0';
	if(preg_match('/(up.browser|up.link|mmp|symbian|smartphone|midp|wap|phone|iphone|ipad|ipod|android|xoom)/i', strtolower($_SERVER['HTTP_USER_AGENT'])))
		$mobile_browser++;
	if((isset($_SERVER['HTTP_ACCEPT'])) and (strpos(strtolower($_SERVER['HTTP_ACCEPT']),'application/vnd.wap.xhtml+xml') !== false))
		$mobile_browser++;
	if(isset($_SERVER['HTTP_X_WAP_PROFILE']))
		$mobile_browser++;
	if(isset($_SERVER['HTTP_PROFILE']))
		$mobile_browser++;
	$mobile_ua = strtolower(substr($_SERVER['HTTP_USER_AGENT'],0,4));
	$mobile_agents = array(
			'w3c ','acs-','alav','alca','amoi','audi','avan','benq','bird','blac',
			'blaz','brew','cell','cldc','cmd-','dang','doco','eric','hipt','inno',
			'ipaq','java','jigs','kddi','keji','leno','lg-c','lg-d','lg-g','lge-',
			'maui','maxo','midp','mits','mmef','mobi','mot-','moto','mwbp','nec-',
			'newt','noki','oper','palm','pana','pant','phil','play','port','prox',
			'qwap','sage','sams','sany','sch-','sec-','send','seri','sgh-','shar',
			'sie-','siem','smal','smar','sony','sph-','symb','t-mo','teli','tim-',
			'tosh','tsm-','upg1','upsi','vk-v','voda','wap-','wapa','wapi','wapp',
			'wapr','webc','winw','winw','xda','xda-'
	);
	if(in_array($mobile_ua, $mobile_agents)) $mobile_browser++;
	if(strpos(strtolower($_SERVER['ALL_HTTP']), 'operamini') !== false) $mobile_browser++;
	// Pre-final check to reset everything if the user is on Windows
	if(strpos(strtolower($_SERVER['HTTP_USER_AGENT']), 'windows') !== false) $mobile_browser=0;
	// But WP7 is also Windows, with a slightly different characteristic
	if(strpos(strtolower($_SERVER['HTTP_USER_AGENT']), 'windows phone') !== false) $mobile_browser++;
	if($mobile_browser>0) return true;
	else return false;
}

function is_mobile_request(){
	$_SERVER['ALL_HTTP'] = isset($_SERVER['ALL_HTTP']) ? $_SERVER['ALL_HTTP'] : '';
	$mobile_browser = '0';
	if(preg_match('/(up.browser|up.link|mmp|symbian|smartphone|midp|wap|phone|iphone|ipad|ipod|android|xoom)/i', strtolower($_SERVER['HTTP_USER_AGENT'])))
		$mobile_browser++;
	if((isset($_SERVER['HTTP_ACCEPT'])) and (strpos(strtolower($_SERVER['HTTP_ACCEPT']),'application/vnd.wap.xhtml+xml') !== false))
		$mobile_browser++;
	if(isset($_SERVER['HTTP_X_WAP_PROFILE']))
		$mobile_browser++;
	if(isset($_SERVER['HTTP_PROFILE']))
		$mobile_browser++;
	$mobile_ua = strtolower(substr($_SERVER['HTTP_USER_AGENT'],0,4));
	$mobile_agents = array(
			'w3c ','acs-','alav','alca','amoi','audi','avan','benq','bird','blac',
			'blaz','brew','cell','cldc','cmd-','dang','doco','eric','hipt','inno',
			'ipaq','java','jigs','kddi','keji','leno','lg-c','lg-d','lg-g','lge-',
			'maui','maxo','midp','mits','mmef','mobi','mot-','moto','mwbp','nec-',
			'newt','noki','oper','palm','pana','pant','phil','play','port','prox',
			'qwap','sage','sams','sany','sch-','sec-','send','seri','sgh-','shar',
			'sie-','siem','smal','smar','sony','sph-','symb','t-mo','teli','tim-',
			'tosh','tsm-','upg1','upsi','vk-v','voda','wap-','wapa','wapi','wapp',
			'wapr','webc','winw','winw','xda','xda-'
	);
	if(in_array($mobile_ua, $mobile_agents)) $mobile_browser++;
	if(strpos(strtolower($_SERVER['ALL_HTTP']), 'operamini') !== false) $mobile_browser++;
	// Pre-final check to reset everything if the user is on Windows
	if(strpos(strtolower($_SERVER['HTTP_USER_AGENT']), 'windows') !== false) $mobile_browser=0;
	// But WP7 is also Windows, with a slightly different characteristic
	if(strpos(strtolower($_SERVER['HTTP_USER_AGENT']), 'windows phone') !== false) $mobile_browser++;
	if($mobile_browser>0) return true;
	else return false;
}

//url加密
function url_encode($url){
	$crypt=new Crypt();
	return $crypt->encrypt($url, 'baiduyun.57fx.cn powered by hc-cms');
}
//url解密
function url_decode($url){
	$crypt=new Crypt();
	return $crypt->decrypt($url, 'baiduyun.57fx.cn powered by hc-cms');
}


function doctype2type($doctype){
	if (empty($doctype)) $doctype='folder';
	switch (strtolower($doctype)) {
		//video
		case 'mp4':
			return 1;
			break;
		case '3gp':
			return 2;
			break;
		case 'mpg':
			return 3;
			break;
		case 'avi':
			return 4;
			break;
		case 'wmv':
			return 5;
			break;
		case 'flv':
			return 6;
			break;
		case 'swf':
			return 7;
			break;
		case 'f4v':
			return 8;
			break;
		case 'rmvb':
			return 9;
			break;
		case 'rm':
			return 10;
			break;
		case 'asf':
			return 11;
			break;
		case 'vob':
			return 12;
			break;
		case 'mov':
			return 13;
			break;
		case 'mkv':
			return 14;
			break;
		//music	
		case 'mp3':
			return 100;
			break;
		case 'wma':
			return 101;
			break;
		case 'ape':
			return 102;
			break;
		case 'flac':
			return 103;
			break;
		case 'aac':
			return 104;
			break;
		case 'mmf':
			return 105;
			break;
		case 'amr':
			return 106;
			break;
		case 'm4a':
			return 107;
			break;
		case 'm4r':
			return 108;
			break;
		case 'ogg':
			return 109;
			break;
		case 'wav':
			return 110;
			break;
		//种子
		case 'torrent':
			return 200;
			break;
		//软件
		case 'apk':
			return 300;
			break;
		case 'ipa':
			return 301;
			break;
		case 'exe':
			return 302;
			break;
		case 'dmg':
			return 303;
			break;
		//文档
		case 'txt':
			return 400;
			break;
		case 'pdf':
			return 401;
			break;
		case 'doc':
		case 'docx':
		case 'wps':
		case 'rtf':
			return 402;
			break;
		case 'xls':
		case 'xlsx':
		case 'et':
			return 403;
			break;
		case 'ppt':
		case 'pptx':
		case 'pps':
			return 404;
			break;
		case 'umd':
			return 405;
			break;
		case 'epub':
			return 406;
			break;
		case 'chm':
			return 407;
			break;
		//压缩资源
		case 'zip':
			return 500;
			break;
		case 'rar':
			return 501;
			break;
		case '7z':
			return 502;
			break;
		case 'gz':
			return 503;
			break;
		case 'tar':
			return 504;
			break;
		case 'iso':
			return 505;
			break;
		//图片资源
		case 'jpg':
		case 'jpeg':
			return 600;
			break;
		case 'png':
			return 601;
			break;
		case 'gif':
			return 602;
			break;
		case 'bmp':
			return 603;
			break;
		case 'psd':
			return 604;
			break;
		//文件夹
		case 'folder':
			return 700;
			break;
		default:
			return 0;
			break;
	}
	
	
	
}




/**
 * 根据url链接采集网页【暂不支持https的链接，如需支持https，请设置cURL的CURLOPT_SSL_*】
 * @param unknown $url
 * @param string $referer
 * @param string $cookies
 * @param number $timeout
 * @return mixed  */
function crawl($url,$referer='http://www.baidu.com',$cookies='',$timeout=30){
	// 1. 初始化
	$ch = curl_init();

	// 2. 设置选项，包括URL
	curl_setopt($ch, CURLOPT_URL,$url);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	//超时
	curl_setopt($ch, CURLOPT_TIMEOUT, $timeout);
	curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout-5);
	//来路
	curl_setopt($ch, CURLOPT_REFERER, $referer);
	curl_setopt($ch, CURLOPT_HEADER, 0);
	//UA
	curl_setopt($ch,CURLOPT_USERAGENT,'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.75 Safari/537.36');
	//gzip
	curl_setopt($ch, CURLOPT_ENCODING, 'gzip');
	//cookie设置
	if ($cookies) curl_setopt($ch, CURLOPT_COOKIE, $cookies);

	// 3. 执行并获取HTML文档内容
	$content = curl_exec($ch);

	// 4. 释放curl句柄
	curl_close($ch);
	return $content;

}

















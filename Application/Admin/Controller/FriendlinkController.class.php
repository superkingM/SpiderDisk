<?php
namespace Admin\Controller;
use Admin\Model\FriendlinkModel;
use Admin\Model\CommonModel;
use Think\Page;
class FriendlinkController extends BaseController{
  public function index(){
  	if (IS_POST) {
  		$fl=new FriendlinkModel();
  		echo $fl->addLink();die();
  	}else{
  		//分页相关开始
  		$model=new FriendlinkModel();
  		$common=new CommonModel();
  		$listRows=10;												//每页记录数
  		$totalRows=$common->count('friendlink');							//总记录
  		$pn=I('get.p') ? I('get.p'):1;								//第几页.无页码是默认第一页
  		if (!is_numeric($pn)) $this->error('页码参数错误，必须为数字！');	//容错机制
  		$data=$model->get_link($pn,$listRows);						//按页获取记录
  		$page=new Page($totalRows,$listRows);
  		$show=$page->show();
  		$this->assign('data',$data);
  		$this->assign('page',$show);
  		$this->assign('title','友链管理');
  		$this->display();
  	}
  }

  public function setstatus(){
  	if (IS_GET) {
  		M('friendlink')->where(array('id'=>I('get.id')))->save(array('status'=>I('get.status')));
  		echo mysql_affected_rows();die();
  	}
  }
  
  public function delete(){
  	if (IS_GET) {
  		M('friendlink')->delete(I('get.id'));
  		echo mysql_affected_rows();die();
  	}
  }
  
  public function update(){
  		$fl=new FriendlinkModel();
  		echo $fl->updateLink();die();
  }
  
  
}

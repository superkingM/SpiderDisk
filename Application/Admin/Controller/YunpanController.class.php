<?php
namespace Admin\Controller;
use Admin\Model\ShareModel;
use Admin\Model\CommonModel;
use Think\Page;
use Home\Model\SourceModel;
use Admin\Model\DzpostModel;
class YunpanController extends BaseController{
  public function index(){
	  	$model=new ShareModel();
	  	$common=new CommonModel();
	  	$source=new SourceModel();
	  	$listRows=10;												//每页记录数
	  	
	  	$sort=I('get.sortT') ? I('get.sortT') :'';
	  	$kw=I('get.kw') ? I('get.kw') : '';
	  	if ($kw) {
	  		$w['name']=array('like','%'.$kw.'%');
	  		$totalRows=$common->count('yunpan',$w);
	  	}else {
	  		$totalRows=$common->count('yunpan');
	  	}
	  	
	  	$pn=I('get.p') ? I('get.p'):1;								//第几页.无页码是默认第一页
	  	if (!is_numeric($pn)) $this->error('页码参数错误，必须为数字！');	//容错机制
	  	$data=$model->get_yunpan($pn,$listRows,$sort,$kw);
	  	for ($i=0;$i<count($data);$i++){
	  		$data[$i]['size']=$source->conversion($data[$i]['size']);
	  	}
	  	$page=new Page($totalRows,$listRows);
	  	$show=$page->show();
	  	$this->assign('data',$data);
	  	$this->assign('page',$show);
  		$this->assign('title','360云盘资源管理');
//   		die();
  		$this->display();
  }
  
  public function Json(){
  	if (IS_POST) {
  		$Share=new ShareModel();
  		$Share->yunpan($_POST['content'])==1 ? $this->success('添加成功！') : $this->error('添加失败，您添加的资源已存在');
  	}
  }
  

  //360云盘发帖
  public function postYunpan(){
  	if (IS_POST) {
  		$Model=new DzpostModel();
  		$Model->postYunpan(I('post.fid'), I('post.typeid'),I('post.num'));
  		$this->success('发布成功');
  	}
  }
  
  public function setType(){
  	$data['id']=I('get.id');
  	$data['type']=I('get.type');
  	$data['isset']=1;
  	$Yunpan=M('yunpan');
  	$Yunpan->save($data);
  	echo mysql_affected_rows();
  
  }
  
  public function del(){
//   	$data['id']=I('get.id');
  	$Yunpan=M('yunpan');
  	$Yunpan->delete(I('get.id'));
  	echo mysql_affected_rows();
  }
  
  
}

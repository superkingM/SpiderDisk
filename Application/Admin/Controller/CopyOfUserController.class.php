<?php
namespace Admin\Controller;
use Think\Page;
use Admin\Model\UserModel;
use Admin\Model\CommonModel;
use Admin\Model\ShareModel;
class UserController extends BaseController{
	//会员列表
	public function index(){
		//分页相关开始
		$model=new UserModel();
		$common=new CommonModel();
		$listRows=10;												//每页记录数
		$totalRows=$common->count('user');							//总记录
		$pn=I('get.p') ? I('get.p'):1;								//第几页.无页码是默认第一页
		if (!is_numeric($pn)) $this->error('页码参数错误，必须为数字！');	//容错机制
		$data=$model->get_users($pn,$listRows);						//按页获取记录
		$page=new Page($totalRows,$listRows);
		$show=$page->show();
// 		print_r($data);die();
		$this->assign('data',$data);
		$this->assign('page',$show);
		$this->assign('title','会员管理');
		$this->display();
	}
	
	
	//收录会员
	public function collect(){
		if (IS_POST){
			$model=new UserModel();
			$uk=$model->collect_uk(I('post.content'));
			for ($i=0;$i<count($uk);$i++){
				if ($model->check_uk($uk[$i])==0) {//如果在数据库未存在，则收录，否则不再收录
					$data=$model->get_user_info($uk[$i]);
					if (is_object($data) && $data->errno==0) {
						$arr[$i]=$data->user_info;
					}else{
						$fail[]=$uk[$i];
					}
				}
			}
			$model->addAllUserInfo($arr);
			//如果存在cookie，则表示有部分收录失败
			if (is_array($fail)) {
				cookie('uk',$fail);
				$this->redirect('User/fail');
			}else {
				$this->success('恭喜您，全部收录成功！【提示：如果该会员之前已被添加过，系统将不会重复添加】');
			}
		}
	}
	
	//此处待完善
	public function fail(){
		echo "添加失败的会员，请重试或者使用手动添加的方式进行添加";
		print_r($_COOKIE['uk']);
		die();
		
	}
	
	//采集会员资源的操作
	public function collect_source(){
		header("Content-type: text/html; charset=utf-8"); 
		$share=new ShareModel();
		$total=I('get.shareNum');//分享数量
		$uk=I('get.uk');//分享会员的UK
		$pn=I('get.pn') ? I('get.pn'):1;//采集页数
		$limt=60;
		$totalpage=ceil($total/$limt);//总页数
		$start=($pn-1)*$limt;//从第几条记录开始
		echo "<div style='width:100%;line-height:500%;text-align:center;'>本次采集共分为".$totalpage."页，当前正在采集第".$pn." 页...</div>";
		$rs=$share->get_source($uk, $start,$limt);
		if ($rs==1){
			$pn++;
		}else{
			$this->error('采集失败，请重试，或者使用手动式采集');
		}
		if ($pn>$totalpage) {
			$user=M('user');
			$user->where(array('uk'=>$uk))->save(array('status'=>1));
			$d=$user->where(array('status'=>0))->order('share desc')->find();
			//http://baiduyun.57fx.cn/Admin-User-collect_source.html?uk=2500327416&shareNum=642
			$s_n=$d['share']+$d['album'];
			$this->success('恭喜您，成功采集完毕，10秒钟后跳转，如需停止采集，请关闭当前页面..',U('User/collect_source',array('uk'=>$d['uk'],'shareNum'=>$s_n)),10);
		}
		$params=array(
				'shareNum'=>$total,
				'uk'=>$uk,
				'pn'=>$pn
		);
		$this->redirect('User/collect_source',$params,1,'');
	}
	
	//采集订阅
	public function collect_follow(){
		header("Content-type: text/html; charset=utf-8");
		$user=new UserModel();
		sleep(rand(3, 5));//睡眠1-5秒
		$total=I('get.followNum');//订阅数量
		$uk=I('get.uk');//分享会员的UK
		$pn=I('get.pn') ? I('get.pn'):1;//采集页数
		$limt=24;
		$totalpage=ceil($total/$limt);//总页数
		$start=($pn-1)*$limt;//从第几条记录开始
		echo "本次采集共分".$totalpage."页，当前正在采集".$pn."页...";
		$rs=$user->getFollow($uk, $start,$limt);
		if ($pn>$totalpage) {
			$user->where(array('uk'=>$uk))->save(array('is_follow'=>1));
			$d=$user->where(array('is_follow'=>0))->order('follow desc')->find();
			$this->success('恭喜您，采集完毕，还有10秒跳转采集下一个分享达人的订阅。。如需停止采集，请手动关闭当前页面..',U('User/collect_follow',array('uk'=>$d['uk'],'followNum'=>$d['follow'])),10);die();
		}
		$pn++;
// 		$msg="Divided into ".$totalpage."pages in total to collect，the Num".($pn-1)." page is collected，and we come to collect Num ".$pn." page..";
		$params=array(
				'followNum'=>$total,
				'uk'=>$uk,
				'pn'=>$pn
		);
		$delay=1;
		$this->redirect('User/collect_follow',$params,$delay,' ');
	}
	
	//采集粉丝
	public function collect_fans(){
		header("Content-type: text/html; charset=utf-8");
		$user=new UserModel();
		$total=I('get.fansNum');//分享数量
		$uk=I('get.uk');//分享会员的UK
		$pn=I('get.pn') ? I('get.pn'):1;//采集页数
		$limt=24;
		$totalpage=ceil($total/$limt);//总页数
		$start=($pn-1)*$limt;//从第几条记录开始
		echo "本次采集共分".$totalpage."页，当前正在采集".$pn."页...";
		$rs=$user->getFans($uk, $start,$limt);
		$pn++;
		if ($pn>$totalpage || $pn>100) {
			$this->success('恭喜您，成功采集完毕，正在跳转..',U('User/index'));
		}
// 		$msg="Divided into ".$totalpage."pages in total to collect，the Num".($pn-1)." page is collected，and we come to collect Num ".$pn." page..";
		$params=array(
				'fansNum'=>$total,
				'uk'=>$uk,
				'pn'=>$pn
		);
		$delay=1;
		$this->redirect('User/collect_fans',$params,$delay);
	}
	
	
	
	
	//代码采集会员信息
	public function collect_code(){
		if (IS_POST) {
			$user=new UserModel();
			$user->preg_header(I('post.content_code'))==1 ? $this->success('采集成功！',U('User/index')) : $this->error('采集失败，可能是数据库已存在该会员信息！');
		}
	}
	
	//设置会员状态
	public function recommend(){
			$user=new UserModel();
			$user->set_recommend(I('get.uk'), I('get.recommend'))==1 ? $this->success('状态设置成功！') : $this->error('状态设置失败，请刷新页面后再重试');
		
	}
	
	//更新会员信息
	public function update(){
		$user=new UserModel();
		$rs=$user->update(I('get.uk'));
		switch ($rs){
			case -1:
				$this->error('无法抓取百度网盘会员信息，会员信息更新失败，请检查网络是否通畅。');
				break;
			case 0:
				$this->error('该会员信息目前暂时没有变动，无需更新。');
				break;
			case 1:
				$this->success('会员信息更新成功。');
				break;
		}
	}
	
	//json形式收录资源
	public function collect_json(){
		$content=$_POST['content_json'];
		$share=new ShareModel();
		echo $share->get_json($content);die();
	}
	
	//json形式收录会员
	public function collect_user_json(){
		$content=$_POST['user_json'];
		$share=new UserModel();
		echo $share->get_user_json($content);die();
	}
	
	//删除会员
	public function delete_user(){
		$w['uk']=I('get.uk');
		if ($w['uk']) {
			M('user')->where($w)->delete();
			M('share')->where($w)->delete();
			M('user')->where($w)->delete();
			M('album')->where($w)->delete();
			M('album_source')->where($w)->delete();
			$this->success('会员删除成功！');
		}else{
			$this->error('会员删除失败！');
		}
		
		
	}
	
}
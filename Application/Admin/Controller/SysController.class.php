<?php
namespace Admin\Controller;
use Admin\Model\AdminModel;
use Admin\Model\ShareModel;
class SysController extends BaseController{
  public function index(){
  	if (IS_POST) {
  		$sys=M('sys');
	  	$data=$sys->create(I('post.'));
// 	  	print_r($data);die();
	  	$sys->save($data);
	  	mysql_affected_rows()==1 ? $this->success('站点修改成功！') : $this->error('站点修改失败，可能您未做任何修改。');
  	}else{
  		$data=M('sys')->find();
  		$this->assign('title','网站配置');
  		$this->assign('data',$data);
  		$this->display('setting');
  	}
  }
  
  public function seo(){
  	if (IS_POST) {
  		$data=I('post.');
  		$error=0;
  		foreach ($data['type'] as $k=>$v){
  			empty($data['title'][$k]) ? $data['title'][$k]='{title}' : '';
  			empty($data['keywords'][$k]) ? $data['keywords'][$k]='{keywords}' : '';
  			empty($data['desc'][$k]) ? $data['desc'][$k]='{desc}' : '';
  			$d=array(
  				'title'=>$data['title'][$k],
  				'keywords'=>$data['keywords'][$k],
  				'desc'=>$data['desc'][$k],
  			);
  			$w['type']=$v;
  			M('seo')->where($w)->save($d);
  			mysql_affected_rows()==0 ? $error++ : '';
  		}
  		$error<18 ? $this->success('SEO优化提交成功！') : $this->error('SEO优化提交失败，可能您未做任何修改');
  	}else{
  		$this->assign('title','系统设置');
  		$seo=M('seo')->select();
  		$this->assign('seo',$seo);
  		$this->display();
  	}
  	
  }
  
  
  public function nav(){
  	if (IS_POST) {
  		$nav=M('nav');
  		$data=$nav->create(I('post.'));
  		foreach ($data['name'] as $k=>$v){
  			$d=array(
  				'id'=>$data['id'][$k],
  				'name'=>$v,
  				'sort'=>$data['sort'][$k],
  				'flag'=>$data['flag'][$k]
  			);
  			$nav->save($d);
  		}
  		$this->success('修改成功！');
  	}else {
  		$data=M('nav')->order('isopen desc,sort asc')->select();
  		$this->assign('data',$data);
  		$this->assign('title','导航管理');
  		$this->display();
  	}
  }
  
  public function nav_isopen(){
  	M('nav')->where(array('isindex'=>0,'id'=>I('get.id')))->save(array('isopen'=>I('get.isopen')));
  	echo mysql_affected_rows()==1 ? 1: 0;
  }
  
  public function addNav(){
  	if (IS_POST) {
  		$nav=M('nav');
  		$data=$nav->create(I('post.'));
  		$nav->add($data);
  		$this->success('添加成功！');
  	}
  }
  public function deleteNav(){
  	M('nav')->delete(I('get.id'));
  }
  
  //修改密码
  public function updateAdmin(){
  	if (IS_POST) {
  		$admin=new AdminModel();
  	 	$rs=$admin->updateAdmin();
  	 	switch ($rs) {
  	 		case 0:
  	 			$this->error('修改失败，可能您未做任何修改');
  	 			break;
  	 		case 1:
  	 			session('admin',null);
  	 			$this->success('修改成功！请重新登录。');
  	 			break;
  	 		case -2:
  	 			$this->error('如需修改密码，原密码和新密码均不能为空,且新密码不能跟原密码相同。');
  	 			break;
  	 		case -1:
  	 			$this->error('原密码不正确。');
  	 			break;
  	 	}
  	}
  }
  
  
//更新统计与数据
  public function updateCount(){
  	//更新统计
  	$sys=M('Sys');
  	$original_data=$sys->where(array('id'=>5))->find();
	$Share1_model=M('share1');
	$Album_model=M('album');
	$User_model=M('user');	
  	
  	$count['id']=5;//id为5表示的是57百度云搜索的统计
  	$count['yesterday_source']=$original_data['today_source'];
  	$count['today_source']=$Share1_model->where(array('id'=>array('gt',$original_data['max_share_id'])))->count();
  	$count['total_source']=$original_data['total_source']+$count['today_source'];//今日收录的资源数量+昨日的总量
  	$count['yesterday_album']=$original_data['today_album'];
  	$count['today_album']=$Album_model->where(array('id'=>array('gt',$original_data['max_album_id'])))->count();
  	$count['total_album']=$original_data['total_album']+$count['today_album'];
  	$count['yesterday_user']=$original_data['today_user'];
  	$count['today_user']=$User_model->where(array('id'=>array('gt',$original_data['max_user_id'])))->count();//今天收录的用户数量
  	$count['total_user']=$original_data['total_user']+$count['today_user'];//原来的用户数量加上今天的用户数量
  	$count['time']=time();
  	$count['max_album_id']=$Album_model->max('id');
  	echo $Album_model->getLastSql();
  	$count['max_user_id']=$User_model->max('id');
  	$count['max_share_id']=$Share1_model->max('id');
  	print_r($count);
  	$sys->save($count);
  	
  	//更新首页最新10条数据
  	//可能用json存储更好，字节数可以少一点
  	$source=new ShareModel();
  	$album=$Album_model->order('id desc')->limit(10)->field('id,title')->select();
  	$data[0]['content']=serialize($album);
  	$data[0]['type']='album';
  	$data[1]['content']=serialize($source->get_tent_new(array('doctype'=>'apk')));
  	$data[1]['type']='apk';
  	$data[2]['content']=serialize($source->get_tent_new(array('doctype'=>'ipa')));
  	$data[2]['type']='ios';
  	$data[3]['content']=serialize($source->get_tent_new(array('doctype'=>'exe')));
  	$data[3]['type']='exe';
  	$data[4]['content']=serialize($source->get_tent_new(array('doctype'=>array('in',C('VIDEO')))));
  	$data[4]['type']='videos';
  	$data[5]['content']=serialize($source->get_tent_new(array('doctype'=>array('in',C('MUSIC')))));
  	$data[5]['type']='music';
  	$data[6]['content']=serialize($source->get_tent_new(array('doctype'=>'torrent')));
  	$data[6]['type']='bt';
  	$data[7]['content']=serialize($source->get_tent_new(array('doctype'=>array('in',array('doc','docx','wps','ppt','pptx','pps','xls')))));
  	$data[7]['type']='doc';
  	$data[8]['content']=serialize($source->get_tent_new(array('doctype'=>'pdf')));
  	$data[8]['type']='pdf';
  	$data[9]['content']=serialize($source->get_tent_new(array('doctype'=>'txt')));
  	$data[9]['type']='txt';
  	$data[10]['content']=serialize($source->get_tent_new(array('doctype'=>array('in',C('ZIP')))));
  	$data[10]['type']='zip';
  	M('new')->query('TRUNCATE TABLE `hc_new`');
  	M('new')->addAll($data);
  	$this->success('更新成功！现跳转更新列表',U('Sys/updateList'));
  	
  	//mysql_affected_rows() >0 ? $this->success('更新成功！') : $this->error('更新失败。可能您的数据未有变更。');
  	
  }
  
  
  //更新列表，每个列表500条最新记录
  public function updateList(){
  	$pn=I('get.pn') ? I('get.pn') : 1;
  	$Model=M('share1');
  	$Tmp=M('tmp');
  	if ($pn>6) {
//   		$this->success('更新成功',U('Index/index'));
  		$this->success('更新成功！现跳转更新文库列表',U('Sys/updateDoc'));
  		die();
  	}
  	switch ($pn) {
  		//视频
  		case 1:
  			$doctype=array('in',C('VIDEO'));
  			break;
  		//音频
  		case 2:
  			$doctype=array('in',C('MUSIC'));
  			break;
  		//种子
  		case 3:
  			$doctype='torrent';
  			break;
  		//软件
  		case 4:
  			$doctype=array('in',C('SOFTWARE'));
  			break;
  		//电子书
  		case 5:
  			$doctype=array('in',C('BOOK'));
  			break;
  		//打包资源
  		case 6:
  			$doctype=array('in',C('ZIP'));
  			break;
  	}
  	$where=array('doctype'=>$doctype);
  	$data=$Model->where($where)->order('id desc')->limit(500)->select();
  	echo $Model->getLastSql();
  	$Tmp->where($where)->delete();
  	$Tmp->addAll($data);
  	$this->success('更新成功，继续',U('Sys/updateList',array('pn'=>($pn+1))),rand(3, 6));
  }
  
  
  //更新列表，每个列表1000条最新记录
  public function updateDoc(){
  	$pn=I('get.pn') ? I('get.pn') : 0;
  	$Model=D('share1');
  	$Tmp=M('tmpdoc');
  	if ($pn>7) {
  		$share1_doc_count=$Model->where(array('type'=>array('in','400,401,402,403,404,405,406,407')))->count();
  		$total=$share1_doc_count+1463378;
  		M('syswk')->save(array('id'=>1,'total'=>$total));
  		$this->success('更新成功',U('Index/index'));
  		die();
  	}
  	$type="40".$pn;
  	echo $Tmp->where('`type`='.$type)->delete().'|||';
  	$data=$Model->where('`type`='.$type)->order('id desc')->limit(1000)->select();
  	echo $Model->getLastSql();
  	$Tmp->addAll($data);
  	$this->success('更新成功，继续',U('Sys/updateDoc',array('pn'=>($pn+1))),rand(3, 6));
  }
  
 
}

<?php
namespace Admin\Controller;
use Admin\Model\CommonModel;
use Think\Page;
class AdController extends BaseController{
  public function index(){
  	if (IS_POST) {
  		$data=I('post.');
  		unserialize($data);
  		$data['start_time']=strtotime($data['start_time']);
  		$data['end_time']=strtotime($data['end_time']);
  		$data['time']=time();
  		$d=D('ad');
  		$d->create($data);
  		$d->add();
  		echo mysql_affected_rows()==1 ? 1:0;die();
  	}else{
  		$this->assign('title','广告位_广告管理');
  		$this->display();
  	}
  }
  
  public function adlist(){
  	$model=M('ad');
  	$common=new CommonModel();
  	$listRows=10;												//每页记录数
  	$totalRows=$common->count('ad');							//总记录
  	$pn=I('get.p') ? I('get.p'):1;								//第几页.无页码是默认第一页
  	if (!is_numeric($pn)) $this->error('页码参数错误，必须为数字！');	//容错机制
  	$data=$model->limit(($pn-1)*$listRows,$listRows)->field('id,name,status,type,position,start_time,end_time,time')->order('status desc,id desc')->select();
  	foreach ($data as &$v){
  		switch ($v['status']) {
  			case 1:
  				$v['status_text']="开启";
  				break;
  			default:
  				$v['status_text']="关闭";
  				break;
  		}
  		if ($v['end_time']<time()) $v['status_text']="过期";
  		$type=$v['type'].$v['position'];
  		switch ($type) {
  			case 'global1':
  				$v['type']='全局 顶部广告';
  				break;
  			case 'global2':
  				$v['type']='全局 底部广告';
  				break;
  			case 'nav1':
  				$v['type']='导航 底部广告';
  				break;
  			case 'index1':
  				$v['type']='首页 通栏广告';
  				break;
  			case 'common1':
  				$v['type']='普通 左侧广告';
  				break;
  			case 'detail1':
  				$v['type']='详情 左侧广告';
  				break;
  			case 'detail2':
  				$v['type']='详情 通栏广告';
  				break;
  			case 'so1':
  				$v['type']='搜索 右侧广告';
  				break;
  		}
  		
  	}
  	$page=new Page($totalRows,$listRows);
  	$show=$page->show();
  	$this->assign('data',$data);
  	$this->assign('page',$show);
  	$this->assign('title','所有广告_广告管理');
//   	print_r($data);die();
  	$this->display('list');
  }
  
  
  public function get_code(){
  	$data=M('ad')->where(array('id'=>I('get.id')))->field('code')->find();
  	$data['code']=html_entity_decode($data['code']);
  	$this->ajaxReturn($data);
  }
  
  public function edit_ad(){
  	if (IS_POST){
  		M('ad')->where(array('id'=>I('post.id')))->save(array('code'=>I('post.code')));
  		echo mysql_affected_rows()==1 ? 1:0;
  	}else{
  		echo 0;
  	}
  }
  
  
  public function del_ad(){
  	M('ad')->where(array('id'=>I('get.id')))->delete();
  	echo mysql_affected_rows()==1 ? 1:0;
  }
  
  public function set_status(){
  	M('ad')->where(array('id'=>I('get.id')))->save(array('status'=>I('get.status')));
  	echo mysql_affected_rows()==1 ? 1:0;
  }
  
  
  public function mobile_ad(){
  	$Mobile=M('mobile_ad');
  	if (IS_POST) {
  		$data=$Mobile->create(I('post.'));
  		$rs=$Mobile->add($data);
  		$rs ? $this->success('成功。') : $this->error('失败。');
  	}else{
  		$data=$Mobile->select();
  		$this->assign('title','手机广告');
  		$this->assign('data',$data);
  		$this->display('mobile');
  	}
  }
  
  public function del_mobile(){
  	echo M('mobile_ad')->delete(I('get.id'));
  }
  
  
  
  
  
  
}

<?php
namespace Admin\Controller;
use Think\Controller;
use Think\Verify;
use Admin\Model\AdminModel;
class LoginController extends Controller{
	public function index(){
		$admin=new AdminModel();
		if (IS_POST) {
			$rs=$admin->admin_login();
			if ($rs) {
				$this->success('登录成功！',U('Index/index'));
			}else {
				$this->error('登录失败，账号或密码或手机号码错误，请重新登录。。一切非迷信且又无法用科学去解释的问题都归咎于传说中的人品问题！');
			}
		}else {
			$admin->admin_is_login();//判断是否登录,如果已登录，则直接进入后台
			layout(false);
			$this->assign('title','管理员登录');
			$this->display();
		}
	}
	
	//设置中文验证码
	public function code(){
		$verify=new Verify();
		$verify->useCurve=false;
		$verify->fontttf="huawenxingkai.ttf";
		$verify->useZh=true;
		$verify->entry(1);
	}
	
	//jQuery验证提交的验证码
	public function check_code(){
		$verify=new Verify();
// 		dump($_POST);die();
		if (IS_POST) {
			if ($verify->check(I('post.code'),1)) {
				echo 1;
				exit();
			}else {
				echo 0;
				exit();
			}
		}
	}
	
	public function logout(){
		session('admin',null);
		$this->success('退出成功！');
	}
	
	
	
}
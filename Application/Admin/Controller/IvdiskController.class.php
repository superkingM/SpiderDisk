<?php
namespace Admin\Controller;
use Admin\Model\ShareModel;
use Admin\Model\CommonModel;
use Think\Page;
use Home\Model\SourceModel;
use Admin\Model\DzpostModel;
use Admin\Model\VdiskModel;
use Think\Snoopy;
class IvdiskController extends BaseController{
  public function index(){
	  	$model=new ShareModel();
	  	$common=new CommonModel();
	  	$source=new SourceModel();
	  	$listRows=10;												//每页记录数
	  	
	  	$sort=I('get.sortT') ? I('get.sortT') :'';
	  	$kw=I('get.kw') ? I('get.kw') : '';
	  	if ($kw) {
	  		$w['name']=array('like','%'.$kw.'%');
	  		$totalRows=$common->count('vdisk',$w);
	  	}else {
	  		$totalRows=$common->count('vdisk');
	  	}
	  	
	  	$pn=I('get.p') ? I('get.p'):1;								//第几页.无页码是默认第一页
	  	if (!is_numeric($pn)) $this->error('页码参数错误，必须为数字！');	//容错机制
	  	$data=$model->get_yunpan($pn,$listRows,$sort,$kw);
	  	for ($i=0;$i<count($data);$i++){
	  		$data[$i]['size']=$source->conversion($data[$i]['size']);
	  	}
	  	$page=new Page($totalRows,$listRows);
	  	$show=$page->show();
	  	$this->assign('data',$data);
	  	$this->assign('page',$show);
  		$this->assign('title','新浪微盘资源管理');
//   		die();
  		$this->display();
  }
  
  public function Json(){
  	if (IS_POST) {
  		$Share=new ShareModel();
  		$Share->yunpan($_POST['content'])==1 ? $this->success('添加成功！') : $this->error('添加失败，您添加的资源已存在');
  	}
  }
  
  public function collect(){
  	$model=new VdiskModel();
  	$url='http://vdisk.weibo.com/s/qOcHimKhlXVI';
// 	$url='http://vdisk.weibo.com/share/ajaxSlideShare?_=0.4816518928958943';
  	$model->collectContent($url);
  	die();
  }
  
  public function collectUrl(){
  	
  }

  //新浪微盘发帖
  public function postYunpan(){
  	if (IS_POST) {
  		$Model=new DzpostModel();
  		$Model->postYunpan(I('post.fid'), I('post.typeid'),I('post.num'));
  		$this->success('发布成功');
  	}
  }
  
  public function setType(){
  	$data['id']=I('get.id');
  	$data['type']=I('get.type');
  	$data['isset']=1;
  	$Yunpan=M('vdisk');
  	$Yunpan->save($data);
  	echo mysql_affected_rows();
  }
  
  public function del(){
//   	$data['id']=I('get.id');
  	$Yunpan=M('vdisk');
  	$Yunpan->delete(I('get.id'));
  	echo mysql_affected_rows();
  }
  
  public function snoopy(){
  	$snoopy=new Snoopy();
  	$url='http://www.sogou.com/web?query=site%3Avdisk.weibo.com+%E7%9A%84%E5%88%86%E4%BA%AB+%E5%BE%AE%E7%9B%98&cid=&page=1&ie=utf8&p=40040100&dp=1&w=01059900&dr=1';
  	$snoopy->fetchlinks($url);
  	$data=$snoopy->results;
  	$pre_pattern='#http\:\/\/vdisk\.weibo\.com\/u\/([0-9]{10})#isU';
  	$U=M('vdisk_u');
  	for ($i=0;$i<count($data);$i++){
  		preg_match($pre_pattern,$data[$i],$uid);
  		if ($uid[1]) {
  			$arr[]=$uid[1];
  		}
  	}
  	$uni=array_unique($arr);
  	for ($k=0;$k<count($uni);$k++){
  		$U->add(array('uid'=>$uni[$k]));
  		echo $uni[$k]."|".($k+1).'<br/>';
  	}
// 	$U->addAll($uni);
  	print_r($uni);die();
  }
  
}

<?php
namespace Admin\Controller;
use Think\Page;
use Admin\Model\CommonModel;
use Admin\Model\ShareModel;
use Admin\Model\AlbumModel;
class SourceController extends BaseController{
	
	//分享管理
  public function index(){
		$model=new ShareModel();
		$common=new CommonModel();
		$listRows=10;												//每页记录数
		
		$sort=I('get.sortT') ? I('get.sortT') :'';
		$kw=I('get.kw') ? I('get.kw') : '';
		if ($kw) {
			$w['title']=array('like','%'.$kw.'%');
			$totalRows=$common->count('share',$w);
		}else {
			$totalRows=$common->count('share');
		}
		
		$pn=I('get.p') ? I('get.p'):1;								//第几页.无页码是默认第一页
		if (!is_numeric($pn)) $this->error('页码参数错误，必须为数字！');	//容错机制
		$data=$model->get_share($pn,$listRows,$sort,$kw);	
		$page=new Page($totalRows,$listRows);
		$show=$page->show();
		$this->assign('data',$data);
		$this->assign('page',$show);
		$this->assign('title','分享管理');
		$this->display();
  }

  //专辑管理
  public function album(){
	  	$model=new ShareModel();
	  	$common=new CommonModel();
	  	$listRows=10;												//每页记录数
	  	
	  	$sort=I('get.sortT') ? I('get.sortT') :'';
	  	$kw=I('get.kw') ? I('get.kw') : '';
	  	if ($kw) {
	  		$w['title']=array('like','%'.$kw.'%');
	  		$totalRows=$common->count('album',$w);
	  	}else {
	  		$totalRows=$common->count('album');
	  	}
	  	$pn=I('get.p') ? I('get.p'):1;								//第几页.无页码是默认第一页
	  	if (!is_numeric($pn)) $this->error('页码参数错误，必须为数字！');	//容错机制
	  	$data=$model->get_album($pn,$listRows,$sort,$kw);
	  	$page=new Page($totalRows,$listRows);
	  	$show=$page->show();
// 	  	print_r($data);die();
	  	$this->assign('data',$data);
	  	$this->assign('page',$show);
	  	$this->assign('title','专辑管理');
	  	$this->display();
  }
  
 
  //删除资源
  public function delete_source(){
 	$common=new CommonModel();
 	$common->delete('share', I('get.id'))==1 ? $this->success('删除成功') : $this->error('删除失败，请刷新页面后重试!');
  }
  
  //采集专辑资源
  public function getAlbumSource(){
  	header("Content-type: text/html; charset=utf-8");
  	set_time_limit(60);
  	$share=new ShareModel();
  	$total=I('get.num');//分享数量
  	$uk=I('get.uk');//分享会员的UK
  	$album_id=I('get.album_id');
  	$pn=I('get.pn') ? I('get.pn'):1;//采集页数
  	$limit=60;
  	$totalpage=ceil($total/$limit);//总页数
  	$start=($pn-1)*$limit;//从第几条记录开始
  	echo "<div style='width:100%;line-height:500%;text-align:center;'>本次采集共分为".$totalpage."页，当前正在采集第".$pn."页...</div>";
  	$share->getAlbumSource($album_id, $uk,$start,$limit);
  	$pn++;
  	if ($pn>$totalpage) {
  		$album=M('album');
  		$album->where(array('album_id'=>$album_id))->save(array('status'=>1));
  		$d=$album->where(array('status'=>0))->order('filecount desc')->find();
  		$this->success('恭喜您，采集完毕，还有3秒跳转采集下一个专辑。。如需停止采集，请手动关闭当前页面..',U('Source/getAlbumSource',array('uk'=>$d['uk'],'album_id'=>$d['album_id'],'num'=>$d['filecount'])),1);
  		die();
  	}
  	$params=array(
  			'shareNum'=>$total,
  			'uk'=>$uk,
  			'pn'=>$pn,
  			'album_id'=>$album_id
  	);
  	$this->redirect('Source/getAlbumSource',$params,1,'<div style="width:100%;line-height:500%;text-align:center;">程序正在采集中，请勿关闭当前页面...</div>');
  }
  
  //批量采集专辑
  public function batchAlbum(){
  	header("Content-type: text/html; charset=utf-8");
  	$share=new ShareModel();
  	$num=I('get.num');
  	$status=I('get.status');
  	$step=I('get.step') ? I('get.step') : 0;
  	$album=M('album');
  	if ($step<$num) {
  		$data=$album->where(array('status'=>$status))->order('time desc')->limit(1)->find();
  		if (!is_array($data)) $step=$num;
  		$limit=60;
  		$totalPn=ceil($data['filecount']/$limit);
  		for ($i=0;$i<$totalPn;$i++){
  			$start=$i*$limit;
  			$share->getAlbumSource($data['album_id'], $data['uk'],$start,$limit);
  		}
  		$album->where(array('album_id'=>$data['album_id']))->save(array('status'=>1));
  		$step++;
  		$msg="<div style='width:100%;line-height:500%;text-align:center;'>正在采集".($step+1)."个专辑...</div>";
  		$params=array(
  				'num'=>$num,
  				'status'=>$status,
  				'step'=>$step,
  		);
  		$this->redirect('Source/batchAlbum',$params,1,$msg);
  	}else {
  		$this->success('专辑批量采集成功！',U('Source/album'));
  	}
  }
  
  //删除专辑
  public function delete_album(){
  	$w['album_id']=I('get.album_id');
  	if ($w['album_id']) {
  		M('album')->where($w)->delete();
  		M('album_source')->where($w)->delete();
  		$this->success('专辑删除成功！');
  	}else{
  		$this->error('专辑删除失败');
  	}
  }
  
  public function init_album(){
  	set_time_limit(600);
  	$album=M('album');
  	$source=M('album_source');
  	$albumid_arr=$album->where(array('status'=>1))->field('album_id')->select();
  	echo count($albumid_arr);
  	$num=0;
  	foreach ($albumid_arr as $v){
  		$arr=$source->where(array('album_id'=>$v['album_id']))->find();
  		if (empty($arr)) {
  			$num++;
  			echo $num.'|';
  			$album->where(array('album_id'=>$v['album_id']))->save(array('status'=>0));
  		}
  	}
  	echo $num;
  }
  
  public function multiple(){
  	$Model=new ShareModel();
  	$Model->multiple_album(20);
  	$this->success('再来一次',U('multiple'));
  }
  
}

<?php
namespace Admin\Controller;
use Admin\Model\DzpostModel;
class VdiskController extends BaseController{
  	public function index(){
  		//url形式：http://vdisk.weibo.com/share/hot?cid=6&type=&page=1
			if (I('get.collect')) {
				$cid=I('get.cid');
				$page=I('get.page');
		  		$url='http://vdisk.weibo.com/share/hot?cid='.$cid.'&type=&page='.$page;
		  		$content=file_get_contents($url);
		  		
				//匹配datainfo
				$preg_datainfo='#data-info=\'(.*)\'#isU';
				preg_match_all($preg_datainfo, $content,$datainfo);
				$arr=array_unique($datainfo[1]);
				if ($arr) {
					$data=array();
					$time=time();
					foreach ($arr as $k=>$v){
						$v=json_decode($v);
						$info = pathinfo($v->filename);
						$data[$k]['extension']=$info['extension'];
						$data[$k]['copy_ref']=$v->copy_ref;
						$data[$k]['filename']=$v->filename;
						$data[$k]['uid']=$v->uid;
						$data[$k]['username']=$v->username;
						$data[$k]['is_dir']=$v->is_dir;
						$data[$k]['cid']=$cid;
						$data[$k]['time']=$time;
					}
// 					print_r($data);die();
					M('vdisk_source')->addAll($data);
					$next_page=$page+1;
					$this->success('第'.$page.'页采集完毕，正在跳转采集第'.$next_page.'页',U('Vdisk/index',array('collect'=>1,'page'=>$next_page,'cid'=>$cid)),5);
				}else{
					$this->success('采集完毕',U('Vdisk/index'));
				}
			}else{
				$this->assign('title','新浪微盘采集');
				$this->display();
			}
  	}
  	

  	//发帖
  	public function post_vdisk(){
  		if (IS_POST) {
  			$vdisk=new DzpostModel();
  			$cid_arr=explode('|', I('post.cid_name'));
  			$vdisk->postVdisk(I('post.fid'), I('post.typeid'), $cid_arr[0], $cid_arr[1]);
  			$this->success('发布成功');
  		}
  	}
  	
  	
  	
  	
  	
  	
  	
  	
  	
  	
  	
  	
  	
  	
  	
  	
  	
  	
  	
  	
  	
  	
  	
  	
  	
  	
  	//   	public function sharelink(){
  	//   		$URI='http://vdisk.weibo.com/share/hot?cid=1&type=&page=20';
  	//   		$content=file_get_contents($URI);
  	//   		$preg_link='#href=\"http\:\/\/vdisk\.weibo\.com/s/(.*)\"#isU';
  	//   		preg_match_all($preg_link, $content,$link);
  	//   		$arr=array_unique($link[1]);
  	//   		print_r($arr);
  	// //   		dump($link);
  	//   	}	
  	
  	//下面这个方法是备份
  	public function backupFunction(){
  	
//   		//   		$url='http://vdisk.weibo.com/s/tOSyrI3grUg';
//   		//http://vdisk.weibo.com/s/dU4i1QKymMt0
//   		$url='http://vdisk.weibo.com/share/hot?cid=6&right_recd';
  	
//   		$content=file_get_contents($url);
//   		//   		$content=file_get_contents('2.html');
//   		//fileinfo: {"uid":1796469287,"username":"\u5fae\u76d8","filename":"\u9ad8\u7aef\u6280\u80fd\uff1a\u600e\u6837\u533a\u5206\u73b0\u4ee3\u827a\u672f\u4e0e\u5e7c\u513f\u6d82\u9e26\uff01.jpg","weiboid":null,"link":"http:\/\/vdisk.weibo.com\/s\/tOSyrI3grUg","url":"http:\/\/vdisk.weibo.com\/s\/tOSyrI3grUg","copy_ref":"tOSyrI3grUg","vuid":"56531","fid":"34350794644121566736","bytes":"996113"}};
//   		//fileDown.init({"content_id":"21122751","share_id":"64698796","category_id":"0","obj_id":"1113101097","obj_type":"0","type":"image\/jpeg","md5":"878339f223285d208d0addab75e01289","sha1":"185f208c823fc25e3ea7d6f78bd6b9811c9abc36","uid":"56531","share_status":"1","count_browse":"1316","count_download":"65","count_copy":"93","count_like":"2","web_hot":false,"ios_hot":false,"android_hot":false,"path":"\u5fae\u76d8\u7684\u5206\u4eab\/\u9ad8\u7aef\u6280\u80fd\uff1a\u600e\u6837\u533a\u5206\u73b0\u4ee3\u827a\u672f\u4e0e\u5e7c\u513f\u6d82\u9e26\uff01.jpg","category_name":"","rev":null,"name":"\u9ad8\u7aef\u6280\u80fd\uff1a\u600e\u6837\u533a\u5206\u73b0\u4ee3\u827a\u672f\u4e0e\u5e7c\u513f\u6d82\u9e26\uff01.jpg","revision":"1113101097","bytes":"996113","size":"972.77 KB","is_dir":false,"icon":"page_white_picture","is_preview":false,"is_stream":false,"url":"http:\/\/14.17.79.253\/file2.data.weipan.cn\/156531\/185f208c823fc25e3ea7d6f78bd6b9811c9abc36?ssig=ofa8ojUU%2FD&Expires=1422120322&KID=sae,l30zoo1wmz&fn=%E9%AB%98%E7%AB%AF%E6%8A%80%E8%83%BD%EF%BC%9A%E6%80%8E%E6%A0%B7%E5%8C%BA%E5%88%86%E7%8E%B0%E4%BB%A3%E8%89%BA%E6%9C%AF%E4%B8%8E%E5%B9%BC%E5%84%BF%E6%B6%82%E9%B8%A6%EF%BC%81.jpg&skiprd=2&se_ip_debug=125.73.23.252&corp=1","download_list":["http:\/\/sinastorage.com\/file2.data.weipan.cn\/156531\/185f208c823fc25e3ea7d6f78bd6b9811c9abc36?ssig=ofa8ojUU%2FD&Expires=1422120322&KID=sae,l30zoo1wmz&fn=%E9%AB%98%E7%AB%AF%E6%8A%80%E8%83%BD%EF%BC%9A%E6%80%8E%E6%A0%B7%E5%8C%BA%E5%88%86%E7%8E%B0%E4%BB%A3%E8%89%BA%E6%9C%AF%E4%B8%8E%E5%B9%BC%E5%84%BF%E6%B6%82%E9%B8%A6%EF%BC%81.jpg&skiprd=2&se_ip_debug=125.73.23.252&corp=6","http:\/\/file2.data.weipan.cn.wscdns.com\/156531\/185f208c823fc25e3ea7d6f78bd6b9811c9abc36?ssig=ofa8ojUU%2FD&Expires=1422120322&KID=sae,l30zoo1wmz&fn=%E9%AB%98%E7%AB%AF%E6%8A%80%E8%83%BD%EF%BC%9A%E6%80%8E%E6%A0%B7%E5%8C%BA%E5%88%86%E7%8E%B0%E4%BB%A3%E8%89%BA%E6%9C%AF%E4%B8%8E%E5%B9%BC%E5%84%BF%E6%B6%82%E9%B8%A6%EF%BC%81.jpg&skiprd=2&se_ip_debug=125.73.23.252&corp=2"],"copy_ref":"tOSyrI3grUg","long_share_id":"34350796688824314544","long_content_id":"34350794644121566736","mime_type":"image\/jpeg","thumb_exists":true,"link":"http:\/\/vdisk.weibo.com\/s\/tOSyrI3grUg","source_id":null,"root":"basic","share_type":"0","share_auth":"1","title":"\u9ad8\u7aef\u6280\u80fd\uff1a\u600e\u6837\u533a\u5206\u73b0\u4ee3\u827a\u672f\u4e0e\u5e7c\u513f\u6d82\u9e26\uff01.jpg","description":"\u9ad8\u7aef\u6280\u80fd\uff1a\u600e\u6837\u533a\u5206\u73b0\u4ee3\u827a\u672f\u4e0e\u5e7c\u513f\u6d82\u9e26\uff01.jpg","share_time":"Sun, 08 Dec 2013 05:06:45 +0000","nick":"","price":"0","degree":"0","app_key":"1221134","sina_uid":"1796469287","modified":"Sun, 08 Dec 2013 05:06:45 +0000","video_flv_url":"","video_thumbnail":"","audio_mp3_url":"","doc_swf_url":"","doc_thumbnail":"","archive_url":"","thumbnail":"http:\/\/vdisk-thumb-4.wcdn.cn\/maxwidth.513\/download.weipan.cn\/156531\/185f208c823fc25e3ea7d6f78bd6b9811c9abc36?ssig=mqCOTODcJL&Expires=1422120322&KID=sae,l30zoo1wmz","t":"","fid":"34350794644121566736"});
//   		file_put_contents('list.html', $content);
  	
//   		//匹配fileinfo
//   		$preg_fileinfo='#fileinfo: (.*)\};#isU';
//   		preg_match_all($preg_fileinfo, $content,$fileinfo);
//   		dump($fileinfo);
//   		//   		dump(json_decode($fileinfo[1][0]));
  	
//   		//匹配datainfo
//   		$preg_datainfo='#data-info=\'(.*)\'#isU';
//   		preg_match_all($preg_datainfo, $content,$datainfo);
//   		$arr=array_unique($datainfo[1]);
//   		dump($arr);
  	
//   		//匹配filedown
//   		$preg_fileDown='#fileDown.init\((.*)\);#isU';
//   		preg_match_all($preg_fileDown, $content,$fileDown);
//   		dump($fileDown);
  		//   		dump(json_decode($fileDown[1][0]));
  	
  	
  	}
}

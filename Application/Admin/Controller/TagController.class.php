<?php
namespace Admin\Controller;
use Admin\Model\TagModel;
use Admin\Model\CommonModel;
use Think\Page;
class TagController extends BaseController{
  public function index(){
	$model=new TagModel();
	$common=new CommonModel();
	$listRows=105;												//每页记录数
	$totalRows=$common->count('tag');							//总记录
	$pn=I('get.p') ? I('get.p'):1;								//第几页.无页码是默认第一页
	if (!is_numeric($pn)) $this->error('页码参数错误，必须为数字！');	//容错机制
	$data=$model->get_tag($pn,$listRows);
	$page=new Page($totalRows,$listRows);
	$show=$page->show();
	$this->assign('data',$data);
	$this->assign('page',$show);
	$type=$model->get_type(); 
	$this->assign('type',$type);
	$this->assign('type2',$type);
  	$this->assign('title','标签管理');
  	$this->display();
  }
  
  //获取子分类
  public function getsontype(){
  	if (IS_POST) {
  		$tag=new TagModel();
  		$data=$tag->get_type(I('post.pid'));
		$this->ajaxReturn($data);
  	}
  }
  
  //添加标签
  public function addtag(){
  	if (IS_POST) {
  		$tag=new TagModel();
  		$tag->addtag(I('post.pid'), I('post.sonid'), I('post.content')) ==1 ? $this->success('标签添加成功！') : $this->error('标签添加失败，数据库中可能已存在此标签');
  	}
  }
  
  //采集百度风云榜标签
  public function collect_baidu_tag(){
  	if (IS_POST) {
//   		die();
  		$tag=new TagModel();
  		$tag->baidutag(I('post.pid'), I('post.sonid'), I('post.url'))==1 ? $this->success('标签添加成功！') : $this->error('标签添加失败，数据库中可能已存在此标签');
  	}
  }
  
  //删除标签
  public function tag_delete(){
  	$common=new CommonModel();
  	echo $common->delete('tag', I('get.id'))==1 ? 1:0;die();
  }
  
  
}

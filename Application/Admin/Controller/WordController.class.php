<?php
namespace Admin\Controller;
use Admin\Model\WordModel;
use Admin\Model\CommonModel;
use Think\Page;
class WordController extends BaseController{
	public function index(){
		//分页相关开始
		$model=new WordModel();
		$common=new CommonModel();
		$listRows=10;												//每页记录数
		$sort=I('get.sortT') ? I('get.sortT') :'';
		$word=I('get.word') ? I('get.word') : '';
		if ($word) {
			$w['word']=array('like','%'.$word.'%');
			$totalRows=$common->count('word',$w);
		}else {
			$totalRows=$common->count('word');
		}
		$pn=I('get.p') ? I('get.p'):1;								//第几页.无页码是默认第一页
		if (!is_numeric($pn)) $this->error('页码参数错误，必须为数字！');	//容错机制
		$data=$model->get_word($pn,$listRows,$sort,$word);						//按页获取记录
		$page=new Page($totalRows,$listRows);
		$show=$page->show();
// 		print_r($data);die();
		$this->assign('data',$data);
		$this->assign('page',$show);
		$this->assign('title','会员管理');
		$this->display();
	}
	
	public function combine(){
		$word=M('word');
		$php_id=$word->where(array('word'=>'php'))->getField('id_set');
		$mysql_id=$word->where(array('word'=>'mysql'))->getField('id_set');
		$php=explode(',', $php_id);
		$mysql=explode(',', $mysql_id);
		
// 		print_r($php);
		echo '|';
// 		print_r($mysql);
		echo '|';
		echo count($php);
		echo '|';
		echo count($mysql);
		echo '|';
		$arr=array_unique(array_intersect($php, $mysql));
		print_r($arr);
		
	}
	
	public function share(){
		$numPerPage=5000;
		$p=I('get.p') ? I('get.p') : 1;
		$SHARE=M('share');
		$SO=M('so');
		if (!$_COOKIE['share_maxid']) {
			$maxid=$SO->where(array('type'=>'share'))->Max('s_id');
			if (!$maxid) {
				$maxid=0;
			}
			cookie('share_maxid',$maxid);
		}
		$data=$SHARE->where(array('dCnt'=>array('gt',0),'vCnt'=>array('gt',0),'tCnt'=>array('gt',0),'id'=>array('gt',$_COOKIE['share_maxid'])))
				->limit(($p-1)*$numPerPage,$numPerPage)
				->field('id,title,vCnt,dCnt,tCnt,doctype')
				->order('id asc')
				->select();
		if ($data) {
			for ($i=0;$i<count($data);$i++){
				$sodata[$i]['title']=$data[$i]['title'];
				$sodata[$i]['s_id']=$data[$i]['id'];
				$sodata[$i]['weight']=$data[$i]['vCnt']+2*($data[$i]['tCnt']+$data[$i]['dCnt']);
				$sodata[$i]['doctype']=$data[$i]['doctype'] ? $data[$i]['doctype'] : 'folder';
				$sodata[$i]['vCnt']=$data[$i]['vCnt'];
				$sodata[$i]['dCnt']=$data[$i]['dCnt'];
				$sodata[$i]['tCnt']=$data[$i]['tCnt'];
			}
			$SO->addAll($sodata);
			$this->success('成功，正在跳转到'.($p+1).'页',U('Word/share',array('p'=>$p+1)));
		}else{
			$this->success('更新完毕',U('Word/index'));
		}
	}
	
	public function album(){
		$numPerPage=5000;
		$p=I('get.p') ? I('get.p') : 1;
		$SHARE=M('album');
		$SO=M('so');
		if (!$_COOKIE['album_maxid']) {
			$maxid=$SO->where(array('type'=>'album'))->Max('s_id');
			if (!$maxid) {
				$maxid=0;
			}
			cookie('album_maxid',$maxid);
		}
		$data=$SHARE->where(array('dCnt'=>array('gt',0),'vCnt'=>array('gt',0),'tCnt'=>array('gt',0),'id'=>array('gt',$_COOKIE['album_maxid'])))
		->limit(($p-1)*$numPerPage,$numPerPage)
		->field('id,title,vCnt,dCnt,tCnt')
		->order('id asc')
		->select();
		if ($data) {
			for ($i=0;$i<count($data);$i++){
				$sodata[$i]['title']=$data[$i]['title'];
				$sodata[$i]['s_id']=$data[$i]['id'];
				$sodata[$i]['weight']=$data[$i]['vCnt']+2*($data[$i]['tCnt']+$data[$i]['dCnt']);
				$sodata[$i]['type']='album';
				$sodata[$i]['vCnt']=$data[$i]['vCnt'];
				$sodata[$i]['dCnt']=$data[$i]['dCnt'];
				$sodata[$i]['tCnt']=$data[$i]['tCnt'];
			}
			$SO->addAll($sodata);
			$this->success('成功，正在跳转到'.($p+1).'页',U('Word/album',array('p'=>$p+1)));
		}else{
			$this->success('更新完毕',U('Word/index'));
		}
	}
	
	
	public function file(){
		$numPerPage=5000;
		$p=I('get.p') ? I('get.p') : 1;
		$SHARE=M('album_source');
		$SO=M('so');
		if (!$_COOKIE['file_maxid']) {
			$maxid=$SO->where(array('type'=>'file'))->Max('s_id');
			if (!$maxid) {
				$maxid=0;
			}
			cookie('file_maxid',$maxid);
		}
		$data=$SHARE->where(array('dCnt'=>array('gt',0),'vCnt'=>array('gt',0),'tCnt'=>array('gt',0),'id'=>array('gt',$_COOKIE['file_maxid'])))
		->limit(($p-1)*$numPerPage,$numPerPage)
		->field('id,server_filename,vCnt,dCnt,tCnt,doctype')
		->order('id asc')
		->select();
		if ($data) {
			for ($i=0;$i<count($data);$i++){
				$sodata[$i]['title']=$data[$i]['server_filename'];
				$sodata[$i]['s_id']=$data[$i]['id'];
				$sodata[$i]['weight']=$data[$i]['vCnt']+2*($data[$i]['tCnt']+$data[$i]['dCnt']);
				$sodata[$i]['type']='file';
				$sodata[$i]['vCnt']=$data[$i]['vCnt'];
				$sodata[$i]['doctype']=$data[$i]['doctype'];
				$sodata[$i]['dCnt']=$data[$i]['dCnt'];
				$sodata[$i]['tCnt']=$data[$i]['tCnt'];
			}
			$SO->addAll($sodata);
			$this->success('成功，正在跳转到'.($p+1).'页',U('Word/file',array('p'=>$p+1)));
		}else{
			$this->success('更新完毕',U('Word/index'));
		}
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
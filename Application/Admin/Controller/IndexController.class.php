<?php
namespace Admin\Controller;
class IndexController extends BaseController{
  public function index(){
  	$this->assign('title','后台首页');
  	$this->assign('count_user',M('user')->count());
  	$this->assign('count_source',M('share1')->count());
  	$this->assign('left_source',M('user')->where(array('status'=>0))->sum('share'));
  	$this->assign('count_news',M('news')->count());
  	$this->assign('count_album',M('album')->count());
  	$this->assign('left_album',M('user')->where(array('status'=>0))->sum('album'));
  	$this->assign('count_tag',M('tag')->count());
  	$this->assign('count_report',M('report')->count());
  	$this->assign('count_ad',M('ad')->count());
  	$this->display();
  }
}

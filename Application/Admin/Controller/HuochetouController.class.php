<?php
namespace Admin\Controller;
use Admin\Model\ShareModel;
class HuochetouController extends BaseController{
  
  public function index(){
  	$Model = M('content_270','data_','mysql://root:@localhost:3306/huochetou');
  	$Share=new ShareModel();
  	$User=M('user');
  	$data=$Model->where(array('status'=>0))->order('Id asc')->find();
  	if (!$data) {
  		$this->success('处理完成，跳转处理status=2的数据',U('Huochetou/do_status_equal_2',array('t'=>time())));
  		exit();
  	}
  	$json=$data['json'];
  	$obj=json_decode($json);
  	echo $data['Id'].'<br/>';
  	$uk=$obj->records[0]->uk;
  	$rs=$Share->get_json($json);
//   	echo '<br/>====================<br/>'.$json;
  	if (strlen($json)>20 && count($obj)<1) {
  		$Model->where(array('Id'=>$data['Id']))->save(array('status'=>2));
  		echo '2<br/>';
  	}else{
  		$Model->where(array('Id'=>$data['Id']))->save(array('status'=>1));
  		echo '1<br/>';
  	}
  	$User->where(array('uk'=>$uk))->save(array('status'=>1));
  	$this->success('数据录入成功，继续',U('Huochetou/index',array('t'=>time())),1);
  }
  
//   public function recovery(){
//   	$Model = M('content_270','data_','mysql://root:@localhost:3306/huochetou');
//   	$Share=new ShareModel();
//   	$User=M('user');
//   	$data=$Model->where(array('status'=>1))->order('Id asc')->find();
//   	$url=$data['PageUrl'];
//   	$pas=parse_url($url);
//   	parse_str($pas['query']);
//   	echo $data['Id'].'<br/>';
//   	echo $uk=$query_uk;;
//   	$User->where(array('uk'=>$uk))->save(array('status'=>0));
//   	$Model->where(array('Id'=>$data['Id']))->save(array('status'=>0));
//   	$this->success('数据录入成功，继续',U('Huochetou/index',array('t'=>time())),1);
//   }
  
  
  public function desc(){
  	$Model = M('content_270','data_','mysql://root:@localhost:3306/huochetou');
  	$Share=new ShareModel();
  	$User=M('user');
  	$data=$Model->where(array('status'=>0))->order('Id desc')->find();
  	if (!$data) {
  		echo '<script type="text/javascript">window.close();</script>';
  		exit();
  	}
  	echo $data['Id'].'<br/>';
  	$json=$data['json'];
  	$obj=json_decode($json);
  	$uk=$obj->records[0]->uk;
  	$rs=$Share->get_json($json);
  	
  	if (strlen($json)>20 && count($obj)<1) {
  		$Model->where(array('Id'=>$data['Id']))->save(array('status'=>2));
  		echo '2<br/>';
  	}else{
  		$Model->where(array('Id'=>$data['Id']))->save(array('status'=>1));
  		echo '1<br/>';
  	}
  	$User->where(array('uk'=>$uk))->save(array('status'=>1));
  	$this->success('数据录入成功，继续',U('Huochetou/desc',array('t'=>time())),1);
  }
  
  public function do_status_equal_2(){
  	$Model = M('content_270','data_','mysql://root:@localhost:3306/huochetou');
  	$Share=new ShareModel();
  	$User=M('user');
  	$data=$Model->where(array('status'=>2))->order('Id desc')->find();
  	$url=$data['PageUrl'];
  	$json=file_get_contents($url);
  	$obj=json_decode($json);
  	$uk=$obj->records[0]->uk;
  	
  	$rs=$Share->get_json($json);
  	 
  	if (strlen($json)>20 && count($obj)<1) {
  		$Model->where(array('Id'=>$data['Id']))->save(array('status'=>2));
  		echo '2<br/>';
  	}else{
  		$Model->where(array('Id'=>$data['Id']))->save(array('status'=>1));
  		echo '1<br/>';
  	}
  	$User->where(array('uk'=>$uk))->save(array('status'=>1));
  	$this->success('数据录入成功，继续',U('Huochetou/do_status_equal_2',array('t'=>time())),1);
  }
  
  
  public function multiple(){
  	$Model = M('content_270','data_','mysql://root:@localhost:3306/huochetou');
  	$Share=new ShareModel();
  	$User=M('user');
  	$rs=$Model->where(array('status'=>0))->limit(4)->order('Id asc')->select();
  	if (!$rs) $this->success('处理完成，跳转处理status=2的数据',U('Huochetou/do_status_equal_2',array('t'=>time())));
  	foreach ($rs as $data){
  		$json=$data['json'];
  		$obj=json_decode($json);
  		$uk=$obj->records[0]->uk;
  		$rs=$Share->get_json($json);
  		if (strlen($json)>20 && count($obj)<1) {
  			$Model->where(array('Id'=>$data['Id']))->save(array('status'=>2));
  			echo '2<br/>';
  		}else{
  			$Model->where(array('Id'=>$data['Id']))->save(array('status'=>1));
  			echo '1<br/>';
  		}
  		echo $data['Id'].'<br/>';
  		$User->where(array('uk'=>$uk))->save(array('status'=>1));
  	}
  	$this->success('数据录入成功，继续',U('Huochetou/multiple',array('t'=>time())),1);
  }
  
}

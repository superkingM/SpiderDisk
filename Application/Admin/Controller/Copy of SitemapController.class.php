<?php
namespace Admin\Controller;
class SitemapController extends BaseController{
  
	//生成sitemap
	public function index() {
		$page_size = 10000; //每页条数
		cookie('page_size',$page_size);
		$album=M('album');
		$album_file=M('album_source');
		$share=M('share');
		$share1=M('share1');
		//1w个地址生成一个子地图，判断需要生成几个？
		$album_rows = $album->count();
		$album_count = ceil($album_rows/$page_size);
		cookie('album_count',$album_count);
		
		$album_file_rows = $album_file->count();
		$album_file_count = ceil($album_file_rows/$page_size);
		cookie('album_file_count',$album_file_count);
		
		
		$share_rows = $share->count();
		$share_count = ceil($share_rows/$page_size);
		cookie('share_count',$share_count);
		
		$share1_rows = $share1->count();
		$share1_count = ceil($share1_rows/$page_size);
		cookie('share1_count',$share1_count);
		
		$this->create_index($album_count,$album_file_count,$share_count,$share1_count);    			//生成主sitemap index
		$this->album_sitemap($album_count, $page_size);
	}
	 
	//生成主sitemap
	protected function create_index($album_count,$album_file_count,$share_count,$share1_count) {
		set_time_limit(60000);
		$content= "<?xml version=\"1.0\" encoding=\"utf-8\"?>\r\n<sitemapindex>";
		for($i=1;$i<=$album_count;$i++) {
			$content.="\r\n<sitemap>\r\n\t<loc>http://baiduyun.57fx.cn/Sitemap/sitemap_album".$i.".xml</loc>\r\n\t<lastmod>".date('Y-m-d')."</lastmod>\r\n</sitemap>";
		}
		for($j=1;$j<=$album_file_count;$j++) {
			$content.="\r\n<sitemap>\r\n\t<loc>http://baiduyun.57fx.cn/Sitemap/sitemap_file".$j.".xml</loc>\r\n\t<lastmod>".date('Y-m-d')."</lastmod>\r\n</sitemap>";
		}
		for($k=1;$k<=$share_count;$k++) {
			$content.="\r\n<sitemap>\r\n\t<loc>http://baiduyun.57fx.cn/Sitemap/sitemap_share".$k.".xml</loc>\r\n\t<lastmod>".date('Y-m-d')."</lastmod>\r\n</sitemap>";
		}
		for($k=$share_count+1;$k<=$share_count+$share1_count;$k++) {
			$content.="\r\n<sitemap>\r\n\t<loc>http://baiduyun.57fx.cn/Sitemap/sitemap_share".$k.".xml</loc>\r\n\t<lastmod>".date('Y-m-d')."</lastmod>\r\n</sitemap>";
		}
		$content .= "\r\n</sitemapindex>";
		file_put_contents('sitemap.xml', $content);
	}
	
	
	//专辑站点地图
	public function album_sitemap($album_count,$page_size,$current_page=0) {
		$album_count=I('get.album_count') ? I('get.album_count') : $album_count;
		$page_size=I('get.page_size') ? I('get.page_size') : $page_size;
		$current_page=I('get.current_page') ? I('get.current_page') : $current_page;
		if ($current_page<$album_count) {
			$list = M('album')->field('id,time')->order('id asc')->limit($current_page*$page_size.','.$page_size)->select();
			$sitemap = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\r\n<urlset xmlns=\"http://www.sitemaps.org/schemas/sitemap/0.9\">\r\n";
			foreach($list as $k=>$v){
				$sitemap .= "<url>\r\n\t"."<loc>http://baiduyun.57fx.cn/album-detail-id-".$v['id'].".html</loc>\r\n\t"."<priority>0.9</priority>\r\n\t<lastmod>".date('Y-m-d',$v['time'])."</lastmod>\r\n\t<changefreq>weekly</changefreq>\r\n</url>\r\n";
			}
			$sitemap .= '</urlset>';
			file_put_contents(SITEMAP.'sitemap_album'.($current_page+1).'.xml', $sitemap);
			$this->success('正在跳转继续生成专辑站点地图',U('Sitemap/album_sitemap',array('album_count'=>$album_count,'page_size'=>$page_size,'current_page'=>$current_page+1)),3);
		}else{
			$this->success('正在跳转继续生成专辑文件站点地图',U('Sitemap/file_sitemap',array('file_count'=>cookie('album_file_count'),'page_size'=>cookie('page_size'),0)),3);
		}
	}
	
	//专辑文件站点地图
	public function file_sitemap($file_count,$page_size,$current_page=0) {
		$file_count=I('get.file_count') ? I('get.file_count') : $file_count;
		$page_size=I('get.page_size') ? I('get.page_size') : $page_size;
		$current_page=I('get.current_page') ? I('get.current_page') : $current_page;
		if ($current_page<$file_count) {
			$list = M('album_source')->field('id,time')->order('id asc')->limit($current_page*$page_size.','.$page_size)->select();
			$sitemap = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\r\n<urlset xmlns=\"http://www.sitemaps.org/schemas/sitemap/0.9\">\r\n";
			foreach($list as $k=>$v){
				$sitemap .= "<url>\r\n\t"."<loc>http://baiduyun.57fx.cn/album-file-id-".$v['id'].".html</loc>\r\n\t"."<priority>0.9</priority>\r\n\t<lastmod>".date('Y-m-d',$v['time'])."</lastmod>\r\n\t<changefreq>weekly</changefreq>\r\n</url>\r\n";
			}
			$sitemap .= '</urlset>';
			file_put_contents(SITEMAP.'sitemap_file'.($current_page+1).'.xml', $sitemap);
			$this->success('正在跳转继续生成专辑站点地图',U('Sitemap/file_sitemap',array('file_count'=>$file_count,'page_size'=>$page_size,'current_page'=>$current_page+1)),3);
		}else{
			$this->success('正在跳转继续生成专辑文件站点地图',U('Sitemap/share_sitemap',array('share_count'=>cookie('share_count'),'page_size'=>cookie('page_size'),0)),3);
		}
	}
	
	
	//分享资源站点地图
	public function share_sitemap($share_count,$page_size,$current_page=0) {
		$share_count=I('get.share_count') ? I('get.share_count') : $share_count;
		$page_size=I('get.page_size') ? I('get.page_size') : $page_size;
		$current_page=I('get.current_page') ? I('get.current_page') : $current_page;
		if ($current_page<$share_count) {
			$list = M('share')->field('id,time')->order('id asc')->limit($current_page*$page_size.','.$page_size)->select();
			$sitemap = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\r\n<urlset xmlns=\"http://www.sitemaps.org/schemas/sitemap/0.9\">\r\n";
			foreach($list as $k=>$v){
				$sitemap .= "<url>\r\n\t"."<loc>http://baiduyun.57fx.cn/detail-index-id-".$v['id'].".html</loc>\r\n\t"."<priority>0.9</priority>\r\n\t<lastmod>".date('Y-m-d',$v['time'])."</lastmod>\r\n\t<changefreq>weekly</changefreq>\r\n</url>\r\n";
			}
			$sitemap .= '</urlset>';
			file_put_contents(SITEMAP.'sitemap_share'.($current_page+1).'.xml', $sitemap);
			$this->success('正在跳转继续生成分享资源站点地图',U('Sitemap/share_sitemap',array('share_count'=>$share_count,'page_size'=>$page_size,'current_page'=>$current_page+1)),3);
		}else{
			$this->success('正在跳转继续生成分享资源分表的站点地图',U('Sitemap/share1_sitemap',array('share_count'=>cookie('share1_count'),'page_size'=>cookie('page_size'),0)),3);
		}
	}
	
	//分享资源站点地图
	public function share1_sitemap($share_count,$page_size,$current_page=0) {
		$share_count=I('get.share_count');
		$page_size=I('get.page_size');
		$current_page=I('get.current_page') ? I('get.current_page') : $current_page;
		$original_share_count=cookie('share_count');
		if ($current_page<$share_count) {
			$list = M('share1')->field('id,time')->order('id asc')->limit($current_page*$page_size.','.$page_size)->select();
			$sitemap = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\r\n<urlset xmlns=\"http://www.sitemaps.org/schemas/sitemap/0.9\">\r\n";
			foreach($list as $k=>$v){
				$sitemap .= "<url>\r\n\t"."<loc>http://baiduyun.57fx.cn/detail-index-id-".$v['id'].".html</loc>\r\n\t"."<priority>0.9</priority>\r\n\t<lastmod>".date('Y-m-d',$v['time'])."</lastmod>\r\n\t<changefreq>weekly</changefreq>\r\n</url>\r\n";
			}
			$sitemap .= '</urlset>';
			file_put_contents(SITEMAP.'sitemap_share'.($current_page+1+$original_share_count).'.xml', $sitemap);
			$this->success('正在跳转继续生成分享资源站点地图',U('Sitemap/share1_sitemap',array('share_count'=>$share_count,'page_size'=>$page_size,'current_page'=>$current_page+1)),3);
		}else{
			$this->success('所有站点地图生成成功！',U('Sys/index'));
		}
	}
	
	
  
  
}

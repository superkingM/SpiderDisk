<?php
namespace Admin\Model;
use Think\Model;
class DiscuzModel extends Model{
	private $fid = null;//版块
	private $title = null;//标题
	private $content = null;//内容
	private $author = null;//作者
	private $author_id = null;//作者ID,即UID
	private $current_time = null;//当前时间，即发帖时间
	private $displayorder = null; //0:正常，-2:待审核
	private $typeid = 0; //主题分类id
	
	/* 
	 * 提示：记得添加上主题分类id，即typeid
	 * 
	 *  
	 *  */
	
	public function __construct($fid, $title, $content, $author, $author_id,$typeid=0,$displayorder = 0){
		$this->fid          = $fid;
		$this->title        = $title;
		$this->content      = $content;
		$this->author       = $author;
		$this->author_id    = $author_id;
		$this->current_time = time();
		$this->displayorder = $displayorder;
		$this->typeid		=$typeid;
	}
	
	//发帖方法,发帖调用此方法即可
	public function post_posts(){
		//发表主题,获取tid
		$tid=$this->do_pre_forum_thread();
		//获取自增pid
		$pid=$this->do_pre_forum_post_tableid();
		//将帖子内容写入帖子表
		$this->do_pre_forum_post($pid, $tid);
		//更新版块帖子数量
		$this->do_pre_forum_forum();
		//更新会员帖子数量
		$this->do_pre_common_member_count();
	}
	
	
	
	
	//操作主题表pre_forum_thread，返回tid
	public function do_pre_forum_thread(){
		$data                 = array();
		$data['fid']          = $this->fid;
		$data['author']       = $this->author;
		$data['authorid']     = $this->author_id;
		$data['subject']      = $this->title;
		$data['dateline']     = $this->current_time;
		$data['lastpost']     = $this->current_time;
		$data['lastposter']   = $this->author;
		$data['displayorder'] = $this->displayorder;
		$data['typeid']		  = $this->typeid;
		$thread = M('forum_thread','pre_',C('DB_CONFIG2'));
		$thread->add($data);
		
// 		$sql=$thread->getLastSql();
// 		file_put_contents('DiscuzModel'.time().'.sql', $sql);
		
		if(mysql_affected_rows()) return $tid=$thread->getLastInsID();
	}
	
	//操作分表协调表 pre_forum_post_tableid，返回pid
	public function do_pre_forum_post_tableid(){
		$tableid = M('forum_post_tableid','pre_',C('DB_CONFIG2'));
		$tableid->add(array('pid'=>NULL));
		if(mysql_affected_rows()) return $pid=$tableid->getLastInsID();
	}
	
	//发帖
	private function do_pre_forum_post($pid, $tid){
		$data             = array();
		$data['pid']      = $pid;
		$data['fid']      = $this->fid;
		$data['tid']      = $tid;
		$data['first']    = 0;//是否是首页
		$data['author']   = $this->author;
		$data['authorid'] = $this->author_id;
		$data['subject']  = $this->title;
		$data['dateline'] = $this->current_time;
		$data['message']  = $this->content;
		$forum_post = M('forum_post','pre_',C('DB_CONFIG2'));
		$result=$forum_post->add($data);
		return ($result == 1) ? true : false;
	}
	
	//更新版块帖子数量
	private function do_pre_forum_forum(){
		$forum = M('forum_forum','pre_',C('DB_CONFIG2'));
// 		$forum->where(array('fid'=>$this->fid))->setInc('threads');
// 		$forum->where(array('fid'=>$this->fid))->setInc('threads');
// 		$forum->where(array('fid'=>$this->fid))->setInc('threads');
		$sql = "UPDATE `pre_forum_forum` SET `threads`=threads+1,`posts`=posts+1,`todayposts`=todayposts+1 WHERE `fid`={$this->fid}";
		$result=$forum->query($sql);
		return ($result == 1) ? true : false;
	}
	
	//更新用户帖子数量
	private function do_pre_common_member_count(){
		$sql = "UPDATE `pre_common_member_count` SET `threads`=threads+1 WHERE `uid`={$this->author_id}";
		$member_count = M('common_member_count','pre_',C('DB_CONFIG2'));
		$result = $member_count->query($sql);
		return ($result == 1) ? true : false;
	}
	
}
?>
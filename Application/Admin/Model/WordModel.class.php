<?php
namespace Admin\Model;
use Think\Model;
class WordModel extends Model{
	public function get_word($page=1,$listRows=20,$sort='',$word=''){
		switch ($sort) {
			case 'times':
				$sort='times desc';
				break;
			case 'rows':
				$sort='rows desc';
				break;
			case 'new':
				$sort='id desc';
				break;
			default:
				$sort='';
				break;
		}
		if ($word) {
			return $this->page($page.','.$listRows)->where(array('word'=>array('like','%'.$word.'%')))->select();
		}else{
			return $this->page($page.','.$listRows)->order($sort)->select();
		}
	}
	
	
	public function updateRows($id){
		$word=$this->where(array('id'=>$id))->getField('word');
		$kw=explode(' ', $word);
		for ($i=0;$i<count($kw);$i++){
			$kw[$i]='%'.$kw[$i].'%';
		}
		
		$data['rows']=M('share')->where(array('title'=>array('like',$kw,'and')))->count();
		$data['id']=$id;
		$data['updatetime']=time();
		$this->save($data);
		return mysql_affected_rows();
// 		M('share')->
	}
	
	
}
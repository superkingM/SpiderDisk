<?php
namespace Admin\Model;
use Think\Model;
class AdminModel extends Model{
	protected $_validate=array(
			array('name','require','用户名不得为空'),
			array('password' ,'6,20',' 密码不得小于6位，不得大于20位',3,'length' ),
			array('phone' , 'email',' 手机号码不正确！ ' ),
	);
	protected $_auto=array(
			array('password','md5',3,'function'),//密码自动加密
	);
	public function admin_login(){
		$data=$this->create($_POST);
		if($data){
			$is_admin=$this->where($data)->field('id,name,phone')->find();
			if(is_array($is_admin)){
				session('admin','admin');
				session('adm_arr',$is_admin);
				return 1;
			}else {
				return 0;
			}
		}else {
			$this->getError();
			return 0;
		}
	}
	
	public function admin_is_login(){
		session('admin')=='admin' ? redirect(U('Index/index')) : '';
	}
	
	public function updateAdmin(){
		$data['phone']=I('post.phone');
		$data['name']=I('post.admin');
		$data['id']=$_SESSION['adm_arr']['id'];
		$opwd=I('post.opwd');
		$npwd=I('post.npwd');
		if ((empty($opwd) && !empty($npwd)) || (!empty($opwd) && empty($npwd)) || ($opwd==$npwd)) return -2;
		if (!empty($opwd) && !empty($npwd) && $npwd!=$opwd) {
			$is=$this->where(array('id'=>$data['id'],'password'=>md5($opwd)))->find();
			if ($is) {
				$data['password']=md5($npwd);
			}else{
				return -1;
			}
		}
		$this->save($data);
		return mysql_affected_rows();
		
	}
	

	
	
}
<?php
namespace Admin\Model;
use Think\Model;
class NewsModel extends Model{
	//获取新闻资讯分类
	public function get_type($isDisplayCount=true){
		$type=M('news_type');
		if ($isDisplayCount) {
			$data=$type->select();
			foreach ($data as &$v){
				$rs=$this->field("count('id') as count")->where(array('tid'=>$v['id']))->find();
				$v['count']=$rs['count'];
			}
		}else {
			$data=$type->select();
		}
// 		echo $type->getLastSql();print_r($data);die();
		return $data;
	}	
	
	//添加新闻资讯分类
	public function add_type($str){
		$str=trim($str,',');
		$arr=explode(',', $str);
		foreach ($arr as $v){
			$data[]=array(
					'name'=>$v
			);
		}
// 		print_r($data);die();
		if (is_array($data)) {
			M('news_type')->addAll($data);
			return mysql_affected_rows()>0 ? 1:0;
		}else{
			return 0;
		}
		
	}
	
	//添加新闻资讯
	public function addNews(){
		$this->create();
		$this->time=time();
		$this->click=rand(100, 1000);
		$this->add();
		return mysql_affected_rows()==1 ? 1 : 0;
	}
	
	//修改资讯
	public function edit(){
		$this->create();
// 		print_r($data);die();
		$this->save();
		return mysql_affected_rows()==1 ? 1:0;
	}
	
	//获取全部新闻资讯信息
	public function get_news($page=1,$listRows=20){
		return $this->alias('n')->page($page.','.$listRows)->order('n.id desc')
				->join("__NEWS_TYPE__ t on t.id=n.tid",'left')
				->field('t.name type,n.id nid,n.title,n.tags,n.desc,n.time,n.click')->select();
	}
	
	public function getNewsContent($id){
		return $this->alias('n')
					->join('__NEWS_TYPE__ t on t.id=n.tid','left')
					->where(array('n.id'=>$id))
					->field('t.name,t.id tid,n.*')->find();
	}
	
	
	
}
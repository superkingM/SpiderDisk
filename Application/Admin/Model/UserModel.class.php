<?php
namespace Admin\Model;
use Think\Model;
use Think\Snoopy;
class UserModel extends Model{
	
	//获取全部会员信息
	public function get_users($page=1,$listRows=20,$sort='status asc,share desc,time desc',$user=''){
		switch ($sort) {
			case 'new':
				$sort='time desc';
				break;
			case 'share':
				$sort='share desc';
				break;
			case 'album':
				$sort='status asc,album desc';
				break;
			case 'follow':
				$sort='follow desc';
				break;
			case 'fans':
				$sort='fans desc';
				break;
			default:
				$sort='status asc,share desc,time desc';
				break;
		}
		if ($user) {
			return $this->page($page.','.$listRows)->where(array('status'=>0,'name'=>array('like','%'.$user.'%')))->order($sort)->select();
		}else{
			return $this->page($page.','.$listRows)->order($sort)->where(array('status'=>0))->select();
		}
	}
	
	
	//收集会员
	public function collect_uk($content){
		$content=str_replace('&amp;', '&', $content);
		//匹配会员UK
		$preg_uk="/uk=(\d+)/";
		preg_match_all($preg_uk, $content,$arr_uk);//会员名称
// 		print_r($arr_uk);die();
		return $arr_uk[1];

	}
	
	//根据UK检测会员是否已存在
	public function check_uk($uk){
		$rs=$this->where(array('uk'=>$uk))->find();
		return is_array($rs) ? 1:0;//存在，返回1，否则返回0
	}
	
	//采集会员信息
	public function get_user_info($uk){
		$url='http://yun.baidu.com/pcloud/user/getinfo?bdstoken=c4150076d53c135f7eafaf72ea2ca8da&query_uk='.$uk.'&t=1414592426984&channel=chunlei&clienttype=0&web=1';
		// 1. 初始化
		$ch = curl_init();
		//
		// 2. 设置选项，包括URL
		curl_setopt($ch, CURLOPT_URL,$url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
// 		curl_setopt($ch, CURLOPT_HTTPHEADER, array('X-FORWARDED-FOR:'.get_client_ip(0), 'CLIENT-IP:'.get_client_ip(0)));//IP
		curl_setopt($ch, CURLOPT_REFERER, "http://yun.baidu.com/share/home?uk=".$uk."&view=share");   //来路
		curl_setopt($ch, CURLOPT_HEADER, 0);
		// 3. 执行并获取HTML文档内容
		$content = curl_exec($ch);
		// 4. 释放curl句柄
		curl_close($ch);
		
		return json_decode($content);
		
	}
	
	
	//批量添加
	public function addAllUserInfo($arr){
// 		print_r($arr);die();
		foreach ($arr as $data){
			$add_data[]=array(
				'name'=>$data->uname,
				'avatar'=>$data->avatar_url,
				'fans'=>$data->fans_count,
				'share'=>$data->pubshare_count,
				'uk'=>$data->uk,
				'album'=>$data->album_count,
				'follow'=>$data->follow_count,
				'show'=>$data->intro,
				'name'=>$data->uname,
				'time'=>time(),
				'status'=>0,
			);
		}
		$this->addAll($add_data);
		return mysql_affected_rows()>0 ? 1:0;
// 		print_r($add_data);die();
	}
	
	//匹配头部需采集的会员信息
	public function preg_header($content){
		$content=html_entity_decode($content);
		$content=str_replace("&quot;", "\"", $content);
// 		dump($content);die();
		$preg_name="#class=\"homepagelink\">(.*)</span>#isU";//会员名称
		$preg_avatar="#http\:\/\/himg\.bdimg\.com\/sys\/portrait\/item\/(.*)\.jpg#isU";//会员头像
		$preg_uk="#\"uk\":\"(.*)\"#isU";//会员uk
		$preg_share="#<em id=\"sharecnt_cnt\" class=\"publiccnt\">(.*)</em>#isU";//分享文件数量
		$preg_album="#<em id=\"albumcnt_cnt\" class=\"albumcnt\">(.*)</em>#isU";//专辑数量
		$preg_follow="#<em class=\"concerncnt\">(.*)</em>订阅#isU";//订阅
		$preg_fans="#<em class=\"fanscnt\">(.*)</em>#isU";//粉丝
		$preg_show="#<p class=\"personal-info\" title=\"(.*)\">(.*)</p>#isU";//个人说明
	
		preg_match_all($preg_name, $content,$arr_name);//会员名称
		preg_match_all($preg_avatar, $content,$arr_avatar);//会员头像
		preg_match_all($preg_uk, $content,$arr_uk);//会员UK
		preg_match_all($preg_share, $content,$arr_share);//分享
		preg_match_all($preg_album, $content,$arr_album);//专辑
		preg_match_all($preg_follow, $content,$arr_follow);//订阅
		preg_match_all($preg_fans, $content,$arr_fans);//粉丝
		preg_match_all($preg_show, $content,$arr_show);//个人说明
	
		$avatar=$arr_avatar[0][0];
		
		foreach ($arr_name[1] as $k=>$v){
			if ($this->check_uk($arr_uk[1][$k])==0){
				$data[]=array(
						'name'=>$v,
						'avatar'=>$avatar,
						'uk'=>$arr_uk[1][$k],
						'share'=>$arr_share[1][$k],
						'album'=>$arr_album[1][$k],
						'follow'=>$arr_follow[1][$k],
						'fans'=>$arr_fans[1][$k],
						'show'=>$arr_show[1][$k],
						'time'=>time(),
						'status'=>0,
				);
			}
		}
// 		print_r($data);die();
		if (is_array($data)) {
			$this->addAll($data);
			return  mysql_affected_rows()>0 ? 1:0;
		}else{
			return 0;
		}
	
	}
	
	//匹配头部需采集的会员信息
	public function preg_wapFollows($uk,$start,$fans=null){
		$snoopy=new Snoopy();
		if ($fans) {
			$url='http://pan.baidu.com/wap/share/home/fans?uk='.$uk.'&third=0&start='.$start;
		}else{
			$url='http://pan.baidu.com/wap/share/home/followers?uk='.$uk.'&third=0&start='.$start;
		}
		echo $url.'<br/>';
// 		$url='http://pan.baidu.com/wap/share/home/followers?uk=4096827176&third=0';
		$snoopy->fetchlinks($url);
		$rs=$snoopy->results;
		$str=implode('', $rs);
		$pattern="#uk\=([0-9]+)\&#isU";
		preg_match_all($pattern, $str,$arr);
		$model=M('wap');
		for ($i=0;$i<count($arr[1]);$i++){
			$data[]['uk']=$arr[1][$i];
		}
		echo '<br/><script type="text/javascript" src="http://libs.baidu.com/jquery/2.0.0/jquery.min.js"></script><script type="text/javascript">$(function(){setTimeout(function(){location.reload();},20000);});</script><a href="#">goods【20秒自动刷新】</a><br/>';
		//注意，这里需要将Model.class.php的addAll方法和mysql.class.php的insertAll方法修改下
		print_r($data);
		$model->addAll($data,'',false,true);
		return true;
	}
	
	
	//设置会员推荐状态
	public function set_recommend($uk,$recommend){
		$this->where(array('uk'=>$uk))->save(array('recommend'=>$recommend));
		return mysql_affected_rows()==1 ? 1:0;
	}
	
	//更新会员信息
	public function update($uk){
		$json_data=$this->get_user_info($uk);
		if ($json_data->errno==0) {
			$save_data=array(
				'name'=>$json_data->user_info->uname,
				'avatar'=>$json_data->user_info->avatar_url,
				'share'=>$json_data->user_info->pubshare_count,
				'album'=>$json_data->user_info->album_count,
				'follow'=>$json_data->user_info->follow_count,
				'fans'=>$json_data->user_info->fans_count,
				'show'=>$json_data->user_info->intro,
			);
			$this->where(array('uk'=>$uk))->save($save_data);
			return mysql_affected_rows()==1 ? 1:0;
		}else{
			return -1;
		}
	}
	
	public function getFollow($uk,$start,$limit=24){
		$url='http://yun.baidu.com/pcloud/friend/getfollowlist?query_uk='.$uk.'&limit='.$limit.'&start='.$start.'&bdstoken=3afc5bc1d59fe96ef24dfceb33fe70d2&channel=chunlei&clienttype=0&web=1';
// 		// 1. 初始化
// 		$ch = curl_init();
// 		// 2. 设置选项，包括URL
// 		curl_setopt($ch, CURLOPT_URL,$url);
// 		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
// 		curl_setopt($ch, CURLOPT_HTTPHEADER, array('X-FORWARDED-FOR:'.get_client_ip(0), 'CLIENT-IP:'.get_client_ip(0)));//IP
// 		curl_setopt($ch, CURLOPT_REFERER, "http://yun.baidu.com/pcloud/friendpage?type=follow&uk=".$uk."&self=0");   //来路
// 		curl_setopt($ch, CURLOPT_HEADER, 0);
// 		// 3. 执行并获取HTML文档内容
// 		$content = curl_exec($ch);
// 		// 4. 释放curl句柄
// 		curl_close($ch);
		
		$referer='http://yun.baidu.com/pcloud/friendpage?type=follow&uk='.$uk.'&self=0';
		$content=crawl($url,$referer);
		
		echo date('Y-m-d H:i:s').'<br/><script type="text/javascript" src="http://libs.baidu.com/jquery/2.0.0/jquery.min.js"></script><script type="text/javascript">$(function(){setTimeout(function(){location.reload();},15000);});</script><a href="#">goods</a>';
		dump($content->follow_list);
		print_r($content);
		if ($content->errno==-55) return -55;
		return $this->get_user_json($content);

		//以下是snoopy方式
// 		return $this->get_user_json($this->snoopy_follows($uk, $start, $limit));
	}
	
	public function getFollowMuti($connomains,$uk){
		//初始化curl多线程
		$mh = curl_multi_init();
		foreach ($connomains as $i => $url) {
			$conn[$i]=curl_init($url);
			curl_setopt($conn[$i],CURLOPT_RETURNTRANSFER,1);
			curl_setopt($conn[$i], CURLOPT_URL,$url);
			curl_setopt($conn[$i], CURLOPT_HTTPHEADER, array('X-FORWARDED-FOR:'.get_client_ip(0), 'CLIENT-IP:'.get_client_ip(0)));//IP
			curl_setopt($conn[$i], CURLOPT_REFERER, "http://yun.baidu.com/pcloud/friendpage?type=follow&uk=".$uk."&self=0");   //来路
			curl_setopt($conn[$i], CURLOPT_HEADER, 0);
			curl_multi_add_handle ($mh,$conn[$i]);
		}
		do { $n=curl_multi_exec($mh,$active); } while ($active);
		foreach ($connomains as $i => $url) {
			$res[$i]=curl_multi_getcontent($conn[$i]);
			$this->get_user_json($res[$i]);
			curl_close($conn[$i]);
		}
		print_r($res);
		return true;
	}
	
	
	
	public function getFans($uk,$start,$limit=24){
// 		$url='http://yun.baidu.com/pcloud/friend/getfanslist?query_uk='.$uk.'&limit='.$limit.'&start='.$start.'&bdstoken=3afc5bc1d59fe96ef24dfceb33fe70d2&channel=chunlei&clienttype=0&web=1';
// 		// 1. 初始化
// 		$ch = curl_init();
// 		// 2. 设置选项，包括URL
// 		curl_setopt($ch, CURLOPT_URL,$url);
// 		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
// // 		curl_setopt($ch, CURLOPT_HTTPHEADER, array('X-FORWARDED-FOR:'.get_client_ip(0), 'CLIENT-IP:'.get_client_ip(0)));//IP
// 		curl_setopt($ch, CURLOPT_REFERER, "http://yun.baidu.com/pcloud/friendpage?type=fans&uk=".$uk."&self=0");   //来路
// 		curl_setopt($ch, CURLOPT_HEADER, 0);
// 		// 3. 执行并获取HTML文档内容
// 		$content = curl_exec($ch);
// 		// 4. 释放curl句柄
// 		curl_close($ch);
// 		sleep(rand(1, 3));
		return $this->get_user_json($this->snoopy_fans($uk, $start, $limit));
	}
	
	//采集会员【json】
	public function get_user_json($content){
		$content=json_decode($content);
		//订阅
		if($content->errno!=0){
// 			echo "<script>window.close();</script>";
			die('waiting refresh.');
		}
		if (is_object($content) && $content->errno==0 && is_array($content->follow_list)) {
			foreach ($content->follow_list as $v){
				if ($v->pubshare_count>0 || $v->album_count>0 || $v->follow_count>0) {
					$data[]=array(
						'name'=>$v->follow_uname,
						'avatar'=>$v->avatar_url,
						'uk'=>$v->follow_uk,
						'share'=>$v->pubshare_count,//分享
						'album'=>$v->album_count,//专辑
						'follow'=>$v->follow_count,
						'fans'=>$v->fans_count,
						'show'=>$v->intro,
						'time'=>time(),
						'status'=>0,
						'is_follow'=>$v->follow_count==0 ? 1 :0
					);
				}
			}
		}else{//粉丝
			foreach ($content->fans_list as $v){
				if ($v->pubshare_count>0 || $v->album_count>0 || $v->follow_count>0) {
					$data[]=array(
							'name'=>$v->fans_uname,
							'avatar'=>$v->avatar_url,
							'uk'=>$v->fans_uk,
							'share'=>$v->pubshare_count,//分享
							'album'=>$v->album_count,//专辑
							'follow'=>$v->follow_count,
							'fans'=>$v->fans_count,
							'show'=>$v->intro,
							'time'=>time(),
							'status'=>0,
							'is_follow'=>$v->follow_count==0 ? 1 : 0
					);
				}
			}
		}
// 		print_r($data);die();
		if (is_array($data)) {
			$this->addAll($data,'','',true);
		}
		return true;
// 		echo mysql_affected_rows();die();
// 		return mysql_affected_rows()>0 ? 1:0;
	}
	
	private function snoopy_fans($uk,$start,$limit){
		$snoopy=new Snoopy();
		$url='http://yun.baidu.com/pcloud/friend/getfanslist?query_uk='.$uk.'&limit='.$limit.'&start='.$start.'&bdstoken=3afc5bc1d59fe96ef24dfceb33fe70d2&channel=chunlei&clienttype=0&web=1';
		$snoopy->scheme='http';
		$snoopy->agent='Mozilla/5.0 (Windows NT 6.1; WOW64; rv:33.0) Gecko/20100101 Firefox/33.0';
		$snoopy->accept='application/json, text/javascript, */*; q=0.01';
		$snoopy->host='pan.baidu.com';
		$snoopy->referer='http://yun.baidu.com/pcloud/friendpage?type=fans&uk='.$uk.'&self=0';
		$snoopy->rawheaders["Pragma"] = "no-cache";
		// 		$snoopy->proxy_host='182.88.126.27';
		// 		$snoopy->proxy_port='80';
// 		$snoopy->rawheaders['X_FORWARDED_FOR']='127.0.0.1';
// 		$snoopy->rawheaders['CLIENT-IP'] ='127.0.0.1';
		$snoopy->fetch($url);
// 		sleep(rand(1, 3));
		return $content=$snoopy->results;
	}
	private function snoopy_follows($uk,$start,$limit){
		$snoopy=new Snoopy();
		$url='http://pan.baidu.com/pcloud/friend/getfollowlist?query_uk='.$uk.'&limit='.$limit.'&start='.$start.'&bdstoken=8fec66888b7f79fa838d9254856c1998&channel=chunlei&clienttype=0&web=1';
		$snoopy->scheme='http';
		$snoopy->rawheaders['Connection']='keep-alive';
		$snoopy->rawheaders['X-Requested-With']='XMLHttpRequest';
		//:
		$snoopy->agent='Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/31.0.1650.63 Safari/537.36';
		$snoopy->accept='application/json, text/javascript, */*; q=0.01';
		$snoopy->host='pan.baidu.com';
		$snoopy->referer='http://pan.baidu.com/pcloud/friendpage?type=follow&uk='.$uk.'&self=0';
		$snoopy->rawheaders["Pragma"] = "no-cache";
		// 		$snoopy->proxy_host='182.88.126.27';
		// 		$snoopy->proxy_port='80';
		$snoopy->rawheaders['X_FORWARDED_FOR'] =get_client_ip(0);
		$snoopy->rawheaders['CLIENT-IP'] = get_client_ip(0);
// 		echo get_client_ip(0);
		$snoopy->fetch($url);
		print_r($snoopy);
		print_r($snoopy->results);
// 		sleep(rand(1, 3));
		return $content=$snoopy->results;
	}
	
	
	
	
	/*
	 * 以下是spider专用
	 * @author 皇虫
	 * */
	
	
	//获取用户json【本地spider专用】
	public function getFollowSerialize($uk,$start,$limit=24){
		$url='http://yun.baidu.com/pcloud/friend/getfollowlist?query_uk='.$uk.'&limit='.$limit.'&start='.$start.'&bdstoken=3afc5bc1d59fe96ef24dfceb33fe70d2&channel=chunlei&clienttype=0&web=1';
		// 1. 初始化
		$ch = curl_init();
		// 2. 设置选项，包括URL
		curl_setopt($ch, CURLOPT_URL,$url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('X-FORWARDED-FOR:'.get_client_ip(0), 'CLIENT-IP:'.get_client_ip(0)));//IP
		curl_setopt($ch, CURLOPT_REFERER, "http://yun.baidu.com/pcloud/friendpage?type=follow&uk=".$uk."&self=0");   //来路
		curl_setopt($ch, CURLOPT_HEADER, 0);
		// 3. 执行并获取HTML文档内容
		$content = curl_exec($ch);
		// 4. 释放curl句柄
		curl_close($ch);
		$content=json_decode($content);
		if (is_object($content) && $content->errno==0 && is_array($content->follow_list)) {
			foreach ($content->follow_list as $v){
				if ($v->pubshare_count>2 || $v->album_count>0 && $this->check_uk($v->follow_uk)==0) {
					$data[]=array(
							'name'=>$v->follow_uname,
							'avatar'=>$v->avatar_url,
							'uk'=>$v->follow_uk,
							'share'=>$v->pubshare_count,//分享
							'album'=>$v->album_count,//专辑
							'follow'=>$v->follow_count,
							'fans'=>$v->fans_count,
							'show'=>$v->intro,
							'time'=>time(),
							'status'=>0,
					);
				}
			}
		}else{//粉丝
			foreach ($content->fans_list as $v){
				if ($v->pubshare_count>0 || $v->album_count>0 && $this->check_uk($v->fans_uk)==0) {
					$data[]=array(
							'name'=>$v->fans_uname,
							'avatar'=>$v->avatar_url,
							'uk'=>$v->fans_uk,
							'share'=>$v->pubshare_count,//分享
							'album'=>$v->album_count,//专辑
							'follow'=>$v->follow_count,
							'fans'=>$v->fans_count,
							'show'=>$v->intro,
							'time'=>time(),
							'status'=>0,
					);
				}
			}
		}
		return serialize($data);
// 		return json_encode($data);
	}
	
	public function serialize_post($url,$data){
		$ch = curl_init ();
		curl_setopt ( $ch, CURLOPT_URL, $url );
		curl_setopt ( $ch, CURLOPT_POST, 1 );
		curl_setopt ( $ch, CURLOPT_HEADER, 0 );
		curl_setopt ( $ch, CURLOPT_RETURNTRANSFER, 1 );
		curl_setopt ( $ch, CURLOPT_POSTFIELDS, $data );
		$return = curl_exec ( $ch );
		curl_close ( $ch );
		dump($return);
	}
	
	//多线程采集百度网盘会员info信息
	public function getMutiUserInfo($url_arr){
		set_time_limit(300);
		//初始化curl多线程
		$mh = curl_multi_init();
		foreach ($url_arr as $i => $url) {
			$conn[$i]=curl_init($url);
// 			curl_setopt($conn[$i], CURLOPT_HTTPHEADER, array('X-FORWARDED-FOR:'.get_client_ip(0), 'CLIENT-IP:'.get_client_ip(0)));//IP
			//curl_setopt($ch, CURLOPT_REFERER, "http://yun.baidu.com/pcloud/friendpage?type=follow&uk=".$uk."&self=0");   //来路
			//http://yun.baidu.com/share/home?uk=2636867889&view=share
			curl_setopt($conn[$i], CURLOPT_REFERER, 'http://yun.baidu.com/share/home?uk='.$uk.'&view=share');   //来路
			curl_setopt($conn[$i],CURLOPT_RETURNTRANSFER,1);
// 			curl_setopt($conn[$i], CURLOPT_PROXY, 'http://183.230.53.94:8123');
			curl_setopt($conn[$i], CURLOPT_URL,$url);
			curl_setopt($conn[$i], CURLOPT_HEADER, 0);
			curl_multi_add_handle ($mh,$conn[$i]);
		}
		do { $n=curl_multi_exec($mh,$active); } while ($active);
		foreach ($url_arr as $i => $url) {
			$res[$i]=curl_multi_getcontent($conn[$i]);
// 			$res[$i]=file_get_contents($url);
			curl_close($conn[$i]);
		}
// 		print_r($res);
		return $this->wapUserInfoInsert($res);
	}
	
	//多线程采集百度网盘会员info信息
	public function getMutiUserInfo2($uk){
		$uri='http://pan.baidu.com/wap/share/home?uk='.$uk.'&third=0';
		$patternInfo="#<em>(.*)</em>#isU";
		$patternAvatar='#<img alt="(.*)" onerror="imgerr\(this\)" src="(.*)"/>#';
		$res=file_get_contents($uri);
		$content=html_entity_decode($res);
		preg_match_all($patternInfo, $res,$arr_info);
		preg_match_all($patternAvatar, $res,$arr_avatar);
		$data=array(
					'name'=>$arr_avatar[1][0],
					'avatar'=>$arr_avatar[1][1],
					'uk'=>$uk,
					'share'=>$arr_info[1][0],//分享
					'album'=>0,//专辑
					'follow'=>$arr_info[1][1],
					'fans'=>$arr_info[1][2],
					'show'=>'',
					'time'=>time(),
					'status'=>0,
				);
		print_r($data);
		return $this->add($data);
	}	
	
	public function wapUserInfoInsert($arr){
		$trueN=0;
		for ($i = 0; $i < count($arr); $i++) {
			$v=$arr[$i];
			$obj=json_decode($v);
			print_r($obj);
			if ($obj->errno==0) {
				if ($obj->user_info->pubshare_count>0 || $obj->user_info->album_count>0) {
					$data[]=array(
							'name'=>$obj->user_info->uname,
							'avatar'=>$obj->user_info->avatar_url,
							'uk'=>$obj->user_info->uk,
							'share'=>$obj->user_info->pubshare_count,//分享
							'album'=>$obj->user_info->album_count,//专辑
							'follow'=>$obj->user_info->follow_count,
							'fans'=>$obj->user_info->fans_count,
							'show'=>$obj->user_info->intro,
							'time'=>time(),
							'status'=>0,
					);
				}
				$uk[]=$obj->user_info->uk;
			}
		}
		if ($uk) {
			if ($data) {
				echo '<p/>';
				print_r($data);
				$this->addAll($data,'',false,true);
			}
			echo '<p/>';
			print_r($uk);
			return $uk;
		}else{
			return false;
		}
	}
	
	public function pregUserInfo(){
		$snoopy=new Snoopy();
// 		$snoopy->fetchtext('http://pan.baidu.com/wap/share/home?uk=3189004793&third=0');
// 		echo $str=$snoopy->results;
		$snoopy->fetch('http://pan.baidu.com/wap/share/home?uk=3189004793&third=0');
		echo $arr=$snoopy->results;
		die();
		$pattern_avatar="#\<img alt\=\"(.*)\" onerror\=\"imgerr(this)\" src\=\"(.*)\"\>#isU";
		preg_match_all($pattern_avatar, $str,$avatar);
		print_r($avatar);
		
	}
	
	
	/**
	 * 多线成采集分享资源
	 * @param 线程数量 $thread
	 * @return string  */
	public function multiple($thread=10){
		set_time_limit(300);
		$users=$this->where(array('status'=>0,'share'=>array(array('lt',61),array('gt',0),'and')))->limit($thread)->select();
		//初始化curl多线程
		$mh = curl_multi_init();
		foreach ($users as $i => $user) {
			$url='http://yun.baidu.com/pcloud/feed/getsharelist?t=1458459999666&category=0&auth_type=1&request_location=share_home&start=0&limit='.($user['share']+$user['album']).'&query_uk='.$user['uk'].'&channel=chunlei&clienttype=0&web=1&bdstoken=732edcdf8be5f225e2b15d4043a9cf4b';
			$conn[$i]=curl_init($url);
			curl_setopt($conn[$i], CURLOPT_REFERER, 'http://yun.baidu.com/share/home?uk='.$user['uk'].'&view=share');   //来路
			curl_setopt($conn[$i],CURLOPT_RETURNTRANSFER,1);
			curl_setopt($conn[$i], CURLOPT_URL,$url);
			curl_setopt($conn[$i], CURLOPT_HEADER, 0);
			curl_multi_add_handle ($mh,$conn[$i]);
		}
		do { $n=curl_multi_exec($mh,$active); } while ($active);
		foreach ($users as $i => $user) {
			$res[$i]=curl_multi_getcontent($conn[$i]);
			$rt=$this->parse_json($res[$i]);
			if ($rt>10) {
				$uk[]=$rt;
			}else{
				$uk2[]=$user['uk'];
			}
			curl_close($conn[$i]);
		}
		print_r($uk);
		echo '<br/><br/><br/>';
		print_r($uk2);
		$this->where(array('uk'=>array('in',implode(',', $uk))))->save(array('status'=>1));
		$this->where(array('uk'=>array('in',implode(',', $uk2))))->save(array('status'=>2));
		return true;
	}
	
	/**
	 * 解析json数据，并录入临时数据表
	 * @param string $json  */
	public function parse_json($json){
		if (!$json) return true;
		$obj=json_decode($json);
		if ($obj->errno!=0) return false;
		$records=$obj->records;
		foreach ($records as $record){
			if ($record->feed_type=='album') {
				$data_album[]=array(
						'album_id'=>$record->album_id,
						'title'=>$record->title,
						'desc'=>$record->desc,
						'uk'=>$record->uk,
						'vCnt'=>$record->vCnt,
						'tCnt'=>$record->tCnt,
						'dCnt'=>$record->dCnt,
						'click'=>rand(0, 100),
						'filecount'=>$record->filecount,
						'time'=>time(),
				);
			}else{
				$doctype=rtrim(strtolower(pathinfo($record->title,PATHINFO_EXTENSION)),'等');
				$data_share[]=array(
						'shareid'=>$record->shareid,
						'title'=>$record->title,
						'uk'=>$record->uk,
						'size'=>$v->filelist[0]->size ? $v->filelist[0]->size:0,
						'time_stamp'=>substr($record->feed_time, 0,10),
						'uk'=>$record->uk,
						'vCnt'=>$record->vCnt,
						'tCnt'=>$record->tCnt,
						'dCnt'=>$record->dCnt,
						'click'=>rand(0, 100),
						'shorturl'=>$record->shorturl?$record->shorturl:'',
						'time'=>time(),
						'doctype'=>$doctype?$doctype:'',
						'type'=>doctype2type($doctype)
				);
			}
		}
		M('share1')->addAll($data_share);
		M('album')->addAll($data_album);
		return $record->uk;
	}
	
	
	
	
	
	
	
}
<?php
namespace Admin\Model;
use Think\Model;
class FriendlinkModel extends Model{
	//获取普通分享资源
	public function get_link($page=1,$listRows=10){
		return $this->page($page.','.$listRows)->order('status desc,sort asc')->select();
	}
	
	
	//添加友链
	public function addLink(){
		$data=I('post.');
		$data['time']=time();
		$this->add($data);
		return mysql_affected_rows()==1 ? 1:0;
	}
	
	//更新友链
	public function updateLink(){
		$data=I('post.');
		$this->save($data);
		return mysql_affected_rows()==1 ? 1:0;
	}
	
	
	
	
	
	
	
}
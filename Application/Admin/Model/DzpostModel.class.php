<?php
namespace Admin\Model;
use Think\Model;
use Home\Model\SourceModel;
class DzpostModel extends Model{
	public function index($number,$table,$fid,$typeid=0,$keyword=''){
		//根据数据表，确定调用哪个发帖方法
		switch ($table) {
			case 'share':
				$this->post_share($number, $fid,$typeid,$keyword);
				break;
			case 'album':
				$this->post_album($number, $fid,$typeid,$keyword);
				break;
			case 'album_file':
				$this->post_album_file($number, $fid,$typeid,$keyword);
				break;
			case 'user':
				$this->post_user($number, $fid,$typeid,$keyword);
				break;
			default:
				$this->post_share($number, $fid,$typeid,$keyword);
				break;
		}
	}
	
	public function postVdisk($fid,$typeid=0,$cid,$cid_name,$author='微盘机器人',$author_id='44408'){
		$vdisk=M('vdisk_source');
		$data=$vdisk->where(array('cid'=>$cid))->select();
		
		foreach ($data as $v){
			if ($v['filename']) {
				if (empty($v["extension"])) {
					$extension='文件集';
				}else {
					$extension=strtoupper($v["extension"]);
				}
				$title='['.$cid_name.']'.$v['filename'];
				
				$content='[size=5][b]'.$v['filename'].'[/b]
				
						
资源类型：'.$extension.'
				
				
分享达人：'.$v['username'].'
				
收录时间：'.date('Y-m-d H:i:s',$v['time']).'
				
下载地址：[hide]http://vdisk.weibo.com/s/'.$v['copy_ref'].'[/hide]
				
				
  声明：本帖 “'.$title.'” 由"'.$author.'"发布，"'.$author.'"程序由HC-CMS提供，网址==>[url]http://www.hc-cms.com[/url]。本帖若存在不良信息，请向当前站点管理员反馈和举报。感谢您的支持。
				
				
	[/size]';
				$discuz=new DiscuzModel($fid, $title, $content, $author, $author_id,$typeid);
		
				$discuz->post_posts();
		
				$id.=$v['id'].',';
			}
		}
		$vdisk->where(array('id'=>array('in',rtrim($id,','))))->save(array('dz'=>1));
		return true;
	}
	
	
	public function postYunpan($fid,$typeid=0,$limit=1000,$author='360云盘机器侠',$author_id='56069'){
		$Yunpan=M('yunpan');
		$converse=new SourceModel();
		$w['dz']=0;
		switch ($typeid) {
			case 9:
				$w['type']='video';
				break;
			case 10:
				$w['type']='cartoon';
				break;
			case 11:
				$w['type']='music';
				break;
			case 12:
				$w['type']='game';
				break;
			case 13:
				$w['type']='course';
				break;
			case 14:
				$w['type']='it';
				break;
			case 15:
				$w['type']='book';
				break;
			case 16:
				$w['type']='other';
				break;
			case 99:
				$w['type']='edu';
				break;
			case 100:
				$w['type']='soft';
				break;
			
		}
		$data=$Yunpan->where($w)->limit($limit)->select();
// 		$$converse=new 
		foreach ($data as $v){
			if ($v['name']) {
				if (empty($v["doctype"])) {
					$pre='';
				}else {
					$pre='【'.strtoupper($v["doctype"]).'】';
				}
				$size=$converse->conversion($v['size']);
				$title=$pre.$v['name'];
				$down_url='http://yunpan.cn/'.$v['url'];
				$content='[color=#34495e][font=Lato, Helvetica, Arial, sans-serif][size=5]'.$title.'[/size][/font][/color]
	[size=5]
	资源大小 : '.$size.'
	资源类型 : '.$v["doctype"].'
	收录时间：'.date('Y-m-d H:i:s',$v['time']).'
	分享时间 : '.date('Y-m-d H:i:s',$v['share_time']).'
	资源密码 : [hide]'.$v['pwd'].'
	[/hide][/size]
	[size=5]下载地址：[/size][size=5][hide]'.$down_url.'[/hide][/size]
	[size=5]
	[/size]
	[size=3]声明：本帖  “'.$title.'” 由[/size][size=3]"[/size][size=3][/size][size=3]'.$author.'[/size][size=3]"发布，"'.$author.'"程序由[b]HC-CMS[/b]提供，网址==>[/size][url=http://www.hc-cms.com]http://www.hc-cms.com[/url]。本帖若存在不良信息，请向当前站点管理员反馈和举报。感谢您的支持。';
				$discuz=new DiscuzModel($fid, $title, $content,$author, $author_id,$typeid);
// 				$discuz=new DiscuzModel('2', $title, $content,'admin','1','1');//测试专用
				
				$discuz->post_posts();
				$id.=$v['id'].',';
			}
		}
		$Yunpan->where(array('id'=>array('in',rtrim($id,','))))->save(array('dz'=>1));
		return true;
	}
	
	//115网盘
	public function postOof($fid,$typeid=0,$limit=1000,$author='115网盘机器侠',$author_id='56105'){
		$Yunpan=M('oof');
		$converse=new SourceModel();
		$w['dz']=0;
		switch ($typeid) {
			case 49:
				$w['type']='video';
				break;
			case 50:
				$w['type']='cartoon';
				break;
			case 51:
				$w['type']='music';
				break;
			case 52:
				$w['type']='game';
				break;
			case 53:
				$w['type']='course';
				break;
			case 54:
				$w['type']='it';
				break;
			case 55:
				$w['type']='book';
				break;
			case 56:
				$w['type']='other';
				break;
			case 109:
				$w['type']='edu';
				break;
			case 110:
				$w['type']='soft';
				break;
					
		}
		$data=$Yunpan->where($w)->limit($limit)->select();
		foreach ($data as $v){
			if ($v['name']) {
				if (empty($v["doctype"])) {
					$pre='';
				}else {
					$pre='【'.strtoupper($v["doctype"]).'】';
				}
				$size=$converse->conversion($v['size']);
				$title=$pre.$v['name'];
				$down_url='http://115.com/lb/'.$v['url'];
				$content='[color=#34495e][font=Lato, Helvetica, Arial, sans-serif][size=5]'.$title.'[/size][/font][/color]
	[size=5]
	资源大小 : '.$size.'
	资源类型 : '.$v["doctype"].'
	收录时间：'.date('Y-m-d H:i:s',$v['time']).'
	分享时间 : '.date('Y-m-d H:i:s',$v['share_time']).'
	[/size]
	[size=5]下载地址：[/size][size=5][hide]'.$down_url.'[/hide][/size]
	[size=5]
	[/size]
	[size=3]声明：本帖  “'.$title.'” 由[/size][size=3]"[/size][size=3][/size][size=3]'.$author.'[/size][size=3]"发布，"'.$author.'"程序由[b]HC-CMS[/b]提供，网址==>[/size][url=http://www.hc-cms.com]http://www.hc-cms.com[/url]。本帖若存在不良信息，请向当前站点管理员反馈和举报。感谢您的支持。';
				$discuz=new DiscuzModel($fid, $title, $content,$author, $author_id,$typeid);
				//$discuz=new DiscuzModel('2', $title, $content,'admin','1','1');//测试专用
	
				$discuz->post_posts();
				$id.=$v['id'].',';
			}
		}
		$Yunpan->where(array('id'=>array('in',rtrim($id,','))))->save(array('dz'=>1));
		return true;
	}
	
	public function post_share($number,$fid,$typeid,$keyword){
		$converse=new SourceModel();
		$share=M('share1');
		//根据fid获取发帖人
		$arr=$this->poster($fid,$keyword);//返回查询条件和发帖人
		if ($keyword) {
			$kwarr=explode(' ', $keyword);
			for ($i=0;$i<count($kwarr);$i++){
				$kw[$i]='%'.$kwarr[$i].'%';
			}
			unset($arr['w']['dz']);
			$arr['w']['kw']=0;
			$arr['w']['title']=array('like',$kw,'and');
		}
		if ($number<0) {
			$data=$share->where($arr['w'])->limit(10000)->select();
// 			$data=$share->limit(10000)->select();
		}else{
			$data=$share->where($arr['w'])->limit($number)->select();
			
// 			$sql=$share->getLastSql();
// 			file_put_contents('dzpost'.time().'.sql', $sql);
			
// 			$data=$share->limit(10000)->select();
		}
		foreach ($data as $v){
			if ($v['title']) {
				if (empty($v["doctype"])) {
					$pre='';
				}else {
					$pre='【'.strtoupper($v["doctype"]).'】';
				}
				
				
				$size=$converse->conversion($v['size']);
				$title=$pre.$v['title'];
				$down_url=empty($v['shareid']) ? $v['shorturl'] : 'http://pan.baidu.com/share/link?uk='.$v["uk"].'&shareid='.$v["shareid"];
				
				if ($fid==91 || $fid==94 || $fid==95 || $fid==87) {
					$hide='[hide]'.$down_url.'[/hide]';
				}else{
					$hide=$down_url;
				}
				$content='[color=#34495e][font=Lato, Helvetica, Arial, sans-serif][size=5]'.$v["title"].'[/size][/font][/color]
	[size=5]
	资源大小 : '.$size.'
	资源类型 : '.$v["doctype"].'
	浏览次数 : '.$v["vCnt"].'
	下载次数 : '.$v["dCnt"].'
	转存次数 : '.$v["tCnt"].'
	[/size]
	[size=5]下载地址：[/size][size=5]'.$hide.'[/size]
	[size=5]
	[/size]
	[size=3]声明：本帖  “'.$v['title'].'” 由[/size][size=3]"[/size][size=3][/size][size=3]'.$arr['u']['author'].'[/size][size=3]"发布，"'.$arr['u']['author'].'"程序由[b]HC-CMS[/b]提供，网址==>[/size][url=http://www.hc-cms.com]http://www.hc-cms.com[/url]。本帖若存在不良信息，请向当前站点管理员反馈和举报。感谢您的支持。';
				$discuz=new DiscuzModel($fid, $title, $content, $arr['u']['author'], $arr['u']['author_id'],$typeid);
// 				$discuz=new DiscuzModel('2', $title, $content,'admin','1','1');//测试专用
				
				$discuz->post_posts();
				
				$id.=$v['id'].',';
			}
		}
		if ($keyword) {
			$share->where(array('id'=>array('in',rtrim($id,','))))->save(array('kw'=>1));
		}else{
			$share->where(array('id'=>array('in',rtrim($id,','))))->save(array('dz'=>1));
		}
		
		
	}
	
	public function post_album($number,$fid,$typeid,$keyword){
		$album=M('album');
		//根据fid获取发帖人
		$arr=$this->poster($fid,$keyword);//返回查询条件和发帖人
		if ($keyword) {
			$kwarr=explode(' ', $keyword);
			for ($i=0;$i<count($kwarr);$i++){
				$kw[$i]='%'.$kwarr[$i].'%';
			}
			unset($arr['w']['dz']);
			$arr['w']['kw']=0;
			$arr['w']['title']=array('like',$kw,'and');
		}
		if ($number<0) {
			$data=$album->where($arr['w'])->limit(10000)->select();
		}else{
			$data=$album->where($arr['w'])->limit($number)->select();
		}
		
		
		
		
		foreach ($data as $v){
			if ($v['title']) {
				$title=$v['title'];
				$content='[b][font=Arial][size=4]'.$v["title"].'[/size][/font][/b]
	
	[font=Arial][size=4]'.$v["desc"].'                
	
	
	文件数量  : '.$v["filecount"].'
	资源类型  : 专辑
	浏览次数  : '.$v["vCnt"].'
	下载次数  : '.$v["dCnt"].'
	转存次数  : '.$v["tCnt"].'
	
	下载地址：[hide]http://yun.baidu.com/pcloud/album/info?uk='.$v["uk"].'&album_id='.$v["album_id"].'[/hide]
	
	
	[/size][/font]
	[size=3]声明：本帖 “'.$v['title'].'” 由[/size][size=3]"[/size][size=3]'.$arr['u']['author'].'[/size][size=3]"发布，"'.$arr['u']['author'].'"程序由[b]HC-CMS[/b]提供，网址==>[/size][url=http://www.hc-cms.com]http://www.hc-cms.com[/url]。本帖若存在不良信息，请向当前站点管理员反馈和举报。感谢您的支持。';
				
				$discuz=new DiscuzModel($fid, $title, $content, $arr['u']['author'], $arr['u']['author_id'],$typeid);
// 				$discuz=new DiscuzModel('2', $title, $content,'admin','1');//测试专用
				$discuz->post_posts();
				$id.=$v['id'].',';
			}
		}
		if ($keyword) {
			$album->where(array('id'=>array('in',rtrim($id,','))))->save(array('kw'=>1));
		}else{
			$album->where(array('id'=>array('in',rtrim($id,','))))->save(array('dz'=>1));
		}
		
	}
	
	
	
	public function post_user($number,$fid,$typeid,$keyword){
		$user=M('user');
		//根据fid获取发帖人
		$arr=$this->poster($fid,$keyword);//返回查询条件和发帖人
		$data=$user->where($arr['w'])->limit($number)->select();
		foreach ($data as $v){
			$title='【百度达人】【分享'.$v['share'].'|专辑'.$v['album'].'】'.$v['name'];
			$content='[size=4]'.$title.'
		
		'.$v['show'].'
		
		分享'.$v['share'].'
		专辑'.$v['album'].'
		订阅 '.$v['follow'].'
		粉丝'.$v['fans'].'
		
		网盘达人百度网盘主页地址：[hide]http://pan.baidu.com/share/home?uk='.$v['uk'].'#category/type=0[/hide]
		
		
		[/size][size=3]声明：本帖 “'.$title.'” 由[/size][size=3]"[/size][size=3]'.$arr['u']['author'].'[/size][size=3]"发布，"'.$arr['u']['author'].'"程序由[b]HC-CMS[/b]提供，网址==>[/size][url=http://www.hc-cms.com]http://www.hc-cms.com[/url]。本帖若存在不良信息，请向当前站点管理员反馈和举报。感谢您的支持。';
				
			$discuz=new DiscuzModel($fid, $title, $content, $arr['u']['author'], $arr['u']['author_id']);
// 			$discuz=new DiscuzModel('2', $title, $content,'admin','1');//测试专用
			$discuz->post_posts();
			$id.=$v['id'].',';
		}
		$user->where(array('id'=>array('in',rtrim($id,','))))->save(array('dz'=>1));
		
	}
	
	
	
	
	public function post_album_file($number,$fid,$typeid,$keyword){
		$album_file=M('album_source');
		$converse=new SourceModel();
		//根据fid获取发帖人
		$arr=$this->poster($fid,$keyword);//返回查询条件和发帖人
		if ($keyword) {
			$kwarr=explode(' ', $keyword);
			for ($i=0;$i<count($kwarr);$i++){
				$kw[$i]='%'.$kwarr[$i].'%';
			}
			unset($arr['w']['dz']);
			$arr['w']['kw']=0;
			$arr['w']['server_filename']=array('like',$kw,'and');
		}
		if ($number<0) {
			$data=$album_file->where($arr['w'])->limit(10000)->select();;
		}else{
			$data=$album_file->where($arr['w'])->limit($number)->select();
		}
		
		
		
		
		foreach ($data as $v){
			if (empty($v["doctype"])) {
				$pre='';
			}else {
				$pre='【'.strtoupper($v["doctype"]).'】';
			}
			if ($v['server_filename']) {
				$size=$converse->conversion($v['size']);
				$title='【专辑文件】'.$pre.$v['server_filename'];
				$content='[size=4]'.$title.'
	
	
	资源大小  : '.$size.'
	资源类型  : '.$v['doctype'].'
	浏览次数  : '.$v['vCnt'].'
	下载次数  : '.$v['dCnt'].'
	转存次数  : '.$v['tCnt'].'
	
	
	下载地址：[hide]http://yun.baidu.com/pcloud/album/file?album_id='.$v['album_id'].'&uk='.$v['uk'].'&fsid='.$v['fs_id'].'[/hide]
	
	[/size]
	[size=3]声明：本帖 “'.$v['server_filename'].'” 由[/size][size=3]"[/size][size=3]'.$arr['u']['author'].'[/size][size=3]"发布，"'.$arr['u']['author'].'"程序由[b]HC-CMS[/b]提供，网址==>[/size][url=http://www.hc-cms.com]http://www.hc-cms.com[/url]。本帖若存在不良信息，请向当前站点管理员反馈和举报。感谢您的支持。';
			
				$discuz=new DiscuzModel($fid, $title, $content, $arr['u']['author'], $arr['u']['author_id']);
// 				$discuz=new DiscuzModel('2', $title, $content,'admin','1');//测试专用
				$discuz->post_posts();
				$id.=$v['id'].',';
			}
		}
		if ($keyword) {
			$album_file->where(array('id'=>array('in',rtrim($id,','))))->save(array('kw'=>1));;
		}else{
			$album_file->where(array('id'=>array('in',rtrim($id,','))))->save(array('dz'=>1));
		}
		
	}
	
	/* 
	 *
	 * 影视：87    	影视机器人41222
		音乐 88		音乐机器人41223
		种子:91		Torrent机器人41224
		文件集:94	文件集机器人41225
		专辑：95	网盘专辑机器人41226
		压缩：90	压缩资源机器人41227
		安卓：93	安卓机器人41228
		PC：89	PC软件机器人41232
		苹果：96	苹果机器人41229
		达人：97	网盘达人机器人41230
		电子书：92	电子书机器人41231
 */
	
	private function poster($fid,$keyword){
		$w['dz']=0;
		if ($keyword) {//如果关键字存在，则表示关键字发帖
			$u['author_id']='43203';
			$u['author']='机器侠';
		}else{
			switch ($fid) {
				case 87://video
					$u['author_id']='41222';
					$u['author']='影视机器人';
					$w['doctype']=array('in',C('VIDEO'));
					break;
				case 88://music
					$u['author_id']='41223';
					$u['author']='音乐机器人';
					$w['doctype']=array('in',C('MUSIC'));
					break;
				case 89://pc
					$u['author_id']='41232';
					$u['author']='PC软件机器人';
					$w['doctype']=array('in',array('exe','dmg'));
					break;
				case 90://zip
					$u['author_id']='41227';
					$u['author']='压缩资源机器人';
					$w['doctype']=array('in',C('ZIP'));
					break;
				case 91://torrent
					$u['author_id']='41224';
					$u['author']='Torrent机器人';
					$w['doctype']='torrent';
					break;
				case 92://book
					$u['author_id']='41231';
					$u['author']='电子书机器人';
					$w['doctype']=array('in',C('BOOK'));;
					break;
				case 93://apk
					$u['author_id']='41228';
					$u['author']='安卓机器人';
					$w['doctype']=array('in',array('apk','gpk'));;
					break;
				case 94://file
					$u['author_id']='41225';
					$u['author']='文件集机器人';
					$w['doctype']=array('EXP','IS NULL');
					break;
				case 95://album
					$u['author_id']='41226';
					$u['author']='网盘专辑机器人';
					break;
				case 96://ios
					$u['author_id']='41229';
					$u['author']='苹果机器人';
					$w['doctype']='ipa';
					break;
				case 97://user
					$u['author_id']='41230';
					$u['author']='网盘达人机器人';
					break;
			}
		}
		return array('w'=>$w,'u'=>$u);
	}
	
	
	
	
	
	
	
	
	
	
	
}
?>
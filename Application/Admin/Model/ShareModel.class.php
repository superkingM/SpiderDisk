<?php
namespace Admin\Model;
use Think\Model;
use Think\Snoopy;
class ShareModel extends Model{
	//获取普通分享资源
	public function get_share($page=1,$listRows=20,$order='time desc',$kw){
		switch ($order) {
			case 'new':
				$sort='time desc';
				break;
			case 'dCnt':
				$sort='dCnt desc';
				break;
			case 'vCnt':
				$sort='vCnt desc';
				break;
			case 'tCnt':
				$sort='tCnt desc';
				break;
			case 'click':
				$sort='click desc';
				break;
			default:
				$sort='time desc';
				break;
		}
		if ($kw) {
			return $this->page($page.','.$listRows)->where(array('title'=>array('like','%'.$kw.'%')))->order($sort)->select();
		}else{
			return $this->page($page.','.$listRows)->order($sort)->select();
		}
	}
	
		
	//获取专辑
	public function get_album($page=1,$listRows=20,$order='status asc,time desc',$kw){
		switch ($order) {
			case 'new':
				$sort='time desc';
				break;
			case 'dCnt':
				$sort='dCnt desc';
				break;
			case 'vCnt':
				$sort='vCnt desc';
				break;
			case 'tCnt':
				$sort='tCnt desc';
				break;
			case 'click':
				$sort='click desc';
				break;
			default:
				$sort='status asc,time desc';
				break;
		}
		if ($kw) {
			return M('album')->page($page.','.$listRows)->where(array('title'=>array('like','%'.$kw.'%')))->order($sort)->select();
		}else{
			return M('album')->page($page.','.$listRows)->order($sort)->select();
		}
	}
	
	public function get_tent_new($type_array){
// 		return $this->where($type_array)->order('id desc')->field('id,title')->limit(10)->select();
		return M('share2')->where($type_array)->order('id desc')->field('id,title')->limit(10)->select();
	}
	
	//获取360云盘分享资源
	public function get_yunpan($page=1,$listRows=20,$order='time desc',$kw){
		$Yunpan=M('yunpan');
		switch ($order) {
			case 'new':
				$sort='isset asc,id desc';
				break;
			case 'shareTime':
				$sort='isset asc,share_time desc';
				break;
			case 'size':
				$sort='isset asc,size desc';
				break;
			case 'doctype':
				$sort='isset asc,doctype asc';
				break;
			default:
				$sort='isset asc,id desc';
				break;
				
		}
		if ($kw) {
			return $Yunpan->page($page.','.$listRows)->where(array('name'=>array('like','%'.$kw.'%')))->order($sort)->select();
		}else{
			return $Yunpan->page($page.','.$listRows)->order($sort)->select();
		}
	}
	
	//获取115网盘分享资源
	public function get_oof($page=1,$listRows=20,$order='time desc',$kw){
		$Yunpan=M('oof');
		switch ($order) {
			case 'new':
				$sort='isset asc,id desc';
				break;
			case 'shareTime':
				$sort='isset asc,share_time desc';
				break;
			case 'size':
				$sort='isset asc,size desc';
				break;
			case 'doctype':
				$sort='isset asc,doctype asc';
				break;
			default:
				$sort='isset asc,id desc';
				break;
	
		}
		if ($kw) {
			return $Yunpan->page($page.','.$listRows)->where(array('name'=>array('like','%'.$kw.'%')))->order($sort)->select();
		}else{
			return $Yunpan->page($page.','.$listRows)->order($sort)->select();
		}
	}
	
	//采集资源【自动】
	public function get_source($uk,$start,$limit=60,$total=''){
		$cookies=file_get_contents('cookies.txt');
		
		$url='http://pan.baidu.com/pcloud/feed/getsharelist?t=1434269768615&category=0&auth_type=1&request_location=share_home&start='.$start.'&limit='.$limit.'&query_uk='.$uk.'&channel=chunlei&clienttype=0&web=1&bdstoken=53331ce564a5c5877066e18c033f55df';
		
		$json=crawl($url,'http://pan.baidu.com/share/home?uk='.$uk.'&view=share',$cookies);
		
		$content=json_decode($json);
		
		if (is_object($content) && $content->errno==0) {
			foreach ($content->records as $v){
				if($v->feed_type=="share"){
// 					if($this->check_share($v->uk, $v->shareid)==0){
						$info = pathinfo($v->title);
						$doctype=strtolower(str_replace('等', '', $info['extension']));
						$share_arr[]=array(
								'shareid'=>$v->shareid,
								'title'=>$v->title,
								'uk'=>$v->uk,
								'size'=>$v->filelist[0]->size,
								'time_stamp'=>substr($v->feed_time, 0,10),
								'shorturl'=>$v->shorturl,
								'vCnt'=>$v->vCnt,
								'dCnt'=>$v->dCnt,
								'tCnt'=>$v->tCnt,
								'click'=>rand(100, 500),
								'doctype'=>$doctype,
								'type'=>doctype2type($doctype),
								'status'=>0,
								'time'=>time(),
								'recommend'=>0
						);
// 					}
				}
				if($v->feed_type=="album"){
// 					if ($this->check_album($v->uk, $v->album_id)==0) {
						$album_arr[]=array(
								'album_id'=>$v->album_id,
								'title'=>$v->title,
								'desc'=>$v->desc,
								'uk'=>$v->uk,
								'vCnt'=>$v->vCnt,
								'tCnt'=>$v->tCnt,
								'dCnt'=>$v->dCnt,
								'filecount'=>$v->operation[0]->file_count,
								'click'=>rand(200, 1000),
								'time'=>time(),
								'status'=>0,
								'recommend'=>0
						);
// 					}
					
				}
			}
		}else{
			return 0;
		}
		if (is_array($share_arr)) {
			print_r($share_arr);
// 			$this->addAll($share_arr);//分表后，将数据存入share2表;分表前，把这句话注释去掉，把下句注释掉
// 			M('share2')->addAll($share_arr);
// 			print_r($share_arr);
			M('share2')->addAll($share_arr,'',false,true);
			//addAll($data,'',false,true);
		}
		if (is_array($album_arr)) {
			print_r($album_arr);
			M('album')->addAll($album_arr);
		}
// 		sleep(rand(1, 3));
		return 1;
// 		return mysql_affected_rows()>0 ? 1:0;		
	}
	
	//检查下数据库是否已存在该资源
	public function check_share($uk,$shareid){
		$rs=M('share')->where(array('uk'=>$uk,'shareid'=>$shareid))->find();
		return is_array($rs) ? 1:0;
	}
	
	//检查下数据库是否已存在该专辑
	public function check_album($uk,$albumid){
		$rs=M('album')->where(array('uk'=>$uk,'album_id'=>$albumid))->find();
		return is_array($rs) ? 1:0;
	}
	
	//手动匹配采集资源
	public function preg_file($content){
		$user=new UserModel();
		$content=html_entity_decode($content);
		$content=str_replace("&amp;", "&", $content);
// 		file_put_contents('1.html', $content);
// 		dump($content);die();
		$preg_uk="#\/share\/home\?uk\=(.*)\&view\=share#isU";//会员uk
		$preg_name="#<div title=\"(.*)\" class=\"col file-col\">#isU";//资源名称
		$preg_view="#<div class=\"size-col col\"><span>(.*)次<\/span><\/div>#isU";//浏览次数
		$preg_save="#<div class=\"remaining-col col\"><span>(.*)次<\/span><\/div>#isU";//保存次数
		$preg_download="#<div class=\"remaining-col col\"><span class=\"d-cnt\">(.*)次<\/span><\/div>#isU";//下载次数
		$preg_sharetime="#<div class=\"time-col col\"><span>(.*)<\/span><\/div>#isU";//分享时间
		$preg_url="#_link=\"链接:(.*)\"#isU";//分享链接
	 
	
		preg_match_all($preg_name, $content,$arr_name);//资源名称
		preg_match_all($preg_view, $content,$arr_view);//浏览次数
		preg_match_all($preg_save, $content,$arr_save);//保存次数
		preg_match_all($preg_uk, $content,$arr_uk);//保存次数
		preg_match_all($preg_download, $content,$arr_download);//下载次数
		preg_match_all($preg_sharetime, $content,$arr_sharetime);//分享时间
		preg_match_all($preg_url, $content,$arr_url);//链接地址
		
		$uk=$arr_uk[1][0];
		
		if ($user->check_uk($uk)==0) {
			$user->preg_header($content);
		}
		$content="";//清空
		foreach ($arr_name[1] as $k=>$v){
			if ($this->check_source($uk, $v)==0){//如果该资源未存在，则添加
				$info = pathinfo($v);
				$doc_type=$info['extension'];
				if (empty($doc_type)) $doc_type=0;//专辑
				$data[]=array(
						'title'=>$v,
						'vCnt'=>$arr_view[1][$k],
						'tCnt'=>$arr_save[1][$k],
						'uk'=>$uk,
						'dCnt'=>$arr_download[1][$k],
						'time_stamp'=>strtotime($arr_sharetime[1][$k]),
						'shorturl'=>$arr_url[1][$k],
						'time'=>time(),
						'doctype'=>$doc_type,
						'hand'=>1,
						'click'=>rand(100, 500),
				);
			}
		}
		if (is_array($data)) {
			$this->addAll($data);
			return  mysql_affected_rows()>0 ? 1:0;
		}else{
			return 0;
		}
		
	}
	
	//检查手动采集的会员的资源是否已存在
	public function check_source($uk,$source_name){
		$data=$this->where(array('uk'=>$uk,'title'=>$source_name))->find();
		return is_array($data) ? 1 : 0;
	}
	
	//获取专辑资源
	//http://yun.baidu.com/pcloud/album/listfile?album_id=7084397146237876529&query_uk=53993635&start=0&limit=60&bdstoken=fabf54083df61c3328dc3f55b3bef5b2&channel=chunlei&clienttype=0&web=1
	public function getAlbumSource($album_id,$uk,$start=0,$limit=60){
// 		$url="http://yun.baidu.com/pcloud/album/listfile?album_id=".$album_id."&query_uk=".$uk."&start=".$start."&limit=".$limit."&bdstoken=fabf54083df61c3328dc3f55b3bef5b2&channel=chunlei&clienttype=0&web=1";
// 		// 1. 初始化
// 		$ch = curl_init();
// 		// 2. 设置选项，包括URL
// 		curl_setopt($ch, CURLOPT_URL,$url);
// 		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
// 		//http://yun.baidu.com/pcloud/album/info?uk=1428919760&album_id=4703098152858397132
// // 		curl_setopt($ch, CURLOPT_HTTPHEADER, array('X-FORWARDED-FOR:'.get_client_ip(0), 'CLIENT-IP:'.get_client_ip(0)));//IP
// 		curl_setopt($ch, CURLOPT_REFERER, "http://yun.baidu.com/pcloud/album/info?uk=".$uk."&album_id=".$album_id);   //来路
// 		curl_setopt($ch, CURLOPT_HEADER, 0);
// 		// 3. 执行并获取HTML文档内容
// 		$content = curl_exec($ch);
// 		// 4. 释放curl句柄
// 		curl_close($ch);
// 		$content=json_decode($content);
		$content=json_decode($this->snoopy_album($album_id,$uk, $start,$limit));//此处采用snoopy
		print_r($content);
		if (is_object($content) && $content->errno==0) {
			foreach ($content->list as $v){
				$info = pathinfo($v->server_filename);
				$doc_type=$info['extension'];
				if ($this->check_album_source($album_id,$v->server_filename)==0) {
					$source[]=array(
							'album_id'=>$album_id,
							'server_filename'=>$v->server_filename,
							'doctype'=>$doc_type,
							'size'=>$v->size,
							'uk'=>$uk,
							'fs_id'=>$v->fs_id,
							'time_stamp'=>$v->add_time,
							'status'=>0,
							'recommend'=>0,
							'time'=>time(),
							'click'=>rand(100, 500),
							'tCnt'=>$v->tCnt,
							'vCnt'=>$v->vCnt,
							'dCnt'=>$v->dCnt
					);
				}
			}
		}
// 				print_r($source);die();
		M('album_source')->addAll($source);
// 		sleep(rand(1, 3));
		return mysql_affected_rows()>0 ? 1:0;
// 		print_r($source);die();
		
	}
	
	
	//检查专辑内容是否存在
	public function check_album_source($album_id,$server_filename){
		$arr=M('album_source')->where(array('album_id'=>$album_id,'server_filename'=>$server_filename))->find();
		return is_array($arr) ? 1 :0;
	}
	
	
	//采集资源【自动】
	public function get_json($content){
		$content=json_decode($content);
		if (is_object($content) && $content->errno==0) {
			foreach ($content->records as $v){
				if($v->feed_type=="share"){
					if($this->check_share($v->uk, $v->shareid)==0){
						$info = pathinfo($v->title);
						$doctype=$info['extension'];
						$share_arr[]=array(
								'shareid'=>$v->shareid,
								'title'=>$v->title,
								'uk'=>$v->uk,
								'size'=>$v->filelist[0]->size,
								'time_stamp'=>$v->filelist[0]->time_stamp,
								'shorturl'=>$v->shorturl,
								'vCnt'=>$v->vCnt,
								'dCnt'=>$v->dCnt,
								'tCnt'=>$v->tCnt,
								'click'=>rand(100, 500),
								'doctype'=>$doctype,
								'type'=>doctype2type($doctype),
								'status'=>0,
								'time'=>time(),
								'recommend'=>0
						);
					}
				}
				if($v->feed_type=="album"){
					if ($this->check_album($v->uk, $v->album_id)==0) {
						$album_arr[]=array(
								'album_id'=>$v->album_id,
								'title'=>$v->title,
								'desc'=>$v->desc,
								'uk'=>$v->uk,
								'vCnt'=>$v->vCnt,
								'tCnt'=>$v->tCnt,
								'dCnt'=>$v->dCnt,
								'filecount'=>$v->operation[0]->file_count,
								'click'=>rand(200, 1000),
								'time'=>time(),
								'status'=>0,
								'recommend'=>0
						);
					}
						
				}
			}
		}
		if (is_array($share_arr)) {
// 			print_r($share_arr);
			M('share2')->addAll($share_arr);
		}
		if (is_array($album_arr)) {
// 			print_r($album_arr);
			M('album')->addAll($album_arr);
		}
		return mysql_affected_rows()>0 ? 1:0;
	}

	public function do_json($content){
		$content=json_decode($content);
		if (is_object($content) && $content->errno==0) {
			foreach ($content->records as $v){
				if($v->feed_type=="share"){
					if($this->check_share($v->uk, $v->shareid)==0){
						$info = pathinfo($v->title);
						$doctype=$info['extension'];
						$share_arr[]=array(
								'shareid'=>$v->shareid,
								'title'=>$v->title,
								'uk'=>$v->uk,
								'size'=>$v->filelist[0]->size,
								'time_stamp'=>$v->filelist[0]->time_stamp,
								'shorturl'=>$v->shorturl,
								'vCnt'=>$v->vCnt,
								'dCnt'=>$v->dCnt,
								'tCnt'=>$v->tCnt,
								'click'=>rand(100, 500),
								'doctype'=>$doctype,
								'type'=>doctype2type($doctype),
								'status'=>0,
								'time'=>time(),
								'recommend'=>0
						);
					}
				}
				if($v->feed_type=="album"){
					if ($this->check_album($v->uk, $v->album_id)==0) {
						$album_arr[]=array(
								'album_id'=>$v->album_id,
								'title'=>$v->title,
								'desc'=>$v->desc,
								'uk'=>$v->uk,
								'vCnt'=>$v->vCnt,
								'tCnt'=>$v->tCnt,
								'dCnt'=>$v->dCnt,
								'filecount'=>$v->operation[0]->file_count,
								'click'=>rand(200, 1000),
								'time'=>time(),
								'status'=>0,
								'recommend'=>0
						);
					}
	
				}
			}
		}else{
			return 0;
		}
		if (is_array($share_arr)) {
// 			print_r($share_arr);
			M('share2')->addAll($share_arr);
		}
		if (is_array($album_arr)) {
// 			print_r($album_arr);
			M('album')->addAll($album_arr);
		}
		return 1;
	}
	
	
	//360云盘
	public function yunpan($json){
		$con=json_decode($json);
		if ($con->error==0) {
			$Yunpan=M('yunpan');
			$list=$con->data->linklist;
			$affected=0;
			foreach ($list as $v){
				if ($v->count_size==0) {
					$doctype='文件集';
				}else{
					$info = pathinfo($v->name);
					$doctype=$info['extension'];
				}
				$data=array(
					'name'=>$v->name,
					'url'=>$v->shorturl,
					'pwd'=>$v->password,
					'doctype'=>$doctype,
					'size'=>$v->count_size,
					'share_time'=>$v->update_time,
					'time'=>time(),
				);
				$Yunpan->add($data);
			}
			return 1;
		}
	}
	
	
	//115网盘
	public function oof($json){
		$con=json_decode($json);
		if ($con->state==1) {
			$Model=M('oof');
			$list=$con->data;
			foreach ($list as $v){
				if ($v->size==0) {
					$doctype='文件集';
				}else{
					$info = pathinfo($v->remark);
					$doctype=$info['extension'];
				}
				$data=array(
						'name'=>$v->remark,
						'url'=>$v->gift_code,
						'doctype'=>$doctype,
						'size'=>$v->size,
						'share_time'=>strtotime($v->ctime),
						'time'=>time(),
				);
				$Model->add($data);
			}
			return 1;
		}
	}
	
	private function snoopy_share($uk,$start,$limit=60){
		$cookies_cont=file_get_contents('./cookies.txt');
		foreach (explode(';', $cookies_cont) as $v){
			$vv=explode('=', $v);
			$cookies[$vv[0]]=$vv[1];
		}
		$snoopy=new Snoopy();
		$url='http://pan.baidu.com/pcloud/feed/getsharelist?t=1434269768615&category=0&auth_type=1&request_location=share_home&start='.$start.'&limit='.$limit.'&query_uk='.$uk.'&channel=chunlei&clienttype=0&web=1&bdstoken=53331ce564a5c5877066e18c033f55df';
// 		$url='http://pan.baidu.com/pcloud/feed/getsharelist?t=1417835783754&category=0&auth_type=1&request_location=share_home&start='.$start.'&limit='.$limit.'&query_uk='.$uk.'&channel=chunlei&clienttype=0&web=1&bdstoken=3afc5bc1d59fe96ef24dfceb33fe70d2';
		$snoopy->scheme='http';
		$snoopy->agent='Mozilla/5.0 (Windows NT 6.1; WOW64; rv:33.0) Gecko/20100101 Firefox/33.0';
		$snoopy->accept='application/json, text/javascript, */*; q=0.01';
		$snoopy->host='pan.baidu.com';
		$snoopy->referer='http://pan.baidu.com/share/home?uk='.$uk.'&view=share';
		$snoopy->rawheaders["Pragma"] = "no-cache";
		$snoopy->cookies=$cookies;
		$snoopy->fetch($url);
		return $content=$snoopy->results;
	}
	private function snoopy_album($album_id,$uk,$start,$limit=60){
		$referer='http://yun.baidu.com/pcloud/album/info?uk='.$uk.'&album_id='.$album_id;
		$url="http://yun.baidu.com/pcloud/album/listfile?album_id=".$album_id."&query_uk=".$uk."&start=".$start."&limit=".$limit."&bdstoken=fabf54083df61c3328dc3f55b3bef5b2&channel=chunlei&clienttype=0&web=1";
		return crawl($url,$referer);
		
		$snoopy=new Snoopy();
		$url="http://yun.baidu.com/pcloud/album/listfile?album_id=".$album_id."&query_uk=".$uk."&start=".$start."&limit=".$limit."&bdstoken=fabf54083df61c3328dc3f55b3bef5b2&channel=chunlei&clienttype=0&web=1";
		$snoopy->scheme='http';
		$snoopy->agent='Mozilla/5.0 (Windows NT 6.1; WOW64; rv:33.0) Gecko/20100101 Firefox/33.0';
		$snoopy->accept='application/json, text/javascript, */*; q=0.01';
		$snoopy->host='pan.baidu.com';
		$snoopy->referer='http://yun.baidu.com/pcloud/album/info?uk='.$uk.'&album_id='.$album_id;
		$snoopy->rawheaders["Pragma"] = "no-cache";
		// 		$snoopy->proxy_host='182.88.126.27';
		// 		$snoopy->proxy_port='80';
		$snoopy->fetch($url);
// 		sleep(rand(1, 3));
		return $content=$snoopy->results;
	}
	
	//文库专有函数
	public function get_share_doc($page=1,$listRows=20){
		$data=$this->page($page.','.$listRows)->field('id shareid')->where(array('doctype'=>array('in',C('BOOK'))))->select();
		if ($data) {
			M('wenku')->addAll($data);
			return 1;
		}else{
			return 0;
		}
	
	}
	
	
	/**
	 * 多线成采集分享资源
	 * @param 线程数量 $thread
	 * @return string  */
	public function multiple_album($thread=10){
		set_time_limit(300);
		$albums=M('album')->where(array('status'=>0,'filecount'=>array('lt',61)))->limit($thread)->select();
		//初始化curl多线程
		$mh = curl_multi_init();
		foreach ($albums as $i => $album) {
			$referer='http://yun.baidu.com/pcloud/album/info?uk='.$album['uk'].'&album_id='.$album['album_id'];
			$url="http://yun.baidu.com/pcloud/album/listfile?album_id=".$album['album_id']."&query_uk=".$album['uk']."&start=0&limit=60&bdstoken=fabf54083df61c3328dc3f55b3bef5b2&channel=chunlei&clienttype=0&web=1";
			$conn[$i]=curl_init($url);
			curl_setopt($conn[$i], CURLOPT_REFERER,$referer);//来路
			curl_setopt($conn[$i],CURLOPT_RETURNTRANSFER,1);
			curl_setopt($conn[$i], CURLOPT_URL,$url);
			curl_setopt($conn[$i], CURLOPT_HEADER, 0);
			curl_multi_add_handle ($mh,$conn[$i]);
		}
		do { $n=curl_multi_exec($mh,$active); } while ($active);
		foreach ($albums as $i => $album) {
			$res[$i]=curl_multi_getcontent($conn[$i]);
			$rt=$this->parse_album_json($res[$i]);
			$uk[]=$album['uk'];
			curl_close($conn[$i]);
		}
		M('album')->where(array('uk'=>array('in',implode(',', $uk))))->save(array('status'=>1));
		return true;
	}
	
	
	
	private function parse_album_json($json){
		$content=json_decode($json);//此处采用snoopy
		if (is_object($content) && $content->errno==0){
			foreach ($content->list as $v){
				$info = pathinfo($v->server_filename);
				$doc_type=$info['extension'];
				if ($this->check_album_source($album_id,$v->server_filename)==0) {
					$source[]=array(
							'album_id'=>$album_id,
							'server_filename'=>$v->server_filename,
							'doctype'=>$doc_type,
							'size'=>$v->size,
							'uk'=>$uk,
							'fs_id'=>$v->fs_id,
							'time_stamp'=>$v->add_time,
							'status'=>0,
							'recommend'=>0,
							'time'=>time(),
							'click'=>rand(100, 500),
							'tCnt'=>$v->tCnt,
							'vCnt'=>$v->vCnt,
							'dCnt'=>$v->dCnt
					);
				}
			}
		}
		echo '<br/>';
		print_r($source);
		return true;
	}
	
	
	
	
	
	
	
	
	
	
	
}
<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title><?php echo ($title); ?>_HC-CMS</title>
		<!-- 新 Bootstrap 核心 CSS 文件 -->
		<link rel="stylesheet" href="/newwangpan/Public/Admin/css/bootstrap.min.css">
		<!-- 可选的Bootstrap主题文件（一般不用引入） -->
		<link rel="stylesheet" href="/newwangpan/Public/Admin/css/bootstrap-theme.min.css">
		<link rel="stylesheet" href="/newwangpan/Public/css/admin.css">
		<!-- jQuery文件。务必在bootstrap.min.js 之前引入 -->
		<script src="/newwangpan/Public/Admin/js/jquery.min.js"></script>
		<!-- 最新的 Bootstrap 核心 JavaScript 文件 -->
		<script src="/newwangpan/Public/Admin/js/bootstrap.min.js"></script>
		<style type="text/css">
			
		</style>
	</head>
	<body>
		<nav id="top_nav" class="navbar navbar-inverse navbar-fixed-top" role="navigation">
	      <div class="container-fluid">
	        <div class="navbar-header">
	          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
	            <span class="sr-only">Toggle navigation</span>
	            <span class="icon-bar"></span>
	            <span class="icon-bar"></span>
	            <span class="icon-bar"></span>
	          </button>
	          <a class="navbar-brand" href="#">HC-CMS后台管理面板</a>
	        </div>
	        <div id="navbar" class="navbar-collapse collapse">
	          <ul class="nav navbar-nav navbar-right">
	            <li><a href="#">当前用户：<?php echo ($_SESSION['adm_arr']['name']); ?></a></li>
	            <li><a href="javascript:void(0);" data-toggle="modal" data-target="#setAdmin_modal">设置</a></li>
	            <li><a href="<?php echo U('Login/logout');?>">退出</a></li>
	            <li><a href="<?php echo U('Home/Index/index');?>" target="_blank">前台首页</a></li>
	            <li> </li>
	          </ul>
	        </div>
	      </div>
	    </nav>

	    <div class="container-fluid">
	    	<div class="row">
        <div class="col-sm-3 col-md-2 sidebar">
          <ul class="nav nav-sidebar">
	      	<li <?php if(CONTROLLER_NAME== 'Index'): ?>class="active"<?php endif; ?>><a href="<?php echo U('Index/index');?>">首页</a></li>
	      	<li <?php if(CONTROLLER_NAME== 'Sys'): ?>class="active"<?php endif; ?>><a href="<?php echo U('Sys/index');?>">系统设置</a></li>
	      	<li <?php if(CONTROLLER_NAME== 'User'): ?>class="active"<?php endif; ?>><a href="<?php echo U('User/index');?>">资源达人</a></li>
	     	<li <?php if(CONTROLLER_NAME== 'Source'): ?>class="active"<?php endif; ?>><a href="<?php echo U('Source/index');?>">资源管理</a></li>
	      	<li <?php if(CONTROLLER_NAME== 'News'): ?>class="active"<?php endif; ?>><a href="<?php echo U('News/index');?>">资讯管理</a></li>
	      <!-- 	<li <?php if(CONTROLLER_NAME== 'Report'): ?>class="active"<?php endif; ?>><a href="<?php echo U('Report/index');?>">举报管理</a></li> 举报管理，后面再搭建-->
	      	<li <?php if(CONTROLLER_NAME== 'Tag'): ?>class="active"<?php endif; ?>><a href="<?php echo U('Tag/index');?>">标签管理</a></li>
	      	<li <?php if(CONTROLLER_NAME== 'Ad'): ?>class="active"<?php endif; ?>><a href="<?php echo U('Ad/index');?>">广告管理</a></li>
	      	<li <?php if(CONTROLLER_NAME== 'Friendlink'): ?>class="active"<?php endif; ?>><a href="<?php echo U('Friendlink/index');?>">友链管理</a></li>
	      	<li <?php if(CONTROLLER_NAME== 'Word'): ?>class="active"<?php endif; ?>><a href="<?php echo U('Word/index');?>">搜索管理</a></li>
	      	<li <?php if(CONTROLLER_NAME== 'Discuz'): ?>class="active"<?php endif; ?>><a href="<?php echo U('Discuz/index');?>">发帖机器人</a></li>
<!-- 	      	<li <?php if(CONTROLLER_NAME== 'Vdisk'): ?>class="active"<?php endif; ?>><a href="<?php echo U('Vdisk/index');?>">新浪微盘</a></li>
 -->	      	
 			<li <?php if(CONTROLLER_NAME== 'Yunpan'): ?>class="active"<?php endif; ?>><a href="<?php echo U('Yunpan/index');?>">360云盘</a></li>
	      	<li <?php if(CONTROLLER_NAME== 'Oof'): ?>class="active"<?php endif; ?>><a href="<?php echo U('Oof/index');?>">115网盘</a></li>
	      	<li <?php if(CONTROLLER_NAME== 'Ivdisk'): ?>class="active"<?php endif; ?>><a href="<?php echo U('Ivdisk/index');?>">新浪微盘</a></li>
	      	
	      	<li <?php if(CONTROLLER_NAME== 'Wenku'): ?>class="active"<?php endif; ?>><a href="<?php echo U('Wenku/index');?>">文库</a></li>
	      	
	      	
          </ul>
        </div>
        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
        	<style>
.source_name{width:20%;}
td.source_name{text-align:left;}
div.row div.position_block{border:1px solid #eee;display:block;width:150px;font-size:12px;margin:5px 10px;text-align:center;padding:10px;float:left;}
div.row div.position_block:hover{background:#F5F9FD;}
#hidden-position{display: none;}
</style>
<div class="row">
<a href="<?php echo U('Ad/index');?>"><button type="button" class="btn btn-primary  active">广告位</button></a>
<a href="<?php echo U('Ad/adlist');?>"><button type="button" class="btn btn-primary">所有广告</button></a>
<a href="<?php echo U('Ad/mobile_ad');?>"><button type="button" class="btn btn-primary">手机广告</button></a>

</div>

<h3>手机广告</h3>
<form class="form-horizontal" method="post" action="<?php echo U('Ad/mobile_ad');?>" id="mobile_ad_form">
	<div class="form-group">
		<div class="col-sm-6">
			<input type="text" name="name" id="name" value="" placeholder="请输入广告名称" class="form-control"/>
		</div>
	</div>
	<div class="form-group">
		<div class="col-sm-6">
			<select name="position" class="form-control" id="mobile_ad_position">
				<option value="0">手机广告位置选择</option>
				<option value="global_top">global_top</option>
				<option value="global_bottom">global_bottom</option>
				<option value="global_footer">global_footer</option>
				<option value="detail_top">detail_top</option>
				<option value="detail_bottom">detail_bottom</option>
			</select>
		</div>
	</div>
	<div class="form-group">
		<div class="col-sm-6">
			<textarea name="code" rows="3" class="form-control" placeholder="广告代码"></textarea>
		</div>
	</div>
	<input type="button" value="提交广告" class="btn btn-success" id="mobile_ad_btn"/>
</form>

<hr />

<div class="row" style="margin-top:30px;">
	<!-- 广告位 -->
	<div class="col-xs-4">	
		<div class="position_block">
			<img alt="" src="/newwangpan/Public/Admin/img/ad_headerbanner.gif">
			<br/>
			<a href="#" class="add_ad" data-type="global" data-position="1">全局  顶部广告</a>
		</div>
	</div>
	<div class="col-xs-4">	
		<div class="position_block">
			<img alt="" src="/newwangpan/Public/Admin/img/ad_footerbanner.gif">
			<br/>
			<a href="#" class="add_ad"  data-type="global" data-position="2">全局  底部广告</a>
		</div>
	</div>
	
	<div class="col-xs-4">	
		<div class="position_block">
			<img alt="" src="/newwangpan/Public/Admin/img/ad_nav.gif">
			<br/>
			<a href="#" class="add_ad"  data-type="nav" data-position="1">全局  导航栏广告</a>
		</div>
	</div>
	
	<div class="col-xs-4">	
		<div class="position_block">
			<img alt="" src="/newwangpan/Public/Admin/img/ad_threadlist.gif">
			<br/>
			<a href="#" class="add_ad"  data-type="index" data-position="1">首页 通栏广告</a>
		</div>
	</div>
	
	
	<div class="col-xs-4">			
		<div class="position_block">
			<img alt="" src="/newwangpan/Public/Admin/img/ad_left.gif">
			<br/>
			<a href="#" class="add_ad" data-type="detail" data-position="1">详情  左侧广告</a>
		</div>
	</div>
	<div class="col-xs-4">	
		<div class="position_block">
			<img alt="" src="/newwangpan/Public/Admin/img/ad_threadlist.gif">
			<br/>
			<a href="#" class="add_ad"  data-type="detail" data-position="2">详情  通栏广告</a>
		</div>
	</div>
	
	<div class="col-xs-4">
		<div class="position_block">
			<img alt="" src="/newwangpan/Public/Admin/img/ad_nav_left.png">
			<br/>
			<a href="#" class="add_ad"   data-type="common" data-position="1">普通 左侧广告</a>
		</div>
	</div>

	<div class="col-xs-4">	
		<div class="position_block">
			<img alt="" src="/newwangpan/Public/Admin/img/ad_search.gif">
			<br/>
			<a href="#" class="add_ad"  data-type="so" data-position="1">搜索 右侧广告</a>
		</div>
	</div>
	
</div>





<!-- 模态框（Modal） -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
   <div class="modal-dialog">
      <div class="modal-content">
      	<div class="modal-header">
            <h4 class="modal-title" id="myModalLabel">模态框（Modal）标题</h4>
         </div>
         <div class="modal-body">
         <!-- 
         common :video,music
          -->
			<form class="form-horizontal" role="form">
			   <div class="form-group">
			      <label for="firstname" class="col-sm-2 control-label">广告名称</label>
			      <div class="col-sm-10">
			         <input type="text" class="form-control reset" name="name" id="name" placeholder="请输入广告名称">
			      </div>
			   </div>
			   <div class="form-group">
			      <label for="lastname" class="col-sm-2 control-label">广告代码</label>
			      <div class="col-sm-10">
			        	<textarea id="code" name="code" class="form-control reset" rows="3"></textarea>
			      </div>
			   </div>
			    <div class="form-group">
			      <label for="lastname" class="col-sm-2 control-label">起止时间</label>
			      <div class="col-sm-5">
			        	<input type="text" name="start_time" class="form-control reset" id="start_time">
			      </div>
			      <div class="col-sm-5">
			        	<input type="text" name="end_time"   class="form-control reset" id="end_time">
			      </div>
			   </div>
			   <div class="form-group" id="hidden-position" style="display: none;">
			      <label for="position" class="col-sm-2 control-label">广告位置</label>
			      <div class="col-sm-10">
			      	<select name="position" class="form-control reset" id="position">
			      		<option value="1" selected="selected">通栏1</option>
			      		<option value="2">通栏2</option>
			      		<option value="3">通栏3</option>
			      		<option value="4">通栏4</option>
			      	</select>
			      	<p class="help-block">首页通栏广告,位置由下到上从通栏1到通栏4</p>
			      </div>
			   </div>
			   
			   
			   <!--<input type="hidden" name="position" class="form-control reset" id="position">-->
			   <input type="hidden" name="type" class="form-control reset" id="type">
			   
			</form>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-default close_modal" data-dismiss="modal">关闭
            </button>
            <button type="button" id="add_ad_btn" class="btn btn-primary">添加</button>
         </div>
      </div><!-- /.modal-content -->
</div><!-- /.modal -->
</div>
<link rel="stylesheet" type="text/css" href="/newwangpan/Public/datepicker/jquery.datetimepicker.css"/ >
<script src="/newwangpan/Public/datepicker/jquery.datetimepicker.js"></script>
<script>
$(function(){
	$('#start_time,#end_time').datetimepicker({
		lang:'ch',
	});
	$(".add_ad").click(function(){
		var type=$(this).attr('data-type');//广告类型
		if (type=='index') {
			$('#hidden-position').show();
		}else{
			$('#hidden-position').hide();
//			$("#position").value('1');
		}
		var position=$(this).attr('data-position');
		var title=$(this).text();
		$("#position").val(position);
		$("#type").val(type);
		$("#myModalLabel").text(title);
		$('#myModal').modal('show');
	});
	$("#add_ad_btn").click(function(){
//		if($("#name").val()=='' || $("#type").val()=='' || $("#start_time").val()=='' || $("#end_time").val()==''){
//			alert('广告名称、广告代码、起止时间均是必填项，请填写完毕。');
//		}else{
		var data = $("form").serialize();
		$.post("<?php echo U('Ad/index');?>",data,function(d){
			if(d==1){
				alert('恭喜您，广告添加成功！');
				$('.reset').val('');
				location.reload();
			}else{
				alert('广告添加失败，请重试！');
			}
		});
//		}
	});
	
	$("#mobile_ad_btn").click(function(){
		if ($("#mobile_ad_position").val()==0) {
			alert('请选择广告位置。');
			return false;
		} else{
			$("#mobile_ad_form").submit();
		}
	});
	
	$('.close_modal').click(function(){
		$('.reset').val('');
	});
	
});

</script>

          </div>
        </div>
      </div>
      
      <!-- 模态框（Modal） 设置管理员信息-->
<div class="modal fade" id="setAdmin_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
   <div class="modal-dialog">
      <div class="modal-content">
         <div class="modal-body">
         <form role="form" id="setAdmin_form" method="post" action="<?php echo U('Sys/updateAdmin');?>">
         	<div class="form-group">
    			<label for="admin">管理员</label>
    			<input type="text" class="form-control" id="admin" name="admin" value="<?php echo ($_SESSION['adm_arr']['name']); ?>" placeholder="为空，则管理员账号默认为admin">
  			</div>
  			<div class="form-group">
    			<label for="opwd">原密码</label>
    			<input type="text" class="form-control" id="opwd" name="opwd" value="" placeholder="您如果需要修改登陆密码，请在此处输入原密码">
  			</div>
  			<div class="form-group">
    			<label for="npwd">新密码</label>
    			<input type="text" class="form-control" id="npwd" name="npwd" placeholder="您如果需要修改登陆密码，请在此处输入新密码">
  			</div>
  			<div class="form-group">
    			<label for="phone">手机号码</label>
    			<input type="text" class="form-control" id="phone" name="phone" value="<?php echo ($_SESSION['adm_arr']['phone']); ?>" placeholder="【此处输入的必须是Email，否则会造成无法登陆管理后台的情况】">
  			</div>
		</form>
         </div>
         
         <div class="modal-footer">
            <button type="button" class="btn btn-default" 
               data-dismiss="modal">关闭
            </button>
            <button type="button" id="updateAdminBtn" class="btn btn-primary">确定修改</button>
         </div>
      </div><!-- /.modal-content -->
</div><!-- /.modal -->
</div>
<script>
$(function(){
	$("#updateAdminBtn").click(function(){
		$("#setAdmin_form").submit();
	});
})

</script>
	</body>
</html>
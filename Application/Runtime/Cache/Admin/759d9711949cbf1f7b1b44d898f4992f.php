<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title><?php echo ($title); ?>_HC-CMS</title>
		<!-- 新 Bootstrap 核心 CSS 文件 -->
		<link rel="stylesheet" href="/newwangpan/Public/Admin/css/bootstrap.min.css">
		<!-- 可选的Bootstrap主题文件（一般不用引入） -->
		<link rel="stylesheet" href="/newwangpan/Public/Admin/css/bootstrap-theme.min.css">
		<link rel="stylesheet" href="/newwangpan/Public/css/admin.css">
		<!-- jQuery文件。务必在bootstrap.min.js 之前引入 -->
		<script src="/newwangpan/Public/Admin/js/jquery.min.js"></script>
		<!-- 最新的 Bootstrap 核心 JavaScript 文件 -->
		<script src="/newwangpan/Public/Admin/js/bootstrap.min.js"></script>
		<style type="text/css">
			
		</style>
	</head>
	<body>
		<nav id="top_nav" class="navbar navbar-inverse navbar-fixed-top" role="navigation">
	      <div class="container-fluid">
	        <div class="navbar-header">
	          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
	            <span class="sr-only">Toggle navigation</span>
	            <span class="icon-bar"></span>
	            <span class="icon-bar"></span>
	            <span class="icon-bar"></span>
	          </button>
	          <a class="navbar-brand" href="#">HC-CMS后台管理面板</a>
	        </div>
	        <div id="navbar" class="navbar-collapse collapse">
	          <ul class="nav navbar-nav navbar-right">
	            <li><a href="#">当前用户：<?php echo ($_SESSION['adm_arr']['name']); ?></a></li>
	            <li><a href="javascript:void(0);" data-toggle="modal" data-target="#setAdmin_modal">设置</a></li>
	            <li><a href="<?php echo U('Login/logout');?>">退出</a></li>
	            <li><a href="<?php echo U('Home/Index/index');?>" target="_blank">前台首页</a></li>
	            <li> </li>
	          </ul>
	        </div>
	      </div>
	    </nav>

	    <div class="container-fluid">
	    	<div class="row">
        <div class="col-sm-3 col-md-2 sidebar">
          <ul class="nav nav-sidebar">
	      	<li <?php if(CONTROLLER_NAME== 'Index'): ?>class="active"<?php endif; ?>><a href="<?php echo U('Index/index');?>">首页</a></li>
	      	<li <?php if(CONTROLLER_NAME== 'Sys'): ?>class="active"<?php endif; ?>><a href="<?php echo U('Sys/index');?>">系统设置</a></li>
	      	<li <?php if(CONTROLLER_NAME== 'User'): ?>class="active"<?php endif; ?>><a href="<?php echo U('User/index');?>">资源达人</a></li>
	     	<li <?php if(CONTROLLER_NAME== 'Source'): ?>class="active"<?php endif; ?>><a href="<?php echo U('Source/index');?>">资源管理</a></li>
	      	<li <?php if(CONTROLLER_NAME== 'News'): ?>class="active"<?php endif; ?>><a href="<?php echo U('News/index');?>">资讯管理</a></li>
	      <!-- 	<li <?php if(CONTROLLER_NAME== 'Report'): ?>class="active"<?php endif; ?>><a href="<?php echo U('Report/index');?>">举报管理</a></li> 举报管理，后面再搭建-->
	      	<li <?php if(CONTROLLER_NAME== 'Tag'): ?>class="active"<?php endif; ?>><a href="<?php echo U('Tag/index');?>">标签管理</a></li>
	      	<li <?php if(CONTROLLER_NAME== 'Ad'): ?>class="active"<?php endif; ?>><a href="<?php echo U('Ad/index');?>">广告管理</a></li>
	      	<li <?php if(CONTROLLER_NAME== 'Friendlink'): ?>class="active"<?php endif; ?>><a href="<?php echo U('Friendlink/index');?>">友链管理</a></li>
	      	<li <?php if(CONTROLLER_NAME== 'Word'): ?>class="active"<?php endif; ?>><a href="<?php echo U('Word/index');?>">搜索管理</a></li>
	      	<li <?php if(CONTROLLER_NAME== 'Forbidden'): ?>class="active"<?php endif; ?>><a href="<?php echo U('Forbidden/index');?>">屏蔽管理</a></li>
	      	<li <?php if(CONTROLLER_NAME== 'Discuz'): ?>class="active"<?php endif; ?>><a href="<?php echo U('Discuz/index');?>">发帖机器人</a></li>
<!-- 	      	<li <?php if(CONTROLLER_NAME== 'Vdisk'): ?>class="active"<?php endif; ?>><a href="<?php echo U('Vdisk/index');?>">新浪微盘</a></li>
 -->	      	
 			<li <?php if(CONTROLLER_NAME== 'Yunpan'): ?>class="active"<?php endif; ?>><a href="<?php echo U('Yunpan/index');?>">360云盘</a></li>
	      	<li <?php if(CONTROLLER_NAME== 'Oof'): ?>class="active"<?php endif; ?>><a href="<?php echo U('Oof/index');?>">115网盘</a></li>
	      	<li <?php if(CONTROLLER_NAME== 'Ivdisk'): ?>class="active"<?php endif; ?>><a href="<?php echo U('Ivdisk/index');?>">新浪微盘</a></li>
	      	
	      	<li <?php if(CONTROLLER_NAME== 'Wenku'): ?>class="active"<?php endif; ?>><a href="<?php echo U('Wenku/index');?>">文库</a></li>
	      	
	      	
          </ul>
        </div>
        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
        	<style>
	.logo img{cursor: pointer;max-width: 300px;max-height: 150px;}
</style>
<div class="row" style="margin-bottom: 20px;border-bottom: 1px solid #EEEEEE;">
<a href="<?php echo U('Sys/index');?>"><button type="button" class="btn btn-primary active">网站配置</button></a>
<a href="<?php echo U('Sys/seo');?>"><button type="button" class="btn btn-primary">SEO优化</button></a>
<a href="<?php echo U('Sys/nav');?>"><button type="button" class="btn btn-primary">导航管理</button></a>
<a href="<?php echo U('Sys/updateCount');?>"><button type="button" class="btn btn-primary">更新统计</button></a>
<a href="<?php echo U('Sys/keyworld');?>"><button type="button" class="btn btn-primary">关键词生成</button></a>
<a href="<?php echo U('Sitemap/index');?>"><button type="button" class="btn btn-primary">生成地图</button></a>
</div>
<div class="row">
	<div class="panel panel-primary">
	   <div class="panel-heading">
	      <h3 class="panel-title">站点设置</h3>
	   </div>
	   <div class="panel-body">
	      <form role="form" method="post" id="setting_form" action="<?php echo U('Sys/index');?>" enctype="multipart/form-data">
			   <div class="form-group">
			      <label for="sitename">网站名称</label>
			      <input type="hidden" name="id" value="<?php echo ($data['id']); ?>"/>
			      <input type="text" class="form-control" name="sitename" id="sitename" placeholder="请输入站点名称" value="<?php echo ($data['sitename']); ?>">
			   </div>
			   <label for="name">网站状态</label>
				<div class="form-group">
				   <label class="checkbox-inline">
				      <input type="radio" name="isopen" value="1" <?php if($data['isopen'] == 1): ?>checked="checked"<?php endif; ?> id="open">打开
				   </label>
				   <label class="checkbox-inline">
				      <input type="radio" name="isopen" value="0" <?php if($data['isopen'] == 0): ?>checked="checked"<?php endif; ?> id="close">关闭
				   </label>
				</div>
			<div class="form-group close_show" style="display: none;">
			    <label for="statement">站点关闭说明</label>
			    <textarea class="form-control" rows="3" id="statement" name="statement"><?php echo ($data['statement']); ?></textarea>
			  </div>
			  <div class="form-group">
			    <label for="tongji">站点统计代码</label>
			    <textarea class="form-control" rows="3" id="tongji" name="tongji"><?php echo ($data['tongji']); ?></textarea>
			  </div>
			  <div class="form-group">
			    <label for="copyright">底部声明[支持HTML代码]</label>
			    <span class="help-block">如果使用HTML代码，建议使用百分比的形式限定DIV等HTML标签以适应响应式布局</span>
			    <textarea class="form-control" rows="3" id="copyright" name="copyright"><?php echo ($data['copyright']); ?></textarea>
			  </div>
			  <div class="form-group">
			      <label for="beian">备案号</label>
			      <input type="text" class="form-control" name="beian" id="beian" placeholder="请输入网站备案号" value="<?php echo ($data['beian']); ?>">
			   </div>
			  <div class="form-group">
					<button type="button" id="submitBtn" class="btn btn-default">提交设置</button>
			  </div>
			 <!--  <button type="submit" class="btn btn-default">提交</button>-->
			</form>
	   </div>
	</div>
	
	
</div>
<script>
	$(function(){
		$('.logo').click(function(){
			$("#logo").trigger('click');
		});
		
		/*站点开关*/
		$('#open').click(function(){
			$('.close_show').hide();
		});
		$('#close').click(function(){
			$('.close_show').show();
		});
		
		$('#submitBtn').click(function(){
			if ($('#sitename').val()=='') {
				alert('站点名称不能为空');
				return false;
			} else{
				$('#setting_form').submit();
			}
		});
		
		
		
		
		
	});
</script>

          </div>
        </div>
      </div>
      
      <!-- 模态框（Modal） 设置管理员信息-->
<div class="modal fade" id="setAdmin_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
   <div class="modal-dialog">
      <div class="modal-content">
         <div class="modal-body">
         <form role="form" id="setAdmin_form" method="post" action="<?php echo U('Sys/updateAdmin');?>">
         	<div class="form-group">
    			<label for="admin">管理员</label>
    			<input type="text" class="form-control" id="admin" name="admin" value="<?php echo ($_SESSION['adm_arr']['name']); ?>" placeholder="为空，则管理员账号默认为admin">
  			</div>
  			<div class="form-group">
    			<label for="opwd">原密码</label>
    			<input type="text" class="form-control" id="opwd" name="opwd" value="" placeholder="您如果需要修改登陆密码，请在此处输入原密码">
  			</div>
  			<div class="form-group">
    			<label for="npwd">新密码</label>
    			<input type="text" class="form-control" id="npwd" name="npwd" placeholder="您如果需要修改登陆密码，请在此处输入新密码">
  			</div>
  			<div class="form-group">
    			<label for="phone">手机号码</label>
    			<input type="text" class="form-control" id="phone" name="phone" value="<?php echo ($_SESSION['adm_arr']['phone']); ?>" placeholder="【此处输入的必须是Email，否则会造成无法登陆管理后台的情况】">
  			</div>
		</form>
         </div>
         
         <div class="modal-footer">
            <button type="button" class="btn btn-default" 
               data-dismiss="modal">关闭
            </button>
            <button type="button" id="updateAdminBtn" class="btn btn-primary">确定修改</button>
         </div>
      </div><!-- /.modal-content -->
</div><!-- /.modal -->
</div>
<script>
$(function(){
	$("#updateAdminBtn").click(function(){
		$("#setAdmin_form").submit();
	});
})

</script>
	</body>
</html>
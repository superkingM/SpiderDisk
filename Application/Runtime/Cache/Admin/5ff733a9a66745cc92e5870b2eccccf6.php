<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title><?php echo ($title); ?>_HC-CMS</title>
		<!-- 新 Bootstrap 核心 CSS 文件 -->
		<link rel="stylesheet" href="/newwangpan/Public/Admin/css/bootstrap.min.css">
		<!-- 可选的Bootstrap主题文件（一般不用引入） -->
		<link rel="stylesheet" href="/newwangpan/Public/Admin/css/bootstrap-theme.min.css">
		<link rel="stylesheet" href="/newwangpan/Public/css/admin.css">
		<!-- jQuery文件。务必在bootstrap.min.js 之前引入 -->
		<script src="/newwangpan/Public/Admin/js/jquery.min.js"></script>
		<!-- 最新的 Bootstrap 核心 JavaScript 文件 -->
		<script src="/newwangpan/Public/Admin/js/bootstrap.min.js"></script>
		<style type="text/css">
			
		</style>
	</head>
	<body>
		<nav id="top_nav" class="navbar navbar-inverse navbar-fixed-top" role="navigation">
	      <div class="container-fluid">
	        <div class="navbar-header">
	          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
	            <span class="sr-only">Toggle navigation</span>
	            <span class="icon-bar"></span>
	            <span class="icon-bar"></span>
	            <span class="icon-bar"></span>
	          </button>
	          <a class="navbar-brand" href="#">HC-CMS后台管理面板</a>
	        </div>
	        <div id="navbar" class="navbar-collapse collapse">
	          <ul class="nav navbar-nav navbar-right">
	            <li><a href="#">当前用户：<?php echo ($_SESSION['adm_arr']['name']); ?></a></li>
	            <li><a href="javascript:void(0);" data-toggle="modal" data-target="#setAdmin_modal">设置</a></li>
	            <li><a href="<?php echo U('Login/logout');?>">退出</a></li>
	            <li><a href="<?php echo U('Home/Index/index');?>" target="_blank">前台首页</a></li>
	            <li> </li>
	          </ul>
	        </div>
	      </div>
	    </nav>

	    <div class="container-fluid">
	    	<div class="row">
        <div class="col-sm-3 col-md-2 sidebar">
          <ul class="nav nav-sidebar">
	      	<li <?php if(CONTROLLER_NAME== 'Index'): ?>class="active"<?php endif; ?>><a href="<?php echo U('Index/index');?>">首页</a></li>
	      	<li <?php if(CONTROLLER_NAME== 'Sys'): ?>class="active"<?php endif; ?>><a href="<?php echo U('Sys/index');?>">系统设置</a></li>
	      	<li <?php if(CONTROLLER_NAME== 'User'): ?>class="active"<?php endif; ?>><a href="<?php echo U('User/index');?>">资源达人</a></li>
	     	<li <?php if(CONTROLLER_NAME== 'Source'): ?>class="active"<?php endif; ?>><a href="<?php echo U('Source/index');?>">资源管理</a></li>
	      	<li <?php if(CONTROLLER_NAME== 'News'): ?>class="active"<?php endif; ?>><a href="<?php echo U('News/index');?>">资讯管理</a></li>
	      <!-- 	<li <?php if(CONTROLLER_NAME== 'Report'): ?>class="active"<?php endif; ?>><a href="<?php echo U('Report/index');?>">举报管理</a></li> 举报管理，后面再搭建-->
	      	<li <?php if(CONTROLLER_NAME== 'Tag'): ?>class="active"<?php endif; ?>><a href="<?php echo U('Tag/index');?>">标签管理</a></li>
	      	<li <?php if(CONTROLLER_NAME== 'Ad'): ?>class="active"<?php endif; ?>><a href="<?php echo U('Ad/index');?>">广告管理</a></li>
	      	<li <?php if(CONTROLLER_NAME== 'Friendlink'): ?>class="active"<?php endif; ?>><a href="<?php echo U('Friendlink/index');?>">友链管理</a></li>
	      	<li <?php if(CONTROLLER_NAME== 'Word'): ?>class="active"<?php endif; ?>><a href="<?php echo U('Word/index');?>">搜索管理</a></li>
	      	<li <?php if(CONTROLLER_NAME== 'Discuz'): ?>class="active"<?php endif; ?>><a href="<?php echo U('Discuz/index');?>">发帖机器人</a></li>
<!-- 	      	<li <?php if(CONTROLLER_NAME== 'Vdisk'): ?>class="active"<?php endif; ?>><a href="<?php echo U('Vdisk/index');?>">新浪微盘</a></li>
 -->	      	
 			<li <?php if(CONTROLLER_NAME== 'Yunpan'): ?>class="active"<?php endif; ?>><a href="<?php echo U('Yunpan/index');?>">360云盘</a></li>
	      	<li <?php if(CONTROLLER_NAME== 'Oof'): ?>class="active"<?php endif; ?>><a href="<?php echo U('Oof/index');?>">115网盘</a></li>
	      	<li <?php if(CONTROLLER_NAME== 'Ivdisk'): ?>class="active"<?php endif; ?>><a href="<?php echo U('Ivdisk/index');?>">新浪微盘</a></li>
	      	
	      	<li <?php if(CONTROLLER_NAME== 'Wenku'): ?>class="active"<?php endif; ?>><a href="<?php echo U('Wenku/index');?>">文库</a></li>
	      	
	      	
          </ul>
        </div>
        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
        	<style>
	input{border: 1px solid #DDDDDD;border-radius: 5px;}
</style>
<div class="row" style="margin-bottom: 20px;border-bottom: 1px solid #EEEEEE;">
<a href="<?php echo U('Sys/index');?>"><button type="button" class="btn btn-primary">网站配置</button></a>
<a href="<?php echo U('Sys/seo');?>"><button type="button" class="btn btn-primary">SEO优化</button></a>
<a href="<?php echo U('Sys/nav');?>"><button type="button" class="btn btn-primary active">导航管理</button></a>
<a href="<?php echo U('Sys/updateCount');?>"><button type="button" class="btn btn-primary">更新统计与数据</button></a>
<a href="<?php echo U('Sitemap/index');?>"><button type="button" class="btn btn-primary">生成站点地图</button></a>
</div>
<div class="row">
	<div class="panel panel-primary">
	   <div class="panel-body">
	   	<form action="" class="subnav" method="post">
	      <table class="table table-hover">
			   <thead>
			      <tr>
			         <th>排序</th>
			         <th>名称</th>
			         <th>网址</th>
			         <th>状态</th>
			         <th>操作</th>
			      </tr>
			   </thead>
			   <tbody>
			   	<?php if(is_array($data)): $i = 0; $__LIST__ = $data;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$data): $mod = ($i % 2 );++$i;?><tr>
			         <td><input type="number" name="sort[]" value="<?php echo ($data['sort']); ?>"/></td>
			         <td><input type="text" name="name[]" value="<?php echo ($data['name']); ?>"/></td>
			         <td>
			         	<input type="hidden" name="id[]" value="<?php echo ($data['id']); ?>"/>
			         	<input type="hidden" name="flag[]" value="<?php echo ($data['flag']); ?>" />
			         		<?php switch($data["flag"]): case "index": ?><a href="<?php echo U('Home/Index/index');?>" target="_blank">查看</a><?php break;?>
								<?php case "video": ?><a href="<?php echo U('Home/Video/index');?>" target="_blank">查看</a><?php break;?>
								<?php case "music": ?><a href="<?php echo U('Home/Music/index');?>" target="_blank">查看</a><?php break;?>
								<?php case "album": ?><a href="<?php echo U('Home/Album/index');?>" target="_blank">查看</a><?php break;?>
								<?php case "torrent": ?><a href="<?php echo U('Home/Torrent/index');?>" target="_blank">查看</a><?php break;?>
								<?php case "software": ?><a href="<?php echo U('Home/Software/index');?>" target="_blank">查看</a><?php break;?>
								<?php case "tag": ?><a href="<?php echo U('Home/Tag/index');?>" target="_blank">查看</a><?php break;?>
								<?php case "news": ?><a href="<?php echo U('Home/News/index');?>" target="_blank">查看</a><?php break;?>
								<?php case "user": ?><a href="<?php echo U('Home/User/index');?>" target="_blank">查看</a><?php break;?>
								<?php case "book": ?><a href="<?php echo U('Home/Book/index');?>" target="_blank">查看</a><?php break;?>
								<?php case "zip": ?><a href="<?php echo U('Home/Zip/index');?>" target="_blank">查看</a><?php break;?>
								<?php default: ?><a href="<?php echo ($data['site']); ?>" target="_blank">查看</a><?php endswitch;?>
			         	<!--<a href="<?php echo U('Home/Index/index');?>" target="_blank">查看</a>-->
			         </td>
			         <td>
			         	<?php if($data["isopen"] == 1): ?><span style="color: green;">开启</span>
			         	<?php else: ?>
			         		<span style="color: red;">关闭</span><?php endif; ?>
			         </td>
			         <td>
			         	<?php if($data["isopen"] == 1): ?><a href="javascript:void(0);" class="closeBtn" data-id="<?php echo ($data['id']); ?>">关闭</a> 
			         	<?php else: ?>
			         		<a href="javascript:void(0);" class="open" data-id="<?php echo ($data['id']); ?>">开启</a><?php endif; ?>
			         	<?php if($data["flag"] == 'diy'): ?><a href="javascript:void(0);" class="delete" data-id="<?php echo ($data['id']); ?>">删除</a><?php endif; ?>
			         </td>
			      </tr><?php endforeach; endif; else: echo "" ;endif; ?>
			   </tbody>
			</table>
	   	</form>
	   	<div class="col-xs-12 text-right">
	   		<button class="btn btn-default" id="add">新增导航</button> 
			<button class="btn btn-default" id="submitBtn">提交修改</button>
	   	</div>
	   </div>
	</div>
</div>
<!--模态框-->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" 
   aria-labelledby="myModalLabel" aria-hidden="true">
   <div class="modal-dialog">
      <div class="modal-content">
         <div class="modal-body">
         	<form method="post" class="addnav" role="form" action="<?php echo U('Sys/addNav');?>">
			   <div class="form-group">
			      <label for="addname">名称</label>
			      <input type="text" class="form-control" name="name" id="addname" placeholder="请输入导航名称">
			   </div>
			   <div class="form-group">
			      <label for="addsort">排序</label>
			      <input type="number" name="sort" class="form-control" id="addsort" value="0">
			   </div>
			   <div class="form-group">
			      <label for="addsite">网址</label>
			      <input type="text" class="form-control" name="site" id="addsite" placeholder="请输入网址">
			   </div>
			   <div class="form-group">
			   	 <label for="addsite">状态</label>
				    <label class="checkbox-inline">
				      <input type="radio" name="isopen" value="1" checked>开启
				   	</label>
				   	<label class="checkbox-inline">
				      <input type="radio" name="isopen" value="0">关闭
				   	</label>
			   </div>
         	</form>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
            <button type="button" class="btn btn-default addnavbtn">确定添加</button>
         </div>
      </div><!-- /.modal-content -->
</div><!-- /.modal -->
<script>
	$(function(){
		$('#submitBtn').click(function(){
			$('form.subnav').submit();
		});
		$('.closeBtn').click(function(){
			$.get("<?php echo U('Sys/nav_isopen');?>",{id:$(this).attr('data-id'),isopen:0},function(ret){
				if(ret==1){
					location.reload();
				}else{
					alert('关闭失败。如果当前页面为首页，则不能对当前页面进行关闭。');
				}
			});
		});
		$('.open').click(function(){
			$.get("<?php echo U('Sys/nav_isopen');?>",{id:$(this).attr('data-id'),isopen:1},function(ret){
				if(ret==1){
					location.reload();
				}else{
					alert('开启失败。。请刷新页面后重试');
				}
			});
		});
		
		$("#add").click(function(){
			$('#myModal').modal('show');
		});
		$('.addnavbtn').click(function(){
			$('form.addnav').submit();
		});
		
		$('.delete').click(function(){
			if(confirm('您确定要删除当前导航吗？')){
				$.get("<?php echo U('Sys/deleteNav');?>",{id:$(this).attr('data-id')},function(){
					location.reload();
				});
			}
		});
	});
</script>







          </div>
        </div>
      </div>
      
      <!-- 模态框（Modal） 设置管理员信息-->
<div class="modal fade" id="setAdmin_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
   <div class="modal-dialog">
      <div class="modal-content">
         <div class="modal-body">
         <form role="form" id="setAdmin_form" method="post" action="<?php echo U('Sys/updateAdmin');?>">
         	<div class="form-group">
    			<label for="admin">管理员</label>
    			<input type="text" class="form-control" id="admin" name="admin" value="<?php echo ($_SESSION['adm_arr']['name']); ?>" placeholder="为空，则管理员账号默认为admin">
  			</div>
  			<div class="form-group">
    			<label for="opwd">原密码</label>
    			<input type="text" class="form-control" id="opwd" name="opwd" value="" placeholder="您如果需要修改登陆密码，请在此处输入原密码">
  			</div>
  			<div class="form-group">
    			<label for="npwd">新密码</label>
    			<input type="text" class="form-control" id="npwd" name="npwd" placeholder="您如果需要修改登陆密码，请在此处输入新密码">
  			</div>
  			<div class="form-group">
    			<label for="phone">手机号码</label>
    			<input type="text" class="form-control" id="phone" name="phone" value="<?php echo ($_SESSION['adm_arr']['phone']); ?>" placeholder="【此处输入的必须是Email，否则会造成无法登陆管理后台的情况】">
  			</div>
		</form>
         </div>
         
         <div class="modal-footer">
            <button type="button" class="btn btn-default" 
               data-dismiss="modal">关闭
            </button>
            <button type="button" id="updateAdminBtn" class="btn btn-primary">确定修改</button>
         </div>
      </div><!-- /.modal-content -->
</div><!-- /.modal -->
</div>
<script>
$(function(){
	$("#updateAdminBtn").click(function(){
		$("#setAdmin_form").submit();
	});
})

</script>
	</body>
</html>
<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title><?php echo ($title); ?>_HC-CMS</title>
		<!-- 新 Bootstrap 核心 CSS 文件 -->
		<link rel="stylesheet" href="/newwangpan/Public/Admin/css/bootstrap.min.css">
		<!-- 可选的Bootstrap主题文件（一般不用引入） -->
		<link rel="stylesheet" href="/newwangpan/Public/Admin/css/bootstrap-theme.min.css">
		<link rel="stylesheet" href="/newwangpan/Public/css/admin.css">
		<!-- jQuery文件。务必在bootstrap.min.js 之前引入 -->
		<script src="/newwangpan/Public/Admin/js/jquery.min.js"></script>
		<!-- 最新的 Bootstrap 核心 JavaScript 文件 -->
		<script src="/newwangpan/Public/Admin/js/bootstrap.min.js"></script>
		<style type="text/css">
			
		</style>
	</head>
	<body>
		<nav id="top_nav" class="navbar navbar-inverse navbar-fixed-top" role="navigation">
	      <div class="container-fluid">
	        <div class="navbar-header">
	          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
	            <span class="sr-only">Toggle navigation</span>
	            <span class="icon-bar"></span>
	            <span class="icon-bar"></span>
	            <span class="icon-bar"></span>
	          </button>
	          <a class="navbar-brand" href="#">HC-CMS后台管理面板</a>
	        </div>
	        <div id="navbar" class="navbar-collapse collapse">
	          <ul class="nav navbar-nav navbar-right">
	            <li><a href="#">当前用户：<?php echo ($_SESSION['adm_arr']['name']); ?></a></li>
	            <li><a href="javascript:void(0);" data-toggle="modal" data-target="#setAdmin_modal">设置</a></li>
	            <li><a href="<?php echo U('Login/logout');?>">退出</a></li>
	            <li><a href="<?php echo U('Home/Index/index');?>" target="_blank">前台首页</a></li>
	            <li> </li>
	          </ul>
	        </div>
	      </div>
	    </nav>

	    <div class="container-fluid">
	    	<div class="row">
        <div class="col-sm-3 col-md-2 sidebar">
          <ul class="nav nav-sidebar">
	      	<li <?php if(CONTROLLER_NAME== 'Index'): ?>class="active"<?php endif; ?>><a href="<?php echo U('Index/index');?>">首页</a></li>
	      	<li <?php if(CONTROLLER_NAME== 'Sys'): ?>class="active"<?php endif; ?>><a href="<?php echo U('Sys/index');?>">系统设置</a></li>
	      	<li <?php if(CONTROLLER_NAME== 'User'): ?>class="active"<?php endif; ?>><a href="<?php echo U('User/index');?>">资源达人</a></li>
	     	<li <?php if(CONTROLLER_NAME== 'Source'): ?>class="active"<?php endif; ?>><a href="<?php echo U('Source/index');?>">资源管理</a></li>
	      	<li <?php if(CONTROLLER_NAME== 'News'): ?>class="active"<?php endif; ?>><a href="<?php echo U('News/index');?>">资讯管理</a></li>
	      <!-- 	<li <?php if(CONTROLLER_NAME== 'Report'): ?>class="active"<?php endif; ?>><a href="<?php echo U('Report/index');?>">举报管理</a></li> 举报管理，后面再搭建-->
	      	<li <?php if(CONTROLLER_NAME== 'Tag'): ?>class="active"<?php endif; ?>><a href="<?php echo U('Tag/index');?>">标签管理</a></li>
	      	<li <?php if(CONTROLLER_NAME== 'Ad'): ?>class="active"<?php endif; ?>><a href="<?php echo U('Ad/index');?>">广告管理</a></li>
	      	<li <?php if(CONTROLLER_NAME== 'Friendlink'): ?>class="active"<?php endif; ?>><a href="<?php echo U('Friendlink/index');?>">友链管理</a></li>
	      	<li <?php if(CONTROLLER_NAME== 'Word'): ?>class="active"<?php endif; ?>><a href="<?php echo U('Word/index');?>">搜索管理</a></li>
	      	<li <?php if(CONTROLLER_NAME== 'Forbidden'): ?>class="active"<?php endif; ?>><a href="<?php echo U('Forbidden/index');?>">屏蔽管理</a></li>
	      	<li <?php if(CONTROLLER_NAME== 'Discuz'): ?>class="active"<?php endif; ?>><a href="<?php echo U('Discuz/index');?>">发帖机器人</a></li>
<!-- 	      	<li <?php if(CONTROLLER_NAME== 'Vdisk'): ?>class="active"<?php endif; ?>><a href="<?php echo U('Vdisk/index');?>">新浪微盘</a></li>
 -->	      	
 			<li <?php if(CONTROLLER_NAME== 'Yunpan'): ?>class="active"<?php endif; ?>><a href="<?php echo U('Yunpan/index');?>">360云盘</a></li>
	      	<li <?php if(CONTROLLER_NAME== 'Oof'): ?>class="active"<?php endif; ?>><a href="<?php echo U('Oof/index');?>">115网盘</a></li>
	      	<li <?php if(CONTROLLER_NAME== 'Ivdisk'): ?>class="active"<?php endif; ?>><a href="<?php echo U('Ivdisk/index');?>">新浪微盘</a></li>
	      	
	      	<li <?php if(CONTROLLER_NAME== 'Wenku'): ?>class="active"<?php endif; ?>><a href="<?php echo U('Wenku/index');?>">文库</a></li>
	      	
	      	
          </ul>
        </div>
        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
        	<div class="row">
<style>
img{width:50px;border:none;}
.my_red{color:red;}
.my_green{color:green;}
.Tselect select{border:1px solid #CCCCCC;border-radius:5px;padding-left:5px;height: 30px;width: 100px;}
.Tselect select option{padding-left: 5px;}
.Tsearch input{height: 30px;border-radius: 5px;border: 1px solid #CCCCCC;padding-left: 5px;line-height: 30px;}
</style>
<!-- 表示一个成功的或积极的动作 -->
<div class="col-xs-6">
<a href="<?php echo U('User/index');?>"><button type="button" class="btn btn-primary active">达人管理</button></a>
<button type="button" class="btn btn-primary"  data-toggle="modal" data-target="#myModal">添加达人</button>
<button type="button" class="btn btn-primary"  data-toggle="modal" data-target="#myModal2">手动添加达人</button>
<button type="button" class="btn btn-primary"  data-toggle="modal" data-target="#myModal3">JSON采集资源</button>
</div>
<section class="col-xs-3 Tselect">
	<form role="form">
		     <select name="sortT" class="option">
		     	<option value="default">默认</option>
				<option value="new" >最新</option>
				<option value="share" >分享</option>
				<option value="album" >专辑</option>
				<option value="follow" >订阅</option>
				<option value="fans" >粉丝</option>
			</select>
		<button type="submit" class="btn btn-default">排序</button>
	</form>
</section>
<section class="col-xs-3 Tsearch">
	<form role="form">
		<input type="text" name="user" placeholder="网盘达人搜索" value="<?php echo ($_GET['user']); ?>"/>    
		<button type="submit" class="btn btn-default">搜索</button>
	</form>
</section>
<table class="table table-hover table-responsive">
   <thead>
      <tr>
         <th>达人</th>
         <th>UK</th>
         <th>分享</th>
         <th>专辑</th>
         <th>订阅</th>
         <th>粉丝</th>
         <th>状态</th>
         <th>操作</th>
         <th>收录时间</th>
      </tr>
   </thead>
   <tbody>
   <?php if(is_array($data)): $i = 0; $__LIST__ = $data;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$data): $mod = ($i % 2 );++$i;?><tr>
         <td><img src="<?php echo ($data["avatar"]); ?>" title="<?php echo ($data["show"]); ?>"  data-toggle="tooltip" 
   data-placement="top"/><br><a href="<?php echo U('Home/User/home',array('id'=>$data['id']));?>" target="_blank" title="访问他在本站的主页"  data-toggle="tooltip" 
   data-placement="top"><?php echo ($data["name"]); ?></a></td>
         <td><br><a href="http://yun.baidu.com/share/home?uk=<?php echo ($data["uk"]); ?>" target="_blank"  title="访问他的百度网盘主页"  data-toggle="tooltip" 
   data-placement="top"><?php echo ($data["uk"]); ?></a></td>
         <td><br><a href="http://yun.baidu.com/share/home?uk=<?php echo ($data["uk"]); ?>&view=share"  target="_blank"  title="访问他的百度网盘分享"  data-toggle="tooltip" 
   data-placement="top"><?php echo ($data["share"]); ?></a></td>
         <td><br><a href="http://yun.baidu.com/share/home?uk=<?php echo ($data["uk"]); ?>&view=album"  target="_blank"  title="访问他的百度网盘专辑"  data-toggle="tooltip" 
   data-placement="top"><?php echo ($data["album"]); ?></a></td>
         <td><br><a href="<?php echo U('User/muti_collect_follow',array('uk'=>$data['uk'],'followNum'=>$data['follow']));?>"  target="_blank"  title="多线程采集会员订阅"  data-toggle="tooltip" 
   data-placement="top"><?php echo ($data["follow"]); ?></a></td>
         <td><br><a href="http://yun.baidu.com/share/home?uk=<?php echo ($data["uk"]); ?>&view=fans"  target="_blank"  title="访问他的百度网盘粉丝"  data-toggle="tooltip" 
   data-placement="top"><?php echo ($data["fans"]); ?></a></td>
         <td><br>
	         	<?php switch($data["status"]): case "0": ?><span class="my_red">未采集</span><?php break;?>
					<?php case "1": ?><span class="my_green">已采集</span><?php break;?>
					<?php default: ?>待决定状态<?php endswitch;?>
         </td>
         <td><br>
			<a href="<?php echo U('User/collect_source');?>?uk=<?php echo ($data["uk"]); ?>&shareNum=<?php echo ($data['share']+$data['album']); ?>" title="马上采集该达人的资源"  data-toggle="tooltip" 
   data-placement="top">采集资源</a>
   			|  <a href="<?php echo U('User/collect_follow');?>?uk=<?php echo ($data["uk"]); ?>&followNum=<?php echo ($data["follow"]); ?>" title="马上采集该达人的订阅"  data-toggle="tooltip" 
   data-placement="top">采集订阅</a>
   			<!--| <a href="<?php echo U('User/collect_fans');?>?uk=<?php echo ($data["uk"]); ?>&fansNum=<?php echo ($data["fans"]); ?>" title="马上采集该达人的粉丝"  data-toggle="tooltip" 
   data-placement="top">采集粉丝</a>-->
   			<!--|-->  
			 <!--<?php if($data["recommend"] == 0): ?><a href="<?php echo U('User/recommend');?>?uk=<?php echo ($data["uk"]); ?>&recommend=1" title="将该达人设置为推荐达人"  data-toggle="tooltip" 
   data-placement="top">推荐</a>
			 <?php else: ?>
			 	<a href="<?php echo U('User/recommend');?>?uk=<?php echo ($data["uk"]); ?>&recommend=0" title="取消推荐该达人"  data-toggle="tooltip" 
   data-placement="top">取消推荐</a><?php endif; ?>-->
			 | 
			<a href="" title="查看他在本站的主页"  data-toggle="tooltip" 
   data-placement="top">查看</a>
			 <!--|--> 
			<!--<a href="<?php echo U('User/update');?>?uk=<?php echo ($data["uk"]); ?>" title="点击再次更新该达人的达人信息"  data-toggle="tooltip" 
   data-placement="top">更新</a>
    		 |  
			<a class="delete_user" href="<?php echo U('User/delete_user',array('uk'=>$data['uk']));?>" title="删除该达人，将会删除站内收录的该达人所有的分享资源，所以请认真考虑清楚再做删除"  data-toggle="tooltip" 
   data-placement="top">删除</a>-->
         </td>
         <td><br><?php echo (date("Y-m-d H:i:s",$data["time"])); ?></td>
      </tr><?php endforeach; endif; else: echo "" ;endif; ?>
   </tbody>
</table>
<?php echo ($page); ?>

<!-- 模态框（Modal） -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" 
   aria-labelledby="myModalLabel" aria-hidden="true">
   <div class="modal-dialog">
      <div class="modal-content">
         <div class="modal-body">
         <form role="form" id="collect_form" method="post" action="<?php echo U('User/collect');?>">
  			<div class="form-group">
    			<label for="content">资源达人收录</label>
    			<textarea id="content" name="content" class="form-control" rows="10" placeholder='http://yun.baidu.com/share/home?uk=4080815635&view=share#category/type=0'></textarea>
    			<span class="help-block">请输入百度网盘达人主页链接，多个请按回车换行</span>
  			</div>
		</form>
         </div>
         
         <div class="modal-footer">
            <button type="button" class="btn btn-default" 
               data-dismiss="modal">关闭
            </button>
            <button type="button" id="collect" class="btn btn-primary">
               马上采集收录
            </button>
         </div>
      </div><!-- /.modal-content -->
</div><!-- /.modal -->
</div>
<!-- 模态框（Modal） -->
<div class="modal fade" id="myModal2" tabindex="-1" role="dialog" 
   aria-labelledby="myModalLabel" aria-hidden="true">
   <div class="modal-dialog">
      <div class="modal-content">
       
         <div class="modal-body">
         <form role="form" id="collect_form_code" method="post" action="<?php echo U('User/collect_code');?>">
  			<div class="form-group">
    			<label for="content_code">资源达人收录</label>
    			<textarea id="content_code" name="content_code" class="form-control" rows="10" placeholder='请输入class="homeheader"中的内容，你懂的'></textarea>
  			</div>
		</form>
         </div>
         
         <div class="modal-footer">
            <button type="button" class="btn btn-default" 
               data-dismiss="modal">关闭
            </button>
            <button type="button" id="collect_code" class="btn btn-primary">
               马上采集收录
            </button>
         </div>
      </div><!-- /.modal-content -->
</div><!-- /.modal -->
</div>
<!-- 模态框（Modal） -->
<div class="modal fade" id="myModal3" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
   <div class="modal-dialog">
      <div class="modal-content">
         <div class="modal-body">
         <form role="form" method="post" >
  			<div class="form-group">
    			<label for="content_json">JSON资源收录</label>
    			<textarea id="content_json" name="content_json" class="form-control" rows="10" placeholder='请粘贴入json内容'></textarea>
  			</div>
		</form>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">关闭 </button>
            <button type="button" id="collect_btn_json" class="btn btn-primary">
               马上采集收录
            </button>
         </div>
      </div><!-- /.modal-content -->
</div><!-- /.modal -->
</div>

</div>
<script>
   $(function () {$("[data-toggle='tooltip']").tooltip(); });
   $(document).ready(function(){
   	//默认选项
   		var sortT="<?php echo ($_GET['sortT']); ?>";
   		$option=$('.option option');
   		$.each($option, function() {    
   			if($(this).val() ==sortT){
   				$(this).attr('selected','selected');
   			}
   		});
   		
   		
	   $("#collect").click(function(){
		   if($("#content").val()==''){
			   alert('内容不能为空');
			   return false;
		   }else{
			   $("#collect_form").submit(); 
		   }
	   });
	   $("#collect_code").click(function(){
		   if($("#content_code").val()==''){
			   alert('内容不能为空');
			   return false;
		   }else{
			   $("#collect_form_code").submit();
		   }
	   });

	   $("#collect_btn_json").click(function(){
		   if($("#content_json").val()==''){
			   alert('内容不能为空');
			   return false;
		   }else{
			   $.post("<?php echo U('User/collect_json');?>",{content_json:$("#content_json").val()},function(data){
				   if(data==1){
					   alert('恭喜您，资源收录成功！欢迎继续添加');
					   $("#content_json").val('');
				   }else{
					   alert('很遗憾，添加失败，可能您添加的资源已在数据库中存在。');
					   $("#content_json").val('');
				   }
			   });
		   }
	   });
	   
	   //删除达人
	   $(".delete_user").click(function(){
			if(!confirm('您确定要删除该达人吗？删除该达人之后，所有收录自该达人的资源也将被删除。')) return false;
	   });
   });
</script>









          </div>
        </div>
      </div>
      
      <!-- 模态框（Modal） 设置管理员信息-->
<div class="modal fade" id="setAdmin_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
   <div class="modal-dialog">
      <div class="modal-content">
         <div class="modal-body">
         <form role="form" id="setAdmin_form" method="post" action="<?php echo U('Sys/updateAdmin');?>">
         	<div class="form-group">
    			<label for="admin">管理员</label>
    			<input type="text" class="form-control" id="admin" name="admin" value="<?php echo ($_SESSION['adm_arr']['name']); ?>" placeholder="为空，则管理员账号默认为admin">
  			</div>
  			<div class="form-group">
    			<label for="opwd">原密码</label>
    			<input type="text" class="form-control" id="opwd" name="opwd" value="" placeholder="您如果需要修改登陆密码，请在此处输入原密码">
  			</div>
  			<div class="form-group">
    			<label for="npwd">新密码</label>
    			<input type="text" class="form-control" id="npwd" name="npwd" placeholder="您如果需要修改登陆密码，请在此处输入新密码">
  			</div>
  			<div class="form-group">
    			<label for="phone">手机号码</label>
    			<input type="text" class="form-control" id="phone" name="phone" value="<?php echo ($_SESSION['adm_arr']['phone']); ?>" placeholder="【此处输入的必须是Email，否则会造成无法登陆管理后台的情况】">
  			</div>
		</form>
         </div>
         
         <div class="modal-footer">
            <button type="button" class="btn btn-default" 
               data-dismiss="modal">关闭
            </button>
            <button type="button" id="updateAdminBtn" class="btn btn-primary">确定修改</button>
         </div>
      </div><!-- /.modal-content -->
</div><!-- /.modal -->
</div>
<script>
$(function(){
	$("#updateAdminBtn").click(function(){
		$("#setAdmin_form").submit();
	});
})

</script>
	</body>
</html>
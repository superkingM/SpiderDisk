<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title><?php echo ($title); ?>_HC-CMS</title>
		<!-- 新 Bootstrap 核心 CSS 文件 -->
		<link rel="stylesheet" href="/newwangpan/Public/Admin/css/bootstrap.min.css">
		<!-- 可选的Bootstrap主题文件（一般不用引入） -->
		<link rel="stylesheet" href="/newwangpan/Public/Admin/css/bootstrap-theme.min.css">
		<link rel="stylesheet" href="/newwangpan/Public/css/admin.css">
		<!-- jQuery文件。务必在bootstrap.min.js 之前引入 -->
		<script src="/newwangpan/Public/Admin/js/jquery.min.js"></script>
		<!-- 最新的 Bootstrap 核心 JavaScript 文件 -->
		<script src="/newwangpan/Public/Admin/js/bootstrap.min.js"></script>
		<style type="text/css">
			
		</style>
	</head>
	<body>
		<nav id="top_nav" class="navbar navbar-inverse navbar-fixed-top" role="navigation">
	      <div class="container-fluid">
	        <div class="navbar-header">
	          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
	            <span class="sr-only">Toggle navigation</span>
	            <span class="icon-bar"></span>
	            <span class="icon-bar"></span>
	            <span class="icon-bar"></span>
	          </button>
	          <a class="navbar-brand" href="#">HC-CMS后台管理面板</a>
	        </div>
	        <div id="navbar" class="navbar-collapse collapse">
	          <ul class="nav navbar-nav navbar-right">
	            <li><a href="#">当前用户：<?php echo ($_SESSION['adm_arr']['name']); ?></a></li>
	            <li><a href="javascript:void(0);" data-toggle="modal" data-target="#setAdmin_modal">设置</a></li>
	            <li><a href="<?php echo U('Login/logout');?>">退出</a></li>
	            <li><a href="<?php echo U('Home/Index/index');?>" target="_blank">前台首页</a></li>
	            <li> </li>
	          </ul>
	        </div>
	      </div>
	    </nav>

	    <div class="container-fluid">
	    	<div class="row">
        <div class="col-sm-3 col-md-2 sidebar">
          <ul class="nav nav-sidebar">
	      	<li <?php if(CONTROLLER_NAME== 'Index'): ?>class="active"<?php endif; ?>><a href="<?php echo U('Index/index');?>">首页</a></li>
	      	<li <?php if(CONTROLLER_NAME== 'Sys'): ?>class="active"<?php endif; ?>><a href="<?php echo U('Sys/index');?>">系统设置</a></li>
	      	<li <?php if(CONTROLLER_NAME== 'User'): ?>class="active"<?php endif; ?>><a href="<?php echo U('User/index');?>">资源达人</a></li>
	     	<li <?php if(CONTROLLER_NAME== 'Source'): ?>class="active"<?php endif; ?>><a href="<?php echo U('Source/index');?>">资源管理</a></li>
	      	<li <?php if(CONTROLLER_NAME== 'News'): ?>class="active"<?php endif; ?>><a href="<?php echo U('News/index');?>">资讯管理</a></li>
	      <!-- 	<li <?php if(CONTROLLER_NAME== 'Report'): ?>class="active"<?php endif; ?>><a href="<?php echo U('Report/index');?>">举报管理</a></li> 举报管理，后面再搭建-->
	      	<li <?php if(CONTROLLER_NAME== 'Tag'): ?>class="active"<?php endif; ?>><a href="<?php echo U('Tag/index');?>">标签管理</a></li>
	      	<li <?php if(CONTROLLER_NAME== 'Ad'): ?>class="active"<?php endif; ?>><a href="<?php echo U('Ad/index');?>">广告管理</a></li>
	      	<li <?php if(CONTROLLER_NAME== 'Friendlink'): ?>class="active"<?php endif; ?>><a href="<?php echo U('Friendlink/index');?>">友链管理</a></li>
	      	<li <?php if(CONTROLLER_NAME== 'Word'): ?>class="active"<?php endif; ?>><a href="<?php echo U('Word/index');?>">搜索管理</a></li>
	      	<li <?php if(CONTROLLER_NAME== 'Discuz'): ?>class="active"<?php endif; ?>><a href="<?php echo U('Discuz/index');?>">发帖机器人</a></li>
<!-- 	      	<li <?php if(CONTROLLER_NAME== 'Vdisk'): ?>class="active"<?php endif; ?>><a href="<?php echo U('Vdisk/index');?>">新浪微盘</a></li>
 -->	      	
 			<li <?php if(CONTROLLER_NAME== 'Yunpan'): ?>class="active"<?php endif; ?>><a href="<?php echo U('Yunpan/index');?>">360云盘</a></li>
	      	<li <?php if(CONTROLLER_NAME== 'Oof'): ?>class="active"<?php endif; ?>><a href="<?php echo U('Oof/index');?>">115网盘</a></li>
	      	<li <?php if(CONTROLLER_NAME== 'Ivdisk'): ?>class="active"<?php endif; ?>><a href="<?php echo U('Ivdisk/index');?>">新浪微盘</a></li>
	      	
	      	<li <?php if(CONTROLLER_NAME== 'Wenku'): ?>class="active"<?php endif; ?>><a href="<?php echo U('Wenku/index');?>">文库</a></li>
	      	
	      	
          </ul>
        </div>
        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
        	<style>
	ul,li{padding: 0px;margin: 0px;list-style: none;}
	.panel li{padding: 5px;}
	.panel li span{width: 100px;display:inline-block;}
	.panel li input{width: 400px;padding: 2px 5px;border-radius: 5px;border: 1px solid #ccc;line-height: 30px;}
	.tips{border: 1px dashed #DDDDDD;padding: 10px;margin: 10px;margin-left: 0px;background: #FAF7D3;border-radius: 5px;}
</style>
<div class="row" style="margin-bottom: 20px;border-bottom: 1px solid #EEEEEE;">
<a href="<?php echo U('Sys/index');?>"><button type="button" class="btn btn-primary">网站配置</button></a>
<a href="<?php echo U('Sys/seo');?>"><button type="button" class="btn btn-primary active">SEO优化</button></a>
<a href="<?php echo U('Sys/nav');?>"><button type="button" class="btn btn-primary">导航管理</button></a>
<a href="<?php echo U('Sys/updateCount');?>"><button type="button" class="btn btn-primary">更新统计与数据</button></a>
<a href="<?php echo U('Sitemap/index');?>"><button type="button" class="btn btn-primary">生成站点地图</button></a>
</div>
<div class="row">
	<div class="col-md-12 com-sm-12 col-xs-12">
		<div class="tips">
			TIPS<br />
			1. 多个关键字，请使用英文逗号(,)分隔；{title},{keywords},{desc}分别为默认的SEO优化标题、关键字、描述，{sitename}为站点设置里您自定义的站点名称。建议在添加自定义SEO优化的时候不要删掉默认的SEO优化<br />
			2. 根据SEO优化规则，建议title一般不超过80个字符,keywords一般不超过100个字符,description一般不超过200个字符
		</div>
	</div>
	<div class="col-md-12 com-sm-12 col-xs-12">
		<form id="seoform" method="post" action="<?php echo U('Sys/seo');?>">
		<div class="panel-group" id="accordion">
		  <div class="panel panel-default">
		    <div class="panel-heading">
		      <h4 class="panel-title">
		    	<a data-toggle="collapse" data-parent="#accordion" href="#seoindex">首页</a>
		      </h4>
		    </div>
		    <div id="seoindex" class="panel-collapse collapse in">
		      <div class="panel-body">
			        <ul>
			        	<input type="hidden" name='type[]' value="index"/>
			        	<?php if(is_array($seo)): $i = 0; $__LIST__ = $seo;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$seoindex): $mod = ($i % 2 );++$i; if($seoindex["type"] == 'index'): ?><li><span>title</span><input type="text" name="title[]" value="<?php echo ($seoindex["title"]); ?>"/></li>
			        			<li><span>keywords</span><input type="text" name="keywords[]" value="<?php echo ($seoindex["keywords"]); ?>"/></li>
			        			<li><span>description</span><input type="text" name="desc[]" value="<?php echo ($seoindex["desc"]); ?>"/></li><?php endif; endforeach; endif; else: echo "" ;endif; ?>
			        </ul>
		      </div>
		    </div>
		  </div>
		  <div class="panel panel-default">
		    <div class="panel-heading">
		      <h4 class="panel-title">
		        <a data-toggle="collapse" data-parent="#accordion" href="#seovideo">影视</a>
		      </h4>
		    </div>
		    <div id="seovideo" class="panel-collapse collapse">
		      <div class="panel-body">
			        <ul>
			        	<input type="hidden" name='type[]' value="video"/>
			        	<?php if(is_array($seo)): $i = 0; $__LIST__ = $seo;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$seovideo): $mod = ($i % 2 );++$i; if($seovideo["type"] == 'video'): ?><li><span>title</span><input type="text" name="title[]" value="<?php echo ($seovideo["title"]); ?>"/></li>
			        			<li><span>keywords</span><input type="text" name="keywords[]" value="<?php echo ($seovideo["keywords"]); ?>"/></li>
			        			<li><span>description</span><input type="text" name="desc[]" value="<?php echo ($seovideo["desc"]); ?>"/></li><?php endif; endforeach; endif; else: echo "" ;endif; ?>
			        </ul>
		      </div>
		    </div>
		  </div>
		  <div class="panel panel-default">
		    <div class="panel-heading">
		      <h4 class="panel-title">
		        <a data-toggle="collapse" data-parent="#accordion" href="#seomusic">音乐</a>
		      </h4>
		    </div>
		    <div id="seomusic" class="panel-collapse collapse">
		      <div class="panel-body">
			        <ul>
			        	<input type="hidden" name='type[]' value="music"/>
						<?php if(is_array($seo)): $i = 0; $__LIST__ = $seo;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$seomusic): $mod = ($i % 2 );++$i; if($seomusic["type"] == 'music'): ?><li><span>title</span><input type="text" name="title[]" value="<?php echo ($seomusic["title"]); ?>"/></li>
			        			<li><span>keywords</span><input type="text" name="keywords[]" value="<?php echo ($seomusic["keywords"]); ?>"/></li>
			        			<li><span>description</span><input type="text" name="desc[]" value="<?php echo ($seomusic["desc"]); ?>"/></li><?php endif; endforeach; endif; else: echo "" ;endif; ?>
			        </ul>
		      </div>
		    </div>
		  </div>
		  
		  <div class="panel panel-default">
		    <div class="panel-heading">
		      <h4 class="panel-title">
		        <a data-toggle="collapse" data-parent="#accordion" href="#seosoftware">软件</a>
		      </h4>
		    </div>
		    <div id="seosoftware" class="panel-collapse collapse">
		      <div class="panel-body">
			        <ul>
			        	<input type="hidden" name='type[]' value="software"/>
						<?php if(is_array($seo)): $i = 0; $__LIST__ = $seo;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$seosoftware): $mod = ($i % 2 );++$i; if($seosoftware["type"] == 'software'): ?><li><span>title</span><input type="text" name="title[]" value="<?php echo ($seosoftware["title"]); ?>"/></li>
			        			<li><span>keywords</span><input type="text" name="keywords[]" value="<?php echo ($seosoftware["keywords"]); ?>"/></li>
			        			<li><span>description</span><input type="text" name="desc[]" value="<?php echo ($seosoftware["desc"]); ?>"/></li><?php endif; endforeach; endif; else: echo "" ;endif; ?>
			        </ul>
		      </div>
		    </div>
		   </div>
		   
		   <div class="panel panel-default">
		    <div class="panel-heading">
		      <h4 class="panel-title">
		        <a data-toggle="collapse" data-parent="#accordion" href="#seoalbum">专辑</a>
		      </h4>
		    </div>
		    <div id="seoalbum" class="panel-collapse collapse">
		      <div class="panel-body">
			        <ul>
			        	<input type="hidden" name='type[]' value="album"/>
						<?php if(is_array($seo)): $i = 0; $__LIST__ = $seo;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$seoalbum): $mod = ($i % 2 );++$i; if($seoalbum["type"] == 'album'): ?><li><span>title</span><input type="text" name="title[]" value="<?php echo ($seoalbum["title"]); ?>"/></li>
			        			<li><span>keywords</span><input type="text" name="keywords[]" value="<?php echo ($seoalbum["keywords"]); ?>"/></li>
			        			<li><span>description</span><input type="text" name="desc[]" value="<?php echo ($seoalbum["desc"]); ?>"/></li><?php endif; endforeach; endif; else: echo "" ;endif; ?>
			        </ul>
		      </div>
		    </div>
		   </div>
		   
		   
		    <div class="panel panel-default">
		    <div class="panel-heading">
		      <h4 class="panel-title">
		        <a data-toggle="collapse" data-parent="#accordion" href="#seotorrent">种子</a>
		      </h4>
		    </div>
		    <div id="seotorrent" class="panel-collapse collapse">
		      <div class="panel-body">
			        <ul>
			        	<input type="hidden" name='type[]' value="torrent"/>
						<?php if(is_array($seo)): $i = 0; $__LIST__ = $seo;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$seotorrent): $mod = ($i % 2 );++$i; if($seotorrent["type"] == 'torrent'): ?><li><span>title</span><input type="text" name="title[]" value="<?php echo ($seotorrent["title"]); ?>"/></li>
			        			<li><span>keywords</span><input type="text" name="keywords[]" value="<?php echo ($seotorrent["keywords"]); ?>"/></li>
			        			<li><span>description</span><input type="text" name="desc[]" value="<?php echo ($seotorrent["desc"]); ?>"/></li><?php endif; endforeach; endif; else: echo "" ;endif; ?>
			        </ul>
		      </div>
		    </div>
		  </div>
		  
		  <div class="panel panel-default">
		    <div class="panel-heading">
		      <h4 class="panel-title">
		        <a data-toggle="collapse" data-parent="#accordion" href="#seouser">达人</a>
		      </h4>
		    </div>
		    <div id="seouser" class="panel-collapse collapse">
		      <div class="panel-body">
			        <ul>
			        	<input type="hidden" name='type[]' value="user"/>
						<?php if(is_array($seo)): $i = 0; $__LIST__ = $seo;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$seouser): $mod = ($i % 2 );++$i; if($seouser["type"] == 'user'): ?><li><span>title</span><input type="text" name="title[]" value="<?php echo ($seouser["title"]); ?>"/></li>
			        			<li><span>keywords</span><input type="text" name="keywords[]" value="<?php echo ($seouser["keywords"]); ?>"/></li>
			        			<li><span>description</span><input type="text" name="desc[]" value="<?php echo ($seouser["desc"]); ?>"/></li><?php endif; endforeach; endif; else: echo "" ;endif; ?>
			        </ul>
		      </div>
		    </div>
		  </div>
		  
		  <div class="panel panel-default">
		    <div class="panel-heading">
		      <h4 class="panel-title">
		        <a data-toggle="collapse" data-parent="#accordion" href="#seousercenter">达人中心</a>
		      </h4>
		    </div>
		    <div id="seousercenter" class="panel-collapse collapse">
		      <div class="panel-body">
			        <ul>
			        	<input type="hidden" name='type[]' value="usercenter"/>
						<?php if(is_array($seo)): $i = 0; $__LIST__ = $seo;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$seousercenter): $mod = ($i % 2 );++$i; if($seousercenter["type"] == 'usercenter'): ?><li><span>title</span><input type="text" name="title[]" value="<?php echo ($seousercenter["title"]); ?>"/></li>
			        			<li><span>keywords</span><input type="text" name="keywords[]" value="<?php echo ($seousercenter["keywords"]); ?>"/></li>
			        			<li><span>description</span><input type="text" name="desc[]" value="<?php echo ($seousercenter["desc"]); ?>"/></li><?php endif; endforeach; endif; else: echo "" ;endif; ?>
			        </ul>
		      </div>
		    </div>
		  </div>
		  
		  <div class="panel panel-default">
		    <div class="panel-heading">
		      <h4 class="panel-title">
		        <a data-toggle="collapse" data-parent="#accordion" href="#seotag">标签</a>
		      </h4>
		    </div>
		    <div id="seotag" class="panel-collapse collapse">
		      <div class="panel-body">
			        <ul>
			        	<input type="hidden" name='type[]' value="tag"/>
			        	<?php if(is_array($seo)): $i = 0; $__LIST__ = $seo;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$seotag): $mod = ($i % 2 );++$i; if($seotag["type"] == 'tag'): ?><li><span>title</span><input type="text" name="title[]" value="<?php echo ($seotag["title"]); ?>"/></li>
			        			<li><span>keywords</span><input type="text" name="keywords[]" value="<?php echo ($seotag["keywords"]); ?>"/></li>
			        			<li><span>description</span><input type="text" name="desc[]" value="<?php echo ($seotag["desc"]); ?>"/></li><?php endif; endforeach; endif; else: echo "" ;endif; ?>
			        </ul>
		      </div>
		    </div>
		  </div>
		  
		  <div class="panel panel-default">
		    <div class="panel-heading">
		      <h4 class="panel-title">
		        <a data-toggle="collapse" data-parent="#accordion" href="#seonews">资讯</a>
		      </h4>
		    </div>
		    <div id="seonews" class="panel-collapse collapse">
		      <div class="panel-body">
			        <ul>
			        	<input type="hidden" name='type[]' value="news"/>
						<?php if(is_array($seo)): $i = 0; $__LIST__ = $seo;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$seonews): $mod = ($i % 2 );++$i; if($seonews["type"] == 'news'): ?><li><span>title</span><input type="text" name="title[]" value="<?php echo ($seonews["title"]); ?>"/></li>
			        			<li><span>keywords</span><input type="text" name="keywords[]" value="<?php echo ($seonews["keywords"]); ?>"/></li>
			        			<li><span>description</span><input type="text" name="desc[]" value="<?php echo ($seonews["desc"]); ?>"/></li><?php endif; endforeach; endif; else: echo "" ;endif; ?>
			        </ul>
		      </div>
		    </div>
		  </div>
		  
		  <div class="panel panel-default">
		    <div class="panel-heading">
		      <h4 class="panel-title">
		        <a data-toggle="collapse" data-parent="#accordion" href="#seobook">电子书</a>
		      </h4>
		    </div>
		    <div id="seobook" class="panel-collapse collapse">
		      <div class="panel-body">
			        <ul>
			        	<input type="hidden" name='type[]' value="book"/>
						<?php if(is_array($seo)): $i = 0; $__LIST__ = $seo;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$seobook): $mod = ($i % 2 );++$i; if($seobook["type"] == 'book'): ?><li><span>title</span><input type="text" name="title[]" value="<?php echo ($seobook["title"]); ?>"/></li>
			        			<li><span>keywords</span><input type="text" name="keywords[]" value="<?php echo ($seobook["keywords"]); ?>"/></li>
			        			<li><span>description</span><input type="text" name="desc[]" value="<?php echo ($seobook["desc"]); ?>"/></li><?php endif; endforeach; endif; else: echo "" ;endif; ?>
			        </ul>
		      </div>
		    </div>
		  </div>
		  
		  <div class="panel panel-default">
		    <div class="panel-heading">
		      <h4 class="panel-title">
		        <a data-toggle="collapse" data-parent="#accordion" href="#seozip">压缩资源</a>
		      </h4>
		    </div>
		    <div id="seozip" class="panel-collapse collapse">
		      <div class="panel-body">
			        <ul>
			        	<input type="hidden" name='type[]' value="zip"/>
						<?php if(is_array($seo)): $i = 0; $__LIST__ = $seo;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$seozip): $mod = ($i % 2 );++$i; if($seozip["type"] == 'zip'): ?><li><span>title</span><input type="text" name="title[]" value="<?php echo ($seozip["title"]); ?>"/></li>
			        			<li><span>keywords</span><input type="text" name="keywords[]" value="<?php echo ($seozip["keywords"]); ?>"/></li>
			        			<li><span>description</span><input type="text" name="desc[]" value="<?php echo ($seozip["desc"]); ?>"/></li><?php endif; endforeach; endif; else: echo "" ;endif; ?>
			        </ul>
		      </div>
		    </div>
		  </div>
		  
		  <div class="panel panel-default">
		    <div class="panel-heading">
		      <h4 class="panel-title">
		        <a data-toggle="collapse" data-parent="#accordion" href="#seocommondetail">资源详情</a>
		      </h4>
		    </div>
		    <div id="seocommondetail" class="panel-collapse collapse">
		      <div class="panel-body">
			        <ul>
			        	<input type="hidden" name='type[]' value="common_detail"/>
						<?php if(is_array($seo)): $i = 0; $__LIST__ = $seo;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$seocommon_detail): $mod = ($i % 2 );++$i; if($seocommon_detail["type"] == 'common_detail'): ?><li><span>title</span><input type="text" name="title[]" value="<?php echo ($seocommon_detail["title"]); ?>"/></li>
			        			<li><span>keywords</span><input type="text" name="keywords[]" value="<?php echo ($seocommon_detail["keywords"]); ?>"/></li>
			        			<li><span>description</span><input type="text" name="desc[]" value="<?php echo ($seocommon_detail["desc"]); ?>"/></li><?php endif; endforeach; endif; else: echo "" ;endif; ?>
			        </ul>
		      </div>
		    </div>
		  </div>
		  
		  <div class="panel panel-default">
		    <div class="panel-heading">
		      <h4 class="panel-title">
		        <a data-toggle="collapse" data-parent="#accordion" href="#seoalbumdetail">专辑详情</a>
		      </h4>
		    </div>
		    <div id="seoalbumdetail" class="panel-collapse collapse">
		      <div class="panel-body">
			        <ul>
			        	<input type="hidden" name='type[]' value="album_detail"/>
						<?php if(is_array($seo)): $i = 0; $__LIST__ = $seo;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$seoalbum_detail): $mod = ($i % 2 );++$i; if($seoalbum_detail["type"] == 'album_detail'): ?><li><span>title</span><input type="text" name="title[]" value="<?php echo ($seoalbum_detail["title"]); ?>"/></li>
			        			<li><span>keywords</span><input type="text" name="keywords[]" value="<?php echo ($seoalbum_detail["keywords"]); ?>"/></li>
			        			<li><span>description</span><input type="text" name="desc[]" value="<?php echo ($seoalbum_detail["desc"]); ?>"/></li><?php endif; endforeach; endif; else: echo "" ;endif; ?>
			        </ul>
		      </div>
		    </div>
		  </div>
		  
		  <div class="panel panel-default">
		    <div class="panel-heading">
		      <h4 class="panel-title">
		        <a data-toggle="collapse" data-parent="#accordion" href="#seoalbumfile">专辑文件</a>
		      </h4>
		    </div>
		    <div id="seoalbumfile" class="panel-collapse collapse">
		      <div class="panel-body">
			        <ul>
			        	<input type="hidden" name='type[]' value="album_file"/>
						<?php if(is_array($seo)): $i = 0; $__LIST__ = $seo;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$seoalbum_file): $mod = ($i % 2 );++$i; if($seoalbum_file["type"] == 'album_file'): ?><li><span>title</span><input type="text" name="title[]" value="<?php echo ($seoalbum_file["title"]); ?>"/></li>
			        			<li><span>keywords</span><input type="text" name="keywords[]" value="<?php echo ($seoalbum_file["keywords"]); ?>"/></li>
			        			<li><span>description</span><input type="text" name="desc[]" value="<?php echo ($seoalbum_file["desc"]); ?>"/></li><?php endif; endforeach; endif; else: echo "" ;endif; ?>
			        </ul>
		      </div>
		    </div>
		  </div>
		  
		  
		  <div class="panel panel-default">
		    <div class="panel-heading">
		      <h4 class="panel-title">
		        <a data-toggle="collapse" data-parent="#accordion" href="#seoso">搜索</a>
		      </h4>
		    </div>
		    <div id="seoso" class="panel-collapse collapse">
		      <div class="panel-body">
			        <ul>
			        	<input type="hidden" name='type[]' value="so"/>
			        	<?php if(is_array($seo)): $i = 0; $__LIST__ = $seo;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$seoso): $mod = ($i % 2 );++$i; if($seoso["type"] == 'so'): ?><li><span>title</span><input type="text" name="title[]" value="<?php echo ($seoso["title"]); ?>"/></li>
			        			<li><span>keywords</span><input type="text" name="keywords[]" value="<?php echo ($seoso["keywords"]); ?>"/></li>
			        			<li><span>description</span><input type="text" name="desc[]" value="<?php echo ($seoso["desc"]); ?>"/></li><?php endif; endforeach; endif; else: echo "" ;endif; ?>
			        </ul>
		      </div>
		    </div>
		  </div>
		  
		  <div class="panel panel-default">
		    <div class="panel-heading">
		      <h4 class="panel-title">
		        <a data-toggle="collapse" data-parent="#accordion" href="#seosodetail">搜索结果</a>
		      </h4>
		    </div>
		    <div id="seosodetail" class="panel-collapse collapse">
		      <div class="panel-body">
			        <ul>
			        	<input type="hidden" name='type[]' value="sodetail"/>
						<?php if(is_array($seo)): $i = 0; $__LIST__ = $seo;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$seosodetail): $mod = ($i % 2 );++$i; if($seosodetail["type"] == 'sodetail'): ?><li><span>title</span><input type="text" name="title[]" value="<?php echo ($seosodetail["title"]); ?>"/></li>
			        			<li><span>keywords</span><input type="text" name="keywords[]" value="<?php echo ($seosodetail["keywords"]); ?>"/></li>
			        			<li><span>description</span><input type="text" name="desc[]" value="<?php echo ($seosodetail["desc"]); ?>"/></li><?php endif; endforeach; endif; else: echo "" ;endif; ?>
			        </ul>
		      </div>
		    </div>
		  </div>
		  
		  <div class="panel panel-default">
		    <div class="panel-heading">
		      <h4 class="panel-title">
		        <a data-toggle="collapse" data-parent="#accordion" href="#seonews_detail">资讯内容</a>
		      </h4>
		    </div>
		    <div id="seonews_detail" class="panel-collapse collapse">
		      <div class="panel-body">
			        <ul>
			        	<input type="hidden" name='type[]' value="news_detail"/>
						<?php if(is_array($seo)): $i = 0; $__LIST__ = $seo;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$seonews_detail): $mod = ($i % 2 );++$i; if($seonews_detail["type"] == 'news_detail'): ?><li><span>title</span><input type="text" name="title[]" value="<?php echo ($seonews_detail["title"]); ?>"/></li>
			        			<li><span>keywords</span><input type="text" name="keywords[]" value="<?php echo ($seonews_detail["keywords"]); ?>"/></li>
			        			<li><span>description</span><input type="text" name="desc[]" value="<?php echo ($seonews_detail["desc"]); ?>"/></li><?php endif; endforeach; endif; else: echo "" ;endif; ?>
			        </ul>
		      </div>
		    </div>
		  </div>
		  		  
		 <button type="submit" class="btn btn-success" style="margin-top:50px;margin-bottom: 100px;">提交SEO优化</button>
		</div>
	</div>
	</form>
</div>

          </div>
        </div>
      </div>
      
      <!-- 模态框（Modal） 设置管理员信息-->
<div class="modal fade" id="setAdmin_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
   <div class="modal-dialog">
      <div class="modal-content">
         <div class="modal-body">
         <form role="form" id="setAdmin_form" method="post" action="<?php echo U('Sys/updateAdmin');?>">
         	<div class="form-group">
    			<label for="admin">管理员</label>
    			<input type="text" class="form-control" id="admin" name="admin" value="<?php echo ($_SESSION['adm_arr']['name']); ?>" placeholder="为空，则管理员账号默认为admin">
  			</div>
  			<div class="form-group">
    			<label for="opwd">原密码</label>
    			<input type="text" class="form-control" id="opwd" name="opwd" value="" placeholder="您如果需要修改登陆密码，请在此处输入原密码">
  			</div>
  			<div class="form-group">
    			<label for="npwd">新密码</label>
    			<input type="text" class="form-control" id="npwd" name="npwd" placeholder="您如果需要修改登陆密码，请在此处输入新密码">
  			</div>
  			<div class="form-group">
    			<label for="phone">手机号码</label>
    			<input type="text" class="form-control" id="phone" name="phone" value="<?php echo ($_SESSION['adm_arr']['phone']); ?>" placeholder="【此处输入的必须是Email，否则会造成无法登陆管理后台的情况】">
  			</div>
		</form>
         </div>
         
         <div class="modal-footer">
            <button type="button" class="btn btn-default" 
               data-dismiss="modal">关闭
            </button>
            <button type="button" id="updateAdminBtn" class="btn btn-primary">确定修改</button>
         </div>
      </div><!-- /.modal-content -->
</div><!-- /.modal -->
</div>
<script>
$(function(){
	$("#updateAdminBtn").click(function(){
		$("#setAdmin_form").submit();
	});
})

</script>
	</body>
</html>
<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title><?php echo ($title); ?>_HC-CMS</title>
		<!-- 新 Bootstrap 核心 CSS 文件 -->
		<link rel="stylesheet" href="/newwangpan/Public/Admin/css/bootstrap.min.css">
		<!-- 可选的Bootstrap主题文件（一般不用引入） -->
		<link rel="stylesheet" href="/newwangpan/Public/Admin/css/bootstrap-theme.min.css">
		<link rel="stylesheet" href="/newwangpan/Public/css/admin.css">
		<!-- jQuery文件。务必在bootstrap.min.js 之前引入 -->
		<script src="/newwangpan/Public/Admin/js/jquery.min.js"></script>
		<!-- 最新的 Bootstrap 核心 JavaScript 文件 -->
		<script src="/newwangpan/Public/Admin/js/bootstrap.min.js"></script>
		<style type="text/css">
			
		</style>
	</head>
	<body>
		<nav id="top_nav" class="navbar navbar-inverse navbar-fixed-top" role="navigation">
	      <div class="container-fluid">
	        <div class="navbar-header">
	          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
	            <span class="sr-only">Toggle navigation</span>
	            <span class="icon-bar"></span>
	            <span class="icon-bar"></span>
	            <span class="icon-bar"></span>
	          </button>
	          <a class="navbar-brand" href="#">HC-CMS后台管理面板</a>
	        </div>
	        <div id="navbar" class="navbar-collapse collapse">
	          <ul class="nav navbar-nav navbar-right">
	            <li><a href="#">当前用户：<?php echo ($_SESSION['adm_arr']['name']); ?></a></li>
	            <li><a href="javascript:void(0);" data-toggle="modal" data-target="#setAdmin_modal">设置</a></li>
	            <li><a href="<?php echo U('Login/logout');?>">退出</a></li>
	            <li><a href="<?php echo U('Home/Index/index');?>" target="_blank">前台首页</a></li>
	            <li> </li>
	          </ul>
	        </div>
	      </div>
	    </nav>

	    <div class="container-fluid">
	    	<div class="row">
        <div class="col-sm-3 col-md-2 sidebar">
          <ul class="nav nav-sidebar">
	      	<li <?php if(CONTROLLER_NAME== 'Index'): ?>class="active"<?php endif; ?>><a href="<?php echo U('Index/index');?>">首页</a></li>
	      	<li <?php if(CONTROLLER_NAME== 'Sys'): ?>class="active"<?php endif; ?>><a href="<?php echo U('Sys/index');?>">系统设置</a></li>
	      	<li <?php if(CONTROLLER_NAME== 'User'): ?>class="active"<?php endif; ?>><a href="<?php echo U('User/index');?>">资源达人</a></li>
	     	<li <?php if(CONTROLLER_NAME== 'Source'): ?>class="active"<?php endif; ?>><a href="<?php echo U('Source/index');?>">资源管理</a></li>
	      	<li <?php if(CONTROLLER_NAME== 'News'): ?>class="active"<?php endif; ?>><a href="<?php echo U('News/index');?>">资讯管理</a></li>
	      <!-- 	<li <?php if(CONTROLLER_NAME== 'Report'): ?>class="active"<?php endif; ?>><a href="<?php echo U('Report/index');?>">举报管理</a></li> 举报管理，后面再搭建-->
	      	<li <?php if(CONTROLLER_NAME== 'Tag'): ?>class="active"<?php endif; ?>><a href="<?php echo U('Tag/index');?>">标签管理</a></li>
	      	<li <?php if(CONTROLLER_NAME== 'Ad'): ?>class="active"<?php endif; ?>><a href="<?php echo U('Ad/index');?>">广告管理</a></li>
	      	<li <?php if(CONTROLLER_NAME== 'Friendlink'): ?>class="active"<?php endif; ?>><a href="<?php echo U('Friendlink/index');?>">友链管理</a></li>
	      	<li <?php if(CONTROLLER_NAME== 'Word'): ?>class="active"<?php endif; ?>><a href="<?php echo U('Word/index');?>">搜索管理</a></li>
	      	<li <?php if(CONTROLLER_NAME== 'Forbidden'): ?>class="active"<?php endif; ?>><a href="<?php echo U('Forbidden/index');?>">屏蔽管理</a></li>
	      	<li <?php if(CONTROLLER_NAME== 'Discuz'): ?>class="active"<?php endif; ?>><a href="<?php echo U('Discuz/index');?>">发帖机器人</a></li>
<!-- 	      	<li <?php if(CONTROLLER_NAME== 'Vdisk'): ?>class="active"<?php endif; ?>><a href="<?php echo U('Vdisk/index');?>">新浪微盘</a></li>
 -->	      	
 			<li <?php if(CONTROLLER_NAME== 'Yunpan'): ?>class="active"<?php endif; ?>><a href="<?php echo U('Yunpan/index');?>">360云盘</a></li>
	      	<li <?php if(CONTROLLER_NAME== 'Oof'): ?>class="active"<?php endif; ?>><a href="<?php echo U('Oof/index');?>">115网盘</a></li>
	      	<li <?php if(CONTROLLER_NAME== 'Ivdisk'): ?>class="active"<?php endif; ?>><a href="<?php echo U('Ivdisk/index');?>">新浪微盘</a></li>
	      	
	      	<li <?php if(CONTROLLER_NAME== 'Wenku'): ?>class="active"<?php endif; ?>><a href="<?php echo U('Wenku/index');?>">文库</a></li>
	      	
	      	
          </ul>
        </div>
        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
        	<style>
.source_name{width:20%;}
td.source_name{text-align:left;}
.Tselect select{border:1px solid #CCCCCC;border-radius:5px;padding-left:5px;height: 30px;width: 100px;}
.Tselect select option{padding-left: 5px;}
.Tsearch input{height: 30px;border-radius: 5px;border: 1px solid #CCCCCC;padding-left: 5px;line-height: 30px;}
</style>
<div class="row">
	<div class="col-xs-6">
		<!-- 表示一个成功的或积极的动作 -->
		<a href="<?php echo U('Source/index');?>"><button type="button" class="btn btn-primary active">分享管理</button></a>
		<a href="<?php echo U('Source/album');?>"><button type="button" class="btn btn-primary">专辑管理</button></a>
	</div>
	<section class="col-xs-3 Tselect">
		<form role="form">
		     <select name="sortT" class="option">
		     	<option value="default">默认</option>
				<option value="new" >最新</option>
				<option value="tCnt" >转存</option>
				<option value="dCnt" >下载</option>
				<option value="vCnt" >浏览</option>
				<option value="click" >点击</option>
			</select>
			<button type="submit" class="btn btn-default">排序</button>
		</form>
	</section>
		<section class="col-xs-3 Tsearch">
			<form role="form">
				<input type="text" name="kw" placeholder="资源名称" value="<?php echo ($_GET['kw']); ?>"/>    
				<button type="submit" class="btn btn-default">搜索</button>
			</form>
		</section>
</div>
<table class="table table-hover table-responsive">
   <thead>
      <tr>
         <th class="source_name">名称</th>
         <th>浏览</th>
         <th>保存</th>
         <th>下载</th>
         <th>点击</th>
         <th>类型</th>
         <!--<th>状态</th>-->
         <th>操作</th>
         <th>收录时间</th>
      </tr>
   </thead>
   <tbody>
   <?php if(is_array($data)): $i = 0; $__LIST__ = $data;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$data): $mod = ($i % 2 );++$i;?><tr>
         <td class="source_name"><?php echo ($data["title"]); ?></td>
         <td><?php echo ($data["click"]); ?></td>
         <td><?php echo ($data["tCnt"]); ?></td>
         <td><?php echo ($data["dCnt"]); ?></td>
         <td><?php echo ($data["click"]); ?></td>
         <td><?php echo ($data["doctype"]); ?></td>
         <!--<td><?php echo ($data["status"]); ?></td>-->
         <td>
         	<a href="<?php echo U('Home/Detail/index',array('id'=>$data['id']));?>" target="_blank">查看 </a> 
         	<a href="<?php echo U('Source/delete_source');?>?id=<?php echo ($data["id"]); ?>" class="delete_source">删除</a>
         </td>
         <td><?php echo (date("Y-m-d H:i:s",$data["time"])); ?></td>
      </tr><?php endforeach; endif; else: echo "" ;endif; ?>
   </tbody>
</table>
<?php echo ($page); ?>
<script>
   $(document).ready(function(){
	   //删除会员
	   $(".delete_source").click(function(){
			if(!confirm('您确定要删除该资源吗？删除之后将不能再恢复，只能重新再收录。')) return false;
	   });
	   //默认选项
   		var sortT="<?php echo ($_GET['sortT']); ?>";
   		$option=$('.option option');
   		$.each($option, function() {    
   			if($(this).val() ==sortT){
   				$(this).attr('selected','selected');
   			}
   		});
   });
</script>








          </div>
        </div>
      </div>
      
      <!-- 模态框（Modal） 设置管理员信息-->
<div class="modal fade" id="setAdmin_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
   <div class="modal-dialog">
      <div class="modal-content">
         <div class="modal-body">
         <form role="form" id="setAdmin_form" method="post" action="<?php echo U('Sys/updateAdmin');?>">
         	<div class="form-group">
    			<label for="admin">管理员</label>
    			<input type="text" class="form-control" id="admin" name="admin" value="<?php echo ($_SESSION['adm_arr']['name']); ?>" placeholder="为空，则管理员账号默认为admin">
  			</div>
  			<div class="form-group">
    			<label for="opwd">原密码</label>
    			<input type="text" class="form-control" id="opwd" name="opwd" value="" placeholder="您如果需要修改登陆密码，请在此处输入原密码">
  			</div>
  			<div class="form-group">
    			<label for="npwd">新密码</label>
    			<input type="text" class="form-control" id="npwd" name="npwd" placeholder="您如果需要修改登陆密码，请在此处输入新密码">
  			</div>
  			<div class="form-group">
    			<label for="phone">手机号码</label>
    			<input type="text" class="form-control" id="phone" name="phone" value="<?php echo ($_SESSION['adm_arr']['phone']); ?>" placeholder="【此处输入的必须是Email，否则会造成无法登陆管理后台的情况】">
  			</div>
		</form>
         </div>
         
         <div class="modal-footer">
            <button type="button" class="btn btn-default" 
               data-dismiss="modal">关闭
            </button>
            <button type="button" id="updateAdminBtn" class="btn btn-primary">确定修改</button>
         </div>
      </div><!-- /.modal-content -->
</div><!-- /.modal -->
</div>
<script>
$(function(){
	$("#updateAdminBtn").click(function(){
		$("#setAdmin_form").submit();
	});
})

</script>
	</body>
</html>
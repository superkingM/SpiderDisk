<?php if (!defined('THINK_PATH')) exit(); G('begin'); ?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<meta name="apple-mobile-web-app-capable" content="yes">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="renderer" content="webkit">
<title><?php echo ($seo['title']); echo ($title); ?>-Powered By HC-CMS</title>
<meta name="keywords" content="<?php echo ($seo['keywords']); ?>" />
<meta name="description" content="<?php echo ($seo['desc']); ?>" />
<meta name="author" content="皇虫【QQ:1272881215】" />
<meta name="Copyright" content="HC-CMS版权所有" /> 
 <!-- Loading Bootstrap -->
<!--<link href="/newwangpan/Public/FlatUI/css/vendor/bootstrap.min.css" rel="stylesheet">-->
<!--^.^bootstrap css还是用百度的吧-->
<link href="http://apps.bdimg.com/libs/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet">
<!-- Loading Flat UI
<link href="/newwangpan/Public/FlatUI/css/flat-ui.min.css" rel="stylesheet">-->
<link href="http://cdn.bootcss.com/flat-ui/2.2.1/css/flat-ui.min.css" rel="stylesheet">
<link rel="shortcut icon" href="/newwangpan/Public/img/favicon.ico">

<!-- 请置于所有广告位代码之前 -->
<link rel="stylesheet" type="text/css" href="/newwangpan/Public/Home/default/header.css"/>
<link rel="stylesheet" type="text/css" href="/newwangpan/Public/Home/default/common.css"/>
<style type="text/css">
	.mobile_add{overflow-x: hidden !important;padding-left:0px !important;margin-left:0px !important;}
</style>
</head>
<body>
<!-- 页头开始 -->

<script src="http://apps.bdimg.com/libs/jquery/1.11.1/jquery.min.js"></script>
<style>
body{background: #EEEEEE;}
#second{color: red;font-size: 2em;padding: 0px 5px;}
a,a:visited{color: blue;}
a:hover{text-decoration: underline;}
ul,li{padding: 0px;margin: 0px;list-style: none;}
li{margin:5px 0px}
</style>
<div class="container">
	<div class="row">
		<div class="col-xs-12 text-center" style="margin-top:20px;">
			<p>还有<span id="second"><?php echo ($second); ?></span>秒跳转百度网盘资源.</p>
			<p>您可以选择：<a href="<?php echo ($url); ?>">立即跳转</a>，或者<a href="<?php echo U('So/index');?>">重新搜索</a></p>
		</div>
		<div class="col-xs-12">
			<h4 class="text-center">温馨提示</h4>
			<div class="col-sm-6 col-sm-offset-3 col-xs-12">
				<ul>
					<li>1、如果百度网盘资源已失效，请选择<a href="<?php echo U('So/index');?>">重新搜索</a></li>
					<li>2、在下载和使用百度网盘资源过程中，请遵守国家法律法规</li>
					<li>3、如果是压缩包资源，下载前请尝试使用百度网盘在线解压功能试下是否需要解压密码;由于资源是程序收录的，至于是否需要解压密码以及解压密码是多少，我们无从判断。</li>
					<li style="color: red;">4、
						<p>百度网盘资源侵权举报：<br />
							<a href="http://copyright.baidu.com/index.php/index/complaint" target="_blank">http://copyright.baidu.com/index.php/index/complaint</a>
						
						</p>
						<p>百度网盘资源违规举报：<br />
							<a href="http://help.baidu.com/pan/add" target="_blank">http://help.baidu.com/pan/add</a>
						</p>
					</li>
					<li>5、<a href="U('Index/index')">57百度云搜索</a>不存储任何资源，网站资源来自百度云蜘蛛程序对百度网盘资源的爬取，只作交流和学习使用，如有侵犯到您的权益，请联系百度网盘举报删除，删除资源后本站收录的资源分享链接也将自动失效。如有疑问，也可联系我们。联系邮箱：1272881215@qq.com 联系QQ：1272881215</li>
				</ul>
			</div>
			<div style="display: none;"><?php echo ($sys['tongji']); ?></div
		</div>
	</div>
</div>
<script type="text/javascript">
	$(function(){
		var second="<?php echo ($second); ?>";
		var jump_url="<?php echo ($url); ?>";
		var tt=setInterval(function(){
			second--;
			$("#second").text(second);
			if(second==0) {
				location.href=jump_url;
				clearInterval(tt);
			}
		},1000);
	})
</script>
	<!-- 页脚结束 -->
    <!-- Include all compiled plugins (below), or include individual files as needed 
    <script src="/newwangpan/Public/FlatUI/js/flat-ui.min.js"></script>-->
	<script src="http://cdn.bootcss.com/flat-ui/2.2.1/js/flat-ui.min.js"></script>
    <!-- 加载广告js -->
<script>
$(document).ready(function(){
	var top=$("nav").offset().top;
	$(window).scroll(function(){
	    if($(window).scrollTop()>top){
	    	$("nav").addClass("navbar-fixed-top");
	    	$(".hide").hide();
	    }else{
	    	$("nav").removeClass("navbar-fixed-top");
	    	$(".hide").show();
	    }
	 });
});
</script>
</body>
</html>
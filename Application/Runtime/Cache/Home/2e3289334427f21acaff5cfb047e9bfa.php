<?php if (!defined('THINK_PATH')) exit(); G('begin'); ?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<meta name="apple-mobile-web-app-capable" content="yes">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="renderer" content="webkit">
<title><?php echo ($seo['title']); echo ($title); ?>-Powered By HC-CMS</title>
<meta name="keywords" content="<?php echo ($seo['keywords']); ?>" />
<meta name="description" content="<?php echo ($seo['desc']); ?>" />
<meta name="author" content="皇虫【QQ:1272881215】" />
<meta name="Copyright" content="HC-CMS版权所有" /> 
<link rel="shortcut icon" href="/newwangpan/Public/img/favicon.ico">
<!--<link href="/newwangpan/Public/FlatUI/css/vendor/bootstrap.min.css" rel="stylesheet">-->
<link href="http://apps.bdimg.com/libs/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet">
<!-- <link href="/newwangpan/Public/FlatUI/css/flat-ui.min.css" rel="stylesheet"> -->
<link href="http://cdn.bootcss.com/flat-ui/2.2.1/css/flat-ui.min.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="/newwangpan/Public/Home/default/header.css"/>
<link rel="stylesheet" type="text/css" href="/newwangpan/Public/Home/default/common.css"/>
<style type="text/css">
	.mobile_add{overflow-x: hidden !important;padding-left:0px !important;margin-left:0px !important;}
</style>

	<link rel="stylesheet" type="text/css" href="/newwangpan/Public/Home/default/album_detail.css"/>
	<link rel="stylesheet" type="text/css" href="/newwangpan/Public/css/icon.css"/>

</head>
<body>
<!-- 页头开始 -->

<div class="container">
<!-- 这里是广告位 -->
<div class="row myhide">
		<div class="visible-xs col-xs-12 mobile_add" style="overflow-x: hidden;"><?php echo W('Mobilead/index',array('position'=>'global_top'));?></div>
		<div class="col-xs-12 col-sm-12 col-md-12" style="background-color:#eee;height:40px;line-height:40px;color:#999;">
			<span class="hidden-xs"><a href="javascript:alert('网站基本功能已开发完毕，欢迎大家体验。如有意见和建议，请联系 皇虫，QQ：1272881215')">使用帮助</a> | </span>
			<span style="color:green;font-weight:bold;">爱我就收藏我，管我明天是谁的新娘。</span>
		</div>
		<div class="col-xs-12 col-sm-12 col-md-12 top_ad" id="top_ad"><?php echo ($top_ad); ?></div>
</div>
<div class="row myhide">
	<div class="col-xs-12 col-sm-4 col-md-3">
		<div class="col-xs-12 col-sm-12 col-md-12">
			<a href="<?php echo U('Index/index');?>" ><img src="/newwangpan/Public/img/logo.png" style="max-width:100%" alt="<?php echo ($sys["sitename"]); ?>"/></a>
		</div>
	</div>
	<div class="hidden-xs col-md-offset-4 col-sm-offset-2 col-sm-6 col-md-5">
		<form action="<?php echo U('So/result');?>" method="GET" style="margin-top:20px;">
        	<div class="input-group">
             <input type="text" name="keyword" id="keyword" value="<?php echo ($_GET['keyword']); ?>" placeholder="请输入搜索关键字" class="form-control keyword">
             <span class="input-group-btn">
                <button class="btn btn-default" type="submit">百度云一下</button>
             </span>
         	</div><!-- /input-group -->
        </form>
	</div>
</div>
<div class="row visible-xs" style="margin-top:10px;">
	<div class="col-xs-12">
      <form role="search" action="<?php echo U('So/result');?>">
       <div class="form-group">
         <div class="input-group input-group-lg">
           <input type="search" name="keyword" placeholder="请输入资源关键字" id="navbarInput-01" class="form-control">
           <input type="hidden" name="type" value="share">
           <span class="input-group-btn">
                <button class="btn btn-default" type="submit">百度云一下</button>
           </span>           
         </div>
       </div>               
     </form>
    </div>
</div>
<div class="row">
<nav role="navigation" class="navbar navbar-inverse  col-xs-12 col-sm-12 col-md-12" style="border-radius:0px;">
		<div class="navbar-header visible-xs">
	      <button type="button" class="navbar-toggle" data-toggle="collapse" 
	         data-target="#example-navbar-collapse">
	         <span class="sr-only">切换导航</span>
	         <span class="icon-bar"></span>
	         <span class="icon-bar"></span>
	         <span class="icon-bar"></span>
	      </button>
	      <a class="navbar-brand" href="<?php echo U('Index/index');?>"><?php echo ($sys["sitename"]); ?></a>
   		</div>
		<div class="collapse navbar-collapse" id="example-navbar-collapse">
            <ul class="nav navbar-nav"> 
              <?php if(is_array($nav)): $k = 0; $__LIST__ = $nav;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$nav): $mod = ($k % 2 );++$k; switch($nav["flag"]): case "Index": ?><li <?php if(CONTROLLER_NAME== 'Index' || CONTROLLER_NAME== 'Detail'): ?>class="active"<?php endif; ?>><a href="<?php echo U('Index/index');?>"><?php echo ($nav['name']); ?></a></li><?php break;?>
              			<?php case "Video": ?><li <?php if(CONTROLLER_NAME== 'Video'): ?>class="active"<?php endif; ?>><a href="<?php echo U('Video/index');?>"><?php echo ($nav['name']); ?></a></li><?php break;?>
              			<?php case "Music": ?><li <?php if(CONTROLLER_NAME== 'Music'): ?>class="active"<?php endif; ?>><a href="<?php echo U('Music/index');?>"><?php echo ($nav['name']); ?></a></li><?php break;?>
              			<?php case "Software": ?><li <?php if(CONTROLLER_NAME== 'Software'): ?>class="active"<?php endif; ?>><a href="<?php echo U('Software/index');?>"><?php echo ($nav['name']); ?></a></li><?php break;?>
              			<?php case "Album": ?><li <?php if(CONTROLLER_NAME== 'Album'): ?>class="active"<?php endif; ?>><a href="<?php echo U('Album/index');?>"><?php echo ($nav['name']); ?></a></li><?php break;?>
              			<?php case "Torrent": ?><li <?php if(CONTROLLER_NAME== 'Torrent'): ?>class="active"<?php endif; ?>><a href="<?php echo U('Torrent/index');?>"><?php echo ($nav['name']); ?></a></li><?php break;?>
              			<?php case "User": ?><li <?php if(CONTROLLER_NAME== 'User'): ?>class="active"<?php endif; ?>><a href="<?php echo U('User/index');?>"><?php echo ($nav['name']); ?></a></li><?php break;?>
              			<?php case "Tag": ?><li <?php if(CONTROLLER_NAME== 'Tag'): ?>class="active"<?php endif; ?> class="hidden-sm"><a href="<?php echo U('Tag/index');?>"><?php echo ($nav['name']); ?></a></li><?php break;?>
              			<?php case "News": ?><li <?php if(CONTROLLER_NAME== 'News'): ?>class="active"<?php endif; ?> class="hidden-sm"><a href="<?php echo U('News/index');?>"><?php echo ($nav['name']); ?></a></li><?php break;?>
              			<?php case "Book": ?><li <?php if(CONTROLLER_NAME== 'Book'): ?>class="active"<?php endif; ?> class="hidden-sm"><a href="<?php echo U('Book/index');?>"><?php echo ($nav['name']); ?></a></li><?php break;?>
              			<?php case "Zip": ?><li <?php if(CONTROLLER_NAME== 'Zip'): ?>class="active"<?php endif; ?> class="hidden-sm"><a href="<?php echo U('Zip/index');?>"><?php echo ($nav['name']); ?></a></li><?php break;?>
              			<?php case "Diy": ?><li class="hidden-sm"><a href="<?php echo ($nav['site']); ?>" target="_blank"><?php echo ($nav['name']); ?></a></li><?php break; endswitch; endforeach; endif; else: echo "" ;endif; ?>
              <li class="visible-sm visible-md"><a href="<?php echo U('So/index');?>" style="color:red;">搜索</a></li>
            </ul> 

         </div>
</nav>
<!--导航广告-->
<div class="hidden-xs col-sm-12 col-md-12 nav_ad"><?php echo ($nav_ad); ?></div>
<!-- 分享代码 -->
<script>window._bd_share_config={"common":{"bdSnsKey":{},"bdText":"","bdMini":"2","bdMiniList":false,"bdPic":"","bdStyle":"0","bdSize":"16"},"slide":{"type":"slide","bdImg":"6","bdPos":"right","bdTop":"161"},"image":{"viewList":["qzone","tsina","tqq","renren","weixin"],"viewText":"分享到：","viewSize":"16"},"selectShare":{"bdContainerClass":null,"bdSelectMiniList":["qzone","tsina","tqq","renren","weixin"]}};with(document)0[(getElementsByTagName('head')[0]||body).appendChild(createElement('script')).src='http://bdimg.share.baidu.com/static/api/js/share.js?v=89860593.js?cdnversion='+~(-new Date()/36e5)];</script>
<?php if(CONTROLLER_NAME!= 'Index'): ?><div class="col-xs-12 col-sm-12 col-md-12 current_position">
		<div class="col-xs-12 col-sm-12">
			<strong>当前位置：</strong><a href="<?php echo U('Index/index');?>" title="首页">首页</a>
			<?php switch(CONTROLLER_NAME): case "Video": ?>/ <a href="<?php echo U('Video/index');?>" title="影视">影视</a><?php break;?>
				<?php case "Music": ?>/ <a href="<?php echo U('Music/index');?>" title="音乐">音乐</a><?php break;?>
				<?php case "Software": ?>/ <a href="<?php echo U('Software/index');?>" title="软件">软件</a><?php break;?>
				<?php case "Album": ?>/ <a href="<?php echo U('Album/index');?>" title="专辑">专辑</a><?php break;?>
				<?php case "Torrent": ?>/ <a href="<?php echo U('Torrent/index');?>" title="种子">种子</a><?php break;?>
				<?php case "User": ?>/ <a href="<?php echo U('User/index');?>" title="达人">达人</a><?php break;?>
				<?php case "Tag": ?>/ <a href="<?php echo U('Tag/index');?>" title="标签">标签</a><?php break;?>
				<?php case "News": ?>/ <a href="<?php echo U('News/index');?>" title="资讯">资讯</a><?php break;?>
				<?php case "Book": ?>/ <a href="<?php echo U('Book/index');?>" title="电子书">电子书</a><?php break;?>
				<?php case "Zip": ?>/ <a href="<?php echo U('Zip/index');?>" title="压缩资源">压缩资源</a><?php break;?>
				<?php case "Detail": ?>/ 资源详情<?php break; endswitch;?>
			<?php if(CONTROLLER_NAME== 'Video' && ACTION_NAME== 'index'): ?>/ 影视列表<?php endif; ?>
			<?php if(CONTROLLER_NAME== 'Music' && ACTION_NAME== 'index'): ?>/ 音乐列表<?php endif; ?>
			<?php if(CONTROLLER_NAME== 'Software' && ACTION_NAME== 'index'): ?>/ 软件列表<?php endif; ?>
			<?php if(CONTROLLER_NAME== 'Album' && ACTION_NAME== 'index'): ?>/ 专辑列表<?php endif; ?>
			<?php if(CONTROLLER_NAME== 'Album' && ACTION_NAME== 'detail'): ?>/ 专辑详情<?php endif; ?>
			<?php if(CONTROLLER_NAME== 'Album' && ACTION_NAME== 'detail'): ?>/ 专辑文件<?php endif; ?>
			<?php if(CONTROLLER_NAME== 'Torrent' && ACTION_NAME== 'index'): ?>/ 种子列表<?php endif; ?>
			<?php if(CONTROLLER_NAME== 'User' && ACTION_NAME== 'index'): ?>/ 达人列表<?php endif; ?>
			<?php if(CONTROLLER_NAME== 'User' && ACTION_NAME== 'home'): ?>/ 达人中心<?php endif; ?>
			<?php if(CONTROLLER_NAME== 'Tag' && ACTION_NAME== 'index'): ?>/ 标签列表<?php endif; ?>
			<?php if(CONTROLLER_NAME== 'News' && ACTION_NAME== 'index'): ?>/ 资讯列表<?php endif; ?>
			<?php if(CONTROLLER_NAME== 'News' && ACTION_NAME== 'article'): ?>/ 资讯内容<?php endif; ?>
			<?php if(CONTROLLER_NAME== 'Book' && ACTION_NAME== 'index'): ?>/ 电子书列表<?php endif; ?>
			<?php if(CONTROLLER_NAME== 'Zip' && ACTION_NAME== 'index'): ?>/ 压缩资源列表<?php endif; ?>
			<?php if(CONTROLLER_NAME== 'Forbes' && ACTION_NAME== 'index'): ?>/ 福布斯排行<?php endif; ?>
		</div>
	</div><?php endif; ?>
</div>

  <!--主体部分-->
  
	<div class="row" style="border:1px solid #ddd;">
		<div class="col-xs-12 col-sm-8 source_detail">
			<div class="source_header"><h6><em class="icon-common icon-album"></em><?php echo ($data["title"]); ?></h6></div>
			<div class="visible-xs col-xs-12 mobile_add"><?php echo W('Mobilead/index',array('position'=>'detail_top'));?></div>
			<div class="desc">
				<?php echo ($data["desc"]); ?>
			</div>
			<div class="detail_ad"><?php echo ($detail_left_ad); ?></div>
			<ul>
				<li>文件数量  : <?php echo ($data["filecount"]); ?></li>
				<li>资源类型  : 专辑</li>
				<li>浏览次数  : <?php echo ($data["vCnt"]); ?></li>
				<li>下载次数  : <?php echo ($data["dCnt"]); ?></li>
				<li>转存次数  : <?php echo ($data["tCnt"]); ?></li>
				<li>本站点击  : <?php echo ($data["click"]); ?></li>
				<li>分享达人  : <?php echo ($data["name"]); ?></li>
				<li>收录时间  : <?php echo (date("Y-m-d H:i:s",$data["time"])); ?></li>
				<li class="li_down">下载地址  : 
						<a href="javascript:;" class="download" url="<?php echo U('Jump/jump');?>?url=<?php echo url_encode('http://yun.baidu.com/pcloud/album/info?uk='.$data['uk'].'&album_id='.$data['album_id']);?>" style="background:none;border:none"><img src="/newwangpan/Public/img/wangpan.gif"/></a>
						<a href="<?php echo U('Report/add',array('id'=>$data['id'],'model'=>'album','title'=>urlencode($data['title'])));?>" target="_blank" rel="nofollow" title="举报当前资源"><img height="34px" src="/newwangpan/report-blue.png"/></a>
				</li>
			</ul>
			<div class="visible-xs col-xs-12 mobile_add"><?php echo W('Mobilead/index',array('position'=>'detail_bottom'));?></div>
			
			<a class="col-sm-12 text-right" href="http://ini.litingxin.cn/hezi/qh/setup_xx0626.php" target="_blank" rel="nofollow">
				<img src="/newwangpan/down.png" class="img-responsive"/>
			</a>
			
			<div class="col-xs-12 text-right">
				<div class="bdsharebuttonbox"><a href="#" class="bds_qzone" data-cmd="qzone" title="分享到QQ空间">QQ空间</a><a href="#" class="bds_tsina" data-cmd="tsina" title="分享到新浪微博">新浪微博</a><a href="#" class="bds_tqq" data-cmd="tqq" title="分享到腾讯微博">腾讯微博</a><a href="#" class="bds_renren" data-cmd="renren" title="分享到人人网">人人网</a><a href="#" class="bds_weixin" data-cmd="weixin" title="分享到微信">微信</a><a href="#" class="bds_tieba" data-cmd="tieba" title="分享到百度贴吧">百度贴吧</a><a href="#" class="bds_sqq" data-cmd="sqq" title="分享到QQ好友">QQ好友</a><a href="#" class="bds_ty" data-cmd="ty" title="分享到天涯社区">天涯社区</a><a href="#" class="bds_kaixin001" data-cmd="kaixin001" title="分享到开心网">开心网</a><a href="#" class="bds_more" data-cmd="more">更多</a></div>
	<script>window._bd_share_config={"common":{"bdSnsKey":{},"bdText":"","bdMini":"2","bdMiniList":false,"bdPic":"","bdStyle":"1","bdSize":"24"},"share":{"bdSize":16},"image":{"viewList":["qzone","tsina","tqq","renren","weixin","tieba","sqq","ty","kaixin001"],"viewText":"更多","viewSize":"24"},"selectShare":{"bdContainerClass":null,"bdSelectMiniList":["qzone","tsina","tqq","renren","weixin","tieba","sqq","ty","kaixin001"]}};with(document)0[(getElementsByTagName('head')[0]||body).appendChild(createElement('script')).src='http://bdimg.share.baidu.com/static/api/js/share.js?v=89860593.js?cdnversion='+~(-new Date()/36e5)];</script>
			</div>
		</div>
		<div class="col-xs-12 col-sm-4">
			<div class="user_header" style="border-left:3px solid #1ABC9C;padding-left:5px;"><h6>达人明信片</h6></div>
			<div class="detail_user">
				<div class="avatar">
					<span><a href="<?php echo U('User/home',array('id'=>$data['id']));?>"><img alt="" src="<?php echo ($data["avatar"]); ?>"></a></span><p/>
					<span style="padding-top:20px;"><a href="<?php echo U('User/home',array('id'=>$data['id']));?>" title="<?php echo ($data["name"]); ?>"><?php echo ($data["name"]); ?></a></span>
				</div>
				<div class="info">
					<ul>
						<li>分享<a href="<?php echo U('User/home',array('id'=>$data['id'],'type'=>'share'));?>"><em><?php echo ($data["share"]); ?></em></a></li>
						<li>专辑<a href="<?php echo U('User/home',array('id'=>$data['id'],'type'=>'album'));?>"><em><?php echo ($data["album"]); ?></em></a></li>
						<li>订阅 <a href=""><em><?php echo ($data["follow"]); ?></em></a></li>
						<li>粉丝 <a href=""><em><?php echo ($data["fans"]); ?></em></a></li>
						<li><span class="user_homepage"><a href="<?php echo U('User/home',array('id'=>$data['id']));?>">会员主页</a></span></li>
					</ul>
				</div>
				<span class="show">个人说明<br>　　<?php echo ((isset($data["show"]) && ($data["show"] !== ""))?($data["show"]):"该会员暂无个人说明"); ?></span>
			</div>
		</div>
	</div>
	<div class="row" style="margin-top:10px;text-align: center;">
		<div class="hidden-xs col-sm-12 col-md-12 row_ad"><?php echo ($detail_center_ad); ?></div>
	</div>
	<div class="row" style="border:1px solid #ddd;margin-top:10px;">
	<div class="visible-xs col-xs-12 mobile_add"><?php echo W('Mobilead/index',array('position'=>'detail_bottom'));?></div>
	<div class="more col-sm-8 col-xs-12">
		<div class="div_header">当前专辑文件列表</div>
		<div class="album">
			<ul>
				<?php if(empty($list)): ?><li style="text-align: center;">
						<img src="/newwangpan/Public/img/surprize2.png" style="border-radius: 50%;"/>
						<h6>我和我的小伙伴们都震惊了。。。居然还没找到该资源。。</h6>
					</li><?php endif; ?>
				
				<?php if(is_array($list)): $i = 0; $__LIST__ = $list;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$list): $mod = ($i % 2 );++$i;?><li>
					<a href="<?php echo U('Album/file',array('id'=>$list['id']));?>" title="<?php echo ($list["server_filename"]); ?>"><em class="icon-common icon-<?php echo ($list["doctype"]); ?>"></em><?php echo ($list["server_filename"]); ?></a>
				</li><?php endforeach; endif; else: echo "" ;endif; ?>
			</ul>
			
		</div>
		<?php echo ($page); ?>			
	</div>
	<div class="visible-xs col-xs-12 mobile_add"><?php echo W('Mobilead/index',array('position'=>'detail_bottom'));?></div>
	<div class="more col-sm-4 col-xs-12">
		<div class="div_header">达人随机秀</div>
		<div class="user">
			<ul>
				<?php if(is_array($user)): $i = 0; $__LIST__ = $user;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$user): $mod = ($i % 2 );++$i;?><li>
					<span class="avatar"><a href="<?php echo U('User/home',array('id'=>$user['id']));?>"  title="<?php echo ($user["name"]); ?>"><img alt="<?php echo ($user["name"]); ?>" src="<?php echo ($user["avatar"]); ?>"></a></span>
					<span class="user_name"><a href="<?php echo U('User/home',array('id'=>$user['id']));?>" title="<?php echo ($user["name"]); ?>"><?php echo ($user["name"]); ?></a></span><br/>
					<span class="user_show"><?php echo ((isset($user["show"]) && ($user["show"] !== ""))?($user["show"]):"该会员暂无个人说明"); ?></span>
				</li><?php endforeach; endif; else: echo "" ;endif; ?>
			</ul>
		</div>
	</div>
	</div>

	



<div class="row">
	<div class="hidden-xs col-sm-12 col-md-12" id="bottom_ad"><?php echo ($bottom_ad); ?></div>
	<div class="visible-xs col-xs-12 mobile_add"><?php echo W('Mobilead/index',array('position'=>'global_bottom'));?></div>
</div>
</div>
<link rel="stylesheet" type="text/css" href="/newwangpan/Public/Home/default/footer.css"/>
<div class="container">
	<div class="row footer">
		<div class="col-xs-12">
		<div class="friendlink_header">友情链接</div>
			<ul class="friendlink">
			<?php if(is_array($friendlink)): $i = 0; $__LIST__ = $friendlink;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$friendlink): $mod = ($i % 2 );++$i;?><li><a href="<?php echo ($friendlink['url']); ?>" target="_blank" title="<?php echo ($friendlink['desc']); ?>"><?php echo ($friendlink['name']); ?></a></li><?php endforeach; endif; else: echo "" ;endif; ?>
			</ul>
		</div>
	</div>
	<div class="row">
		<div class="col-xs-12 col-sm-12 text-center copyright"></div>
		<div class="col-xs-12 text-center" style="font-size:14px;color:#6E6E6E;text-align:center;margin:0px"><?php echo ($sys['copyright']); ?></div>
		<div class="col-xs-12 text-center">
			<span style="font-size:0.7em">SpiderDisk For BIDU <em style="font-size:0.8em">beta1.0</em></span> |  
			<span style="font-size:0.7em">Powered By <a href="http://www.hc-cms.com" target="_blank" style="color:blue;text-decoration:underline;">HC-CMS</a></span>
			<br/>
			<span style="font-size: 12px;color: #666;">本次执行耗时:<?php G('end');echo G('begin','end',6).'s'; ?></span>
			<br />
			<span style="font-size:0.7em"><a href="http://www.miitbeian.gov.cn/" target="_blank"><?php echo ($sys['beian']); ?></a></span><br/>
			<?php echo ($sys['tongji']); ?>
		</div>
		<div class="visible-xs col-xs-12 mobile_add"><?php echo W('Mobilead/index',array('position'=>'global_footer'));?></div>
	</div>
</div>

	<a href="http://duobao.nntzd.com/webapp/dowload.html" target="_blank" rel="nofollow" style="position:fixed;bottom:0px;left:0px;width:100%">
		<img src="http://www.weiyoou.com/duobao.gif" style="width: 100%;"/>
	</a>


<script src="http://cdn.bootcss.com/html5shiv/3.7.2/html5shiv.min.js"></script>
<script src="http://cdn.bootcss.com/respond.js/1.4.2/respond.min.js"></script>
<script src="http://apps.bdimg.com/libs/jquery/1.11.1/jquery.min.js"></script>
<!--<script src="/newwangpan/Public/FlatUI/js/flat-ui.min.js"></script>-->
<script src="http://cdn.bootcss.com/flat-ui/2.2.1/js/flat-ui.min.js"></script>
<script>
$(document).ready(function(){
	var top=$("nav").offset().top;
	$(window).scroll(function(){
	    if($(window).scrollTop()>top){
	    	$("nav").addClass("navbar-fixed-top");
	    	$(".hide").hide();
	    }else{
	    	$("nav").removeClass("navbar-fixed-top");
	    	$(".hide").show();
	    }
	 });
});
</script>

	<script>
	$(function(){
		$(".download").click(function(){
			window.open($(this).attr('url'),"_blank");
		});
		var id=<?php echo ($_GET['id']); ?>;
		$.get("<?php echo U('Common/addClick');?>",{id:id,t:'album'});
	});
	</script>

<?php if(!is_mobile()): ?><script>var ld_uid="800212";var ld_maxw=0;</script><script charset="utf-8" src="11http://vip.526d.com/js/cpv_fm_r.js"></script><?php endif; ?>
</body>
</html>
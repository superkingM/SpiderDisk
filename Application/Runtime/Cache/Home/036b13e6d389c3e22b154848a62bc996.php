<?php if (!defined('THINK_PATH')) exit(); G('begin'); ?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<meta name="apple-mobile-web-app-capable" content="yes">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="renderer" content="webkit">
<title><?php echo ($seo['title']); echo ($title); ?>-Powered By HC-CMS</title>
<meta name="keywords" content="<?php echo ($seo['keywords']); ?>" />
<meta name="description" content="<?php echo ($seo['desc']); ?>" />
<meta name="author" content="皇虫【QQ:1272881215】" />
<meta name="Copyright" content="HC-CMS版权所有" /> 
<link rel="shortcut icon" href="/newwangpan/Public/img/favicon.ico">
<!--<link href="/newwangpan/Public/FlatUI/css/vendor/bootstrap.min.css" rel="stylesheet">-->
<link href="http://apps.bdimg.com/libs/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet">
<!-- <link href="/newwangpan/Public/FlatUI/css/flat-ui.min.css" rel="stylesheet"> -->
<link href="http://cdn.bootcss.com/flat-ui/2.2.1/css/flat-ui.min.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="/newwangpan/Public/Home/default/header.css"/>
<link rel="stylesheet" type="text/css" href="/newwangpan/Public/Home/default/common.css"/>
<style type="text/css">
	.mobile_add{overflow-x: hidden !important;padding-left:0px !important;margin-left:0px !important;}
</style>

	<link rel="stylesheet" type="text/css" href="/newwangpan/Public/Home/default/index.css"/>

</head>
<body>
<!-- 页头开始 -->

<div class="container">
<!-- 这里是广告位 -->
<div class="row myhide">
		<div class="visible-xs col-xs-12 mobile_add" style="overflow-x: hidden;"><?php echo W('Mobilead/index',array('position'=>'global_top'));?></div>
		<div class="col-xs-12 col-sm-12 col-md-12" style="background-color:#eee;height:40px;line-height:40px;color:#999;">
			<span class="hidden-xs"><a href="javascript:alert('网站基本功能已开发完毕，欢迎大家体验。如有意见和建议，请联系 皇虫，QQ：1272881215')">使用帮助</a> | </span>
			<span style="color:green;font-weight:bold;">爱我就收藏我，管我明天是谁的新娘。</span>
		</div>
		<div class="col-xs-12 col-sm-12 col-md-12 top_ad" id="top_ad"><?php echo ($top_ad); ?></div>
</div>
<div class="row myhide">
	<div class="col-xs-12 col-sm-4 col-md-3">
		<div class="col-xs-12 col-sm-12 col-md-12">
			<a href="<?php echo U('Index/index');?>" ><img src="/newwangpan/Public/img/logo.png" style="max-width:100%" alt="<?php echo ($sys["sitename"]); ?>"/></a>
		</div>
	</div>
	<div class="hidden-xs col-md-offset-4 col-sm-offset-2 col-sm-6 col-md-5">
		<form action="<?php echo U('So/result');?>" method="GET" style="margin-top:20px;">
        	<div class="input-group">
             <input type="text" name="keyword" id="keyword" value="<?php echo ($_GET['keyword']); ?>" placeholder="请输入搜索关键字" class="form-control keyword">
             <span class="input-group-btn">
                <button class="btn btn-default" type="submit">百度云一下</button>
             </span>
         	</div><!-- /input-group -->
        </form>
	</div>
</div>
<div class="row visible-xs" style="margin-top:10px;">
	<div class="col-xs-12">
      <form role="search" action="<?php echo U('So/result');?>">
       <div class="form-group">
         <div class="input-group input-group-lg">
           <input type="search" name="keyword" placeholder="请输入资源关键字" id="navbarInput-01" class="form-control">
           <input type="hidden" name="type" value="share">
           <span class="input-group-btn">
                <button class="btn btn-default" type="submit">百度云一下</button>
           </span>           
         </div>
       </div>               
     </form>
    </div>
</div>
<div class="row">
<nav role="navigation" class="navbar navbar-inverse  col-xs-12 col-sm-12 col-md-12" style="border-radius:0px;">
		<div class="navbar-header visible-xs">
	      <button type="button" class="navbar-toggle" data-toggle="collapse" 
	         data-target="#example-navbar-collapse">
	         <span class="sr-only">切换导航</span>
	         <span class="icon-bar"></span>
	         <span class="icon-bar"></span>
	         <span class="icon-bar"></span>
	      </button>
	      <a class="navbar-brand" href="<?php echo U('Index/index');?>"><?php echo ($sys["sitename"]); ?></a>
   		</div>
		<div class="collapse navbar-collapse" id="example-navbar-collapse">
            <ul class="nav navbar-nav"> 
              <?php if(is_array($nav)): $k = 0; $__LIST__ = $nav;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$nav): $mod = ($k % 2 );++$k; switch($nav["flag"]): case "Index": ?><li <?php if(CONTROLLER_NAME== 'Index' || CONTROLLER_NAME== 'Detail'): ?>class="active"<?php endif; ?>><a href="<?php echo U('Index/index');?>"><?php echo ($nav['name']); ?></a></li><?php break;?>
              			<?php case "Video": ?><li <?php if(CONTROLLER_NAME== 'Video'): ?>class="active"<?php endif; ?>><a href="<?php echo U('Video/index');?>"><?php echo ($nav['name']); ?></a></li><?php break;?>
              			<?php case "Music": ?><li <?php if(CONTROLLER_NAME== 'Music'): ?>class="active"<?php endif; ?>><a href="<?php echo U('Music/index');?>"><?php echo ($nav['name']); ?></a></li><?php break;?>
              			<?php case "Software": ?><li <?php if(CONTROLLER_NAME== 'Software'): ?>class="active"<?php endif; ?>><a href="<?php echo U('Software/index');?>"><?php echo ($nav['name']); ?></a></li><?php break;?>
              			<?php case "Album": ?><li <?php if(CONTROLLER_NAME== 'Album'): ?>class="active"<?php endif; ?>><a href="<?php echo U('Album/index');?>"><?php echo ($nav['name']); ?></a></li><?php break;?>
              			<?php case "Torrent": ?><li <?php if(CONTROLLER_NAME== 'Torrent'): ?>class="active"<?php endif; ?>><a href="<?php echo U('Torrent/index');?>"><?php echo ($nav['name']); ?></a></li><?php break;?>
              			<?php case "User": ?><li <?php if(CONTROLLER_NAME== 'User'): ?>class="active"<?php endif; ?>><a href="<?php echo U('User/index');?>"><?php echo ($nav['name']); ?></a></li><?php break;?>
              			<?php case "Tag": ?><li <?php if(CONTROLLER_NAME== 'Tag'): ?>class="active"<?php endif; ?> class="hidden-sm"><a href="<?php echo U('Tag/index');?>"><?php echo ($nav['name']); ?></a></li><?php break;?>
              			<?php case "News": ?><li <?php if(CONTROLLER_NAME== 'News'): ?>class="active"<?php endif; ?> class="hidden-sm"><a href="<?php echo U('News/index');?>"><?php echo ($nav['name']); ?></a></li><?php break;?>
              			<?php case "Book": ?><li <?php if(CONTROLLER_NAME== 'Book'): ?>class="active"<?php endif; ?> class="hidden-sm"><a href="<?php echo U('Book/index');?>"><?php echo ($nav['name']); ?></a></li><?php break;?>
              			<?php case "Zip": ?><li <?php if(CONTROLLER_NAME== 'Zip'): ?>class="active"<?php endif; ?> class="hidden-sm"><a href="<?php echo U('Zip/index');?>"><?php echo ($nav['name']); ?></a></li><?php break;?>
              			<?php case "Diy": ?><li class="hidden-sm"><a href="<?php echo ($nav['site']); ?>" target="_blank"><?php echo ($nav['name']); ?></a></li><?php break; endswitch; endforeach; endif; else: echo "" ;endif; ?>
              <li class="visible-sm visible-md"><a href="<?php echo U('So/index');?>" style="color:red;">搜索</a></li>
            </ul> 

         </div>
</nav>
<!--导航广告-->
<div class="hidden-xs col-sm-12 col-md-12 nav_ad"><?php echo ($nav_ad); ?></div>
<!-- 分享代码 -->
<script>window._bd_share_config={"common":{"bdSnsKey":{},"bdText":"","bdMini":"2","bdMiniList":false,"bdPic":"","bdStyle":"0","bdSize":"16"},"slide":{"type":"slide","bdImg":"6","bdPos":"right","bdTop":"161"},"image":{"viewList":["qzone","tsina","tqq","renren","weixin"],"viewText":"分享到：","viewSize":"16"},"selectShare":{"bdContainerClass":null,"bdSelectMiniList":["qzone","tsina","tqq","renren","weixin"]}};with(document)0[(getElementsByTagName('head')[0]||body).appendChild(createElement('script')).src='http://bdimg.share.baidu.com/static/api/js/share.js?v=89860593.js?cdnversion='+~(-new Date()/36e5)];</script>
<?php if(CONTROLLER_NAME!= 'Index'): ?><div class="col-xs-12 col-sm-12 col-md-12 current_position">
		<div class="col-xs-12 col-sm-12">
			<strong>当前位置：</strong><a href="<?php echo U('Index/index');?>" title="首页">首页</a>
			<?php switch(CONTROLLER_NAME): case "Video": ?>/ <a href="<?php echo U('Video/index');?>" title="影视">影视</a><?php break;?>
				<?php case "Music": ?>/ <a href="<?php echo U('Music/index');?>" title="音乐">音乐</a><?php break;?>
				<?php case "Software": ?>/ <a href="<?php echo U('Software/index');?>" title="软件">软件</a><?php break;?>
				<?php case "Album": ?>/ <a href="<?php echo U('Album/index');?>" title="专辑">专辑</a><?php break;?>
				<?php case "Torrent": ?>/ <a href="<?php echo U('Torrent/index');?>" title="种子">种子</a><?php break;?>
				<?php case "User": ?>/ <a href="<?php echo U('User/index');?>" title="达人">达人</a><?php break;?>
				<?php case "Tag": ?>/ <a href="<?php echo U('Tag/index');?>" title="标签">标签</a><?php break;?>
				<?php case "News": ?>/ <a href="<?php echo U('News/index');?>" title="资讯">资讯</a><?php break;?>
				<?php case "Book": ?>/ <a href="<?php echo U('Book/index');?>" title="电子书">电子书</a><?php break;?>
				<?php case "Zip": ?>/ <a href="<?php echo U('Zip/index');?>" title="压缩资源">压缩资源</a><?php break;?>
				<?php case "Detail": ?>/ 资源详情<?php break; endswitch;?>
			<?php if(CONTROLLER_NAME== 'Video' && ACTION_NAME== 'index'): ?>/ 影视列表<?php endif; ?>
			<?php if(CONTROLLER_NAME== 'Music' && ACTION_NAME== 'index'): ?>/ 音乐列表<?php endif; ?>
			<?php if(CONTROLLER_NAME== 'Software' && ACTION_NAME== 'index'): ?>/ 软件列表<?php endif; ?>
			<?php if(CONTROLLER_NAME== 'Album' && ACTION_NAME== 'index'): ?>/ 专辑列表<?php endif; ?>
			<?php if(CONTROLLER_NAME== 'Album' && ACTION_NAME== 'detail'): ?>/ 专辑详情<?php endif; ?>
			<?php if(CONTROLLER_NAME== 'Album' && ACTION_NAME== 'detail'): ?>/ 专辑文件<?php endif; ?>
			<?php if(CONTROLLER_NAME== 'Torrent' && ACTION_NAME== 'index'): ?>/ 种子列表<?php endif; ?>
			<?php if(CONTROLLER_NAME== 'User' && ACTION_NAME== 'index'): ?>/ 达人列表<?php endif; ?>
			<?php if(CONTROLLER_NAME== 'User' && ACTION_NAME== 'home'): ?>/ 达人中心<?php endif; ?>
			<?php if(CONTROLLER_NAME== 'Tag' && ACTION_NAME== 'index'): ?>/ 标签列表<?php endif; ?>
			<?php if(CONTROLLER_NAME== 'News' && ACTION_NAME== 'index'): ?>/ 资讯列表<?php endif; ?>
			<?php if(CONTROLLER_NAME== 'News' && ACTION_NAME== 'article'): ?>/ 资讯内容<?php endif; ?>
			<?php if(CONTROLLER_NAME== 'Book' && ACTION_NAME== 'index'): ?>/ 电子书列表<?php endif; ?>
			<?php if(CONTROLLER_NAME== 'Zip' && ACTION_NAME== 'index'): ?>/ 压缩资源列表<?php endif; ?>
			<?php if(CONTROLLER_NAME== 'Forbes' && ACTION_NAME== 'index'): ?>/ 福布斯排行<?php endif; ?>
		</div>
	</div><?php endif; ?>
</div>

  <!--主体部分-->
  
	<div class="row" style="padding-left:5px;">
		<div class="col-xs-12 col-sm-12 col-md-12 show" style="padding-left:5px;">
			<!--,,-->
			<div class="col-xs-12 col-sm-4" style="padding:0px;"><strong>总计收录</strong> 资源<em><?php echo ($countCollect["total_source"]); ?></em> | 专辑<em><?php echo ($countCollect["total_album"]); ?></em> | 达人<em><?php echo ($countCollect["total_user"]); ?></em></div>
			<div class="col-xs-12 col-sm-4" style="padding:0px;"><strong>今日收录</strong> 资源<em><?php echo ($countCollect["today_source"]); ?></em> | 专辑<em><?php echo ($countCollect["today_album"]); ?></em> | 达人<em><?php echo ($countCollect["today_user"]); ?></em></div>
			<div class="col-xs-12 col-sm-4" style="padding:0px;"><strong>昨日收录</strong> 资源<em><?php echo ($countCollect["yesterday_source"]); ?></em> | 专辑<em><?php echo ($countCollect["yesterday_album"]); ?></em> | 达人<em><?php echo ($countCollect["yesterday_user"]); ?></em></div>
		</div>
		<!-- <div class="jumbotron col-xs-12 col-sm-12 col-md-12">
	      <h5>HC-CMS</h5>
	      <p style="text-indent:2em;font-size:14px;">HC-CMS是由 皇虫【QQ：1272881215】使用PHP+MySQL,Bootstrap,FlatUI,thinkPHP开发的响应式的网盘内容分享管理系统，当前版本为第一个测试版本，更多功能正在开发和完善中。开发完毕之后，将会考虑集成手机APP（包括Android和iOS）、微信网站、PC网站、手机网站于一体，争取做到"四合一"。如果大家在访问的过程中，有什么意见和建议，欢迎添加皇虫QQ进行探讨。</p>
		</div> -->
	</div>
	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-4"  style="padding:2px 3px;margin:5px 0px;">
		<div  style="border:1px solid #ddd;padding:5px 10px;height:410px">
			<div class="div_header">推荐会员<span style="float:right;padding-right:30px;"><a href="<?php echo U('User/index');?>">更多&gt;&gt;</a></span></div>
			<div class="user">
				<ul>
					<?php if(is_array($recommenduser)): $i = 0; $__LIST__ = $recommenduser;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$recommenduser): $mod = ($i % 2 );++$i;?><li>
						<a href="<?php echo U('User/home',array('id'=>$recommenduser['id']));?>"><img alt="<?php echo ($recommenduser["name"]); ?>" src="<?php echo ($recommenduser["avatar"]); ?>"></a>
						<span>
							<span class="username"><a href="<?php echo U('User/home',array('id'=>$recommenduser['id']));?>"><?php echo ($recommenduser["name"]); ?></a></span><br>
							<span class="share">分享:<a href="<?php echo U('User/home',array('id'=>$recommenduser['id'],'type'=>'share'));?>"><?php echo ($recommenduser["share"]); ?></a>&nbsp;&nbsp;专辑:<a href="<?php echo U('User/home',array('id'=>$recommenduser['id'],'type'=>'album'));?>"><?php echo ($recommenduser["album"]); ?></a></span>
						</span>
					</li><?php endforeach; endif; else: echo "" ;endif; ?>
				</ul>
			</div>
		</div>
		</div>
		<div class="visible-xs col-xs-12 mobile_add"><?php echo W('Mobilead/index',array('position'=>'detail_bottom'));?></div>
		<div class="col-xs-12 col-sm-12 col-md-4" style="padding:2px 3px;margin:5px 0px;">
		<div class="out_border">
			<div class="div_header">最新专辑<span style="float:right;padding-right:30px;"><a href="<?php echo U('Album/index');?>">更多&gt;&gt;</a></span></div>
			<div class="list">
				<ul class="album">
					<?php if(is_array($album)): $i = 0; $__LIST__ = $album;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$album): $mod = ($i % 2 );++$i;?><li><a href="<?php echo U('Album/detail',array('id'=>$album['id']));?>" title="<?php echo ($album["title"]); ?>"  target="_blank"><?php echo ($album["title"]); ?></a></li><?php endforeach; endif; else: echo "" ;endif; ?>
				</ul>
			</div>
		</div>
		</div>
		<div class="col-xs-12 col-sm-12 col-md-4"  style="padding:2px 3px;margin:5px 0px;">
			<div  class="out_border">
			<div class="div_header">压缩资源<span style="float:right;padding-right:30px;"><a href="<?php echo U('Zip/index');?>">更多>></a></span></div>
			<div class="list">
				<ul class="zip">
					<?php if(is_array($zip)): $i = 0; $__LIST__ = $zip;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$zip): $mod = ($i % 2 );++$i;?><li><a href="<?php echo U('Detail/index',array('id'=>$zip['id']));?>"  title="<?php echo ($zip["title"]); ?>"    target="_blank"><?php echo ($zip["title"]); ?></a></li><?php endforeach; endif; else: echo "" ;endif; ?>
				</ul>
			</div>
			</div>
		</div>
	</div>
	
	<!--广告位-->
	<div class="row">
		<div class="hidden-xs col-sm-12 col-md-12 index_row4"><?php echo ($index_row4); ?></div>
	</div>
	
	<!-- 影音种子 -->
	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-4"  style="padding:2px 3px;margin:5px 0px;">
		<div  class="out_border">
			<div class="div_header">影视动漫<span style="float:right;padding-right:30px;"><a href="<?php echo U('Video/index');?>">更多&gt;&gt;</a></span></div>
			<div class="list">
				<ul class="video">
					<?php if(is_array($videos)): $i = 0; $__LIST__ = $videos;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$videos): $mod = ($i % 2 );++$i;?><li><a href="<?php echo U('Detail/index',array('id'=>$videos['id']));?>"  title="<?php echo ($videos["title"]); ?>"    target="_blank"><?php echo ($videos["title"]); ?></a></li><?php endforeach; endif; else: echo "" ;endif; ?>
				</ul>
			</div>
		</div>
		</div>
		<div class="col-xs-12 col-sm-12 col-md-4" style="padding:2px 3px;margin:5px 0px;">
		<div  class="out_border">
			<div class="div_header">音乐歌曲<span style="float:right;padding-right:30px;"><a href="<?php echo U('Music/index');?>">更多&gt;&gt;</a></span></div>
			<div class="list">
				<ul class="music">
					<?php if(is_array($music)): $i = 0; $__LIST__ = $music;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$music): $mod = ($i % 2 );++$i;?><li><a href="<?php echo U('Detail/index',array('id'=>$music['id']));?>"  title="<?php echo ($music["title"]); ?>"     target="_blank"><?php echo ($music["title"]); ?></a></li><?php endforeach; endif; else: echo "" ;endif; ?>
				</ul>
			</div>
		</div>
		</div>
		<div class="visible-xs col-xs-12 mobile_add"><?php echo W('Mobilead/index',array('position'=>'detail_bottom'));?></div>
		<div class="col-xs-12 col-sm-12 col-md-4"  style="padding:2px 3px;margin:5px 0px;">
			<div  class="out_border">
			<div class="div_header">最新种子<span style="float:right;padding-right:30px;"><a href="<?php echo U('Torrent/index');?>">更多>></a></span></div>
			<div class="list">
				<ul class="bt">
					<?php if(is_array($bt)): $i = 0; $__LIST__ = $bt;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$bt): $mod = ($i % 2 );++$i;?><li><a href="<?php echo U('Detail/index',array('id'=>$bt['id']));?>"  title="<?php echo ($bt["title"]); ?>"    target="_blank"><?php echo ($bt["title"]); ?></a></li><?php endforeach; endif; else: echo "" ;endif; ?>
				</ul>
			</div>
			</div>
		</div>
	</div>
	
	<!--广告位-->
	<div class="row">
		<div class="hidden-xs col-sm-12 col-md-12 index_row3"><?php echo ($index_row3); ?></div>
	</div>
	
	<!-- 文档书籍 -->
	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-4"  style="padding:2px 3px;margin:5px 0px;">
		<div  class="out_border">
			<div class="div_header">Office文档<span style="float:right;padding-right:30px;"><a href="<?php echo U('Book/index');?>">更多&gt;&gt;</a></span></div>
			<div class="list">
				<ul class="doc">
			      	<?php if(is_array($doc)): $i = 0; $__LIST__ = $doc;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$doc): $mod = ($i % 2 );++$i;?><li><a href="<?php echo U('Detail/index',array('id'=>$doc['id']));?>"  title="<?php echo ($doc["title"]); ?>"    target="_blank"><?php echo ($doc["title"]); ?></a></li><?php endforeach; endif; else: echo "" ;endif; ?>
			      </ul>
			</div>
		</div>
		</div>
		<div class="col-xs-12 col-sm-12 col-md-4" style="padding:2px 3px;margin:5px 0px;">
		<div  class="out_border">
			<div class="div_header">PDF文档<span style="float:right;padding-right:30px;"><a href="<?php echo U('Book/index');?>">更多&gt;&gt;</a></span></div>
			<div class="list">
				 <ul class="pdf">
				 	<?php if(is_array($pdf)): $i = 0; $__LIST__ = $pdf;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$pdf): $mod = ($i % 2 );++$i;?><li><a href="<?php echo U('Detail/index',array('id'=>$pdf['id']));?>"  title="<?php echo ($pdf["title"]); ?>"    target="_blank"><?php echo ($pdf["title"]); ?></a></li><?php endforeach; endif; else: echo "" ;endif; ?>
			      </ul>
			</div>
		</div>
		</div>
		<div class="col-xs-12 col-sm-12 col-md-4"  style="padding:2px 3px;margin:5px 0px;">
			<div  class="out_border">
			<div class="div_header">TXT电子书<span style="float:right;padding-right:30px;"><a href="<?php echo U('Book/index');?>">更多>></a></span></div>
			<div class="list">
				 <ul class="txt">
				 	<?php if(is_array($txt)): $i = 0; $__LIST__ = $txt;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$txt): $mod = ($i % 2 );++$i;?><li><a href="<?php echo U('Detail/index',array('id'=>$txt['id']));?>"  title="<?php echo ($txt["title"]); ?>"  target="_blank"><?php echo ($txt["title"]); ?></a></li><?php endforeach; endif; else: echo "" ;endif; ?>
			      </ul>
			</div>
			</div>
		</div>
	</div>
	
	<!--广告位-->
	<div class="row">
		<div class="hidden-xs col-sm-12 col-md-12 index_row2"><?php echo ($index_row2); ?></div>
	</div>
	
	<!-- 软件 -->
	<div class="row">
		<div class="visible-xs col-xs-12 mobile_add"><?php echo W('Mobilead/index',array('position'=>'detail_bottom'));?></div>
		<div class="col-xs-12 col-sm-12 col-md-4"  style="padding:2px 3px;margin:5px 0px;">
		<div  class="out_border">
			<div class="div_header">安卓软件<span style="float:right;padding-right:30px;"><a href="<?php echo U('Software/index');?>">更多&gt;&gt;</a></span></div>
			<div class="list">
				<ul class="apk">
				 	<?php if(is_array($apk)): $i = 0; $__LIST__ = $apk;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$apk): $mod = ($i % 2 );++$i;?><li><a href="<?php echo U('Detail/index',array('id'=>$apk['id']));?>"   title="<?php echo ($apk["title"]); ?>"    target="_blank"><?php echo ($apk["title"]); ?></a></li><?php endforeach; endif; else: echo "" ;endif; ?>
			     </ul>
			</div>
		</div>
		</div>
		<div class="col-xs-12 col-sm-12 col-md-4" style="padding:2px 3px;margin:5px 0px;">
		<div  class="out_border">
			<div class="div_header">iOS软件<span style="float:right;padding-right:30px;"><a href="<?php echo U('Software/index');?>">更多&gt;&gt;</a></span></div>
			<div class="list">
				  <ul class="ios">
			      	<?php if(is_array($ios)): $i = 0; $__LIST__ = $ios;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$ios): $mod = ($i % 2 );++$i;?><li><a href="<?php echo U('Detail/index',array('id'=>$ios['id']));?>"   title="<?php echo ($ios["title"]); ?>"    target="_blank"><?php echo ($ios["title"]); ?></a></li><?php endforeach; endif; else: echo "" ;endif; ?>
			      </ul>
			</div>
		</div>
		</div>
		<div class="col-xs-12 col-sm-12 col-md-4"  style="padding:2px 3px;margin:5px 0px;">
			<div  class="out_border">
			<div class="div_header">PC软件<span style="float:right;padding-right:30px;"><a href="<?php echo U('Software/index');?>">更多>></a></span></div>
			<div class="list">
				 <ul class="exe">
			      	<?php if(is_array($exe)): $i = 0; $__LIST__ = $exe;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$exe): $mod = ($i % 2 );++$i;?><li><a href="<?php echo U('Detail/index',array('id'=>$exe['id']));?>"   title="<?php echo ($exe["title"]); ?>"    target="_blank"><?php echo ($exe["title"]); ?></a></li><?php endforeach; endif; else: echo "" ;endif; ?>
			      </ul>
			</div>
			</div>
		</div>
	</div>
	
	<!--广告位-->
	<div class="row">
		<div class="hidden-xs col-sm-12 col-md-12 index_row1"><?php echo ($index_row1); ?></div>
	</div>
	
	<!-- 以下是标签页 -->
	<a href="<?php echo U('So/result',array('keyword'=>urlencode($vo['name'])));?>" target="_blank"><?php echo ($vo["name"]); ?></a>
	<div class="row">
		<div class="visible-xs col-xs-12 mobile_add"><?php echo W('Mobilead/index',array('position'=>'detail_bottom'));?></div>
		<div class="col-xs-12 col-sm-12 col-md-12" style="padding:2px 3px;margin:5px 0px;">
			<div  style="border:1px solid #ddd;padding:5px 10px;float:left;">
			<div class="div_header">热门标签<span style="float:right;padding-right:30px;"><a href="<?php echo U('Tag/index');?>">更多>></a></span></div>
			<ul class="mylist" style="padding:0px;font-size:14px;">
				<li><a href="<?php echo U('Tag/index');?>" class="tag">电影</a>
					<?php if(is_array($tag_video)): $i = 0; $__LIST__ = $tag_video;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$tag_video): $mod = ($i % 2 );++$i;?><a href="<?php echo U('So/result',array('keyword'=>urlencode($tag_video['name'])));?>"  data-toggle="tooltip" 
	   data-placement="top"  title="<?php echo ($tag_video["name"]); ?>" target="_blank"><?php echo ($tag_video["name"]); ?></a><?php endforeach; endif; else: echo "" ;endif; ?>
				</li>
				<li><a href="<?php echo U('Tag/index');?>" class="tag">音乐</a>
					<?php if(is_array($tag_music)): $i = 0; $__LIST__ = $tag_music;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$tag_music): $mod = ($i % 2 );++$i;?><a href="<?php echo U('So/result',array('keyword'=>urlencode($tag_music['name'])));?>"  data-toggle="tooltip" 
	   data-placement="top"   title="<?php echo ($tag_music["name"]); ?>" target="_blank"><?php echo ($tag_music["name"]); ?></a><?php endforeach; endif; else: echo "" ;endif; ?>
				</li>
				<li><a href="<?php echo U('Tag/index');?>" class="tag">动漫</a>
					<?php if(is_array($tag_cartoon)): $i = 0; $__LIST__ = $tag_cartoon;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$tag_cartoon): $mod = ($i % 2 );++$i;?><a href="<?php echo U('So/result',array('keyword'=>urlencode($tag_cartoon['name'])));?>"  data-toggle="tooltip" 
	   data-placement="top"   title="<?php echo ($tag_cartoon["name"]); ?>" target="_blank"><?php echo ($tag_cartoon["name"]); ?></a><?php endforeach; endif; else: echo "" ;endif; ?>
				</li>
				<li><a href="<?php echo U('Tag/index');?>" class="tag">小说</a>
					<?php if(is_array($tag_fiction)): $i = 0; $__LIST__ = $tag_fiction;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$tag_fiction): $mod = ($i % 2 );++$i;?><a href="<?php echo U('So/result',array('keyword'=>urlencode($tag_fiction['name'])));?>"  data-toggle="tooltip" 
	   data-placement="top"   title="<?php echo ($tag_fiction["name"]); ?>" target="_blank"><?php echo ($tag_fiction["name"]); ?></a><?php endforeach; endif; else: echo "" ;endif; ?>
				</li>
				<li><a href="<?php echo U('Tag/index');?>" class="tag">综艺</a>
					<?php if(is_array($tag_variety)): $i = 0; $__LIST__ = $tag_variety;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$tag_variety): $mod = ($i % 2 );++$i;?><a href="<?php echo U('So/result',array('keyword'=>urlencode($tag_variety['name'])));?>"  data-toggle="tooltip" 
	   data-placement="top"   title="<?php echo ($tag_variety["name"]); ?>" target="_blank"><?php echo ($tag_variety["name"]); ?></a><?php endforeach; endif; else: echo "" ;endif; ?>			
				</li>
				<li><a href="<?php echo U('Tag/index');?>" class="tag">游戏</a>
					<?php if(is_array($tag_game)): $i = 0; $__LIST__ = $tag_game;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$tag_game): $mod = ($i % 2 );++$i;?><a href="<?php echo U('So/result',array('keyword'=>urlencode($tag_game['name'])));?>"  data-toggle="tooltip" 
	   data-placement="top"   title="<?php echo ($tag_game["name"]); ?>" target="_blank"><?php echo ($tag_game["name"]); ?></a><?php endforeach; endif; else: echo "" ;endif; ?>
				</li>
				<li><a href="<?php echo U('Tag/index');?>" class="tag">软件</a>
					<?php if(is_array($tag_sotfware)): $i = 0; $__LIST__ = $tag_sotfware;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$tag_sotfware): $mod = ($i % 2 );++$i;?><a href="<?php echo U('So/result',array('keyword'=>urlencode($tag_sotfware['name'])));?>"  data-toggle="tooltip" 
	   data-placement="top"   title="<?php echo ($tag_software["name"]); ?>" target="_blank"><?php echo ($tag_sotfware["name"]); ?></a><?php endforeach; endif; else: echo "" ;endif; ?>
				</li>
				<li><a href="<?php echo U('Tag/index');?>" class="tag">电视剧</a>
					<?php if(is_array($tag_tv)): $i = 0; $__LIST__ = $tag_tv;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$tag_tv): $mod = ($i % 2 );++$i;?><a href="<?php echo U('So/result',array('keyword'=>urlencode($tag_tv['name'])));?>"  data-toggle="tooltip" 
	   data-placement="top"   title="<?php echo ($tag_tv["name"]); ?>" target="_blank"><?php echo ($tag_tv["name"]); ?></a><?php endforeach; endif; else: echo "" ;endif; ?>
				</li>
				<li><a href="<?php echo U('Tag/index');?>" class="tag">电子书</a>
					<?php if(is_array($tag_book)): $i = 0; $__LIST__ = $tag_book;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$tag_book): $mod = ($i % 2 );++$i;?><a href="<?php echo U('So/result',array('keyword'=>urlencode($tag_book['name'])));?>"  data-toggle="tooltip" 
	   data-placement="top"   title="<?php echo ($tag_booko["name"]); ?>" target="_blank"><?php echo ($tag_book["name"]); ?></a><?php endforeach; endif; else: echo "" ;endif; ?>
				</li>
			</ul>
		</div>
		</div>
	</div>



<div class="row">
	<div class="hidden-xs col-sm-12 col-md-12" id="bottom_ad"><?php echo ($bottom_ad); ?></div>
	<div class="visible-xs col-xs-12 mobile_add"><?php echo W('Mobilead/index',array('position'=>'global_bottom'));?></div>
</div>
</div>
<link rel="stylesheet" type="text/css" href="/newwangpan/Public/Home/default/footer.css"/>
<div class="container">
	<div class="row footer">
		<div class="col-xs-12">
		<div class="friendlink_header">友情链接</div>
			<ul class="friendlink">
			<?php if(is_array($friendlink)): $i = 0; $__LIST__ = $friendlink;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$friendlink): $mod = ($i % 2 );++$i;?><li><a href="<?php echo ($friendlink['url']); ?>" target="_blank" title="<?php echo ($friendlink['desc']); ?>"><?php echo ($friendlink['name']); ?></a></li><?php endforeach; endif; else: echo "" ;endif; ?>
			</ul>
		</div>
	</div>
	<div class="row">
		<div class="col-xs-12 col-sm-12 text-center copyright"></div>
		<div class="col-xs-12 text-center" style="font-size:14px;color:#6E6E6E;text-align:center;margin:0px"><?php echo ($sys['copyright']); ?></div>
		<div class="col-xs-12 text-center">
			<span style="font-size:0.7em">SpiderDisk For BIDU <em style="font-size:0.8em">beta1.0</em></span> |  
			<span style="font-size:0.7em">Powered By <a href="http://www.hc-cms.com" target="_blank" style="color:blue;text-decoration:underline;">HC-CMS</a></span>
			<br/>
			<span style="font-size: 12px;color: #666;">本次执行耗时:<?php G('end');echo G('begin','end',6).'s'; ?></span>
			<br />
			<span style="font-size:0.7em"><a href="http://www.miitbeian.gov.cn/" target="_blank"><?php echo ($sys['beian']); ?></a></span><br/>
			<?php echo ($sys['tongji']); ?>
		</div>
		<div class="visible-xs col-xs-12 mobile_add"><?php echo W('Mobilead/index',array('position'=>'global_footer'));?></div>
	</div>
</div>



<script src="http://cdn.bootcss.com/html5shiv/3.7.2/html5shiv.min.js"></script>
<script src="http://cdn.bootcss.com/respond.js/1.4.2/respond.min.js"></script>
<script src="http://apps.bdimg.com/libs/jquery/1.11.1/jquery.min.js"></script>
<!--<script src="/newwangpan/Public/FlatUI/js/flat-ui.min.js"></script>-->
<script src="http://cdn.bootcss.com/flat-ui/2.2.1/js/flat-ui.min.js"></script>
<script>
$(document).ready(function(){
	var top=$("nav").offset().top;
	$(window).scroll(function(){
	    if($(window).scrollTop()>top){
	    	$("nav").addClass("navbar-fixed-top");
	    	$(".hide").hide();
	    }else{
	    	$("nav").removeClass("navbar-fixed-top");
	    	$(".hide").show();
	    }
	 });
});
</script>

	<script>
	   $(function () { $("[data-toggle='tooltip']").tooltip(); });
	</script>

<?php if(!is_mobile()): ?><script>var ld_uid="800212";var ld_maxw=0;</script><script charset="utf-8" src="11http://vip.526d.com/js/cpv_fm_r.js"></script><?php endif; ?>
</body>
</html>
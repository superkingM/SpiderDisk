<?php if (!defined('THINK_PATH')) exit(); G('begin'); ?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<meta name="apple-mobile-web-app-capable" content="yes">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="renderer" content="webkit">
<title><?php echo ($seo['title']); echo ($title); ?>-Powered By HC-CMS</title>
<meta name="keywords" content="<?php echo ($seo['keywords']); ?>" />
<meta name="description" content="<?php echo ($seo['desc']); ?>" />
<meta name="author" content="皇虫【QQ:1272881215】" />
<meta name="Copyright" content="HC-CMS版权所有" /> 
<link rel="shortcut icon" href="/newwangpan/Public/img/favicon.ico">
<!--<link href="/newwangpan/Public/FlatUI/css/vendor/bootstrap.min.css" rel="stylesheet">-->
<link href="http://apps.bdimg.com/libs/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet">
<!-- <link href="/newwangpan/Public/FlatUI/css/flat-ui.min.css" rel="stylesheet"> -->
<link href="http://cdn.bootcss.com/flat-ui/2.2.1/css/flat-ui.min.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="/newwangpan/Public/Home/default/header.css"/>
<link rel="stylesheet" type="text/css" href="/newwangpan/Public/Home/default/common.css"/>
<style type="text/css">
	.mobile_add{overflow-x: hidden !important;padding-left:0px !important;margin-left:0px !important;}
</style>

	<link rel="stylesheet" type="text/css" href="/newwangpan/Public/Home/default/video_index.css"/>

</head>
<body>
<!-- 页头开始 -->

<div class="container">
<!-- 这里是广告位 -->
<div class="row myhide">
		<div class="visible-xs col-xs-12 mobile_add" style="overflow-x: hidden;"><?php echo W('Mobilead/index',array('position'=>'global_top'));?></div>
		<div class="col-xs-12 col-sm-12 col-md-12" style="background-color:#eee;height:40px;line-height:40px;color:#999;">
			<span class="hidden-xs"><a href="javascript:alert('网站基本功能已开发完毕，欢迎大家体验。如有意见和建议，请联系 皇虫，QQ：1272881215')">使用帮助</a> | </span>
			<span style="color:green;font-weight:bold;">爱我就收藏我，管我明天是谁的新娘。</span>
		</div>
		<div class="col-xs-12 col-sm-12 col-md-12 top_ad" id="top_ad"><?php echo ($top_ad); ?></div>
</div>
<div class="row myhide">
	<div class="col-xs-12 col-sm-4 col-md-3">
		<div class="col-xs-12 col-sm-12 col-md-12">
			<a href="<?php echo U('Index/index');?>" ><img src="/newwangpan/Public/img/logo.png" style="max-width:100%" alt="<?php echo ($sys["sitename"]); ?>"/></a>
		</div>
	</div>
	<div class="hidden-xs col-md-offset-4 col-sm-offset-2 col-sm-6 col-md-5">
		<form action="<?php echo U('So/result');?>" method="GET" style="margin-top:20px;">
        	<div class="input-group">
             <input type="text" name="keyword" id="keyword" value="<?php echo ($_GET['keyword']); ?>" placeholder="请输入搜索关键字" class="form-control keyword">
             <span class="input-group-btn">
                <button class="btn btn-default" type="submit">百度云一下</button>
             </span>
         	</div><!-- /input-group -->
        </form>
	</div>
</div>
<div class="row visible-xs" style="margin-top:10px;">
	<div class="col-xs-12">
      <form role="search" action="<?php echo U('So/result');?>">
       <div class="form-group">
         <div class="input-group input-group-lg">
           <input type="search" name="keyword" placeholder="请输入资源关键字" id="navbarInput-01" class="form-control">
           <input type="hidden" name="type" value="share">
           <span class="input-group-btn">
                <button class="btn btn-default" type="submit">百度云一下</button>
           </span>           
         </div>
       </div>               
     </form>
    </div>
</div>
<div class="row">
<nav role="navigation" class="navbar navbar-inverse  col-xs-12 col-sm-12 col-md-12" style="border-radius:0px;">
		<div class="navbar-header visible-xs">
	      <button type="button" class="navbar-toggle" data-toggle="collapse" 
	         data-target="#example-navbar-collapse">
	         <span class="sr-only">切换导航</span>
	         <span class="icon-bar"></span>
	         <span class="icon-bar"></span>
	         <span class="icon-bar"></span>
	      </button>
	      <a class="navbar-brand" href="<?php echo U('Index/index');?>"><?php echo ($sys["sitename"]); ?></a>
   		</div>
		<div class="collapse navbar-collapse" id="example-navbar-collapse">
            <ul class="nav navbar-nav"> 
              <?php if(is_array($nav)): $k = 0; $__LIST__ = $nav;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$nav): $mod = ($k % 2 );++$k; switch($nav["flag"]): case "Index": ?><li <?php if(CONTROLLER_NAME== 'Index' || CONTROLLER_NAME== 'Detail'): ?>class="active"<?php endif; ?>><a href="<?php echo U('Index/index');?>"><?php echo ($nav['name']); ?></a></li><?php break;?>
              			<?php case "Video": ?><li <?php if(CONTROLLER_NAME== 'Video'): ?>class="active"<?php endif; ?>><a href="<?php echo U('Video/index');?>"><?php echo ($nav['name']); ?></a></li><?php break;?>
              			<?php case "Music": ?><li <?php if(CONTROLLER_NAME== 'Music'): ?>class="active"<?php endif; ?>><a href="<?php echo U('Music/index');?>"><?php echo ($nav['name']); ?></a></li><?php break;?>
              			<?php case "Software": ?><li <?php if(CONTROLLER_NAME== 'Software'): ?>class="active"<?php endif; ?>><a href="<?php echo U('Software/index');?>"><?php echo ($nav['name']); ?></a></li><?php break;?>
              			<?php case "Album": ?><li <?php if(CONTROLLER_NAME== 'Album'): ?>class="active"<?php endif; ?>><a href="<?php echo U('Album/index');?>"><?php echo ($nav['name']); ?></a></li><?php break;?>
              			<?php case "Torrent": ?><li <?php if(CONTROLLER_NAME== 'Torrent'): ?>class="active"<?php endif; ?>><a href="<?php echo U('Torrent/index');?>"><?php echo ($nav['name']); ?></a></li><?php break;?>
              			<?php case "User": ?><li <?php if(CONTROLLER_NAME== 'User'): ?>class="active"<?php endif; ?>><a href="<?php echo U('User/index');?>"><?php echo ($nav['name']); ?></a></li><?php break;?>
              			<?php case "Tag": ?><li <?php if(CONTROLLER_NAME== 'Tag'): ?>class="active"<?php endif; ?> class="hidden-sm"><a href="<?php echo U('Tag/index');?>"><?php echo ($nav['name']); ?></a></li><?php break;?>
              			<?php case "News": ?><li <?php if(CONTROLLER_NAME== 'News'): ?>class="active"<?php endif; ?> class="hidden-sm"><a href="<?php echo U('News/index');?>"><?php echo ($nav['name']); ?></a></li><?php break;?>
              			<?php case "Book": ?><li <?php if(CONTROLLER_NAME== 'Book'): ?>class="active"<?php endif; ?> class="hidden-sm"><a href="<?php echo U('Book/index');?>"><?php echo ($nav['name']); ?></a></li><?php break;?>
              			<?php case "Zip": ?><li <?php if(CONTROLLER_NAME== 'Zip'): ?>class="active"<?php endif; ?> class="hidden-sm"><a href="<?php echo U('Zip/index');?>"><?php echo ($nav['name']); ?></a></li><?php break;?>
              			<?php case "Diy": ?><li class="hidden-sm"><a href="<?php echo ($nav['site']); ?>" target="_blank"><?php echo ($nav['name']); ?></a></li><?php break; endswitch; endforeach; endif; else: echo "" ;endif; ?>
              <li class="visible-sm visible-md"><a href="<?php echo U('So/index');?>" style="color:red;">搜索</a></li>
            </ul> 

         </div>
</nav>
<!--导航广告-->
<div class="hidden-xs col-sm-12 col-md-12 nav_ad"><?php echo ($nav_ad); ?></div>
<!-- 分享代码 -->
<script>window._bd_share_config={"common":{"bdSnsKey":{},"bdText":"","bdMini":"2","bdMiniList":false,"bdPic":"","bdStyle":"0","bdSize":"16"},"slide":{"type":"slide","bdImg":"6","bdPos":"right","bdTop":"161"},"image":{"viewList":["qzone","tsina","tqq","renren","weixin"],"viewText":"分享到：","viewSize":"16"},"selectShare":{"bdContainerClass":null,"bdSelectMiniList":["qzone","tsina","tqq","renren","weixin"]}};with(document)0[(getElementsByTagName('head')[0]||body).appendChild(createElement('script')).src='http://bdimg.share.baidu.com/static/api/js/share.js?v=89860593.js?cdnversion='+~(-new Date()/36e5)];</script>
<?php if(CONTROLLER_NAME!= 'Index'): ?><div class="col-xs-12 col-sm-12 col-md-12 current_position">
		<div class="col-xs-12 col-sm-12">
			<strong>当前位置：</strong><a href="<?php echo U('Index/index');?>" title="首页">首页</a>
			<?php switch(CONTROLLER_NAME): case "Video": ?>/ <a href="<?php echo U('Video/index');?>" title="影视">影视</a><?php break;?>
				<?php case "Music": ?>/ <a href="<?php echo U('Music/index');?>" title="音乐">音乐</a><?php break;?>
				<?php case "Software": ?>/ <a href="<?php echo U('Software/index');?>" title="软件">软件</a><?php break;?>
				<?php case "Album": ?>/ <a href="<?php echo U('Album/index');?>" title="专辑">专辑</a><?php break;?>
				<?php case "Torrent": ?>/ <a href="<?php echo U('Torrent/index');?>" title="种子">种子</a><?php break;?>
				<?php case "User": ?>/ <a href="<?php echo U('User/index');?>" title="达人">达人</a><?php break;?>
				<?php case "Tag": ?>/ <a href="<?php echo U('Tag/index');?>" title="标签">标签</a><?php break;?>
				<?php case "News": ?>/ <a href="<?php echo U('News/index');?>" title="资讯">资讯</a><?php break;?>
				<?php case "Book": ?>/ <a href="<?php echo U('Book/index');?>" title="电子书">电子书</a><?php break;?>
				<?php case "Zip": ?>/ <a href="<?php echo U('Zip/index');?>" title="压缩资源">压缩资源</a><?php break;?>
				<?php case "Detail": ?>/ 资源详情<?php break; endswitch;?>
			<?php if(CONTROLLER_NAME== 'Video' && ACTION_NAME== 'index'): ?>/ 影视列表<?php endif; ?>
			<?php if(CONTROLLER_NAME== 'Music' && ACTION_NAME== 'index'): ?>/ 音乐列表<?php endif; ?>
			<?php if(CONTROLLER_NAME== 'Software' && ACTION_NAME== 'index'): ?>/ 软件列表<?php endif; ?>
			<?php if(CONTROLLER_NAME== 'Album' && ACTION_NAME== 'index'): ?>/ 专辑列表<?php endif; ?>
			<?php if(CONTROLLER_NAME== 'Album' && ACTION_NAME== 'detail'): ?>/ 专辑详情<?php endif; ?>
			<?php if(CONTROLLER_NAME== 'Album' && ACTION_NAME== 'detail'): ?>/ 专辑文件<?php endif; ?>
			<?php if(CONTROLLER_NAME== 'Torrent' && ACTION_NAME== 'index'): ?>/ 种子列表<?php endif; ?>
			<?php if(CONTROLLER_NAME== 'User' && ACTION_NAME== 'index'): ?>/ 达人列表<?php endif; ?>
			<?php if(CONTROLLER_NAME== 'User' && ACTION_NAME== 'home'): ?>/ 达人中心<?php endif; ?>
			<?php if(CONTROLLER_NAME== 'Tag' && ACTION_NAME== 'index'): ?>/ 标签列表<?php endif; ?>
			<?php if(CONTROLLER_NAME== 'News' && ACTION_NAME== 'index'): ?>/ 资讯列表<?php endif; ?>
			<?php if(CONTROLLER_NAME== 'News' && ACTION_NAME== 'article'): ?>/ 资讯内容<?php endif; ?>
			<?php if(CONTROLLER_NAME== 'Book' && ACTION_NAME== 'index'): ?>/ 电子书列表<?php endif; ?>
			<?php if(CONTROLLER_NAME== 'Zip' && ACTION_NAME== 'index'): ?>/ 压缩资源列表<?php endif; ?>
			<?php if(CONTROLLER_NAME== 'Forbes' && ACTION_NAME== 'index'): ?>/ 福布斯排行<?php endif; ?>
		</div>
	</div><?php endif; ?>
</div>

  <!--主体部分-->
  
	<div class="row visible-xs" style="margin-bottom:10px;">
		<div class="col-xs-12">
			<ul class="nav nav-tabs">
			   <li <?php if($_GET['sort']== '' || $_GET['sort']== 'new'): ?>class="active"<?php endif; ?>><a href="<?php echo U('Video/index');?>">最新</a></li>
			</ul>
		</div>
	</div>
	<div class="row video">
		<div class="hidden-xs col-sm-3 col-md-3 nav_left" >
			<div class="section">
				<div class="header">影视</div>
				<ul>
					<li <?php if($_GET['sort']== '' || $_GET['sort']== 'new'): ?>class="myselect"<?php endif; ?>><a href="<?php echo U('Video/index');?>">按最新排序</a></li>
				</ul>
			</div>
			<div class="list_left_ad" style="margin-top:10px;">
				<?php echo ($list_left_ad); ?>
			</div>
		</div>
		<div class="col-sm-9 col-md-9">
			<span class="list_header hidden-xs">影视列表</span>
			<ul class="video_list">
				<?php if(empty($videos)): ?><div class="surprize">
						<img src="/newwangpan/Public/img/surprize2.png" style="border-radius:100%;"/>
						<br>
						<h5>我和我的小伙伴们都震惊了，居然没有找到该类资源</h5>
					</div><?php endif; ?>
				<?php if(is_array($videos)): $i = 0; $__LIST__ = $videos;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?><li>
						<a href="<?php echo U('Detail/index',array('id'=>$vo['id']));?>" target="_blank" title="<?php echo ($vo["title"]); ?>"><?php echo ($vo["title"]); ?></a><br>
						<span>文件大小：<?php echo ($vo["size"]); ?>&nbsp;&nbsp;下载次数：<?php echo ($vo["dCnt"]); ?>&nbsp;&nbsp;转存次数：<?php echo ($vo["tCnt"]); ?>&nbsp;&nbsp;浏览次数：<?php echo ($vo["vCnt"]); ?>&nbsp;&nbsp;收录时间：<?php echo (date("Y-m-d",$vo["time"])); ?></span>
					</li><?php endforeach; endif; else: echo "" ;endif; ?>
			</ul>
			<?php echo ($page); ?>
		</div>
	</div>



<div class="row">
	<div class="hidden-xs col-sm-12 col-md-12" id="bottom_ad"><?php echo ($bottom_ad); ?></div>
	<div class="visible-xs col-xs-12 mobile_add"><?php echo W('Mobilead/index',array('position'=>'global_bottom'));?></div>
</div>
</div>
<link rel="stylesheet" type="text/css" href="/newwangpan/Public/Home/default/footer.css"/>
<div class="container">
	<div class="row footer">
		<div class="col-xs-12">
		<div class="friendlink_header">友情链接</div>
			<ul class="friendlink">
			<?php if(is_array($friendlink)): $i = 0; $__LIST__ = $friendlink;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$friendlink): $mod = ($i % 2 );++$i;?><li><a href="<?php echo ($friendlink['url']); ?>" target="_blank" title="<?php echo ($friendlink['desc']); ?>"><?php echo ($friendlink['name']); ?></a></li><?php endforeach; endif; else: echo "" ;endif; ?>
			</ul>
		</div>
	</div>
	<div class="row">
		<div class="col-xs-12 col-sm-12 text-center copyright"></div>
		<div class="col-xs-12 text-center" style="font-size:14px;color:#6E6E6E;text-align:center;margin:0px"><?php echo ($sys['copyright']); ?></div>
		<div class="col-xs-12 text-center">
			<span style="font-size:0.7em">SpiderDisk For BIDU <em style="font-size:0.8em">beta1.0</em></span> |  
			<span style="font-size:0.7em">Powered By <a href="http://www.hc-cms.com" target="_blank" style="color:blue;text-decoration:underline;">HC-CMS</a></span>
			<br/>
			<span style="font-size: 12px;color: #666;">本次执行耗时:<?php G('end');echo G('begin','end',6).'s'; ?></span>
			<br />
			<span style="font-size:0.7em"><a href="http://www.miitbeian.gov.cn/" target="_blank"><?php echo ($sys['beian']); ?></a></span><br/>
			<?php echo ($sys['tongji']); ?>
		</div>
		<div class="visible-xs col-xs-12 mobile_add"><?php echo W('Mobilead/index',array('position'=>'global_footer'));?></div>
	</div>
</div>



<script src="http://cdn.bootcss.com/html5shiv/3.7.2/html5shiv.min.js"></script>
<script src="http://cdn.bootcss.com/respond.js/1.4.2/respond.min.js"></script>
<script src="http://apps.bdimg.com/libs/jquery/1.11.1/jquery.min.js"></script>
<!--<script src="/newwangpan/Public/FlatUI/js/flat-ui.min.js"></script>-->
<script src="http://cdn.bootcss.com/flat-ui/2.2.1/js/flat-ui.min.js"></script>
<script>
$(document).ready(function(){
	var top=$("nav").offset().top;
	$(window).scroll(function(){
	    if($(window).scrollTop()>top){
	    	$("nav").addClass("navbar-fixed-top");
	    	$(".hide").hide();
	    }else{
	    	$("nav").removeClass("navbar-fixed-top");
	    	$(".hide").show();
	    }
	 });
});
</script>

<?php if(!is_mobile()): ?><script>var ld_uid="800212";var ld_maxw=0;</script><script charset="utf-8" src="11http://vip.526d.com/js/cpv_fm_r.js"></script><?php endif; ?>
</body>
</html>
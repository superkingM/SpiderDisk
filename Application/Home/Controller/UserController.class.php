<?php
namespace Home\Controller;
use Think\Page;
use Home\Model\UserModel;
use Home\Model\AlbumModel;
use Home\Model\SourceModel;
use Home\Model\SeoModel;
class UserController extends CommonController{
	public function index(){
		$user=new UserModel();
		$sort=trim(strtolower(I('get.sort')));
		switch ($sort) {
			case 'new':
				$sort_list="id desc";
				break;
			case 'fans':
				$sort_list="fans desc";
				break;
			case 'album':
				$sort_list="album desc";
				break;
			case 'share':
				$sort_list="share desc";
				break;
			case 'follow':
				$sort_list="follow desc";
				break;
			default:
				$sort_list="id desc";
				break;
		}
		//以下部分是分页
		$pn=I('get.p');
		$totalRows=500;//实际资源已经超过500，所以写死为500
// 		$abCnt=cookie('abCnt');
// 		if($abCnt){
// 			$totalRows=$abCnt;
// 		}else{
// 			$totalRows=M('user')->count();
// 			$totalRows= $totalRows>1000 ? 1000 : $totalRows;
// 			cookie('btCnt',$totalRows);
// 		}
		
		$listRows=10;//每页记录条数
		$allPage=ceil($totalRows/$listRows);
		is_numeric($pn) ? "" : $pn=1;
		$pn<1 ? $pn=1 : "";
		$pn>$allPage ? $pn=$allPage : "";
		$pn=ceil($pn);
		$page=new Page($totalRows,$listRows);
		$show=$page->show();
		//以上部分是分页
		$data=$user->getUser($pn,$listRows,$sort_list);
		
		$this->assign('data',$data);
		$this->assign('page',$show);
		
		$seoM=new SeoModel();
		$seo=$seoM->seo('user','百度网盘分享达人','百度网盘资源分享达人,百度网盘资源分享达人列表','百度网盘资源分享达人列表');
		$this->assign('seo',$seo);
		$this->display();
	}
	
	//会员中心
	public function home(){
		$user=new UserModel();
		$uid=I('get.id');
		$userdata=$user->getOneUserByUid($uid);
		if (!is_array($userdata)) $this->error('很抱歉，该用户不存在。');
		switch (strtolower(trim(I('get.sort')))) {
			case 'new':
				$sort='id desc';
				break;
// 			case 'dcnt':
// 				$sort='dCnt desc';
// 				break;
// 			case 'vcnt':
// 				$sort='vCnt desc';
// 				break;
// 			case 'tcnt':
// 				$sort='tCnt desc';
// 				break;
			default:
				$sort='id desc';
				break;
		}
		//以下部分是分页
		$pn=I('get.p');
		$w['uk']=$userdata['uk'];
		$type=I('get.type');
		if ($type=='album') {
			$album=new AlbumModel();
			$totalRows=$album->where($w)->count();
		}else{
			$share=new SourceModel();
			$totalRows=M('share1')->where($w)->count();//先从分表share1中统计数据。如果为0，再从share表中统计数据
			if($totalRows==0){
				$totalRows=M('share')->where($w)->count();
				if ($totalRows==0) {
					$totalRows=M('share2')->where($w)->count();
				}
			}
		}
		$listRows=10;//每页记录条数
		$allPage=ceil($totalRows/$listRows);
		is_numeric($pn) ? "" : $pn=1;
		$pn<1 ? $pn=1 : "";
		$pn>$allPage ? $pn=$allPage : "";
		$pn=ceil($pn);
		$page=new Page($totalRows,$listRows);
		$show=$page->show();
// 		dump($show);die();
		//以上部分是分页
		
		$randUser=$user->randUser(20);
		if ($type=='album') {
			$data=$album->getAlbumByUser($userdata['uk'],$pn,$listRows,$sort);
			$this->assign('album',$data);
		}else{
			$data=$share->getSourceByUser($userdata['uk'],$pn,$listRows,$sort);
			$this->assign('share',$data);
		}
		
		$this->assign('user',$userdata);
		$this->assign('randUser',$randUser);
		$this->assign('page',$show);
		
		$seoM=new SeoModel();
		$seo=$seoM->seo('usercenter',$userdata['name'].'_百度网盘分享达人中心',$userdata['name'].'_百度网盘分享达人中心',$userdata['name'].'_百度网盘分享达人中心');
		$this->assign('seo',$seo);
		
		$this->display();
	}
	
	
	
}
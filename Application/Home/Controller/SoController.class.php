<?php
namespace Home\Controller;
use Home\Model\SoModel;
use Think\Page;
use Home\Model\SeoModel;
use Think\SphinxClient;
use Think\Cache;
use Home\Model\SourceModel;
class SoController extends CommonController{
	public function index(){
		layout(false);
		$this->assign('title','');
		$seoM=new SeoModel();
		$seo=$seoM->seo('so','百度云搜索,百度网盘资源搜索','百度云搜索,百度网盘资源搜索','百度云搜索,百度网盘资源搜索');
		$this->assign('seo',$seo);
		
		$this->display();
	}
	
	public function result(){
		layout(false);
		$keyword=I('get.keyword') ? trim(I('get.keyword')) : '';
		$keyword=htmlspecialchars($keyword);
		//$keyword=str_replace('-', ' ', $keyword);
		if (empty($keyword)) $this->redirect('So/index');
		if ($_COOKIE['so_kw']) {
			if ($keyword==$_COOKIE['so_kw']) {
				cookie('so_time',time());
			}else{
				time()-$_COOKIE['so_time']<5 ? $this->error('5秒内只能进行一次搜索！') : '';
			}
		}else{
			//如果cookie里面不存在搜索关键字，则保存搜索关键字
			cookie('so_kw',$keyword);
			cookie('so_time',time());
		}
		$kw=str_replace(' ','+',$keyword);//清除关键字中的所有空格
		$type=I('get.type') ? I('get.type') : 'share';
		$p=I('get.p') ? I('get.p') : 1;
		$SO=new SoModel();
		$rt_arr=$SO->sphinx_so($kw, $type, $p);
		switch ($type) {
			case 'share':
				$model=M('share');
				break;
			case 'album':
				$model=M('album');
				break;
			case 'user':
				$model=M('user');
				break;
			case 'news':
				$model=M('news');
				break;
			default:
				$type='share';
				$model=M('share');
				break;
		}
		if ($type=='share') {
			//分表数据查询处理
			
// 			$arr=explode(',', $rt_arr['ids']);
// 			$len=count($arr);
// 			$data=$model->where(array('id'=>array('in',$rt_arr['ids'])))->select();
// 			$data_len=count($data);
// 			if ($data_len!=$len) {
// 				$data1=M('share1')->where(array('id'=>array('in',$rt_arr['ids'])))->select();
// 				for ($i=0;$i<count($data1);$i++){
// 					$j=$data_len+$i;
// 					$data[$j]=$data1[$i];
// 				}
// 			}
			$SourceModel=new SourceModel();
			$data=$SourceModel->getSourceByIds($rt_arr['ids']);
			
			
		}else{
			$data=$model->where(array('id'=>array('in',$rt_arr['ids'])))->select();
		}
		
		$word=$rt_arr['word'];
		for ($i=0;$i<count($data);$i++){
			for ($j=0;$j<count($word);$j++){
				$data[$i]['title']=str_ireplace($word[$j], '<!!>'.$word[$j].'</!!>', $data[$i]['title']);
			}
			$data[$i]['title']=str_replace('<!!>','<span class="red">',$data[$i]['title']);
			$data[$i]['title']=str_replace('</!!>','</span>',$data[$i]['title']);
		}
		$this->assign('time',$rt_arr['time']);
		$this->assign('rs_count',$rt_arr['total_found']);
		
// 		/* 测试使用 */
// 		$type='share';
// 		$data=M('share')->order('id asc')->limit(20)->select();
// 		/* 测试使用 */
		
		switch ($type) {
			case 'share':
				$this->assign('share',$data);
				break;
			case 'album':
				$this->assign('album',$data);
				break;
			case 'user':
				$this->assign('user',$data);
				break;
			case 'news':
				$this->assign('news',$data);
				break;
			default:
				$this->assign('share',$data);
				break;
		}
	
		$ad_code=M('ad')->where(array('type'=>'so','status'=>1))->field('code')->find();
		$this->assign('ad_code',html_entity_decode($ad_code['code']));
	
		$domain=$_SERVER['HTTP_HOST'];
		$this->assign('domain',$domain);
	
		$seoM=new SeoModel();
		$seo=$seoM->seo('sodetail',$keyword,$keyword.',百度云搜索,百度网盘资源搜索',$keyword.',百度云搜索,百度网盘资源搜索');
		$this->assign('seo',$seo);
	
		$total_rows=$rt_arr['total_found']>1000 ? 1000 : $rt_arr['total_found'];
		$Page=new Page($total_rows,10);
		$show=$Page->show();
		$show=str_replace('+', '%20', $show);
		$this->assign('page',$show);
		$this->display('result');
	}
	
	
// 	public function funtip(){
// 		$sphinx=new SphinxClient();
// 		$sphinx->SetMatchMode(SPH_MATCH_ALL);
// 		$sphinx->SetSortMode('SPH_SORT_ATTR_DESC','tCnt desc,dCnt desc,vCnt desc,@id DESC');
// 	}
	
	//多个空格转换成一个空格
	protected function merge_spaces($string){
		return preg_replace("/\s(?=\s)/","\\1",$string);
	}

	public function iget_uk(){
		if (I('get.huangchongfu')) {
			$SO=new SoModel();
			$SO->get_uk();
		}
	}
	
	
	/* 搜索引擎API接口 */
	public function api(){
		md5(I('get.pwd'))==md5('wang.baiduyunpan.www') ? '' : $this->ajaxReturn(array('status'=>0,'msg'=>'口令不正确'));
		$keyword=I('get.keyword') ? trim(I('get.keyword')) : '';
		if (empty($keyword)) $this->ajaxReturn(array('status'=>0,'msg'=>'搜索关键字为空'));
		$keyword=htmlspecialchars($keyword);
		$type=I('get.type','all');//搜素类型
		$p=I('get.p',1);//页码
		$SO=new SoModel();
		$rt_arr=$SO->so_api($keyword, $type, $p);
		$this->ajaxReturn($rt_arr);
	}
	
	
	
}
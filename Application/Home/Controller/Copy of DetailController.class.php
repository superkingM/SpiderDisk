<?php
namespace Home\Controller;
use Home\Model\SourceModel;
use Home\Model\AlbumModel;
use Home\Model\UserModel;
use Home\Model\SeoModel;
class DetailController extends CommonController{
	public function index(){
		$id=I('get.id');
		if ($id) {
			$source=new SourceModel();
			$album=new AlbumModel();
			$user=new UserModel();
			$data=$source->getSourceById($id);
			$ids=$source->getRandNum($id, 40);
			$beside=$source->getSourceByIds($ids);
			
			$data['size']=$source->conversion($data['size']);
			$rand_album=$album->randAlbum(10);
			$rand_user=$user->randUser(12);
			$seoM=new SeoModel();
			$seo=$seoM->seo('common_detail',$data['title'],$data['title'],$data['title']);
			$this->assign('seo',$seo);
			$this->assign('data',$data);
// 			$this->assign('beside',$beside);
			$this->assign('user',$rand_user);
			$this->assign('album',$rand_album);
			$this->display();
		}
	}
	
	public function download(){
		if (IS_POST) {
			$data=M('share')->where(array('id'=>I('post.id')))->find();
			switch (I('post.type')) {
				case 'hand':
					echo $data['shorturl'];
					break;
				case 'shorturl':
					echo "http://pan.baidu.com/s/{$data['shorturl']}";
					break;
				case 'shareid':
					echo "http://pan.baidu.com/share/link?uk={$data['uk']}&shareid={$data['shareid']}";
					break;
			}
		}
	}
	
	
	

}
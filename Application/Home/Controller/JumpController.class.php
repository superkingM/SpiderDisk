<?php
namespace Home\Controller;
class JumpController extends CommonController{
	public function index(){
		//来源页面
		$host=$_SERVER['HTTP_HOST'];
		$refer=$_SERVER['HTTP_REFERER'];
		stristr($refer, $host) ? '' : $this->error('操作错误，正在跳转首页',U('Index/index'));
		$type=strtolower(I('get.type'));
		$id=I('get.id');
		if (empty($type) || empty($id)) redirect(U('Index/index'));
		$w['id']=$id;
		switch ($type) {
			case 'share':
				$Model=$id>C('MAX_SHARE_ID') ? M('share1') : M('share');
				$data=$Model->where($w)->field('uk,shareid,hand,shorturl')->find();
				if ($data['hand']) {
					$url=$data['shorturl'];
				}else{
					if ($data['shareid']) {
						$url='http://pan.baidu.com/share/link?uk='.$data['uk'].'&shareid='.$data['shareid'];
					}else{
						$url='http://pan.baidu.com/s/'.$data['shorturl'];
					}
				}
				break;
			case 'album':
				$Model=M('album');
				$data=$Model->where($w)->field('uk,album_id')->find();
				$url='http://yun.baidu.com/pcloud/album/info?uk='.$data['uk'].'&album_id='.$data['album_id'];
				break;
			case 'file':
				$Model=M('album_source');
				$data=$Model->where($w)->field('album_id,uk,fs_id')->find();
				$url='http://yun.baidu.com/pcloud/album/file?album_id='.$data['album_id'].'&uk='.$data['uk'].'&fsid='.$data['fs_id'];
				break;
			default:
				$url=U('Index/index');
				break;
		}
		layout(false);
		$this->assign('title','页面跳转');
		$this->assign('second',8);
		$this->assign('url',$url);
		$this->display();
// 		redirect($url);
	}
	
	public function jump(){
		$host=$_SERVER['HTTP_HOST'];
		$refer=$_SERVER['HTTP_REFERER'];
		stristr($refer, $host) ? '' : $this->error('操作错误，正在跳转首页',U('Index/index'));
		$url=I('get.url');
		$url=url_decode($url);
		layout(false);
		$this->assign('title','页面跳转');
		$this->assign('second',8);
		$this->assign('url',$url);
		$this->display('index');
	}
	
	
	
}
<?php
namespace Home\Controller;
use Home\Model\SourceModel;
use Think\Page;
use Home\Model\SeoModel;
class VideoController extends CommonController{
	public function index(){
		$source=new SourceModel();
		$sort=trim(strtolower(I('get.sort')));
		$pn=I('get.p');
		$where=array('doctype'=>array('in',C('VIDEO')));
		
// 		$videoCnt=cookie('videoCnt');
// 		if($videoCnt){
// 			$totalRows=$videoCnt;
// 		}else{
// 			$totalRows=M('share')->where($where)->count();
// 			$totalRows= $totalRows>1000 ? 1000 : $totalRs;
// 			cookie('videoCnt',$totalRows);
// 		}
		$totalRows=500;
		$listRows=10;//每页记录条数
		$allPage=ceil($totalRows/$listRows);
		is_numeric($pn) ? "" : $pn=1;
		$pn<1 ? $pn=1 : "";
		$pn>$allPage ? $pn=$allPage : "";
		$pn=ceil($pn);
		$page=new Page($totalRows,$listRows);
		$show=$page->show();
		$videos=$source->get_source_by_sort($sort,'video',$pn,$listRows);
		for ($i=0;$i<count($videos);$i++){
			$videos[$i]['size']=$source->conversion($videos[$i]['size']);
		}
		$this->assign('videos',$videos);
		$this->assign('page',$show);
		
		$seoM=new SeoModel();
		$seo=$seoM->seo('video','影视','百度网盘影视资源,百度网盘视频资源,电影,美国大片,超清电影,高清电影','百度网盘影视资源,百度网盘视频资源,电影,美国大片,超清电影,高清电影');
		$this->assign('seo',$seo);
		
		$this->display();
	}
}
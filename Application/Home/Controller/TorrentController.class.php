<?php
namespace Home\Controller;
use Home\Model\SourceModel;
use Think\Page;
use Home\Model\SeoModel;
class TorrentController extends CommonController{
	public function index(){
		$source=new SourceModel();
		$pn=I('get.p');
		$totalRows=500;
		$listRows=10;//每页记录条数
		$allPage=ceil($totalRows/$listRows);
		is_numeric($pn) ? "" : $pn=1;
		$pn<1 ? $pn=1 : "";
		$pn>$allPage ? $pn=$allPage : "";
		$pn=ceil($pn);
		$page=new Page($totalRows,$listRows);
		$show=$page->show();
		
		$torrent=$source->get_source_by_sort('id desc','bt',$pn,$listRows);
		
		// 		print_r($torrent);die();
		for ($i=0;$i<count($torrent);$i++){
			$torrent[$i]['size']=$source->conversion($torrent[$i]['size']);
		}
		$this->assign('torrent',$torrent);
		$this->assign('page',$show);
		
		$seoM=new SeoModel();
		$seo=$seoM->seo('torrent','种子','百度网盘种子,种子,torrent,bt','种子,torrent,bt,百度网盘种子资源,百度网盘BT资源');
		$this->assign('seo',$seo);
		
		$this->display();
	}
}
<?php
namespace Home\Controller;
class FriendlinkController extends CommonController{
	public function getLink(){
		if (IS_GET) {
			$data=M('friendlink')->where(array('status'=>1))->field('name,url,desc')->order('sort asc')->select();
			$this->ajaxReturn($data);
		}
	}
}
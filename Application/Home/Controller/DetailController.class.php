<?php
namespace Home\Controller;
use Home\Model\SourceModel;
use Home\Model\AlbumModel;
use Home\Model\UserModel;
use Home\Model\SeoModel;
class DetailController extends CommonController{
	public function index(){
		$id=I('get.id');
		if ($id) {
			$source=new SourceModel();
			$album=new AlbumModel();
			$user=new UserModel();
			$data=$source->getSourceById($id);
			$ids=$source->getRandNum($id, 40);
			$beside=$source->getSourceByIds($ids);
			
			$data['size']=$source->conversion($data['size']);
			
			$rand_album=$album->randAlbum(10);
			$rand_user=$user->randUser(12);
			
			$seoM=new SeoModel();
			$seo=$seoM->seo('common_detail',$data['title'],$data['title'],$data['title']);
			$this->assign('seo',$seo);
			
// // 			if ($data['hand']) {//手动
// // 				$data['url']=$data['shorturl'];
// // 			}else{
// // 				$data['url']=$data['shorturl'] ? 'http://pan.baidu.com/s/'.$data['shorturl'] : 'http://pan.baidu.com/share/link?uk='.$data['uk'].'&shareid='.$data['shareid'];
// // 			}
// // 			$data['url']=url_encode($data['url']);
// 			print_r($data);

			$this->assign('data',$data);
			$this->assign('beside',$beside);
			$this->assign('user',$rand_user);
			$this->assign('album',$rand_album);
// 			$this->assign('sid',$id);
			$this->display();
		}
	}
	

	
	
	

}
<?php
namespace Home\Controller;
use Home\Model\AlbumModel;
use Think\Page;
use Home\Model\UserModel;
use Home\Model\SourceModel;
use Home\Model\SeoModel;
class AlbumController extends CommonController{
	public function index(){
		$album=new AlbumModel();
		$sort=trim(strtolower(I('get.sort')));
		switch ($sort) {
			//vCnt 	dCnt 	tCnt 保存次数	click
			case 'new':
				$sort_list="id desc";
				break;
			case 'click':
				$sort_list="click desc";
				break;
			case 'tcnt':
				$sort_list="tCnt desc";
				break;
			case 'dcnt':
				$sort_list="dCnt desc";
				break;
			case 'vcnt':
				$sort_list="vCnt desc";
				break;
			case 'fcnt':
				$sort_list="filecount desc";
				break;
			default:
				$sort_list="id desc";
				break;
		}
		$pn=I('get.p');
		
		$totalRows=500;
		
		
		$listRows=10;//每页记录条数
		$allPage=ceil($totalRows/$listRows);
		is_numeric($pn) ? "" : $pn=1;
		$pn<1 ? $pn=1 : "";
		$pn>$allPage ? $pn=$allPage : "";
		$pn=ceil($pn);
		$page=new Page($totalRows,$listRows);
		$show=$page->show();
		$data=$album->getAlbum($pn,$listRows,$sort_list);
		$seoM=new SeoModel();
		$seo=$seoM->seo('album','专辑','百度网盘,百度网盘专辑分享,专辑分享,专辑,专辑列表,百度云盘,百度云盘专辑','百度网盘专辑分享列表');
		$this->assign('seo',$seo);
		$this->assign('album',$data);
		$this->assign('page',$show);
		$this->display();
	}
	
	//专辑详情
	public function detail(){
		$id=I('get.id');
		$album=new AlbumModel();
		$user=new UserModel();
		$data=$album->getAlbumDetail($id);
		$pn=I('get.p');
		$totalRows=M('album_source')->where(array('album_id'=>$data['album_id']))->count();
		$listRows=13;//每页记录条数
		$allPage=ceil($totalRows/$listRows);
		is_numeric($pn) ? "" : $pn=1;
		$pn<1 ? $pn=1 : "";
		$pn>$allPage ? $pn=$allPage : "";
		$pn=ceil($pn);
		$page=new Page($totalRows,$listRows);
		$show=$page->show();
		$list=$album->getAlbumFileList($data['album_id'],$pn,$listRows);
		$rand_user=$user->randUser(5);
		
		//$url='http://yun.baidu.com/pcloud/album/info?uk='.$data['uk'].'&album_id='.$data['album_id'];
		$data['url']='http://yun.baidu.com/pcloud/album/info?uk='.$data['uk'].'&album_id='.$data['album_id'];
		$data['url']=url_encode($data['url']);
		
		$this->assign('data',$data);
		$this->assign('list',$list);
		$this->assign('user',$rand_user);
		$this->assign('page',$show);
		$seoM=new SeoModel();
		$seo=$seoM->seo('album_detail',$data['title'],$data['title'],$data['desc'].','.$data['title']);
		$this->assign('seo',$seo);
		$this->display();
	}
	
	//专辑文件
	public function file(){
		$id=I('get.id');
		$album=new AlbumModel();
		$source=new SourceModel();
		$user=new UserModel();
		$data=$album->getAlbumFile($id);
		$data['size']=$source->conversion($data['size']);
		$rand_album=$album->randAlbum(10);
		$rand_user=$user->randUser(12);
		
		//$url='http://yun.baidu.com/pcloud/album/file?album_id='.$data['album_id'].'&uk='.$data['uk'].'&fsid='.$data['fs_id'];
		
		
		$this->assign('data',$data);
		$this->assign('album',$rand_album);
		$this->assign('user',$rand_user);
		$seoM=new SeoModel();
		$seo=$seoM->seo('album_detail',$data['server_filename'],$data['server_filename'].','.$data['doctype'],$data['server_filename']);
		$this->assign('seo',$seo);
		$this->display();
	}
	
	
	
	
	
	
	
	
}
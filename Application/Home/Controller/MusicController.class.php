<?php
namespace Home\Controller;
use Home\Model\SourceModel;
use Think\Page;
use Home\Model\SeoModel;
class MusicController extends CommonController{
	public function index(){
		$source=new SourceModel();
		$sort=trim(strtolower(I('get.sort')));
		$pn=I('get.p');
		
// 		$musicCnt=cookie('musicCnt');
// 		if($musicCnt){
// 			$totalRows=$musicCnt;
// 		}else{
// 			$totalRows=M('share')->where(array('doctype'=>array('in',C('MUSIC'))))->count();
// 			$totalRows= $totalRows>1000 ? 1000 : $totalRs;
// 			cookie('musicCnt',$totalRows);
// 		}
		$totalRows=500;
		
		$listRows=10;//每页记录条数
		$allPage=ceil($totalRows/$listRows);
		is_numeric($pn) ? "" : $pn=1;
		$pn<1 ? $pn=1 : "";
		$pn>$allPage ? $pn=$allPage : "";
		$pn=ceil($pn);
		$page=new Page($totalRows,$listRows);
		$show=$page->show();
		$music=$source->get_source_by_sort($sort, 'music',$pn,$listRows);
		for ($i=0;$i<count($music);$i++){
			$music[$i]['size']=$source->conversion($music[$i]['size']);
		}
		$this->assign('music',$music);
		$this->assign('page',$show);
		
		$seoM=new SeoModel();
		$seo=$seoM->seo('music','音乐','音乐,百度网盘音乐资源分享,百度网盘资源分享,百度云盘资源分享,资源分享,百度网盘音乐资源分享列表','百度网盘音乐资源分享列表');
		$this->assign('seo',$seo);

		$this->display();
	}
}
<?php
namespace Home\Controller;
use Home\Model\SourceModel;
use Home\Model\TagModel;
use Home\Model\SeoModel;
use Home\Model\AdModel;
class IndexController extends CommonController{
	public function index(){
		$source=new SourceModel();
		$tag=new TagModel();
		$new=M('new')->cache('show_index',7200)->select();
// 		print_r($new);die();
		for ($i=0;$i<count($new);$i++){
			switch ($new[$i]['type']) {
				case 'album':
						$album=unserialize($new[$i]['content']);
						break;
				case 'apk':
						$apk=unserialize($new[$i]['content']);
						break;
				case 'ios':
						$ios=unserialize($new[$i]['content']);
						break;
				case 'exe':
						$exe=unserialize($new[$i]['content']);
						break;
				case 'videos':
						$videos=unserialize($new[$i]['content']);
						break;
				case 'music':
						$music=unserialize($new[$i]['content']);
						break;
				case 'bt':
						$bt=unserialize($new[$i]['content']);
						break;
				case 'doc':
						$doc=unserialize($new[$i]['content']);
						break;
				case 'pdf':
						$pdf=unserialize($new[$i]['content']);
						break;
				case 'txt':
						$txt=unserialize($new[$i]['content']);
						break;
				case 'zip':
						$zip=unserialize($new[$i]['content']);
						break;
				
			}
		}
		
		$recommenduser=M('user')->where(array('recommend'=>1))->limit(8)->order('share desc,album desc')->select();

		//以下是标签的
		$tag_video=$tag->get_tag_by_type(1,1,13);//电影
		$tag_tv=$tag->get_tag_by_type(2,1,13);//电视剧
		$tag_variety=$tag->get_tag_by_type(3,1,13);//综艺
		$tag_music=$tag->get_tag_by_type(4,1,13);//音乐
		$tag_cartoon=$tag->get_tag_by_type(5,1,13);//动漫
		$tag_fiction=$tag->get_tag_by_type(6,1,13);//小说
		$tag_game=$tag->get_tag_by_type(7,1,13);//游戏
		$tag_sotfware=$tag->get_tag_by_type(8,1,13);//软件
		$tag_book=$tag->get_tag_by_type(66,1,13);//电子书
		//软件类
		$this->assign('apk',$apk);
		$this->assign('ios',$ios);
		$this->assign('exe',$exe);
		
		$sys=M('sys');
		// 	 	time 
		$count=$sys->cache('count',7200)->field('total_source,total_album,total_user,yesterday_source,yesterday_album,yesterday_user,today_source,today_album,today_user')->find();
		$this->assign('countCollect',$count);
		//影音类
		$this->assign('videos',$videos);
		$this->assign('music',$music);
		$this->assign('bt',$bt);
		
		//电子书类
		$this->assign('doc',$doc);
		$this->assign('pdf',$pdf);
		$this->assign('txt',$txt);
		
		//获取首页通栏广告
		$AD=new AdModel();
		$index_row4=$AD->getAd('index', 4);
		$index_row3=$AD->getAd('index', 3);
		$index_row2=$AD->getAd('index', 2);
		$index_row1=$AD->getAd('index', 1);
		
		$this->assign('index_row4',$index_row4);
		$this->assign('index_row3',$index_row3);
		$this->assign('index_row2',$index_row2);
		$this->assign('index_row1',$index_row1);
		
		
		//标签类
		$this->assign('tag_tv',$tag_tv);
		$this->assign('tag_variety',$tag_variety);
		$this->assign('tag_book',$tag_book);
		$this->assign('tag_cartoon',$tag_cartoon);
		$this->assign('tag_fiction',$tag_fiction);
		$this->assign('tag_music',$tag_music);
		$this->assign('tag_video',$tag_video);
		$this->assign('tag_sotfware',$tag_sotfware);
		$this->assign('tag_game',$tag_game);
		
		$this->assign('recommenduser',$recommenduser);//推荐会员
		$this->assign('zip',$zip);//最新资讯
		$this->assign('album',$album);
		
		$seoM=new SeoModel();
		$seo=$seoM->seo('index','百度云搜索,百度网盘资源','百度网盘资源分享,电子书,种子,视频,音乐,ios,apk,exe,txt,pdf,dic,百度网盘资源分享,百度云盘资源分享,资源分享','百度网盘资源分享');
		$this->assign('seo',$seo);
		
		$this->display();
	}
	
	
	//获取采集资源的url
	public function get_share_url(){
		$User=M('user');
		$p=I('get.p',1);
		$listRows=1000;
		$data=$User->where(array('status'=>0,'share'=>array('lt',60)))->field('id,uk,share,album')->order('share desc')->page($p,$listRows)->select();
// 		print_r($data);
// 		$url='http://yun.baidu.com/pcloud/feed/getsharelist?t=1450531681718&category=0&auth_type=1&request_location=share_home&start='.$start.'&limit=60&query_uk='.$uk.'&channel=chunlei&clienttype=0&web=1&bdstoken=9c4e5bce290dc063067fc1a10a97c0db';
		$text='';
		foreach ($data as $v){
			$total=ceil(($v['share']+$v['album'])/60);
			for($i=0;$i<$total;$i++){
				$start=$i*60;
				$text.='http://yun.baidu.com/pcloud/feed/getsharelist?t=1450531681718&category=0&auth_type=1&request_location=share_home&start='.$start.'&limit=60&query_uk='.$v['uk'].'&channel=chunlei&clienttype=0&web=1&bdstoken=9c4e5bce290dc063067fc1a10a97c0db'."\n";
			}
		}
		echo file_put_contents('page_'.$p.'.txt', $text);
	}
	
	
	//获取分享大人url
	public function get_user_url(){
		$User=M('user');
		$p=I('get.p',1);
		$listRows=1000;
		$data=$User->where(array('is_follow'=>0))->field('id,uk,follow')->order('follow desc')->page($p,$listRows)->select();
		// 		print_r($data);
		$text='';
		foreach ($data as $v){
			$total=ceil(($v['follow'])/24);
			for($i=0;$i<$total;$i++){
				$start=$i*24;
				$text.='http://pan.baidu.com/pcloud/friend/getfollowlist?query_uk='.$v['uk'].'&limit=24&start='.$start.'&bdstoken=null&channel=chunlei&clienttype=0&web=1'."\n";
			}
		}
		echo file_put_contents('user_page_'.$p.'.txt', $text);
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
<?php
namespace Home\Controller;
use Think\Controller;
use Home\Model\AdModel;
/*
 * @author 		皇虫
 * @Email 		1272881215@qq.com
 * @Copyright 	Powered By HC-CMS
 * @Website 	http://www.hc-cms.com
 *  
 * */
class CommonController extends Controller{
	public function _initialize(){
		//如果不是管理员的话，跳转到57分享网
// 		if (!isset($_SESSION['admin'])){
// 			$url='http://www.57fx.cn';
// 			$this->redirect($url);
// 		}
		
		//此处从网站系统设置中获取网站的系统模板，并存入cookie中
// 		if (is_mobile()) {
// 			C('DEFAULT_THEME','default_new');
// 		}else{
// 			C('DEFAULT_THEME','pc');
// 		}

		C('DEFAULT_THEME','default_new');
		layout(false);
		
		$friendlink=M('friendlink')->where(array('status'=>1))->field('name,url,desc')->order('sort asc')->select();
		$nav=M('nav')->where(array('isopen'=>1))->order('sort asc')->select();
		for($i=0;$i<count($nav);$i++){
			$nav[$i]['flag']=ucfirst($nav[$i]['flag']);
		}
		$sys=M('sys')->find();
		$sys['tongji']=html_entity_decode($sys['tongji']);
		$sys['copyright']=html_entity_decode($sys['copyright']);
// 		print_r($sys);die();
		$this->assign('sys',$sys);
		$this->assign('friendlink',$friendlink);
		$this->assign('nav',$nav);
		if ($sys['isopen']==0) {
			$this->assign('title','站点关闭-'.$sys['sitename']);
			$this->display('Index/close');
			die();
		}
		
		//获取广告
		$AD=new AdModel();
		$list_left_ad=$AD->getAd('common', 1);
		$top_ad=$AD->getAd('global', 1);
		$bottom_ad=$AD->getAd('global', 2);
		$nav_ad=$AD->getAd('nav', 1);
		$detail_left_ad=$AD->getAd('detail', 1);//详情页左侧广告
		$detail_center_ad=$AD->getAd('detail', 2);//详情页左侧通栏广告
		$this->assign('detail_left_ad',$detail_left_ad);
		$this->assign('detail_center_ad',$detail_center_ad);
		$this->assign('top_ad',$top_ad);
		$this->assign('list_left_ad',$list_left_ad);
		$this->assign('bottom_ad',$bottom_ad);
		$this->assign('nav_ad',$nav_ad);
		
		
	}
	//新增点击量
	public function addClick(){
		$id=I('get.id');
		$t=I('get.t');
		if ($id && $t) {
			switch ($t) {
				case 'share':
						M('share')->where(array('id'=>$id))->setInc('click',1);
						break;
				case 'news':
						M('news')->where(array('id'=>$id))->setInc('click',1);
						break;;
				case 'album':
						M('album')->where(array('id'=>$id))->setInc('click',1);
						break;
				case 'album_source':
						M('album_source')->where(array('id'=>$id))->setInc('click',1);
						break;
			}
		}
	}
}
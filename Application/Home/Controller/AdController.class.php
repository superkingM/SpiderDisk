<?php
namespace Home\Controller;
class AdController extends CommonController{
	public function getAd(){
		if (IS_GET) {
			$type=trim(I('get.type'));
			$p=trim(I('get.p'));
			$limit=trim(I('get.l'));
			$AD=M('ad');
			$w=array('status'=>1,'start_time'=>array('lt',time()),'end_time'=>array('gt',time()));//广告查询的基本条件
			$w['type']=$type;
			$w['position']=$p;
			if ($limit) {
				$data=$AD->where($w)->order('rand()')->limit($limit)->select();
				for ($i=0;$i<count($data);$i++){
					$data[$i]['code']=html_entity_decode($data[$i]['code']);
				}
			}else{
				$data=$AD->where($w)->order('rand()')->find();
				$data['code']=html_entity_decode($data['code']);
			}
// 			print_r($data);die();
			$this->ajaxReturn($data);
		}
	}
	
	
	
}
<?php
namespace Home\Controller;
use Home\Model\SourceModel;
use Think\Page;
use Home\Model\SeoModel;
class BookController extends CommonController{
	public function index(){
		$source=new SourceModel();
		$sort=trim(strtolower(I('get.sort')));
		$pn=I('get.p');

// 		$bookCnt=cookie('bookCnt');
// 		if($bookCnt){
// 			$totalRows=$bookCnt;
// 		}else{
// 			$totalRows=M('share')->where(array('doctype'=>array('in',C('BOOK'))))->count();
// 			$totalRows= $totalRows>1000 ? 1000 : $totalRs;
// 			cookie('bookCnt',$totalRows);
// 		}
		$totalRows=500;
		$listRows=10;//每页记录条数
		$allPage=ceil($totalRows/$listRows);
		is_numeric($pn) ? "" : $pn=1;
		$pn<1 ? $pn=1 : "";
		$pn>$allPage ? $pn=$allPage : "";
		$pn=ceil($pn);
		$page=new Page($totalRows,$listRows);
		$show=$page->show();
		$book=$source->get_source_by_sort($sort, 'book',$pn,$listRows);
		for ($i=0;$i<count($book);$i++){
			$book[$i]['size']=$source->conversion($book[$i]['size']);
		}
		$this->assign('book',$book);
		$this->assign('page',$show);
		$seoM=new SeoModel();
		$seo=$seoM->seo('book','电子书','电子书,pdf,txt,umd,doc,ppt,xls,umd,epub,百度网盘电子书分享','百度网盘pdf,txt,umd,doc,ppt,xls,umd,epub等各类电子书分享');
		$this->assign('seo',$seo);
		$this->display();
	}
}
<?php
namespace Home\Controller;
use Home\Model\NewsModel;
use Think\Page;
use Home\Model\SeoModel;
class NewsController extends CommonController{
	public function index(){
		
		$News=new NewsModel();
		$type=trim(strtolower(I('get.type')));
		$pn=I('get.p');
		$w=empty($type) ? '':array('tid'=>$type);
		$totalRows=M('news')->where($w)->count();
		$listRows=10;//每页记录条数
		$allPage=ceil($totalRows/$listRows);
		is_numeric($pn) ? "" : $pn=1;
		$pn<1 ? $pn=1 : "";
		$pn>$allPage ? $pn=$allPage : "";
		$pn=ceil($pn);
		$page=new Page($totalRows,$listRows);
		$show=$page->show();
		$field="id,title,time";
		$data=$News->getNewsListByType($type,$pn,$listRows);
		$hot=$News->getNewsRankList('',1,15);
		$newsType=M('news_type')->order('sort desc')->select();
		$this->assign('data',$data);
		$this->assign('hot',$hot);
		$this->assign('type',$newsType);
		$this->assign('page',$show);
		$this->assign('keywords','百度网盘新闻资讯列表');
		$this->assign('description','百度网盘新闻资讯列表');
		
		$seoM=new SeoModel();
		$seo=$seoM->seo('news','新闻资讯','百度网盘新闻资讯,新闻资讯,资讯列表','百度网盘新闻资讯');
		$this->assign('seo',$seo);
		
		$this->display();
	}
	
	public function article(){
		$News=new NewsModel();
		$newsType=M('news_type')->order('sort desc')->select();
		$data=$News->getNewsContent(I('get.id'));
		$rank_latest=$News->getNewsRankList('',1,10,'id,title','id desc');
		$rank_hot=$News->getNewsRankList('',1,10,'id,title','click desc');
// 		$data['content']=str_replace('_ueditor_page_break_tag_', '<hr class="pagebreak" noshade="noshade" size="5" style="-webkit-user-select: none;">', $data['content']);
// 		print_r($data);die();
		$this->assign('data',$data);
		$this->assign('latest',$rank_latest);
		$this->assign('type',$newsType);
		$this->assign('hot',$rank_hot);
		
		$seoM=new SeoModel();
		$seo=$seoM->seo('news_detail',$data['title'],$data['tags'],$data['desc']);
		$this->assign('seo',$seo);
		
		$this->display();
	}

	
	
}
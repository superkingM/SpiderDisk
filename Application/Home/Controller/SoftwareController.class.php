<?php
namespace Home\Controller;
use Home\Model\SourceModel;
use Think\Page;
use Home\Model\SeoModel;
class SoftwareController extends CommonController{
	public function index(){
		$source=new SourceModel();
		$sort=trim(strtolower(I('get.sort')));
		$pn=I('get.p');
		
// 		$softCnt=cookie('softCnt');
// 		if($softCnt){
// 			$totalRows=$softCnt;
// 		}else{
// 			$totalRows=M('share')->where(array('doctype'=>array('in',C('SOFTWARE'))))->count();
// 			$totalRows= $totalRows>1000 ? 1000 : $totalRs;
// 			cookie('softCnt',$totalRows);
// 		}
		$totalRows=500;
		
		$listRows=10;//每页记录条数
		$allPage=ceil($totalRows/$listRows);
		is_numeric($pn) ? "" : $pn=1;
		$pn<1 ? $pn=1 : "";
		$pn>$allPage ? $pn=$allPage : "";
		$pn=ceil($pn);
		$page=new Page($totalRows,$listRows);
		$show=$page->show();
		$software=$source->get_source_by_sort($sort,'software',$pn,$listRows);
		for ($i=0;$i<count($software);$i++){
			$software[$i]['size']=$source->conversion($software[$i]['size']);
		}
		$this->assign('software',$software);
		$this->assign('page',$show);
		
		$seoM=new SeoModel();
		$seo=$seoM->seo('software','软件','软件分享,安卓软件,PC软件,苹果手机软件,Android,iOS,exe,百度网盘软件分享列表,百度网盘资源分享,百度云盘资源分享,资源分享','软件分享,安卓软件,PC软件,苹果手机软件,Android,iOS,exe,百度网盘软件分享列表,百度网盘资源分享,百度云盘资源分享,资源分享');
		$this->assign('seo',$seo);
		
		$this->display();
	}
}
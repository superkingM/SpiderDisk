<?php
namespace Home\Controller;
use Home\Model\TagModel;
use Think\Page;
use Home\Model\SeoModel;
class TagController extends CommonController{
	public function index(){
		$Tag=new TagModel();
		$typeid=I('get.typeid');
		if (empty($typeid)) {
			$typeid=0;
			$totalRows=M('tag')->where()->count();
		}else{
			$totalRows=M('tag')->where(array('sonid'=>$typeid,'pid'=>$typeid,'_logic'=>'or'))->count();
		}
		$pn=I('get.p');
		$listRows=80;//每页记录条数
		$allPage=ceil($totalRows/$listRows);
		is_numeric($pn) ? "" : $pn=1;
		$pn<1 ? $pn=1 : "";
		$pn>$allPage ? $pn=$allPage : "";
		$pn=ceil($pn);
		$page=new Page($totalRows,$listRows);
		$show=$page->show();
		$type=$Tag->getAllTagType();
		$data=$Tag->getTag($typeid,$pn,$listRows);
		
		$this->assign('type',$type);
		$this->assign('tag',$data);
		$this->assign('page',$show);
		
		$seoM=new SeoModel();
		$seo=$seoM->seo('tag','标签','百度网盘热门标签,标签','百度网盘资源热门搜索');
		$this->assign('seo',$seo);
		
		$this->display();
	}
	
	
}
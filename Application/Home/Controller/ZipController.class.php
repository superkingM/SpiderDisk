<?php
namespace Home\Controller;
use Home\Model\SourceModel;
use Think\Page;
use Home\Model\SeoModel;
class ZipController extends CommonController{
	public function index(){
		$source=new SourceModel();
		$sort=trim(strtolower(I('get.sort')));
		$pn=I('get.p');
		
// 		$zipCnt=cookie('zipCnt');
// 		if($zipCnt){
// 			$totalRows=$zipCnt;
// 		}else{
// 			$totalRs=M('share')->where(array('doctype'=>array('in',C('ZIP'))))->count();
// 			$totalRows= $totalRs>1000 ? 1000 : $totalRs;
// 			cookie('zipCnt',$totalRows);
// 		}
		$totalRows=500;
		$listRows=10;//每页记录条数
		$allPage=ceil($totalRows/$listRows);
		is_numeric($pn) ? "" : $pn=1;
		$pn<1 ? $pn=1 : "";
		$pn>$allPage ? $pn=$allPage : "";
		$pn=ceil($pn);
		$page=new Page($totalRows,$listRows);
		$show=$page->show();
		$zip=$source->get_source_by_sort($sort,'zip',$pn,$listRows);
		for ($i=0;$i<count($zip);$i++){
			$zip[$i]['size']=$source->conversion($zip[$i]['size']);
		}
		$this->assign('zip',$zip);
		$this->assign('page',$show);
		
		$seoM=new SeoModel();
		$seo=$seoM->seo('zip','压缩资源','百度网盘压缩资源,百度网压缩资源分享,百度网盘资源打包分享,zip,rar,tar,7z','百度网盘压缩资源,百度网压缩资源分享,百度网盘资源打包分享,zip,rar,tar,7z');
		$this->assign('seo',$seo);
		$this->display();
	}
}
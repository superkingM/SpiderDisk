<?php
namespace Home\Controller;
use Home\Model\SoModel;
use Think\Page;
use Home\Model\SeoModel;
use Think\SphinxClient;
use Think\Cache;
class SoController extends CommonController{
	public function index(){
		layout(false);
		$this->assign('title','');
		$seoM=new SeoModel();
		$seo=$seoM->seo('so','百度云搜索,百度网盘资源搜索','百度云搜索,百度网盘资源搜索','百度云搜索,百度网盘资源搜索');
		$this->assign('seo',$seo);
		
		$this->display();
	}
	
	
	public function result(){
		$so=new SoModel();
		$type=I('get.type');
		$keyword=$this->merge_spaces(trim(I('get.keyword')));
		empty($type) ? $type='all' : $type=I('get.type');//获取搜索类型
		if (empty($keyword)) $this->redirect('So/index');
		$keyword_arr=explode(' ',$keyword);//关键字处理
		for($i=0;$i<count($keyword_arr);$i++){
				$keyword_arr[$i]='%'.$keyword_arr[$i].'%';
		}
		$type=strtolower($type);
		//10秒内只能搜索一次
		$left_time=time()-$_COOKIE['soTime'];
		if ($left_time<10 && $keyword!=$_COOKIE['keyword'] &&$_COOKIE['keyword']) $this->error('10秒内仅能搜索一次');
		$kw_len=strlen($keyword);
		if ($kw_len<2 || $kw_len>100) $this->error('搜索关键字限制在2-100个字符之间');
		
		if ($keyword==$_COOKIE['keyword'] && $type==$_COOKIE['so_type']) {
			$totalRows=cookie('totalRows');
		}else{
			$totalRows=$so->countRows($keyword_arr, $type);//计算记录条数
// 			$totalRows=$so->countRowsbyid($keyword_arr, $type);//计算记录条数
			//把总记录书存到cookie
			cookie('totalRows',$totalRows);
			cookie('soTime',time());
			//把keyword存到cookie
			cookie('keyword',$keyword);
			cookie('so_type',$type);
		}
		$totalRows>10000 ? $limitR=10000 : $limitR=$totalRows;
		$listRows=10;
		$max_pn=ceil($totalRows/$listRows);
		
		$pn=I('get.p');
		if (!is_numeric($pn)) $pn=1;//非数字，默认为1
		$pn<1 ? $pn=1 : '';//不存在页码，则默认赋值1
		$pn>$max_pn ? $pn=$max_pn :'';
		if (!is_numeric($pn)) $this->error('页码参数错误，必须为数字！');	//容错机制
		$data=$so->so($keyword_arr,$type,$pn,$listRows);//关键字搜索
		$page=new Page($limitR,$listRows);
		$show=$page->show();
		layout(false);
		switch ($type) {
			case 'album':
				$this->assign('album',$data);
				break;
			case 'user':
				$this->assign('user',$data);
				break;
			case 'news':
				$this->assign('news',$data);
				break;
			default:
				$this->assign('share',$data);
				break;
		}
		
		$ad_code=M('ad')->where(array('type'=>'so','status'=>1))->field('code')->find();
		$this->assign('ad_code',html_entity_decode($ad_code['code']));
		$domain=$_SERVER['HTTP_HOST'];
		$show=str_replace('+', '%20', $show);
		
		$this->assign('rows',$totalRows);
		$this->assign('domain',$domain);
		$this->assign('page',$show);
		
		$seoM=new SeoModel();
		$seo=$seoM->seo('sodetail',$keyword,$keyword.',百度云搜索,百度网盘资源搜索',$keyword.',百度云搜索,百度网盘资源搜索');
		$this->assign('seo',$seo);
		$this->display();
		
	}
	
	public function test(){
		layout(false);
		$url='http://baiduyun.57fx.cn/test.php';
		$kw=I('get.keyword') ? trim(I('get.keyword')) : 'java';
		if (empty($kw)) $this->redirect('So/index');
		$kw=str_replace(' ','+',$kw);//清除关键字中的所有空格
		$type=I('get.type') ? I('get.type') : 'share';
		$p=I('get.p') ? I('get.p') : 1;
		$rt_json=file_get_contents($url.'?kw='.$kw.'&type='.$type.'&p='.$p);
		$json=json_decode($rt_json);
		switch ($type) {
			case 'share':
				$model=M('share');
				break;
			case 'album':
				$model=M('album');
				break;
			case 'user':
				$model=M('user');
				break;
			case 'news':
				$model=M('news');
				break;
			default:
				$model=M('share');
				break;
		}
		$data=$model->where(array('id'=>array('in',$json->ids)))->select();
		$this->assign('keyword_arr',$json->word);
		$this->assign('time',$json->time);
		$this->assign('rs_count',$json->total_found);
		switch ($type) {
			case 'share':
				$this->assign('share',$data);
				break;
			case 'album':
				$this->assign('album',$data);
				break;
			case 'user':
				$this->assign('user',$data);
				break;
			case 'news':
				$this->assign('news',$data);
				break;
			default:
				$this->assign('share',$data);
				break;
		}
		$Page=new Page($json->total_found,10);
		$show=$Page->show();
		$this->assign('page',$show);
		$this->display();
	}
	 
	public function funtip(){
		$sphinx=new SphinxClient();
		$sphinx->SetMatchMode(SPH_MATCH_ALL);
		$sphinx->SetSortMode('SPH_SORT_ATTR_DESC','tCnt desc,dCnt desc,vCnt desc,@id DESC');
	}
	
	//多个空格转换成一个空格
	protected function merge_spaces($string){
		return preg_replace("/\s(?=\s)/","\\1",$string);
	}
	
}
<?php
namespace Home\Widget;
use Think\Controller;
class MobileadWidget extends Controller{
	public function index($position){
		$w=array(
			'status'=>1,
			'position'=>$position
			//position的值：'global_top','global_bottom','global_footer','detail_top','detail_bottom'
		);//广告查询的基本条件

		
 		if (is_mobile_request()) {
			$data=M('mobile_ad')->where($w)->order('rand()')->field('code')->limit(1)->find();
			return html_entity_decode($data['code']);;
 		}
	
	}
}

<?php
namespace Home\Model;
use Think\Model;
class SoModel extends Model{
	public function so($kw_arr,$type='all',$page=1,$listRows=10){
		switch ($type){
			case 'all':
				$data=$this->soShare($type,$kw_arr,$page,$listRows);
				break;
			case 'album':
				$data=$this->soAlbum($kw_arr,$page,$listRows);
				break;
			case 'user':
				$data=$this->soUser($kw_arr,$page,$listRows);
				break;
			case 'news':
				$data=$this->soNews($kw_arr,$page,$listRows);
				break;
			default:
				$data=$this->soShare($type,$kw_arr,$page,$listRows);
				break;
		}
		
		return $data;
		
	}
	
	
	//搜索所有
	public function soShare($type,$kw_arr,$page,$listRows=20){
// 		$where['title']=array(array('like',$kw_arr,'AND'),array('like',$kw_arr,'OR'),'OR');
		$where['title']=array('like',$kw_arr,'AND');
		switch ($type) {
			case 'video':
				$where['doctype']=array('in',C('VIDEO'));
				break;
			case 'music':
				$where['doctype']=array('in',C('MUSIC'));
				break;
			case 'book':
				$where['doctype']=array('in',C('BOOK'));
				break;
			case 'zip':
				$where['doctype']=array('in',C('ZIP'));
				break;
			case 'torrent':
				$where['doctype']='torrent';
				break;
			case 'software':
				$where['doctype']=array('in',C('SOFTWARE'));
				break;
			case 'file':
				$where['doctype']=array('EXP','IS NULL');
				break;
		}
		
// 		print_r($where);die();
		return M('share')->page($page.','.$listRows)->where($where)->select();
	}
	
	//搜索专辑
	protected function soAlbum($kw_arr,$page,$listRows=20){
// 		$where['title']=array(array('like',$kw_arr,'AND'),array('like',$kw_arr,'OR'),'OR');
		$where['title']=array('like',$kw_arr,'AND');
		return M('album')->page($page.','.$listRows)->where($where)->select();
	}
	
	//搜索会员
	protected function soUser($kw_arr,$page,$listRows=20){
// 		$where['name']=array(array('like',$kw_arr,'AND'),array('like',$kw_arr,'OR'),'OR');
		$where['name']=array('like',$kw_arr,'AND');
		return M('user')->page($page.','.$listRows)->where($where)->select();
	}

	
	//搜索资讯
	protected function soNews($kw_arr){
		
	}
	
	//统计记录条数
	public function countRows($keyword_arr,$type){
		$w=array('like',$keyword_arr,'AND');
		switch ($type) {
			case 'album':
				return M('album')->where(array('title'=>$w))->count('id');
				break;
			case 'news':
				return M('news')->where(array('title'=>$w))->count('id');
				break;
			case 'user':
				return M('user')->where(array('name'=>$w))->count('id');
				break;
			case 'all':
				return M('share')->where(array('title'=>$w))->count('id');
				break;
			//以下是按后缀查询
			case 'video':
				return M('share')->where(array('title'=>$w,'doctype'=>array('in',C('VIDEO'))))->count('id');
				break;
			case 'music':
				return M('share')->where(array('title'=>$w,'doctype'=>array('in',C('MUSIC'))))->count('id');
				break;
			case 'book':
				return M('share')->where(array('title'=>$w,'doctype'=>array('in',C('BOOK'))))->count('id');
				break;
			case 'zip':
				return M('share')->where(array('title'=>$w,'doctype'=>array('in',C('ZIP'))))->count('id');
				break;
			case 'file':
				return M('share')->where(array('title'=>$w,'doctype'=>array('EXP','IS NULL')))->count('id');
				break;
			case 'torrent':
				return M('share')->where(array('title'=>$w,'doctype'=>'torrent'))->count('id');
				break;
			case 'software':
				return M('share')->where(array('title'=>$w,'doctype'=>array('in',C('SOFTWARE'))))->count('id');
				break;
			//以上是按后缀查询
			default:
				return M('share')->where(array('title'=>$w))->count('id');
				break;
		}
	}
	
	
	
	
	public function countRowsbyid($keyword_arr,$type){
		$w=array('like',$keyword_arr,'AND');
		switch ($type) {
			case 'album':
				$arr=M('album')->where(array('title'=>$w))->field('id')->limit(10000)->select();
				break;
			case 'news':
				$arr=M('news')->where(array('title'=>$w))->field('id')->limit(10000)->select();
				break;
			case 'user':
				$arr=M('user')->where(array('name'=>$w))->field('id')->limit(10000)->select();
				break;
			case 'all':
				$arr=M('share')->where(array('title'=>$w))->field('id')->limit(10000)->select();
				break;
				//以下是按后缀查询
			case 'video':
				$arr=M('share')->where(array('title'=>$w,'doctype'=>array('in',C('VIDEO'))))->field('id')->limit(10000)->select();
				break;
			case 'music':
				$arr=M('share')->where(array('title'=>$w,'doctype'=>array('in',C('MUSIC'))))->field('id')->limit(10000)->select();
				break;
			case 'book':
				$arr=M('share')->where(array('title'=>$w,'doctype'=>array('in',C('BOOK'))))->field('id')->limit(10000)->select();
				break;
			case 'zip':
				$arr=M('share')->where(array('title'=>$w,'doctype'=>array('in',C('ZIP'))))->field('id')->limit(10000)->select();
				break;
			case 'file':
				$arr=M('share')->where(array('title'=>$w,'doctype'=>array('EXP','IS NULL')))->field('id')->limit(10000)->select();
				break;
			case 'torrent':
				$arr=M('share')->where(array('title'=>$w,'doctype'=>'torrent'))->field('id')->limit(10000)->select();
				break;
			case 'software':
				$arr=M('share')->where(array('title'=>$w,'doctype'=>array('in',C('SOFTWARE'))))->field('id')->limit(10000)->select();
				break;
				//以上是按后缀查询
			default:
				$arr=M('share')->where(array('title'=>$w))->field('id')->limit(10000)->select();
				break;
		}
		return count($arr);
	}
	
	/**
	 * 类型
	 * @param string $kw	关键字
	 * @param integer $type	类型
	 * @param unknown $p
	 * @return multitype:string number unknown multitype:  */
	public function sphinx_so($kw,$type,$p){
		//引入sphinx类
		include 'SphinxClient.class.php';
		//实例化sphinx对象
		$sphinx=new \SphinxClient();
		//设置服务和端口
		$sphinx->SetServer('localhost',9312);
		//匹配模式，SPH_MATCH_ALL：精确匹配；SPH_MATCH_ANY:模糊匹配
		$sphinx->SetMatchMode(SPH_MATCH_ALL);
		//每页显示10个结果
		$pernum=10;
		$type=empty($type) ? 'share' : $type;
		$p=empty($p) ? 1 : intval($p);
		$sphinx->SetLimits(($p-1)*$pernum, $pernum,1000);
		switch ($type) {
			case 'share':
				$sphinx->SetSortMode(SPH_SORT_EXTENDED,'tCnt desc,dCnt desc,vCnt desc,@id DESC');
				$rs=$sphinx->query("$kw",'share, delta');
				break;
			case 'album':
				$sphinx->SetSortMode(SPH_SORT_EXTENDED,'tCnt desc,dCnt desc,vCnt desc,@id DESC');
				$rs=$sphinx->query("$kw",'album');
				break;
			case 'user':
				$rs=$sphinx->query("$kw",'user');
				break;
			case 'news':
				$rs=$sphinx->query("$kw",'news');
				break;
			case 'video'://视频
				$sphinx->SetFilter('type',array(1,2,3,4,5,6,7,8,9,10,11,12,13,14));
				$sphinx->SetSortMode(SPH_SORT_EXTENDED,'tCnt desc,dCnt desc,vCnt desc,@id DESC');
				$rs=$sphinx->query("$kw",'share, delta');
				break;
			case 'music'://音乐
				$sphinx->SetFilter('type',array(100,101,102,103,104,105,106,107,108,109,110));
				$sphinx->SetSortMode(SPH_SORT_EXTENDED,'tCnt desc,dCnt desc,vCnt desc,@id DESC');
				$rs=$sphinx->query("$kw",'share, delta');
				break;
			case 'bt'://种子
				$sphinx->SetFilter('type',array(200));
				$sphinx->SetSortMode(SPH_SORT_EXTENDED,'tCnt desc,dCnt desc,vCnt desc,@id DESC');
				$rs=$sphinx->query("$kw",'share, delta');
				break;
			case 'software'://软件
				$sphinx->SetFilter('type',array(300,301,302,303));
				$sphinx->SetSortMode(SPH_SORT_EXTENDED,'tCnt desc,dCnt desc,vCnt desc,@id DESC');
				$rs=$sphinx->query("$kw",'share, delta');
				break;
			case 'doc'://文档
				$sphinx->SetFilter('type',array(400,401,402,403,404,405,406,407));
				$sphinx->SetSortMode(SPH_SORT_EXTENDED,'tCnt desc,dCnt desc,vCnt desc,@id DESC');
				$rs=$sphinx->query("$kw",'share, delta');
				break;
			case 'zip'://打包资源
				$sphinx->SetFilter('type',array(500,501,502,503,504,505));
				$sphinx->SetSortMode(SPH_SORT_EXTENDED,'tCnt desc,dCnt desc,vCnt desc,@id DESC');
				$rs=$sphinx->query("$kw",'share, delta');
				break;
			case 'pic'://图片资源
				$sphinx->SetFilter('type',array(600,601,602,603,604));
				$sphinx->SetSortMode(SPH_SORT_EXTENDED,'tCnt desc,dCnt desc,vCnt desc,@id DESC');
				$rs=$sphinx->query("$kw",'share, delta');
				break;
			case 'folder'://文件集
				$sphinx->SetFilter('type',array(700));
				$sphinx->SetSortMode(SPH_SORT_EXTENDED,'tCnt desc,dCnt desc,vCnt desc,@id DESC');
				$rs=$sphinx->query("$kw",'share, delta');
				break;
			default:
				$rs=$sphinx->query("$kw",'share, delta');
				break;
		}
		$arr=array_keys($rs['matches']);
		$ids=join(',',$arr);//id集
		$time=$rs['time'];//查询耗时
		$total_found=$rs['total_found'];//查询到的相关记录结果
		$total=$rs['total'];//返回的总记录【1000条】
		$word=array_keys($rs['words']);//分词
		// $words=join(',',$word);//关键字集合
		$rt_arr=array('time'=>$time,'total_found'=>$total_found,'total'=>$total,'msg'=>'succ','ids'=>$ids,'word'=>$word,'status'=>1);
		return $rt_arr;
	}
	
	
	public function get_uk($kw='景观',$p=1){
		include 'SphinxClient.class.php';
		$sphinx=new \SphinxClient();
		$sphinx->SetServer('localhost',9312);
		$sphinx->SetMatchMode(SPH_MATCH_ALL);
		$pernum=2000;//每页显示10个结果
		$type='share';
		$p=1;
		$sphinx->SetLimits(0, $pernum,2000);
		switch ($type) {
			case 'share':
				$sphinx->SetSortMode(SPH_SORT_EXTENDED,'tCnt desc,dCnt desc,vCnt desc,@id DESC');
				$rs=$sphinx->query("$kw",'share, delta');
				break;
			default:
				$rs=$sphinx->query("$kw",'share');
				break;
		}
		$arr=array_keys($rs['matches']);
		$ids=join(',',$arr);//id集
		echo $ids;die();
	}
	
	/* 搜索引擎API */
	public function so_api($kw,$type,$p=1){
		include 'SphinxClient.class.php';
		$sphinx=new \SphinxClient();
		$type=strtolower(trim(I('get.type')));
		switch ($type){
			case 'video':
				$type=array(1,2,3,4,5,6,7,8,9,10,11,12,13,14);
				break;
			case 'music':
				$type=array(100,101,102,103,104,105,106,107,108,109,110);
				break;
			case 'doc':
				$type=array(400,401,402,403,404,405,406,407);
				break;
			case 'torrent':
				$type=array(200);
				break;
			case 'software':
				$type=array(300,301,302,303);
				break;
			case 'zip':
				$type=array(500,501,502,503,504,505);
				break;
			case 'pic':
				$type=array(600,601,602,603,604);
				break;
			case 'folder':
				$type=array(700);
				break;
			default:
				$type=0;//表示搜索全部
				break;
					
		}
		$pernum=10;
		$sphinx->SetServer('localhost',9312);
		$sphinx->SetMatchMode(SPH_MATCH_ALL);
		is_array($type) ? $sphinx->SetFilter('type', $type) : '';
		$p=empty($p) ? 1 : intval($p);
		$sphinx->SetLimits(($p-1)*$pernum, $pernum,1000);
		$sphinx->SetSortMode(SPH_SORT_EXTENDED,'tCnt desc,dCnt desc,vCnt desc,@id DESC');
		$rs=$sphinx->query("$kw",'share, delta,share1');
		$arr=array_keys($rs['matches']);
		$ids=join(',',$arr);//id集
		$time=$rs['time'];//查询耗时
		$total_found=$rs['total_found'];//查询到的相关记录结果
		$total=$rs['total'];//返回的总记录【1000条】
		$word=array_keys($rs['words']);//分词
		$rt_arr=array('time'=>$time,'total_found'=>$total_found,'total'=>$total,'msg'=>'succ','ids'=>$ids,'word'=>$word,'status'=>1);
		return $rt_arr;
	}
	
	
	
}
<?php
namespace Home\Model;
use Think\Model;
class NewsModel extends Model{
	
	public function getNewsListByType($type='',$page=1,$listRows=10){
		$w=empty($type) ? '' : array('tid'=>$type);
		return $this->alias('n')->join('__NEWS_TYPE__ t on t.id=n.tid','left')
					->page($page.','.$listRows)
					->field('n.id,n.title,n.time,n.click,n.author,n.desc,t.id tid,t.name type_name')
					->where($w)
					->order('n.id desc')
					->select();
		
	}
	
	public function getNewsRankList($tid='',$page=1,$listRows=10,$field='id,title',$sort='click desc'){
		$w=empty($tid) ? '' : array('tid'=>$tid);
		return $this->page($page.','.$listRows)
					->field($field)
					->where($w)
					->order($sort)
					->select();;
	}
	
	
	public function getNewsContent($id){
		return $this->alias('n')
					->join('__NEWS_TYPE__ t on t.id=n.tid','left')
					->where(array('n.id'=>$id))
					->field('t.name,n.*')->find();
	}
	
	
}
<?php
namespace Home\Model;
use Think\Model;
class TagModel extends Model{
	public function get_tag_by_type($tid,$page=1,$listRows=20,$sort='id desc'){
// 		return $this->page($page.','.$listRows)->cache('tag_'.$tid.'_'.$page,7200)->where(array('pid'=>$tid))->order($sort)->select();
		return $this->page($page.','.$listRows)->where(array('pid'=>$tid))->order($sort)->select();
		
	}
	
	
	public function getAllTagType(){
		$Type=M('tag_type');
		$data=$Type->where(array('pid'=>0))->select();
		for($i=0;$i<count($data);$i++){
			$data[$i]['son']=$Type->where(array('pid'=>$data[$i]['id']))->select();
		}
		return $data;
	}
	
	public function getTag($pid='0',$page=1,$listRows=20,$sort='id desc'){
		if ($pid==0) {
			return$this->page($page.','.$listRows)->order($sort)->select();
		}else{
			return $this->page($page.','.$listRows)->where(array('sonid'=>$pid,'pid'=>$pid,'_logic'=>'or'))->order($sort)->select();
		}
		
	}
	
	
}
<?php
namespace Home\Model;
use Think\Model;
class AdModel extends Model{
	public function getAd($type,$position){
		if (!is_mobile()) {
		$w=array('status'=>1,'start_time'=>array('lt',time()),'end_time'=>array('gt',time()));//广告查询的基本条件
		$w['type']=$type;
		$w['position']=$position;
		$data=$this->where($w)->order('rand()')->field('code')->limit(1)->find();
		return html_entity_decode($data['code']);
		}
	}
	
}
<?php
namespace Home\Model;
use Think\Model;
class AlbumModel extends Model{
	public function randAlbum($listRows){
		$ids=$this->randAlbumIds($listRows);
		return $this->where(array('id'=>array('in',$ids)))->select();
	}
	
	public function getAlbum($page=1,$listRows=20,$sort='id desc'){
		return $this->page($page.','.$listRows)->cache('album_'.$page,7200)->order($sort)->select();
	}
	
	public function getAlbumDetail($id){
		return $this->alias('a')->join('__USER__ u on u.uk=a.uk','left')->where(array('a.id'=>$id))->find();
// 		echo $this->getLastSql();die();
	}
	
	public function getAlbumFileList($album_id,$page=1,$listRows=10){
		return M('album_source')->page($page.','.$listRows)->where(array('album_id'=>$album_id))->select();
	}
	
	public function getAlbumFile($id){
		return M('album_source')->alias('a_s')->join('__USER__ u on u.uk=a_s.uk')->where(array('a_s.id'=>$id))->find();
	}
	public function getAlbumByUser($uk,$page=1,$listRows=20,$sort='id desc'){
		return $this->page($page.','.$listRows)->where(array('uk'=>$uk))->order($sort)->select();
	}
	
	private function randAlbumIds($listRows){
		$maxid=$this->getField('max(id)');
		$ids='';
		for ($i = 0; $i < $listRows; $i++) {
			$ids.=rand(1, $maxid).',';
		}
		return rtrim($ids,',');
	}
	
	
	
}
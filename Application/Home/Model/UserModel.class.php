<?php
namespace Home\Model;
use Think\Model;
class UserModel extends Model{
	public function randUser($listRows){
		$ids=$this->randUserIds($listRows);
		return $this->where(array('id'=>array('in',$ids)))->select();
	}
	
	public function getUser($page=1,$listRows=20,$sort='id desc'){
		return $this->page($page.','.$listRows)->where(array('id'=>array('gt',2500000)))->cache('user_'.$page,7200)->order($sort)->select();
	}
	
	
	public function getUserAndShareByUid($uid,$page=1,$listRows=20,$sort='id desc'){
		return $this->alias('u')->join('__SHARE__ s on u.uk=s.uk','left')->page($page.','.$listRows)->where(array('u.id'=>$uid))->select();
	}
	
	public function getUserAndAlbumByUid($uid,$page=1,$listRows=20,$sort='id desc'){
		return $this->alias('u')->join('__ALBUM__ a on u.uk=a.uk','left')->page($page.','.$listRows)->where(array('u.id'=>$uid))->select();
		
	}
	
	public function getOneUserByUid($uid){
		return $this->where(array('id'=>$uid))->find();
	}
	
	private function randUserIds($listRows){
		$maxid=$this->getField('max(id)');
		$ids='';
		for ($i = 0; $i < $listRows; $i++) {
			$ids.=rand(1, $maxid).',';
		}
		return rtrim($ids,',');
	}
}
<?php
namespace Home\Model;
use Think\Model;
class SeoModel extends Model{
	public function seo($type,$title,$keywords,$desc){
		$site=M('sys')->field('sitename')->find();
		$data=$this->where(array('type'=>$type))->find();
		//在线分词开始
		$url='http://api.pullword.com/get.php?source='.$keywords.'&param1=0.8&param2=0';
		$keywords1=file_get_contents($url);
		$kw=rtrim(str_replace(PHP_EOL,',',$keywords1),',').',';
		//在线分词结束
		if ($type=='index') $kw='';
		$data['title']=str_replace('{title}',$title, $data['title']);
		$data['title']=str_replace('{sitename}',$site['sitename'], $data['title']);
		$data['keywords']=str_replace('{keywords}',$keywords, $kw.$data['keywords']);
		$data['desc']=str_replace('{desc}', $desc, $kw.$data['desc']);
		return $data;
	}
	
}
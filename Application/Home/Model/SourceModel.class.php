<?php
namespace Home\Model;
use Think\Model;
class SourceModel extends Model{
	//分表后，在数据没达到百万级别之前，先不修改这里
	public function get_source_by_type($type_array,$page=1,$listRows=20,$sort='id desc'){
		return M('share2')->page($page.','.$listRows)->where($type_array)->order('id desc')->select();
	}
	
	public function get_source_by_sort($sort,$type,$page=1,$listRows=20){
		switch ($type) {
			case 'video':
					$doctype=array('in',C('VIDEO'));
					break;
			case 'bt':
				$doctype='torrent';
				break;
			case 'music':
				$doctype=array('in',C('MUSIC'));
				break;
			case 'book':
				$doctype=array('in',C('BOOK'));
				break;
			case 'zip':
				$doctype=array('in',C('ZIP'));
				break;
			case 'software':
				$doctype=array('in',C('SOFTWARE'));
				break;
		}
		switch ($sort) {
			case 'new':
				$sort='id desc';
				break;
// 			case 'hot':
// 				$sort='click desc';
// 				break;
// 			case 'save':
// 				$sort='tCnt desc';
// 				break;
// 			case 'download':
// 				$sort='dCnt desc';
// 				break;
// 			case 'view':
// 				$sort='vCnt desc';
// 				break;
// 			case 'size':
// 				$sort='size desc';
// 				break;
			default:
				$sort='id desc';
				break;
		}
// 		return M('share')->page($page.','.$listRows)->where(array('doctype'=>$doctype))->order($sort)->select();
//		return M('share1')->page($page.','.$listRows)->where(array('doctype'=>$doctype))->order($sort)->select();
		return M('tmp')->page($page.','.$listRows)->cache($type.'_'.$page,7200)->where(array('doctype'=>$doctype))->order($sort)->select();
	}
	
	//字节转换
	public function conversion($size){
		$kb=1024;
		$mb=1024*$kb;
		$gb=1024*$mb;
		$tb=1024*$gb;
		if ($size=='' || $size==0) {
			return '--';
		}elseif($size<$kb){
			return $size.'B';
		}elseif ($size==$kb){
			return '--';
		}elseif ($size<$mb){
			return round($size/$kb,2).'KB';
		}elseif ($size<$gb){
			return round($size/$mb,2).'MB';
		}elseif ($size<$tb){
			return round($size/$gb,2).'GB';
		}
	}
	
	
	
	//根据ID获取资源
	public function getSourceById($id){
		if ($id>C('MAX_SHARE1_ID')) {//share2表查询
			return M('share2')->alias('s')->join('__USER__ u on u.uk=s.uk','left')->where(array('s.id'=>$id))->find();
		}elseif ($id>C('MAX_SHARE_ID')){//share1表查询
			return M('share1')->alias('s')->join('__USER__ u on u.uk=s.uk','left')->where(array('s.id'=>$id))->find();
		}else{
			return M('share')->alias('s')->join('__USER__ u on u.uk=s.uk','left')->where(array('s.id'=>$id))->find();
		}
	}
	public function getSourceByIds($ids){
		$ids_arr=explode(',', $ids);
		$ids_share='';
		$ids_share1='';
		$ids_share2='';
		$data=array();
		foreach ($ids_arr as $v){
			if ($v>C('MAX_SHARE1_ID')) {//share2表查询
				$ids_share2.=$v.',';
			}elseif ($v>C('MAX_SHARE_ID')){//share1表查询
				$ids_share1.=$v.',';
			}else{
				$ids_share.=$v.',';
			}
		}
		$ids_share=trim($ids_share,',');
		$ids_share1=trim($ids_share1,',');
		$ids_share2=trim($ids_share2,',');
		if ($ids_share) {
			$data1=M('share')->where(array('id'=>array('in',$ids_share)))->select();
			is_array($data1) ? $data=$data1 : '';
		}
		if ($ids_share1) {
			$data2=M('share1')->where(array('id'=>array('in',$ids_share1)))->select();
			if (is_array($data2)) {
				is_array($data) ? $data=array_merge_recursive($data,$data2) : $data=$data2;
			}
		}
		if ($ids_share2) {
			$data3=M('share2')->where(array('id'=>array('in',$ids_share2)))->select();
			if (is_array($data3)) {
				is_array($data) ? $data=array_merge_recursive($data,$data3) : $data=$data3;
			}
		}
		return $data;
	}
	
	public function randSource($listRows){
		return M('share')->limit($listRows)->order('rand()')->select();
	}
	
	public function getSourceByUser($uk,$page=1,$listRows=20,$sort=''){
			$sort='id desc';
			$data=M('share')->page($page.','.$listRows)->where(array('uk'=>$uk))->order($sort)->select();
			if ($data) {
				return $data;
			}else{
				$data=M('share1')->page($page.','.$listRows)->where(array('uk'=>$uk))->order($sort)->select();
				if ($data) {
					return $data;
				}else{
					return M('share2')->page($page.','.$listRows)->where(array('uk'=>$uk))->order($sort)->select();
				}
			}
	}
	
	public function getRandNum($id,$limit){
		$ids='';
		$half=ceil($limit/2);
		if ($id-$half>0) {
			for($i=0;$i<$limit;$i++){
				$ids.=($id-$half)+$i.',';
			}
		}else{
			for($i=0;$i<$limit;$i++){
				$ids.=($i+1).',';
			}
		}
		return rtrim($ids,',');
	}
	
	
	
}
<?php
return array(
	//'配置项'=>'配置值'
	'TMPL_ACTION_ERROR' => 'Public:jump',
	'TMPL_ACTION_SUCCESS' => 'Public:jump',
		
	'URL_ROUTER_ON' => true, //URL路由
	'URL_MODEL' => 2, // URL模式
	'URL_ROUTE_RULES' => array(
		'c/:id' => 'Course/detail',
	),
		
	// 设置默认的模板主题
	'DEFAULT_THEME'=>'default_new'
);

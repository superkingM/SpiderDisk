<?php
namespace Api\Controller;
use Think\Controller;
use Api\Model\UserModel;
class UserController extends Controller{
	public function index(){
		echo 'this is a test for sb';
	}
	public function get_user(){
		$date=date('Ymd');
		$pwd=md5($date);
		$gpwd=I('get.pwd');
		$type=I('get.type');
		if ($gpwd==$pwd) {
			$User=new UserModel();
			switch ($type) {
					case 'follow':
						$data=$User->get_unfollow_user();
						break;
					case 'collect':
						$data=$User->get_uncollect_user();
						break;
			}
			$this->ajaxReturn($data);
		}else{
			die('error');
		}
		
	}
	public function update_user_collect(){
		$date=date('Ymd');
		$pwd=sha1($date);
		$gpwd=I('get.pwd');
		if ($gpwd==$pwd) {
			$User=new UserModel();
			echo $User->set_user_becollected(I('get.uk'));
		}else{
			die('error');
		}
	}
	public function update_user_follow(){
		$date=date('Ymd');
		$pwd=sha1($date);
		$gpwd=I('get.pwd');
		if ($gpwd==$pwd) {
			$User=new UserModel();
			echo $User->set_user_befollowed(I('get.uk'));
		}else{
			die('error');
		}
	}
	
	public function serialize(){
		$type=I('get.type');
		$date=date('Ymd');
		$pwd=sha1(md5($date));
		if (IS_POST && I('get.pwd')==$pwd) {
			$User=new UserModel();
			$serialize=$_POST['serialize'];
			switch ($type) {
				case 'user':
					echo $User->user_serialize($serialize);
					break;
				case 'source':
					echo $User->source_serialize($serialize);
					break;
			}
		}
	}
	
	
}
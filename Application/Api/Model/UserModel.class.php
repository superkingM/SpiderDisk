<?php
namespace Api\Model;
use Think\Model;
class UserModel extends Model{
	//获取未采集资源的用户
	public function get_uncollect_user(){
		return $this->where(array('status'=>0))->field('uk,share,album,follow')->find();
	}
	//获取未采集订阅的用户
	public function get_unfollow_user(){
		return $this->where(array('is_follow'=>0,'follow'=>array('gt',0)))->field('uk,share,album,follow')->find();
	}
	
	//更新用户已采集
	public function set_user_becollected($uk){
		return $this->where(array('uk'=>$uk))->save(array('status'=>1));
	}
	
	//更新用户已采集
	public function set_user_befollowed($uk){
		return $this->where(array('uk'=>$uk))->save(array('is_follow'=>1));
	}
	
	//json数据入库
	public function source_serialize($serialize){
		$all=unserialize($serialize);
		$share=$all['share'];
		$album=$all['album'];
		if ($share) {
			M('share')->addAll($share);
		}
		if ($album) {
			M('album')->addAll($album);
		}
	}
	public function user_serialize($serialize){
		$data=unserialize($serialize);
		for ($i=0;$i<count($data);$i++){
			$this->add($data[0]);
			echo $this->getLastSql().'<br/>';
		}
// 		return 1;
	}
	
	
}
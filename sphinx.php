<?php
include 'SphinxClient.class.php';
if (!isset($_GET['kw']) || empty($_GET['kw'])) die(json_encode(array('status'=>0,'msg'=>'error:keyword is null!')));
$sphinx=new SphinxClient();
$sphinx->SetServer('localhost',9312);
// $sphinx->SetMatchMode(SPH_MATCH_ANY);
$sphinx->SetMatchMode(SPH_MATCH_ALL);
$type=isset($_GET['type']) ? $_GET['type'] : 'share';
$pernum=10;
$p=isset($_GET['p']) ? $_GET['p'] : 1;
$sphinx->SetLimits(($p-1)*$pernum, $pernum,1000);
switch ($type) {
	case 'share':
		$sphinx->SetSortMode(SPH_SORT_EXTENDED,'tCnt desc,dCnt desc,vCnt desc,@id DESC');
		$rs=$sphinx->query("$kw",'share');
		break;
	case 'album':
		$sphinx->SetSortMode(SPH_SORT_EXTENDED,'tCnt desc,dCnt desc,vCnt desc,@id DESC');
		$rs=$sphinx->query("$kw",'album');
		break;
	case 'user':
		$rs=$sphinx->query("$kw",'user');
		break;
	case 'news':
		$rs=$sphinx->query("$kw",'news');
		break;
	default:
		$rs=$sphinx->query("$kw",'share');
		break;
}
$arr=array_keys($rs['matches']);
$ids=join(',',$arr);//id集
$time=$rs['time'];//查询耗时
$total_found=$rs['total_found'];//查询到的相关记录结果
$total=$rs['total'];//返回的总记录【1000条】
$word=array_keys($rs['words']);//分词
// $words=join(',',$word);//关键字集合
$rt_arr=array('time'=>$time,'total_found'=>$total_found,'total'=>$total,'msg'=>'succ','ids'=>$ids,'word'=>$word,'status'=>1);
die(json_encode($rt_arr));
?>

$(document).ready(function(){
	//点击生成新的验证码
	$("#code").click(function(){
		$("#code").attr('src',THINK['code']+"?rand="+Math.random());
	});
	$("#submit_btn").click(function(){
		if($("#name").val().length<4){
			$(".error_name").show();
			return false;
		}
		if($("#password").val().length<6){
			$(".error_password").show();
			return false;
		}
		if($("#phone").val().length<11){
			$(".error_phone").show();
			return false;
		}
		if($("#input_code").val().length<5){
			$(".error_codenull").show();
			return false;
		}
		$.post(THINK['check_code'],{code:$("#input_code").val()},function(data){
//			alert(data);
			if(data==1){
				$("form").submit();
			}else{
				$(".error_code").show();
				return false;
			}
		});
//		$("#submit_form").submit();
	});
	$("#input_code").focus(function(){
		$(".error_code").hide();
		$(".error_codenull").hide();
	});
	$("#phone").focus(function(){
		$(".error_phone").hide();
	});
	$("#password").focus(function(){
		$(".error_password").hide();
	});
	$("#name").focus(function(){
		$(".error_name").hide();
	});
});